<?php
/**
 * another objectcode solution to a problem nobody has addressed.
 * there is no such thing as a plugin that makes a front-end page pull content into the back-end interface of WP.
 * or if there is such a thing, it's been sorely lost from me.
 *
 * @package WordPress
 * @subpackage Administration
 */

/* 
 * Load the wordpress admin navigation on the left side frame
 * 
 */

  /** Load WordPress Administration Bootstrap */
  require_once( dirname( __FILE__ ) . '/admin.php' );

  if (!current_user_can('edit_posts'))
    wp_die(__('You do not have permission to access this area.'));

  //wp_enqueue_script('plupload-handlers');

  $title = __('E-mail Campaigns');
  $parent_file = 'email-campaigns.php';

  require_once( ABSPATH . 'wp-admin/admin-header.php' );
  wp_head();
 // wp_enqueue_scripts();
?>
<div id="content"></div>
<script>

jQuery(document).ready(function($){
  $.ajax({
    url: "/testing"
  }).done(function( html ) {
    $( '#content' ).append( html );
  });
})
</script>
<?php



  include( ABSPATH . 'wp-admin/admin-footer.php' );
