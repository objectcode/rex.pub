<?php
/**
 * Apply a Frameset and pull a website in it.  In this case E-mail campaign management tools.
 *
 * There are many filters in here for media. Plugins can extend functionality
 * by hooking into the filters.
 *
 * @package WordPress
 * @subpackage Administration
 */


if (!isset($_REQUEST['baseframe']) || $_REQUEST['baseframe']=='0') {

$buffer = <<<EOF
<!DOCTYPE html>
<html>
<head>
<title>$title</title>
</head>
<frameset cols="200, *">
   <frame src="/wp-admin/email-campaigns.php?baseframe=1" name="menu_page" />
   <frame src="http://www.large.marketing/" name="main_page" />
   <noframes>
   <body>
      Your browser does not support frames. Sorry but you can't use this tool if you are using an ancient browser.
   </body>
   </noframes>
</frameset>
</html>
EOF;
echo $buffer;
}

/* 
 * Load the wordpress admin navigation on the left side frame
 * 
 */
if (isset($_REQUEST['baseframe']) && $_REQUEST['baseframe']=='1') {

  /** Load WordPress Administration Bootstrap */
  require_once( dirname( __FILE__ ) . '/admin.php' );

  if (!current_user_can('edit_posts'))
    wp_die(__('You do not have permission to access this area.'));

  //wp_enqueue_script('plupload-handlers');

  $title = __('E-mail Campaigns');
  $parent_file = 'email-campaigns.php';

  require_once( ABSPATH . 'wp-admin/admin-header.php' );

  include( ABSPATH . 'wp-admin/admin-footer.php' );
}

