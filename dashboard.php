<?php
/***************************************************************************
 * [REX 3.3+] Bitchin (jQuery/WP/Sencha) Dashboard Loader
 * 
 * ========================================================================
 * Copyright 2015 and beyond
 * @author: Christopher Hogan c/o Objectcode LC and LARGE software limited.
 *
 * ========================================================================
 * Notes:
 * 
 * This bitchin loader is designed to suggest an unorthodox bootstrapping to
 * the browseri, which incorporates all the scripts necessary for wordpress stuff
 * to happen whilst still incorporate external styles, external js, and other
 * pre-requisite elements directly into the dashboard o that they 
 * will *not* need to be haphazardly incorporated into the index.html file.
 *
 * This is pretty darn important for the build process being that Sencha takes
 * the liberty of overwriting that file. As you may imagine, it makes things easier
 * to have a loader like this to abstract from the Sencha bootstrap for a number
 * of reasons.  Now you may be asking why not set the default index file to 
 * something else within sencha... to which I will retort that even though
 * you _can_ specify a different file for sencha to write to, it still does
 * not make building custom ajax requests easier, or incorporating wp/formidable
 * syles, permissions, jscripts, or svg from wordpress within the bootstrap easily.
 * jQuery is not going away:  it is a necessary evil in that it will be performing formidable
 * functions rather permanently, and so rather than asynchronusly loading it directly within sencha
 * I have taken preference to incorporating it as the bootstrapping device itself.
 *
 * These decisions have not been made easily, but I had to make a decision early on
 * about using formidable.  Frankly, it is fucking rad. When paired with sencha, it is hotsauce. 
 *
 * So that being said... I am now about 99.9% certain that
 * I will *not* be utilizing the sencha form building elements in the future, and it is also
 * relatively unlikely I will use much of their charting tools, but who knows.
 *
 * Why do you say?  
 * Because frankly, it is hard enough building the entire data UI, and dealing with sencha, 
 * building the posts table aggregators, forms, and forms-tables, etc. and hooks.
 * If it aint broke dont fix it
 *
 * I would rather not have to build all of the fucking form validation and forms again in sencha.
 * Thats ludicrous.  Plus, then what, if a form changes in the future I have to
 * re-compile the entire sencha app?  ya right.  That shit is like jenga... no way jose.
 * Formidable does forms rather brilliantly, and I intend to keep that process in tact.
 * Not to mention Formidable is instrumental in the data-schema and json building process.
 *
 * This unorthodox jquery bootstrap is the best that I can find in maintenance of a sophisticated
 * build process that is repeatable and wp/formidable friendly.
 *
 * {TLDR} So why is it so bitchin?
 *
 * This jQuery unorthodox bootstrap allows grids and bad-ass view functionality within sencha,
 * while still allowing formidable pro to manage and control all the forms and validation,
 * data set, and json schema, integration, really easy statistics, and post creation/aggregation.
 * In my mind, this is a totally killer setup being the best of both worlds, and because you can literally build the 
 * grids from the JSON REST spec automatically, thereby eliminating any need to do mapping. 
 *
 * Best of all: if you have to change any forms or add/remove fields quickly it is PIMPY AS FUCK!
 * you can make your mods without having to re-compile sencha... so long as it is
 * not a modification that will affect the data grid or layout/style/ etc.
 * most field mods will not affect the grid, so this is perfect for my purposes.
 *
 * This makes me eternally happy, because I hate wasting dev time on forms, and I am an eternally
 * lazy dinosaur.  Always looking for the safest, easiest route.
 *
 * Side Note:  Since this bootstrap handles boostrap.js integration directly, we can also do
 * security checks in here for wp-permissions and auomatically delete the index.html file (or use htaccess to block it).
 *
 * Cheers!
 * ======================================================================== */
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="UTF-8" />
  <title>Disruptive CRM&reg;</title>
  <meta name="generator" content="Objectcode Genus v3.3RC1 for REX dynamic builds 46.0.9">
  <meta name="author" content="CDH for LARGE Software Ltd. Care of Objectcode LC International.">
  <link rel="shortcut icon" type="image/x-icon" href="/wp-content/uploads/2015/06/119x119.png">
  <link rel="apple-touch-icon" href="/wp-content/uploads/2015/06/119x119.png"/>
  <link rel="stylesheet" href="/wp-content/plugins/vc-animated-text/assets/css/animate.css?ver=4.3.1">
  <link rel="stylesheet" href="/wp-content/uploads/formidable/css/formidablepro.css?ver=2.0.15">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css?ver=3.3.5">
  <link rel="stylesheet" href="/t/style.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/font-awesome/css/font-awesome.min.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/elegant-icons/style.min.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/stylesheet.min.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/webkit_stylesheet.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/style_dynamic.css?ver=1446972688">
  <link rel="stylesheet" href="/t/css/responsive.min.css?ver=4.3.1">
  <link rel="stylesheet" href="/t/css/style_dynamic_responsive.css?ver=1446972688">
  <link rel="stylesheet" href="/t/css/custom_css.css?ver=1446972688">
  <script src="/wp-includes/js/jquery/jquery.js?ver=1.11.3"></script>
  <script src="/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>
	<script src="/wp-content/plugins/vc-animated-text/assets/js/jquery.fittext.js?ver=4.3.1"></script>
	<script src="/wp-content/plugins/vc-animated-text/assets/js/jquery.lettering.js?ver=4.3.1"></script>
	<script src="/wp-content/plugins/vc-animated-text/assets/js/jquery.textillate.js?ver=4.3.1"></script>
	<script src="/wp-content/plugins/vc-animated-text/assets/js/jquery.viewportchecker.js?ver=4.3.1"></script>
	<script src="/t/js/plugins.js?ver=4.3.1"></script>
	<script src="/t/js/jquery.carouFredSel-6.2.1.min.js?ver=4.3.1"></script>
	<script src="/t/js/lemmon-slider.min.js?ver=4.3.1"></script>
	<script src="/t/js/default_dynamic.js?ver=1446972688"></script>
	<script src="/t/js/default.min.js?ver=4.3.1"></script>
<!-- viewport testing cause of chrome being a whiny cry baby
<!--
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href='https://fonts.googleapis.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'>
-->
  <!-- genus automagic compression -->
	<style><?php $buffer = <<<EOF
	    /* LARGE overrides outside of sencha build */
	    @media only screen and (min-width:960px){
		/* styles for browsers larger than 960px; */
		body:not(.texts) { 
		   font-size: 16px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    @media only screen and (min-width:1440px){
		/* styles for browsers larger than 1440px; */
		body:not(.texts) { 
		   font-size: 16px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    @media only screen and (min-width:2000px){
		/* for sumo sized (mac) screens */
		body:not(.texts) { 
		   font-size: 16px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    @media only screen and (max-device-width:480px){
	       /* styles for mobile browsers smaller than 480px; (iPhone) */
		body:not(.texts) { 
		   font-size: 10px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    @media only screen and (device-width:768px){
	       /* default iPad screens */
		body:not(.texts) { 
		   font-size: 11px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    /* different techniques for iPad screening */
	    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {
	      /* For portrait layouts only */
		body:not(.texts) { 
		   font-size: 11px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }

	    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
	      /* For landscape layouts only */
		body:not(.texts) { 
		   font-size: 11px!important; 
		   font-weight: normal!important; 
		   font-family: 'Pathway Gothic One',sans-serif!important; 
		}
	    }
	    /* LARGE SOFTWARE BRANDING */
	    .icon-lmg { 
		width: 150px!important; 
		margin-left:-30px!important;
	    }
	    /* Toolbar overrides */
	    .x-btn-default-toolbar-small, .x-btn-default-toolbar-small span { 
	       color: #DC3912!important;
	       background: #fff!important;
	    }
	    .x-btn-default-toolbar-small:hover, .x-btn-default-toolbar-small span:hover { 
	       color: #fff!important;
	       background: #DC3912!important;
	    }
EOF;
echo oc_minify_styles($buffer);
?>
	</style>
	<script type="text/javascript">
		jQuery.getScript( "bootstrap.js", function( data, textStatus, jqxhr ) {
		  console.log( "(ಠ_ಠ)  ℓ٥ﻻ ﻉ√٥υ [reX] unorthodox bootstrap complete!" );
		  console.log( "(ಠ_ಠ)  starting genus 3.3+ bitchin' loader and sanity checks now" );
		});
	</script>
</head>
<body class="page page-id-16696 page-template page-template-template-rex page-template-template-rex-php logged-in  overlapping_content qode-theme-ver-7.6.2 wpb-js-composer js-comp-ver-4.7.1.1 vc_responsive">

            <div class="wrapper">
                <div class="wrapper_inner">
                <header class="scroll_header_top_area dark stick scrolled_not_transparent page_header">
                    <div class="header_inner clearfix">

                    <div class="header_top_bottom_holder">
                    <div class="header_bottom clearfix" style=' background-color:rgba(0,0,0, 1);' >
                                <div class="container">
                          <div class="container_inner clearfix">
                                    <div class="overlapping_content_margin">                                <div class="header_inner_left">
                                                                <div class="logo_wrapper">
                                                          <div class="q_logo">
                                <a href="/">
                                  <img class="normal" src="/wp-content/uploads/2015/08/LMG-4-white-copy.svg" alt="Logo"/>
                                  <img class="light" src="/wp-content/uploads/2015/08/LMG-4-white-copy.svg" alt="Logo"/>
                                  <img class="dark" src="/wp-content/uploads/2015/11/rex2.svg" alt="Logo"/>
                                  <img class="sticky" src="/wp-content/uploads/2015/08/LMG-4-white-copy.svg" alt="Logo"/>
                                  <img class="mobile" src="/wp-content/uploads/2015/11/rex2.svg" alt="Logo"/>
                                                  </a>
                              </div>
                                                        </div>
                                                                          </div>
                                                                                                            <div class="header_inner_right">
                                                <div class="side_menu_button_wrapper right">
                                                                                        <div class="side_menu_button">
                                  
                                                                                                                                                        </div>
                                                </div>
                              </div>
                                        
                                        
                            <nav class="main_menu drop_down right">
                                        </nav>
                                                                        <nav class="mobile_menu">
                                                  </nav>
                                          </div>          </div>
                        </div>
                        </div>
                  </div>
                  </div>

                </header>
    

        <a id='back_to_top' href='#'>
      <span class="fa-stack">
        <i class="fa fa-arrow-up" style=""></i>
      </span>
    </a>
        
    
    
  
    
<div class="content " style="background:#000!important;">
            <div class="content_inner  ">
            <div class="title_outer title_without_animation"    data-height="200">
    <div class="title title_size_small  " style="height:200px;">
      <div class="image not_responsive"></div>
                    <div class="title_holder"  style="padding-top:150px;height:220px;">
          <div class="container">
            <div class="container_inner clearfix">
<div id="vc-animate-text-rexload" class="visible">
      <ul class="texts" style="display: none">
              <li class="texts" >...</li>
              <li class="texts" >...</li>
            </ul>
    </div>
</div>

            </div>
          </div>
        </div>
                </div>
      </div>
      <div class="full_width">
  <div class="full_width_inner" >
                                         
                        </div>
  </div>  
          <div class="content_bottom" >
          </div>
        
  </div>
</div>



  <footer >
    <div class="footer_inner clearfix">
        <div class="footer_top_holder">
                  <div class="footer_top">
                <div class="container">
          <div class="container_inner">
                    <div id="text-14" class="widget widget_text">     <div class="textwidget"></div>
    </div><div id="text-16" class="widget widget_text">     <div class="textwidget"><div style="margin-top: 100px;color:#666; font-family:'Open Sans Condensed'; font-size: 14px;"><img src="/wp-content/uploads/2015/08/LMG-4-white-allwhite.png" width="100" style="width:100px; opacity: .4!important;" alt="LARGE ltd." /><br />All content and images are the intellectual property of LARGE ltd.  <br />Objectcode LC.&trade;.</div></div>
    </div>                  </div>
        </div>
              </div>
          </div>
            </div>
  </footer>
    
</div>
</div>
<script type="text/javascript">;
  (function($) {
    $(document).ready(function() {
      var viweport = true;
      var $vcat = $("#vc-animate-text-rexload");
      $vcat.fitText(1.0, { minFontSize:14, maxFontSize:42});
      $vcat.textillate({
        loop:true,
        minDisplayTime:1,
        initialDelay:0,
        autoStart:true,
        in: { 
          effect:"fadeInLeft", 
          delayScale:.1, 
          delay:0,
          sync:false, 
          shuffle:false, 
          reverse:false,
          callback: function () {  }
        },
        out: { 
          effect:"bounce", 
          delayScale:1.5, 
          delay:100, 
          sync:false, 
          shuffle:false, 
          reverse:true,
          callback: function () {  }
        },
        type:"char"
      });
      
            $("#vc-animate-text-rexload").viewportChecker({
        offset: 100,
        repeat: false,
        callbackFunction: function(elem, action){
          if (action == "add" && viweport) {
            $(elem).textillate("start");
          }
        }
      });
          });
  })(jQuery);
  </script>

<style>
@media  (min-device-width:800px) {
.x-panel-header-title-default > .x-title-text-default:after {

font-family: "Pathway Gothic One"!important;
font-size: 22px;
font-weight: 400;
letter-spacing: 5px;
content: 'REX DISRUPTIVE CRM';
position: absolute;
top: 0;
right: 0;
color: #ccc;

}
</style> 

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<div id="killersoftware"></div>
</body>
</html>

