<?php
/*
 * [REX] REST/JSON GATEWAY  
 * 
 * @since rex build 39
 * @version 47.b
 *
 * a simplified gateway to implement meta-key and meta-value filters across the board 
 * 
 */
if (isset($_GET['query'])) {
   $_GET['my_freaking_query'] = $_REQUEST['query'];
}
if (isset($_GET['fields'])) {
   $_GET['my_freaking_fields'] = json_decode($_GET['fields']);
}
define('DOING_CRON', true); 

require_once "wp-load.php"; 


  /* 
   * some definitions for scope 
   *
   **/
  $f='';
  $h = (isset($_SERVER['HTTPS'])) ? 'https://': 'http://';
  $i[]= $h.$_SERVER['HTTP_HOST'];
  if (isset($_REQUEST['c'])) $i[]='/wp-json/wp/v2/'.$_REQUEST['c'].'/?'; 
  if (isset($_GET['my_freaking_fields'])) {  $mquery = $_GET['my_freaking_query'];
      $fs = $_GET['my_freaking_fields'];
      if (count($fs) && isset($mquery)) { 
         foreach ($fs as $field) {
             $i[]= "filter[meta_query]={$mquery}&filter[meta_key]={$field}&filter[meta_compare]=LIKE&";
         }
      }
  } else {
  if (isset($_REQUEST['f'])) $f= $_REQUEST['f'];
  $sk = 'filter[meta_key]='; 
  $sv = 'filter[meta_value]='; 
  if(is_array($f)) foreach($f as $mk => $mv):
    $i[]=$sk; $i[]=$mk; $i[]='&'.$sv; $i[]=$mv.'&';
  endforeach;
  if (isset($_REQUEST['o']) && $_REQUEST['o']=='id')
    $i[]= 'filter[orderby]=ID&';
  else
    if (isset($_REQUEST['o'])) $i[]= 'filter[meta_key]='.$_REQUEST['o'].'&filter[orderby]=meta_value&';  // o is orderby
  if (isset($_REQUEST['d'])) $i[]= 'filter[order]='.$_REQUEST['d'].'&';    // d is direction  ASC/DESC
  if (isset($_REQUEST['n'])) $i[]= 'filter[posts_per_page]='.$_REQUEST['n'].'&';    // n is for # per page
  if (isset($_REQUEST['p'])) $i[]= 'filter[paged]='.$_REQUEST['p'].'&';    // p is for page #
  }
  $g=implode($i); 
  $url=substr($g,0,-1); 
  $rex = wp_remote_get( $url, array('method'=>'GET') ); print_r($rex['header']);
  header('Content-Type: application/json'); echo $rex['body'];
?>
