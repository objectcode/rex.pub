<?php
/*
Plugin Name: Objectcode Armor
Plugin URI: //objectcodes.com
Description: Objectcode&reg; is an encryption package that protects data within the WP database.  Utilizing M3&trade; multi-layered environments it can ensure HIPAA, GLBA, and PCI-DSS compliance. Patent Pending.
Version: 2.9.5 Genus&trade; for deployment via pTero.
Author: CHO c/o Objectcode LC.
*/


function oc_encrypt(&$item,$key){
  if(class_exists('GDS_Encryption_Class'))
    $item = GDS_Encryption_Class::encrypt($item);
}
function oc_decrypt(&$item,$key){
  if(class_exists('GDS_Encryption_Class'))
   $item = GDS_Encryption_Class::decrypt($item);
}

//add_action('frm_after_create_entry', 'oc_encrypt', 30, 2);
//add_filter('frmpro_fields_replace_shortcodes', 'oc_decrypt', 10, 4);
//add_action('frm_after_update_entry', 'oc_decrypt', 10, 2);
