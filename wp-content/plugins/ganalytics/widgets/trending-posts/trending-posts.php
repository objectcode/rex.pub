<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.7
 * @copyright (c) 2013 cube3x
 *
 * Class for trending posts widget
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
//Creating option variables

class GA_Trending_Posts extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 'classname' => 'ga-trending-posts', 'description' => 'Based on Google Analytics page views' );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ga-trending-posts-widget' );
        $this->WP_Widget( 'ga-trending-posts-widget', 'GA Trending Posts', $widget_ops, $control_ops );
    }

    public function form( $instance ) {
        include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
        /* Set up some default widget settings. */

        $defaults = array(
            'title' => 'GA Trending Posts',
            'timeperiod' => 'week',
            'numposts' => 10,
            'wrapper' => 'ul',
            'itemtemplate' => '&lt;li&gt;%%POST-TITLE%%&lt;/li&gt;');
        $instance = wp_parse_args( (array) $instance, $defaults );
        if($access_token){
            ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"
                       value="<?php echo $instance['title']; ?>" class="widefat" type="text"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'timeperiod' ); ?>" style="width: 50%; display: inline-block;">Time Period: </label>
                <select id="<?php echo $this->get_field_id( 'timeperiod' ); ?>" name="<?php echo $this->get_field_name( 'timeperiod' ); ?>" style="width: 48%;">
                    <option value="day" <?php if ( 'day' == $instance['timeperiod'] ) echo 'selected="selected"'; ?>>Day</option>
                    <option value="week" <?php if ( 'week' == $instance['timeperiod'] ) echo 'selected="selected"'; ?>>Week</option>
                    <option value="month" <?php if ( 'month' == $instance['timeperiod'] ) echo 'selected="selected"'; ?>>Month</option>
                    <option value="all" <?php if ( 'all' == $instance['timeperiod'] ) echo 'selected="selected"'; ?>>All</option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'numposts' ); ?>" style="width: 50%; display: inline-block;">Number of Posts: </label>
                <select id="<?php echo $this->get_field_id( 'numposts' ); ?>" name="<?php echo $this->get_field_name( 'numposts' ); ?>" style="width: 48%;">
                    <?php
                    for($i=1;$i<=20;$i++){
                        ?>
                        <option value="<?php echo $i ?>" <?php if ( $i == $instance['numposts'] ) echo 'selected="selected"'; ?>><?php echo $i ?></option>
                    <?php
                    }
                    ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'wrapper' ); ?>" style="width: 50%; display: inline-block;">HTML Wrapper: </label>
                <input id="<?php echo $this->get_field_id( 'wrapper' ); ?>" name="<?php echo $this->get_field_name( 'wrapper' ); ?>"
                       value="<?php echo $instance['wrapper']; ?>" type="text" style="text-align: left; width: 48%; font-family: courier; font-size:12px;"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'itemtemplate' ); ?>" style="width: 100%; display: inline-block;">Single Item Template: </label>
                <textarea id="<?php echo $this->get_field_id( 'itemtemplate' ); ?>" name="<?php echo $this->get_field_name( 'itemtemplate' ); ?>"
                          style="text-align: left; width: 100%; font-family: courier; font-size: 12px;" rows="6"><?php echo $instance['itemtemplate']; ?></textarea>
            </p>
            <p>
                <strong>Template ShortCodes</strong><br/>
                %%POST-TITLE%%<br/>
                %%POST-PERMALINK%%<br/>
                %%POST-THUMBNAIL%%<br/>
                %%POST-AUTHOR%%<br/>
                %%POST-VISITS%%<br/>
                %%POST-PAGEVIEWS%%<br/>
            </p>
        <?php
        }
        else{
            ?>
            <p>Please connect to Google Analytics from plugin <a href="<?php echo $settings_url; ?>">settings</a> page.</p>
        <?php
        }
    }

    public function update( $new_instance, $old_instance ) {

        try{
            $instance = $old_instance;

            $instance['title'] = strip_tags( $new_instance['title'] );

            $instance['timeperiod'] = $new_instance['timeperiod'];
            $instance['numposts'] = $new_instance['numposts'];
            $instance['wrapper'] = $new_instance['wrapper'];
            $instance['itemtemplate'] = $new_instance['itemtemplate'];

            fn_ga_hourly(true,'widget-trending-posts');
            return $instance;
        }
        catch(Exception $e){}

    }

    public function widget( $args, $instance ) {

        try{
            wp_enqueue_style( 'ga-widget-trending-posts-layout', GANALYTICS_URL.'/widgets/trending-posts/ga-widget-trending-posts.css', array(),GANALYTICS_VERSION);
            include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
            extract( $args );

            // Reading widget title
            $title = apply_filters('widget_title', $instance['title'] );
            echo $before_widget;

            if ( $title )
                echo $before_title . $title . $after_title;
            if($access_token){
                $ganalytics_options = get_option('ganalytics_options');

                // get trending data
                $trending_posts_data = array();
                switch($instance['timeperiod']){
                    case "day":
                        $trending_posts_data = $ganalytics_options['widget_trending_posts_day'];
                        break;
                    case "week":
                        $trending_posts_data = $ganalytics_options['widget_trending_posts_week'];
                        break;
                    case "month":
                        $trending_posts_data = $ganalytics_options['widget_trending_posts_month'];
                        break;
                    case "all":
                        $trending_posts_data = $ganalytics_options['widget_trending_posts_alltime'];
                        break;
                    default:
                        break;
                }

                //display using loop
                $count = 0;
                echo '<'.$instance['wrapper'].'>';
                foreach($trending_posts_data as $key=>$data){
                    $count++;
                    if($count > $instance['numposts'])
                        break;
                    $id = $data['id'];
                    $visits = $data['visits'];
                    $pageviews = $data['pageviews'];
                    $postdata = get_post($id);
                    $post_title = $postdata->post_title;
                    $post_link = get_permalink( $id );
                    $post_author = get_the_author_meta( 'display_name', $postdata->post_author );
                    $post_thumbnail_html = get_the_post_thumbnail($id, "thumbnail");

                    $template = $instance['itemtemplate'];
                    $shortcode_keys = array("%%POST-TITLE%%","%%POST-PERMALINK%%","%%POST-THUMBNAIL%%","%%POST-AUTHOR%%","%%POST-VISITS%%","%%POST-PAGEVIEWS%%");
                    $shortcode_values = array($post_title,$post_link,$post_thumbnail_html,$post_author,$visits,$pageviews);
                    $itemhtml = str_replace($shortcode_keys, $shortcode_values, $template);

                    echo $itemhtml;
                }
                echo '</'.$instance['wrapper'].'>';
            }else{
                ?>
                <p>Sorry! Unable to show total visitors. Widget is not connected to Google Analytics.</p>
            <?php
            }
            echo $after_widget;
        }
        catch(Exception $e){}

    }

}
add_action( 'widgets_init', 'registerTrendingPosts');

function registerTrendingPosts(){
    register_widget( 'GA_Trending_Posts' );
}
?>