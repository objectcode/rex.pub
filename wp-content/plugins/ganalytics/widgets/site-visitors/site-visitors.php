<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.7
 * @copyright (c) 2013 cube3x
 *
 * Class for site visitors widget
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
//Creating option variables

class GA_Site_Visitors extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 'classname' => 'ga-site-visitors', 'description' => 'Total site visitors from Google Analytics' );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ga-site-visitors-widget' );
        $this->WP_Widget( 'ga-site-visitors-widget', 'GA Site Visitors', $widget_ops, $control_ops );
    }

    public function form( $instance ) {
        include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
        /* Set up some default widget settings. */
        $defaults = array( 'title' => 'GA Site Visitors', 'format' => 'compact',
            'alignment' => 'center', 'fontsize' => '36', 'fontcolor' => '333333',
            'fontfamily' => 'arial, sans-serif');
        $instance = wp_parse_args( (array) $instance, $defaults );
        if($access_token){
            ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"
                       value="<?php echo $instance['title']; ?>" class="widefat" type="text"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'format' ); ?>" style="width: 60%; display: inline-block;">Display Format: </label>
                <select id="<?php echo $this->get_field_id( 'format' ); ?>" name="<?php echo $this->get_field_name( 'format' ); ?>" style="width: 38%;">
                    <option value="compact" <?php if ( 'compact' == $instance['format'] ) echo 'selected="selected"'; ?>>Compact</option>
                    <option value="long" <?php if ( 'long' == $instance['format'] ) echo 'selected="selected"'; ?>>Long</option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'alignment' ); ?>" style="width: 60%; display: inline-block;">Text Alignment: </label>
                <select id="<?php echo $this->get_field_id( 'alignment' ); ?>" name="<?php echo $this->get_field_name( 'alignment' ); ?>" style="width: 38%;">
                    <option value="left" <?php if ( 'left' == $instance['alignment'] ) echo 'selected="selected"'; ?>>Left</option>
                    <option value="center" <?php if ( 'center' == $instance['alignment'] ) echo 'selected="selected"'; ?>>Center</option>
                    <option value="right" <?php if ( 'right' == $instance['alignment'] ) echo 'selected="selected"'; ?>>Right</option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'fontsize' ); ?>" style="width: 60%; display: inline-block;">Font Size: </label>
                <input id="<?php echo $this->get_field_id( 'fontsize' ); ?>" name="<?php echo $this->get_field_name( 'fontsize' ); ?>"
                       value="<?php echo $instance['fontsize']; ?>" type="text" style="text-align: left; width: 38%;"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'fontcolor' ); ?>" style="width: 60%; display: inline-block;">Font Color: </label>
                <input id="<?php echo $this->get_field_id( 'fontcolor' ); ?>" name="<?php echo $this->get_field_name( 'fontcolor' ); ?>"
                       value="<?php echo $instance['fontcolor']; ?>" type="text" style="text-align: left; width: 38%;" maxlength="6"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'fontfamily' ); ?>" style="width: 30%; display: inline-block;">Font Family: </label>
                <input id="<?php echo $this->get_field_id( 'fontfamily' ); ?>" name="<?php echo $this->get_field_name( 'fontfamily' ); ?>"
                       value="<?php echo $instance['fontfamily']; ?>" type="text" style="text-align: left; width: 68%;"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'show_track_from_date' ); ?>">
                    <input type="checkbox" id="<?php echo $this->get_field_id( 'show_track_from_date' ); ?>"
                           name="<?php echo $this->get_field_name( 'show_track_from_date' ); ?>" value="show"
                        <?php if($instance['show_track_from_date'] && $instance['show_track_from_date']=="show"){
                            echo "checked";
                        } ?>/>
                    Show Tracking From Date</label>
            </p>
        <?php
        }
        else{
            ?>
            <p>Please connect to Google Analytics from plugin <a href="<?php echo $settings_url; ?>">settings</a> page.</p>
        <?php
        }
    }

    public function update( $new_instance, $old_instance ) {
        try{
            $instance = $old_instance;

            $instance['title'] = strip_tags( $new_instance['title'] );

            $instance['format'] = $new_instance['format'];
            $instance['alignment'] = $new_instance['alignment'];
            $instance['show_track_from_date'] = $new_instance['show_track_from_date'];
            $instance['fontsize'] = $new_instance['fontsize'];
            $instance['fontcolor'] = $new_instance['fontcolor'];
            $instance['fontfamily'] = $new_instance['fontfamily'];

            fn_ga_hourly(true,'widget-site-visitors');
            return $instance;
        }
        catch(Exception $e){}
    }

    public function widget( $args, $instance ) {
        try{
            include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
            extract( $args );

            // Reading widget title
            $title = apply_filters('widget_title', $instance['title'] );

            // Reading total number of visitors
            $ganalytics_options = get_option('ganalytics_options');
            $total_site_visitors = $ganalytics_options['widget_site_visitors_total_visitors'];

            //Formatting output display
            $display_total_site_visitors = '';
            $total_site_visitors = (int)$total_site_visitors;
            if($instance['format'] == "compact"){
                if($total_site_visitors > 1000000000){
                    $display_total_site_visitors = round($total_site_visitors/1000000000,1) . ' B';
                } elseif($total_site_visitors > 1000000){
                    $display_total_site_visitors = round($total_site_visitors/1000000,1) . ' M';
                } elseif($total_site_visitors > 1000){
                    $display_total_site_visitors = round($total_site_visitors/1000,1) . ' K';
                } else{
                    $display_total_site_visitors = $total_site_visitors;
                }
            }
            else{
                $display_total_site_visitors = number_format($total_site_visitors);
            }
            echo $before_widget;

            if ( $title )
                echo $before_title . $title . $after_title;
            if($access_token){
                echo '<p class="ga-widget-site-visitors-count" style="text-align:'.$instance['alignment'].';
        font-size: '.$instance['fontsize'].'px; color: #'.$instance['fontcolor'].'; font-family: '.$instance['fontfamily'].';
        ">' . $display_total_site_visitors . '</p>';
                if($instance['show_track_from_date'] && $instance['show_track_from_date']=="show"){
                    echo '<p style="text-align:'.$instance['alignment'].'; font-size:11px; font-style:italic;">Tracking From ' . $created_time_display . '</p>';
                }
            }else{
                ?>
                <p>Sorry! Unable to show total visitors. Widget is not connected to Google Analytics.</p>
            <?php
            }
            echo $after_widget;
        }
        catch(Exception $e){}
    }

}
add_action( 'widgets_init', 'registerSiteVistors');

function registerSiteVistors(){
    register_widget( 'GA_Site_Visitors' );
}
?>