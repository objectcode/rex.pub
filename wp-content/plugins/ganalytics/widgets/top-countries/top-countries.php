<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.7
 * @copyright (c) 2013 cube3x
 *
 * Class for top countries widget
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
class GA_Top_Countries extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 'classname' => 'ga-top-countries', 'description' => 'Top countries from Google Analytics' );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ga-top-countries-widget' );
        $this->WP_Widget( 'ga-top-countries-widget', 'GA Top Countries', $widget_ops, $control_ops );
    }

    public function form( $instance ) {
        include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
        /* Set up some default widget settings. */
        $defaults = array( 'title' => 'GA Top Countries', 'countries_count' => 10, 'show_flag' => 'show',
            'show_full_width_graph' => 'show', 'graph_style' => 'flat_round',
            'show_number_of_visits' => 'show');
        $instance = wp_parse_args( (array) $instance, $defaults );
        if($access_token){
            ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"
                       value="<?php echo $instance['title']; ?>" class="widefat" type="text"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'countries_count' ); ?>" style="width: 60%; display: inline-block;">Number of Countries: </label>
                <select id="<?php echo $this->get_field_id( 'countries_count' ); ?>" name="<?php echo $this->get_field_name( 'countries_count' ); ?>" style="width: 38%;">
                    <option <?php if ( '1' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>1</option>
                    <option <?php if ( '2' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>2</option>
                    <option <?php if ( '3' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>3</option>
                    <option <?php if ( '4' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>4</option>
                    <option <?php if ( '5' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>5</option>
                    <option <?php if ( '6' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>6</option>
                    <option <?php if ( '7' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>7</option>
                    <option <?php if ( '8' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>8</option>
                    <option <?php if ( '9' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>9</option>
                    <option <?php if ( '10' == $instance['countries_count'] ) echo 'selected="selected"'; ?>>10</option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'graph_style' ); ?>" style="width: 60%; display: inline-block;">Graph Style: </label>
                <select id="<?php echo $this->get_field_id( 'graph_style' ); ?>" name="<?php echo $this->get_field_name( 'graph_style' ); ?>" style="width: 38%;">
                    <option value="flat_square" <?php if ( 'flat_square' == $instance['graph_style'] ) echo 'selected="selected"'; ?>>Flat Square</option>
                    <option value="flat_round" <?php if ( 'flat_round' == $instance['graph_style'] ) echo 'selected="selected"'; ?>>Flat Round</option>
                </select>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'show_flag' ); ?>">
                    <input type="checkbox" id="<?php echo $this->get_field_id( 'show_flag' ); ?>"
                           name="<?php echo $this->get_field_name( 'show_flag' ); ?>" value="show"
                        <?php if($instance['show_flag'] && $instance['show_flag']=="show"){
                            echo "checked";
                        } ?>/>
                    Show Country Flags</label>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'show_full_width_graph' ); ?>">
                    <input type="checkbox" id="<?php echo $this->get_field_id( 'show_full_width_graph' ); ?>"
                           name="<?php echo $this->get_field_name( 'show_full_width_graph' ); ?>" value="show"
                        <?php if($instance['show_full_width_graph'] && $instance['show_full_width_graph']=="show"){
                            echo "checked";
                        } ?>/>
                    Show Full Width Graph Lines</label>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'show_number_of_visits' ); ?>">
                    <input type="checkbox" id="<?php echo $this->get_field_id( 'show_number_of_visits' ); ?>"
                           name="<?php echo $this->get_field_name( 'show_number_of_visits' ); ?>" value="show"
                        <?php if($instance['show_number_of_visits'] && $instance['show_number_of_visits']=="show"){
                            echo "checked";
                        } ?>/>
                    Show Number of Visits</label>
            </p>
        <?php
        }
        else{
            ?>
            <p>Please connect to Google Analytics from plugin <a href="<?php echo $settings_url; ?>">settings</a> page.</p>
        <?php
        }
    }

    public function update( $new_instance, $old_instance ) {
        try{
            include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
            $instance = $old_instance;

            $instance['title'] = strip_tags( $new_instance['title'] );
            $instance['countries_count'] = $new_instance['countries_count'];
            $instance['graph_style'] = $new_instance['graph_style'];
            $instance['show_flag'] = $new_instance['show_flag'];
            $instance['show_full_width_graph'] = $new_instance['show_full_width_graph'];
            $instance['show_number_of_visits'] = $new_instance['show_number_of_visits'];

            // Calculating and storing next request timestamp
            update_option('ga_widget_top_countries_next_fetch_time',strtotime("+12 hours"));
            update_option('ga_widget_top_countries_response',array());
            update_option('ga_widget_top_countries_total_visitors',0);


            // Graph Style
            $instance['graph_style_class'] = "";
            switch($instance['graph_style']){
                case "flat_square":
                    break;
                case "flat_round":
                    $instance['graph_style_class'] = "rounded-graph";
                    break;
                default:
                    break;
            }

            // Full width graph lines
            $instance['full_width_graph_css'] = "";
            if($instance['show_full_width_graph'] && $instance['show_full_width_graph']=="show")
                $instance['full_width_graph_css'] = "graph-full-width";

            fn_ga_hourly(true,'widget-top-countries');

            return $instance;
        }
        catch(Exception $e){}
    }

    public function widget( $args, $instance ) {
        try{
            include GANALYTICS_PATH.'inc/ga-api-setup-include.php';

            wp_enqueue_style( 'ga-widget-top-countries-layout', GANALYTICS_URL.'/widgets/top-countries/css/ga-widget-top-countries-layout.css', array(),GANALYTICS_VERSION);
            wp_enqueue_style( 'ga-widget-top-countries-graph-color', GANALYTICS_URL.'/widgets/top-countries/css/ga-widget-top-countries-graph-color.css', array(),GANALYTICS_VERSION);
            extract( $args );

            // Reading widget title
            $title = apply_filters('widget_title', $instance['title'] );

            // Reading top countries
            $ganalytics_options = get_option('ganalytics_options');
            $result_top_countries = $ganalytics_options['widget_top_countries_response'];


            //Formatting output display
            echo $before_widget;

            if ( $title )
                echo $before_title . $title . $after_title;
            if($access_token){
                ?>
                <div class="ga-widget-top-countries-wrapper <?php echo $instance['full_width_graph_css'].' '.$instance['graph_style_class']; ?>">
                    <?php
                    if(is_array($result_top_countries)){
                        $graph_width = 0;
                        $row_count = 0;
                        foreach($result_top_countries as $key=>$result){
                            $row_count++;
                            if($row_count > $instance['countries_count'])
                                break;
                            $graph_width = round(((int)$result[1]/(int)$ganalytics_options['widget_site_visitors_total_visitors'])*100,2);
                            ?>
                            <div class="ga-widget-row">
                                <div class="ga-country-wrapper">
                                    <?php
                                    if($instance['show_flag'] && $instance['show_flag'] == "show"){
                                        $country_code = fn_ga_get_country_code($result[0]);
                                        $country_code = strtolower($country_code);
                                        if($country_code)
                                            echo '<img src="'.GANALYTICS_URL.'/assets/img/country_flags/'.$country_code.'.png"/> ';
                                    }
                                    ?>
                                    <?php echo $result[0]; ?>
                                    <?php
                                    if($instance['show_number_of_visits'] && $instance['show_number_of_visits'] == "show"){
                                        ?>
                                        <span class="ga-widget-number-of-visits"> - <?php echo $result[1] ?> Visits</span>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="ga-graph-wrapper">
                                    <div class="graph-bg-color"></div>
                                    <div class="graph-value-color value-color-<?php echo $row_count; ?>" style="width: <?php echo $graph_width; ?>%;"></div>
                                    <div class="graph-value-text"><?php echo $graph_width; ?>%</div>
                                </div>
                            </div>
                        <?php
                        }
                    }else{
                        echo "<p>No Country Information Available.</p>";
                    }
                    ?>
                    <div style="clear: both"></div>
                </div>
            <?php
            }else{
                ?>
                <p>Sorry! Widget could not connect to Google Analytics.</p>
            <?php
            }
            echo $after_widget;
        }
        catch(Exception $e){

        }
    }

}
add_action( 'widgets_init', 'addTopCountries');

function addTopCountries(){
    register_widget( 'GA_Top_Countries' );
}
?>