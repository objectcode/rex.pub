/* load css file dynamically */
function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
loadjscssfile("assets/css/style.css", "css");

$(document).ready(function(){
    if(location.protocol == "http:" || location.protocol == "https:"){
        $('.sidebar').load('assets/menu.html');
        $('.meta').load('assets/meta.html');
        $('.header').load('assets/header.html');
    }
    else{
        console.log("error");
        $('.ajax-info').css('display','block');
        $('.container').hide();
    }
});