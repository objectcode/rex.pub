<!DOCTYPE html>
<html>
<head>
    <title>GA Site Visitors - gAnalytics Documentation</title>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/custom.js"></script>
</head>
<body>
<div class="container">
    <div class="header"></div>
    <div class="meta"></div>
    <div class="content">
        <div class="sidebar"></div>
        <div class="main">
            <h2>GA Site Visitors</h2>
            <p>A simple widget to show total site visitors. The widget can added to any widget area by drag and drop.</p>
            <h3>Widget Front End</h3>
            <p>
                Here is a demo on how the widget appears in twenty twelve theme.<br/><br/>
                <img src="assets/img/widget-site-visitors-front-end.png"/>
            </p>
            <h3>Widget Options</h3>
            <p>
                Here is the screenshot of widget options page.<br/><br/>
                <img src="assets/img/widget-site-visitors-back-end.png"/>
            </p>
            <p>
                Let us go through the different widget options:
            </p>
            <p>
                <strong>Title</strong>: Allows to change the widget title.
            </p>
            <p>
                <strong>Request Interval in Hours</strong>: If the value is set to 4, then request for visitors data is
                sent every 4 hours. In between, the value is cached. It accepts only integer values like 1,2,3 etc.
            </p>
            <p>
                <strong>Display format</strong>: Currently the widget supports 2 display formats, <em>Compact</em> and
                <em>Long</em>. If the number of total visitors is 3427123, in compact form it is 3.4M and in long form it
                is 3,427,123.
            </p>
            <p>
                <strong>Text Alignment</strong>: Decides how the output is aligned inside the widget.
            </p>
            <p>
                <strong>Font Size</strong>: Accepts an integer value greater than 0. The unit of font size is in pixels(px).
                Default value is 36.
            </p>
            <p>
                <strong>Font Color</strong>: Hex color codes like #ff0000 have to be given. You should not add # symbol.
            </p>
            <p>
                <strong>Font Family</strong>: CSS font-family property accepted here. Ensure that the font you provide here
                is included in your site.
            </p>
            <p>
                <strong>Show Tracking From Date</strong>: If the checkbox is checked, then the date from which Google
                Analytics started tracking visitor's data is shown.
            </p>
        </div>
    </div>
    <div class="footer"></div>
    <div style="clear: both"></div>
</div>
<div class="ajax-info">You can see <strong>Documentation</strong> link under gAnalytics options, once the plugin is activated.
    <br/><br/>You are now trying to access the documentation directly by visiting the html files. This option is disabled as some
    sections like sidebar menu, contact information contains ajax loading of contents.</div>
</body>
</html>