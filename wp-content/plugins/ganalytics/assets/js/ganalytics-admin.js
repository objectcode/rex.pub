/**
* gAnalytics Wordpress Plugin
* @version 2.5
* @copyright (c) 2013 cube3x
*
* External javascript file for gAnalytics plugin. This file is called only in wordpress admin pages.
* Author Home Page: http://cube3x.com
*
**/
jQuery(function ($) {
    $(document).ready(function(){
        $('.tooltip').tooltipster({
            theme : 'tooltipster-light',
            arrow: false,
            maxWidth: 300,
            speed: 50,
            offsetY : -10,
            offsetY : -10,
            position: 'top-left'
        });
    });
});