<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.5
 * @copyright (c) 2013 cube3x
 *
 * This file shows details about Mobile Devices
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
set_time_limit(0);

//plugin page urls
$mobile_devices_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_mobile_devices';
$settings_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';

$duration = "1month";
if(isset($_GET['duration'])){
    $duration = $_GET['duration'];
}

/*********** Getting dates for duration *************/
$timestamp_yesterday = time() - 86400;

$timestamp_1week_start  =   $timestamp_yesterday    - 604800;

$timestamp_1month_start =   $timestamp_yesterday    - 2592000;

$timestamp_3month_start =   $timestamp_yesterday    - 7776000;

$timestamp_6month_start =   $timestamp_yesterday    - 15552000;

$timestamp_1year_start  =   $timestamp_yesterday    - 31536000;

$date_yesterday = date('Y-m-d',$timestamp_yesterday);

$date_1week_start  =   date('Y-m-d',$timestamp_1week_start);

$date_1month_start =   date('Y-m-d',$timestamp_1month_start);

$date_3month_start =   date('Y-m-d',$timestamp_3month_start);

$date_6month_start =   date('Y-m-d',$timestamp_6month_start);

$date_1year_start  =   date('Y-m-d',$timestamp_1year_start);
/*********** End of Getting Dates **************/

/************** Selecting start and end date ***************/
$report_date_start  = '';
$report_date_end    = $date_yesterday;
switch($duration){
    case "1week":
        $report_date_start  = $date_1week_start;
        break;
    case "1month":
        $report_date_start  = $date_1month_start;
        break;
    case "3months":
        $report_date_start  = $date_3month_start;
        break;
    case "6months":
        $report_date_start  = $date_6month_start;
        break;
    case "1year":
        $report_date_start  = $date_1year_start;
        break;
    default:
        break;
}
/************ End of selecting start and end date **********/

// Reading plugin settings from option variable
$ganalytics_settings = get_option('ganalytics_settings');
require_once GANALYTICS_PATH.'api.php';

if($access_token){
    $client->setAccessToken($access_token);
}

?>
<div class='wrap ganalytics mobile-devices'>
    <div class="icon32 ga-header-app-icon"></div>
    <h2>gAnalytics Mobile Devices
        <?php
        if($access_token){
            ?>
            <select class="ga-mobile-devices-time-period">
                <option value="1week" <?php if($duration == "1week") echo 'selected'; ?>>1 Week (7 Days)</option>
                <option value="1month" <?php if($duration == "1month") echo 'selected'; ?>>1 Month (30 Days)</option>
                <option value="3months" <?php if($duration == "3months") echo 'selected'; ?>>3 Months (90 Days)</option>
                <option value="6months" <?php if($duration == "6months") echo 'selected'; ?>>6 Months (180 Days)</option>
                <option value="1year" <?php if($duration == "1year") echo 'selected'; ?>>1 Year (365 Days)</option>
            </select>
            <span class="ga-mobile-devices-time-label">
                <?php echo date('M j, Y',strtotime($report_date_start));?> - <?php echo date('M j, Y',strtotime($report_date_end));?>
            </span>
        <?php
        }
        ?>
        <script type="text/javascript">
            jQuery(function ($) {
                $(document).ready(function(){
                    $(".ga-mobile-devices-time-period").change(function(){
                        var duration = $(this).val();
                        window.location.href = "<?php echo $mobile_devices_url; ?>&duration="+duration;
                    });
                });
            });
        </script>
    </h2>
    <?php
    try{
        if($access_token){
            ?>
            <div class="ga-mobile-devices-block-wrapper">
                <?php

                //request analytics data
                $report_data = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $report_date_start,
                    $report_date_end,
                    'ga:visits',
                    array(
                        'dimensions' => 'ga:mobileDeviceInfo',
                        'max-results' => 1000,
                        'sort' => '-ga:visits'
                    ));
                ?>
                <table class="widefat ga-mobile-devices-table">
                    <thead>
                    <tr>
                        <th>Sl #</th>
                        <th>Mobile Device</th>
                        <th>Visits</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Sl #</th>
                        <th>Mobile Device</th>
                        <th>Visits</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    if(is_array($report_data->getRows())){
                        $row_count = 0;
                        foreach($report_data->getRows() as $key=>$report){
                            $row_count++;
                            ?>
                            <tr>
                                <td><?php echo $row_count; ?></td>
                                <td><?php echo $report[0]; ?></td>
                                <td><?php echo number_format((int)$report[1]); ?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        <?php
        }
        else{
            ?>
            <div class="updated below-h2">
                <p>
                    Please update <a href="<?php echo $settings_url; ?>">gAnalytics Settings</a>. Once you are connected, you can view your important
                    Google Analytics statistics here.
                </p>
            </div>
        <?php
        }
    }catch(Exception $e){
        ?>
        <div class="updated below-h2" style="margin-left: 0px;">
            <p>
                Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $settings_url; ?>">settings</a> page to check connection status.
            </p>
        </div>
    <?php
    }
    ?>
</div>