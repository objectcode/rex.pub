<div class="wrap ganalytics documentation">
    <div id="icon-options-general" class="icon32 ga-header-app-icon"><br></div>
    <h2>gAnalytics Documentation</h2>
    <div style="text-align: left; margin-top: 2em;">
        <a href="<?php echo GANALYTICS_URL ?>/docs" target="_blank">Click Here</a> to open documentation in a new tab.
    </div>
</div>