<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.6.2
 * @copyright (c) 2013 cube3x
 *
 * This file outputs settings page. It handles authentication of the plugin
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
try{
    set_time_limit(0);
    $notification_message = "";
    $notification_status = "";
    // Including Google Analytics SDK
    require_once GANALYTICS_PATH.'ga-sdk/Google_Client.php';
    require_once GANALYTICS_PATH.'ga-sdk/contrib/Google_AnalyticsService.php';
    $client = new Google_Client();
    $client->setApplicationName('gAnalytics');
    $client->setAccessType('offline');
    $client->setUseObjects(true);

    // Reading plugin settings from option variable
    $ganalytics_settings = get_option('ganalytics_settings');
    $redirect_url = get_bloginfo('wpurl').'/wp-admin/options-general.php?page=wp-google-analytics-mail';
    $ganalytics_options = get_option('ganalytics_options');
    //Analysing data from different forms in the page
    if(isset($_POST['settings-action'])){
        $settings_action = $_POST['settings-action'];
        switch($settings_action){
            case 'save-access-code':
                $ganalytics_settings['google_auth_code'] = $_POST['access-code'];
                update_option('ganalytics_settings', $ganalytics_settings);
                break;

            case 'save-api-credentials':
                $ganalytics_settings['google_api_key'] = $_POST['api-key'];
                $ganalytics_settings['google_client_id'] = $_POST['client-id'];
                $ganalytics_settings['google_client_secret'] = $_POST['client-secret'];
                $ganalytics_settings['google_auth_code'] = "";
                $ganalytics_settings['google_access_token'] = "";
                update_option('ganalytics_settings', $ganalytics_settings);
                $notification_message = "API credentials saved. Please obtain a new Google access code from step 2
                and Connect.";
                $notification_status = "updated";
                break;

            case 'form-reset-api-settings':
                $ganalytics_settings = array(
                    'use_own_api' => false,
                    'google_auth_code' => '',
                    'google_api_key' => 'AIzaSyCCrO5rIlk2DKjj7ZGulhlmHNlab6gJi0k',
                    'google_client_id' => '987871093172-f7v9tra8u3mevo38o5lhutk0vo7gg1n8.apps.googleusercontent.com',
                    'google_client_secret' => 'Qw-S1iXB9tLXUqPoWlnYyJU8',
                    'google_access_token' => '',
                    'ga_active_web_property' => '',
                    'ga_profile_id' => '',
                    'version' => GANALYTICS_VERSION
                );
                update_option('ganalytics_settings', $ganalytics_settings);
                break;

            case 'form-select-profile':
                $notification_message = "New Profile Selected";
                $notification_status = "updated";
                break;

            case 'save-options':
                $ganalytics_options['request_interval'] = $_POST['api-request-frequency'];
                $ganalytics_options['next_request_time'] = time() + ($_POST['api-request-frequency']*3600);
                update_option('ganalytics_options', $ganalytics_options);
                break;
            default:
                break;
        }
    }

    $ganalytics_settings = get_option('ganalytics_settings');

    $client->setClientId($ganalytics_settings['google_client_id']);
    $client->setClientSecret($ganalytics_settings['google_client_secret']);
    $client->setDeveloperKey($ganalytics_settings['google_api_key']);
    $client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');

    $analytics = new Google_AnalyticsService($client);
    // Setting Access Token
    $access_token = $ganalytics_settings['google_access_token'];

    if($access_token){
        $client->setAccessToken($access_token);
    }
    else{
        if($ganalytics_settings['google_auth_code']){
            $client->authenticate($ganalytics_settings['google_auth_code']);
            $ganalytics_settings['google_access_token'] = $client->getAccessToken();
            update_option('ganalytics_settings', $ganalytics_settings);
        }
    }
    ?>
    <div class="wrap ganalytics settings">
    <h2>gAnalytics Settings</h2>
    <?php
    // Show notice message
    if($notification_message){
        ?>
        <div id="message" class="<?php echo $notification_status ?>"><p>
                <?php echo $notification_message; ?>
            </p></div>
    <?php
    }
    ?>
    <?php
    if(!$client->getAccessToken()){
        $authUrl = $client->createAuthUrl();
        ?>
        <form method="post" name="formAuthOption">
            <input type="hidden" name="settings-action" value="save-api-credentials"/>
            <table class="form-table">
                <tbody>
                <tr>
                    <td colspan="2" style="padding-top: 0;">
                        <h3>Step 1: Save API credentials</h3>
                        Plugin connects with Google Analytics with following API credentials. You can change the values
                        to your own API details.
                    </td>
                </tr>
                <tr>
                    <th>
                        Client ID:
                    </th>
                    <td>
                        <input type="text" name="client-id" class="regular-text" style="width: 70%;"
                               value="<?php echo $ganalytics_settings['google_client_id'] ?>" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        Client Secret:
                    </th>
                    <td>
                        <input type="text" name="client-secret" class="regular-text" style="width: 70%;"
                               value="<?php echo $ganalytics_settings['google_client_secret'] ?>" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        API Key:
                    </th>
                    <td>
                        <input type="text" name="api-key" class="regular-text" style="width: 70%;"
                               value="<?php echo $ganalytics_settings['google_api_key'] ?>" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input type="submit" name="access-code-submit" class="button button-primary"
                               value="Save API Details"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <!-- Google Access Code -->
        <form method="post" name="formAccessCode">
            <input type="hidden" name="settings-action" value="save-access-code"/>
            <table class="form-table">
                <tbody>
                <tr>
                    <td colspan="2"><hr class="hr-thin"/>
                        <h3>Step 2: Get access code and connect to Google Analytics</h3>
                        <a href="<?php echo $authUrl; ?>" target="_blank">Click here</a> to obtain your Google Access Code
                    </td>
                </tr>
                <tr>
                    <th>
                        Paste Access Code Here:
                    </th>
                    <td>
                        <input type="text" name="access-code" class="regular-text"
                               value="<?php echo $ganalytics_settings['google_auth_code'] ?>" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th>

                    </th>
                    <td>
                        <input type="submit" name="access-code-submit" class="button button-primary" value="Save and Connect"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    <?php
    }
    else
    {

        ?>
        <table class="form-table">
            <?php
            $blog_url = get_bloginfo( 'url' );
            $parsed_url = parse_url($blog_url);
            $blog_domain = $parsed_url["host"];
            $blog_domain = str_ireplace("www.","",$blog_domain);

            // finding blog profile from list of ganalytics profiles

            // Reading all web properties in connected google analytics
            $props = $analytics->management_webproperties->listManagementWebproperties("~all");
            $items = $props->getItems();
            ?>
            <tbody>
            <tr>
                <td colspan="2"><strong>&#10004; You are now connected to Google Analytics.</strong></td>
            </tr>
            <tr>
                <td colspan="2"><hr/></td>
            </tr>
            <tr>
                <td colspan="2">Google Analytics profile chosen for <?php echo $blog_domain; ?>:</td>
            </tr>
            <?php


            // Saving new profile selected by admin
            if(isset($_POST['settings-action']) && $_POST['settings-action'] == 'form-select-profile'){
                $blog_domain_property = '';

                foreach($items as $key => $property){
                    if($property->websiteUrl == $_POST['ga-select-web-property']){
                        $blog_domain_property = $property;
                        $ganalytics_settings['ga_active_web_property'] = $blog_domain_property;
                        update_option('ganalytics_settings', $ganalytics_settings);
                        break;
                    }
                }
            }else{
                //for initial page load. taking property saved from db
                if($ganalytics_settings['ga_active_web_property']){
                    $blog_domain_property =  $ganalytics_settings['ga_active_web_property'];
                }else{
                    //for first time ever page load. the profile is automatically selected by plugin
                    $blog_domain_property = '';

                    foreach($items as $key => $property){
                        if(strpos($property->websiteUrl, $blog_domain)){
                            $blog_domain_property = $property;
                            $ganalytics_settings['ga_active_web_property'] = $blog_domain_property;
                            update_option('ganalytics_settings', $ganalytics_settings);
                            break;
                        }
                        else{
                            $blog_domain_property = $property;
                            $ganalytics_settings['ga_active_web_property'] = $blog_domain_property;
                            update_option('ganalytics_settings', $ganalytics_settings);
                        }
                    }
                }
            }
            ?>
            <tr>
                <th>Website URL:</th>
                <td><strong><?php echo $blog_domain_property->websiteUrl ?></strong></td>
            </tr>
            <tr>
                <th>Analytics ID:</th>
                <td><strong><?php echo $blog_domain_property->id ?></strong></td>
            </tr>
            <tr>
                <th>Web Property ID:</th>
                <td><strong><?php echo $blog_domain_property->internalWebPropertyId ?></strong></td>
            </tr>
            </tbody>
        </table>

        <form method="post">
            <input type="hidden" name="settings-action" value="form-select-profile"/>
            <table class="form-table ga-web-property-wrapper">
                <tbody>
                <tr>
                    <td colspan="2">If above profile does not corresponds to this site, please select another profile.</td>
                </tr>
                <tr>
                    <th>Select Your Analytics Profile</th>
                    <td>
                        <?php
                        echo '<select name="ga-select-web-property">';
                        foreach($items as $key => $property){
                            $url = $property->websiteUrl;
                            echo '<option value="'.$url.'"';
                            if($property->websiteUrl == $blog_domain_property->websiteUrl)
                                echo 'selected';
                            echo'>'.$url.'</option>';
                        }
                        echo '</select>';
                        ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input type="submit" value="Save Analytics Profile" class="button button-primary"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>

        <form method="post">
            <input type="hidden" name="settings-action" value="save-options"/>
            <table class="form-table">
                <tbody>
                <tr>
                    <td colspan="2">
                        <hr class="hr-thin"/>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;">
                        Analytics API request frequency:
                    </td>
                    <td>
                        <select name="api-request-frequency">
                            <option value="12"
                                <?php echo ($ganalytics_options['request_interval'] == 12)?"selected":""; ?>
                                >12 Hours</option>
                            <option value="24"
                                <?php echo ($ganalytics_options['request_interval'] == 24)?"selected":""; ?>
                                >24 Hours</option>
                            <option value="48"
                                <?php echo ($ganalytics_options['request_interval'] == 48)?"selected":""; ?>
                                >48 Hours</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Save Option" class="button button-primary"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <?php
        //Saving active profile id to options
        $blog_domain_property = $ganalytics_settings['ga_active_web_property'];
        $profile_details = $analytics->management_profiles->listManagementProfiles($blog_domain_property->accountId, $blog_domain_property->id)->getItems();
        $profile_id = $profile_details[0]->id;
        $ganalytics_settings['ga_profile_id'] = $profile_id;
        update_option('ganalytics_settings', $ganalytics_settings);
        fn_ga_hourly(true);
        ?>
        <form method="post">
            <input type="hidden" name="settings-action" value="form-reset-api-settings"/>
            <table class="form-table">
                <tbody>
                <tr>
                    <td>
                        <hr class="hr-thin"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Clicking the button below will reset all your settings.
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Reset API Settings" class="button button-danger"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>

    <?php
    }
    ?>
    </div>
<?php
}
catch(Exception $e){
    ?>
    <div class="updated below-h2" style="margin-left: 0px;">
        <p>
            Could not load needed resources. Please check your internet connection or try after some time.
            <br/><br/>
            Exception message for WordPress developers:<br/>
            <?php echo $e->getMessage(); ?>
        </p>
    </div>
<?php
}
?>