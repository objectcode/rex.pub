<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.5
 * @copyright (c) 2013 cube3x
 *
 * This file outputs gA compare page.
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
try{

    set_time_limit(0);
//compare url
    $compare_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_compare';
    $redirect_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';
//timestamps needed for timeframe dropdown
    $timestamp_yesterday = time() - 86400;

    $timestamp_1week_start  =   $timestamp_yesterday    - 604800;
    $timestamp_11week_end   =   $timestamp_1week_start  - 86400;
    $timestamp_11week_start =   $timestamp_11week_end   - 604800;

    $timestamp_1month_start =   $timestamp_yesterday    - 2592000;
    $timestamp_11month_end  =   $timestamp_1month_start - 86400;
    $timestamp_11month_start=   $timestamp_11month_end  - 2592000;

    $timestamp_3month_start =   $timestamp_yesterday    - 7776000;
    $timestamp_33month_end  =   $timestamp_3month_start - 86400;
    $timestamp_33month_start=   $timestamp_33month_end  - 7776000;

    $timestamp_6month_start =   $timestamp_yesterday    - 15552000;
    $timestamp_66month_end  =   $timestamp_6month_start - 86400;
    $timestamp_66month_start=   $timestamp_66month_end  - 15552000;

    $timestamp_1year_start  =   $timestamp_yesterday    - 31536000;
    $timestamp_11year_end   =   $timestamp_1year_start  - 86400;
    $timestamp_11year_start =   $timestamp_11year_end   - 31536000;

    $date_yesterday = date('Y-m-d',$timestamp_yesterday);

    $date_1week_start  =   date('Y-m-d',$timestamp_1week_start);
    $date_11week_end   =   date('Y-m-d',$timestamp_11week_end);
    $date_11week_start =   date('Y-m-d',$timestamp_11week_start);

    $date_1month_start =   date('Y-m-d',$timestamp_1month_start);
    $date_11month_end  =   date('Y-m-d',$timestamp_11month_end);
    $date_11month_start=   date('Y-m-d',$timestamp_11month_start);

    $date_3month_start =   date('Y-m-d',$timestamp_3month_start);
    $date_33month_end  =   date('Y-m-d',$timestamp_33month_end);
    $date_33month_start=   date('Y-m-d',$timestamp_33month_start);

    $date_6month_start =   date('Y-m-d',$timestamp_6month_start);
    $date_66month_end  =   date('Y-m-d',$timestamp_66month_end);
    $date_66month_start=   date('Y-m-d',$timestamp_66month_start);

    $date_1year_start  =   date('Y-m-d',$timestamp_1year_start);
    $date_11year_end   =   date('Y-m-d',$timestamp_11year_end);
    $date_11year_start =   date('Y-m-d',$timestamp_11year_start);

    $duration = "6months";
    if(isset($_GET['duration'])){
        $duration = $_GET['duration'];
    }

// setting start and end dates for date range
    $latest_date_start  = '';
    $latest_date_end    = $date_yesterday;
    $older_date_start   = '';
    $older_date_end     = '';
    switch($duration){
        case "1week":
            $latest_date_start  = $date_1week_start;
            $older_date_start   = $date_11week_start;
            $older_date_end     = $date_11week_end;
            break;
        case "1month":
            $latest_date_start  = $date_1month_start;
            $older_date_start   = $date_11month_start;
            $older_date_end     = $date_11month_end;
            break;
        case "3months":
            $latest_date_start  = $date_3month_start;
            $older_date_start   = $date_33month_start;
            $older_date_end     = $date_33month_end;
            break;
        case "6months":
            $latest_date_start  = $date_6month_start;
            $older_date_start   = $date_66month_start;
            $older_date_end     = $date_66month_end;
            break;
        case "1year":
            $latest_date_start  = $date_1year_start;
            $older_date_start   = $date_11year_start;
            $older_date_end     = $date_11year_end;
            break;
        default:
            break;
    }
// Reading plugin settings from option variable
    $ganalytics_settings = get_option('ganalytics_settings');
    $settings_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';

    require_once GANALYTICS_PATH.'api.php';

    ?>
<div class="wrap ganalytics compare">
<div id="icon-options-general" class="icon32 ga-header-app-icon"><br></div>
<h2>gAnalytics Compare
    <?php
    if($access_token){
        ?>
        <select class="ga-compare-time-period">
            <option value="1week" <?php if($duration == "1week") echo 'selected'; ?>>1 Week (7 Days)</option>
            <option value="1month" <?php if($duration == "1month") echo 'selected'; ?>>1 Month (30 Days)</option>
            <option value="3months" <?php if($duration == "3months") echo 'selected'; ?>>3 Months (90 Days)</option>
            <option value="6months" <?php if($duration == "6months") echo 'selected'; ?>>6 Months (180 Days)</option>
            <option value="1year" <?php if($duration == "1year") echo 'selected'; ?>>1 Year (365 Days)</option>
        </select>
        <?php
    }
    ?>
</h2>
    <?php
    if($access_token){
        try{
            $client->setAccessToken($access_token);

            $blog_domain_property =  $ganalytics_settings['ga_active_web_property'];
            ?>
        <script type="text/javascript">
            jQuery(function ($) {
                $(document).ready(function(){
                    $(".ga-compare-time-period").change(function(){
                        var duration = $(this).val();
                        window.location.href = "<?php echo $compare_url; ?>&duration="+duration;
                    });
                });
            });
        </script>
        <div class="ga-compare-block-wrapper">
        <table class="form-table" style="margin-top: 0;">
        <tbody>
        <tr>
            <td style="width: 50%; font-weight: bold; text-align: center;">
                <div style="display:inline-block; margin:auto; font-size: 16px;">
                    <span style="float: left;"><?php echo date('M j, Y',strtotime($older_date_start));?></span>
                    <img src="<?php echo GANALYTICS_URL; ?>/assets/img/calendar-icon-16-16.png" style="float:left; margin:0 10px;"/>
                    <span style="float: left;"><?php echo date('M j, Y',strtotime($older_date_end)); ?></span>
                </div>
            </td>
            <td style="width: 50%; font-weight: bold; text-align: center;">
                <div style="display:inline-block; margin:auto; font-size: 16px;">
                    <span style="float: left;"><?php echo date('M j, Y',strtotime($latest_date_start));?></span>
                    <img src="<?php echo GANALYTICS_URL; ?>/assets/img/calendar-icon-16-16.png" style="float:left; margin:0 10px;"/>
                    <span style="float: left;"><?php echo date('M j, Y',strtotime($latest_date_end)); ?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td><strong>Overview:</strong></td>
            <td><strong>Overview:</strong></td>
        </tr>
        <tr>
            <td style="padding: 0;">
                <?php
                $results_overview_old = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $older_date_start,
                    $older_date_end,
                    'ga:visits,ga:pageviews,ga:avgTimeOnSite,ga:entranceBounceRate');
                $visits_old = number_format((int)$results_overview_old->rows[0][0]);
                $pageviews_old = number_format((int)$results_overview_old->rows[0][1]);
                $visitdur_old = gmdate("H:i:s",(int)$results_overview_old->rows[0][2]);
                $bouncerate_old = round((float)$results_overview_old->rows[0][3],2);
                ?>
                <table class="form-table compare-overview-table">
                    <tr>
                        <td style="width: 30%">Visits:</td>
                        <td style="width: 70%"><?php echo $visits_old; ?></td>
                    </tr>
                    <tr>
                        <td>Page Views:</td>
                        <td><?php echo $pageviews_old; ?></td>
                    </tr>
                    <tr>
                        <td>Bounce Rate:</td>
                        <td><?php echo $bouncerate_old; ?>%</td>
                    </tr>
                    <tr>
                        <td>Avg. Visit Duration:</td>
                        <td><?php echo $visitdur_old; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="hr-thin"/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="padding: 0;">
                <?php
                $results_overview_new = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $latest_date_start,
                    $latest_date_end,
                    'ga:visits,ga:pageviews,ga:avgTimeOnSite,ga:entranceBounceRate');
                $visits_new = number_format((int)$results_overview_new->rows[0][0]);
                $pageviews_new = number_format((int)$results_overview_new->rows[0][1]);
                $visitdur_new = gmdate("H:i:s",(int)$results_overview_new->rows[0][2]);
                $bouncerate_new = round((float)$results_overview_new->rows[0][3],2);
                ?>
                <table class="form-table compare-overview-table">
                    <tr>
                        <td style="width: 30%">Visits:</td>
                        <td style="width: 70%"><?php echo $visits_new; ?></td>
                    </tr>
                    <tr>
                        <td>Page Views:</td>
                        <td><?php echo $pageviews_new; ?></td>
                    </tr>
                    <tr>
                        <td>Bounce Rate:</td>
                        <td><?php echo $bouncerate_new; ?>%</td>
                    </tr>
                    <tr>
                        <td>Avg. Visit Duration:</td>
                        <td><?php echo $visitdur_new; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="hr-thin"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Traffic Sources:</strong>
                <?php
                $results_traffic_source_old = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $older_date_start,
                    $older_date_end,
                    'ga:visits',
                    array(
                        'dimensions' => 'ga:medium'
                    ));
                $direct_traffic = 0;
                $search_traffic = 0;
                $referral_traffic = 0;
                $campaign_traffic = 0;
                if(is_array($results_traffic_source_old->getRows())){
                    foreach($results_traffic_source_old->getRows() as $row){
                        if($row[0] == '(none)'){
                            $direct_traffic += (int)$row[1];
                        }
                        else if($row[0] == 'organic'){
                            $search_traffic += (int)$row[1];
                        }
                        else if($row[0] == 'referral'){
                            $referral_traffic += (int)$row[1];
                        }
                        else{
                            $campaign_traffic += (int)$row[1];
                        }
                    }
                }
                ?>
                <div id="chart_pie_traffic_source_1" style="width: 100%; height: 200px;"></div>
                <script type="text/javascript">
                    google.load("visualization", "1", {packages:["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Source', 'Visits'],
                            ['Search',     <?php echo $search_traffic; ?>],
                            ['Referral',      <?php echo $referral_traffic; ?>],
                            ['Direct',  <?php echo $direct_traffic; ?>],
                            ['Campaigns', <?php echo $campaign_traffic; ?>]
                        ]);

                        var options = {
                            chartArea:{
                                height: "90%"
                            },
                            colors: ['#2193d1','#7cac3f','#cd1e45','#f9d03a']
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('chart_pie_traffic_source_1'));
                        chart.draw(data, options);
                    }
                </script>
                <hr class="hr-thin"/>
            </td>
            <td>
                <strong>Traffic Sources:</strong>
                <?php
                $results_traffic_source_new = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $latest_date_start,
                    $latest_date_end,
                    'ga:visits',
                    array(
                        'dimensions' => 'ga:medium'
                    ));
                $direct_traffic = 0;
                $search_traffic = 0;
                $referral_traffic = 0;
                $campaign_traffic = 0;
                if(is_array($results_traffic_source_new->getRows())){
                    foreach($results_traffic_source_new->getRows() as $row){
                        if($row[0] == '(none)'){
                            $direct_traffic += (int)$row[1];
                        }
                        else if($row[0] == 'organic'){
                            $search_traffic += (int)$row[1];
                        }
                        else if($row[0] == 'referral'){
                            $referral_traffic += (int)$row[1];
                        }
                        else{
                            $campaign_traffic += (int)$row[1];
                        }
                    }
                }
                ?>
                <div id="chart_pie_traffic_source_2" style="width: 100%; height: 200px;"></div>
                <script type="text/javascript">
                    google.load("visualization", "1", {packages:["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Source', 'Visits'],
                            ['Search',     <?php echo $search_traffic; ?>],
                            ['Referral',      <?php echo $referral_traffic; ?>],
                            ['Direct',  <?php echo $direct_traffic; ?>],
                            ['Campaigns', <?php echo $campaign_traffic; ?>]
                        ]);

                        var options = {
                            chartArea:{
                                height: "90%"
                            },
                            colors: ['#2193d1','#7cac3f','#cd1e45','#f9d03a']
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('chart_pie_traffic_source_2'));
                        chart.draw(data, options);
                    }
                </script>
                <hr class="hr-thin"/>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Top 5 Countries:</strong>
                <div>
                    <?php
                    $results_top_countries_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $older_date_start,
                        $older_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:country',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_countries_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_countries_old->getRows() as $top_country){
                            echo '<li>';
                            $country_code = fn_ga_get_country_code($top_country[0]);
                            $country_code = strtolower($country_code);
                            if($country_code)
                                echo '<img src="'.GANALYTICS_URL.'/assets/img/country_flags/'.$country_code.'.png"/> ';
                            echo $top_country[0];
                            echo '<span style="color: #aaa;"> - '.$top_country[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }else{
                        echo '<div>No Countries Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
            <td>
                <strong>Top 5 Countries:</strong>
                <div>
                    <?php
                    $results_top_countries_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $latest_date_start,
                        $latest_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:country',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_countries_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_countries_old->getRows() as $top_country){
                            echo '<li>';
                            $country_code = fn_ga_get_country_code($top_country[0]);
                            $country_code = strtolower($country_code);
                            if($country_code)
                                echo '<img src="'.GANALYTICS_URL.'/assets/img/country_flags/'.$country_code.'.png"/> ';
                            echo $top_country[0];
                            echo '<span style="color: #aaa;"> - '.$top_country[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Countries Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Top 5 Pages:</strong>
                <div>
                    <?php
                    $results_top_pages_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $older_date_start,
                        $older_date_end,
                        'ga:pageviews',
                        array(
                            'dimensions' => 'ga:pageTitle,ga:pagePath',
                            'sort' => '-ga:pageviews',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_pages_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_pages_old->getRows() as $top_page){
                            echo '<li>';
                            echo '<a href="'.$blog_domain_property->websiteUrl.$top_page[1].'" target="_blank">'.$top_page[0].'</a> ';
                            echo '<span style="color: #aaa;"> - '.$top_page[2].' Views</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Pages Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
            <td>
                <strong>Top 5 Pages:</strong>
                <div>
                    <?php
                    $results_top_pages_new = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $latest_date_start,
                        $latest_date_end,
                        'ga:pageviews',
                        array(
                            'dimensions' => 'ga:pageTitle,ga:pagePath',
                            'sort' => '-ga:pageviews',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_pages_new->getRows())){
                        echo '<ol>';
                        foreach($results_top_pages_new->getRows() as $top_page){
                            echo '<li>';
                            echo '<a href="'.$blog_domain_property->websiteUrl.$top_page[1].'" target="_blank">'.$top_page[0].'</a> ';
                            echo '<span style="color: #aaa;"> - '.$top_page[2].' Views</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Pages Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Top 5 Keywords:</strong>
                <div>
                    <?php
                    $results_top_keywords_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $older_date_start,
                        $older_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:keyword',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_keywords_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_keywords_old->getRows() as $top_keyword){
                            echo '<li>';
                            echo $top_keyword[0];
                            echo '<span style="color: #aaa;"> - '.$top_keyword[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Keywords Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
            <td>
                <strong>Top 5 Keywords:</strong>
                <div>
                    <?php
                    $results_top_keywords_new = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $latest_date_start,
                        $latest_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:keyword',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_keywords_new->getRows())){
                        echo '<ol>';
                        foreach($results_top_keywords_new->getRows() as $top_keyword){
                            echo '<li>';
                            echo $top_keyword[0];
                            echo '<span style="color: #aaa;"> - '.$top_keyword[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Keywords Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Top 5 Referrals:</strong>
                <div>
                    <?php
                    $results_top_referrals_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $older_date_start,
                        $older_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:source',
                            'filters' => 'ga:medium==referral',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_referrals_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_referrals_old->getRows() as $top_referral){
                            echo '<li>';
                            echo '<a href="http://'.$top_referral[0].'" target="_blank">'.$top_referral[0].'</a> ';
                            echo '<span style="color: #aaa;"> - '.$top_referral[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Referrals Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
            <td>
                <strong>Top 5 Referrals:</strong>
                <div>
                    <?php
                    $results_top_referrals_new = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $latest_date_start,
                        $latest_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:source',
                            'filters' => 'ga:medium==referral',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_referrals_new->getRows())){
                        echo '<ol>';
                        foreach($results_top_referrals_new->getRows() as $top_referral){
                            echo '<li>';
                            echo '<a href="http://'.$top_referral[0].'" target="_blank">'.$top_referral[0].'</a> ';
                            echo '<span style="color: #aaa;"> - '.$top_referral[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Referrals Available</div>';
                    }
                    ?>
                </div>
                <hr class="hr-thin"/>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Top 5 Browsers:</strong>
                <div>
                    <?php
                    $results_top_browsers_old = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $older_date_start,
                        $older_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:browser',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_browsers_old->getRows())){
                        echo '<ol>';
                        foreach($results_top_browsers_old->getRows() as $top_browser){
                            echo '<li>';
                            $browser_icon = fn_ga_get_browser_image($top_browser[0]);
                            echo '<img src="'.GANALYTICS_URL.'/assets/img/browsers/'.$browser_icon.'"/> ';
                            echo $top_browser[0];
                            echo '<span style="color: #aaa;"> - '.$top_browser[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }
                    else{
                        echo '<div>No Browsers Available</div>';
                    }

                    ?>
                </div>
            </td>
            <td>
                <strong>Top 5 Browsers:</strong>
                <div>
                    <?php
                    $results_top_browsers_new = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $latest_date_start,
                        $latest_date_end,
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:browser',
                            'sort' => '-ga:visits',
                            'max-results' => 5
                        ));
                    if(is_array($results_top_browsers_new->getRows())){
                        echo '<ol>';
                        foreach($results_top_browsers_new->getRows() as $top_browser){
                            echo '<li>';
                            $browser_icon = fn_ga_get_browser_image($top_browser[0]);
                            echo '<img src="'.GANALYTICS_URL.'/assets/img/browsers/'.$browser_icon.'"/> ';
                            echo $top_browser[0];
                            echo '<span style="color: #aaa;"> - '.$top_browser[1].' Visits</span>';
                            echo '</li>';
                        }
                        echo '</ol>';
                    }else{
                        echo '<div>No Browsers Available</div>';
                    }

                    ?>
                </div>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
            <?php
        }
        catch(Exception $e){
            ?>
        <div class="updated below-h2">
            <p>
                Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $redirect_url; ?>">settings</a> page to check connection status.
            </p>
        </div>
            <?php
        }
    }
    else{
        ?>
    <div class="updated below-h2">
        <p>
            Please update <a href="<?php echo $redirect_url; ?>">gAnalytics Settings</a>. Once you are connected, you can view your important
            Google Analytics statistics here.
        </p>
    </div>
        <?php
    }
    ?>
</div>
<?php
}
catch(Exception $e){
    ?>
<div class="updated below-h2" style="margin-left: 0px;">
    <p>
        Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $redirect_url; ?>">settings</a> page to check connection status.
    </p>
</div>
<?php
}
?>