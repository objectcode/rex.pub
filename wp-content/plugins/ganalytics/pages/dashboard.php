<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.5
 * @copyright (c) 2013 cube3x
 *
 * This file outputs gAnalytics dashboard page
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
try{
    // Reading plugin settings from option variable
    $ganalytics_settings = get_option('ganalytics_settings');
    $redirect_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';
    require_once GANALYTICS_PATH.'api.php';
    ?>
    <div class='wrap ganalytics dashboard'>
    <div class="icon32 ga-header-app-icon"></div>
    <h2>gAnalytics Dashboard
        <?php
        if($access_token){
            try{
                $client->setAccessToken($access_token);
                $blog_domain_property =  $ganalytics_settings['ga_active_web_property'];
                ?>
                <span style="float: right; font-size: 12px; font-weight: bold">
    <?php
    echo $blog_domain_property->websiteUrl;
    ?> <span style="font-weight: normal;">- Last 3 Months Performance</span>
</span>
            <?php
            }
            catch(Exception $e){}
        }
        ?>
    </h2>
    <?php
    if($access_token){
        try{
            $client->setAccessToken($access_token);
            $blog_domain_property =  $ganalytics_settings['ga_active_web_property'];
            ?>
            <div class="ga-header-block-wrapper">
                <?php
                // PHP code to fetch figures for dashboard header block
                $timestamp_3months_end = time() - 86400;
                $timestamp_3months_start = $timestamp_3months_end - 7776000;
                $timestamp_6months_end = $timestamp_3months_start - 86400;
                $timestamp_6months_start = $timestamp_6months_end - 7776000;

                $date_3months_start = date('Y-m-d',$timestamp_3months_start);
                $date_3months_end = date('Y-m-d',$timestamp_3months_end);
                $date_6months_start = date('Y-m-d',$timestamp_6months_start);
                $date_6months_end = date('Y-m-d',$timestamp_6months_end);

                $results_3months = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $date_3months_start,
                    $date_3months_end,
                    'ga:visits,ga:pageviews,ga:avgTimeOnSite,ga:entranceBounceRate');
                $results_6months = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $date_6months_start,
                    $date_6months_end,
                    'ga:visits,ga:pageviews,ga:avgTimeOnSite,ga:entranceBounceRate');

                //fetching results - last 3 months
                $visits_3month = (int)$results_3months->rows[0][0];
                $pageviews_3month = (int)$results_3months->rows[0][1];
                $visitdur_3month = (int)$results_3months->rows[0][2];
                $bouncerate_3month = round((float)$results_3months->rows[0][3],2);

                //fetching results - last 6 months
                $visits_6month = (int)$results_6months->rows[0][0];
                $pageviews_6month = (int)$results_6months->rows[0][1];
                $visitdur_6month = (int)$results_6months->rows[0][2];
                $bouncerate_6month = round((float)$results_6months->rows[0][3],2);

                //calculate visit data
                $diff_visits = $visits_3month - $visits_6month;
                $change_diff_visits = "up";
                $change_icon_visits = "&#9650;";
                if($diff_visits < 0){
                    $change_diff_visits = "down";
                    $change_icon_visits = "&#9660;";
                }
                else{
                    $change_diff_visits = "up";
                    $change_icon_visits = "&#9650;";
                }
                $formatted_visits_3month = number_format(abs($visits_3month));
                $formatted_diff_visits = number_format(abs($diff_visits));

                //calculate page view data
                $diff_pageviews = $pageviews_3month - $pageviews_6month;
                $change_diff_pageviews = "up";
                $change_icon_pageviews = "&#9650;";
                if($diff_pageviews < 0){
                    $change_diff_pageviews = "down";
                    $change_icon_pageviews = "&#9660;";
                }
                else{
                    $change_diff_pageviews = "up";
                    $change_icon_pageviews = "&#9650;";
                }
                $formatted_pageviews_3month = number_format(abs($pageviews_3month));
                $formatted_diff_pageviews = number_format(abs($diff_pageviews));

                //calculate average visit duration
                $diff_timeduration = $visitdur_3month - $visitdur_6month;
                $change_diff_visitduration = "up";
                $change_icon_visitduration = "&#9650;";
                if($diff_timeduration < 0){
                    $change_diff_visitduration = "down";
                    $change_icon_visitduration = "&#9660;";
                }
                else{
                    $change_diff_visitduration = "up";
                    $change_icon_visitduration = "&#9650;";
                }
                if($visitdur_3month != 0){
                    $percentage_diff_timeduration = abs(round(($diff_timeduration/$visitdur_3month)*100,2));
                }else{
                    $percentage_diff_timeduration = 0;
                }
                $formatted_visit_duration_3months = gmdate("H:i:s",$visitdur_3month);
                $formatted_diff_visit_duration = $percentage_diff_timeduration.'%';

                //calculate bounce rate
                $diff_bouncerate = $bouncerate_3month - $bouncerate_6month;
                $change_icon_bouncerate = "&#9650;";
                if($diff_bouncerate < 0){
                    $change_icon_bouncerate = "&#9660;";
                }
                else {
                    $change_icon_bouncerate = "&#9650;";
                }
                $formatted_bouncerate_3months = $bouncerate_3month.'%';
                $formatted_diff_bouncerate = abs($diff_bouncerate).'%';

                ?>
                <table class="form-table">
                    <tbody>
                    <tr>
                        <td class="ga-header-table-cell">
                            <div class="ga-header-title">
                                Page Visits
                                <img src="<?php echo GANALYTICS_URL; ?>/assets/img/help-icon.png" class="tooltip"
                                     title="Number of visits during last 3 months time period. The change shows the difference compared to previous 3 month time period."/>
                            </div>
                            <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo $formatted_visits_3month; ?>
                            </span>
                            <span class="ga-header-value-change change-<?php echo $change_diff_visits; ?>">
                                <?php echo $change_icon_visits; ?> <?php echo $formatted_diff_visits; ?>
                            </span>
                            </div>
                        </td>
                        <td class="ga-header-table-cell">
                            <div class="ga-header-title">
                                Page Views
                                <img src="<?php echo GANALYTICS_URL; ?>/assets/img/help-icon.png" class="tooltip"
                                     title="Number of page views during last 3 months time period. The change shows the difference compared to previous 3 month time period."/>
                            </div>
                            <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo $formatted_pageviews_3month; ?>
                            </span>
                            <span class="ga-header-value-change change-<?php echo $change_diff_pageviews; ?>">
                                <?php echo $change_icon_pageviews ?> <?php echo $formatted_diff_pageviews; ?>
                            </span>
                            </div>
                        </td>
                        <td class="ga-header-table-cell">
                            <div class="ga-header-title">
                                Avg. Visit Duration
                                <img src="<?php echo GANALYTICS_URL; ?>/assets/img/help-icon.png" class="tooltip"
                                     title="Time of average visit duration during last 3 months time period. The change shows the difference compared to previous 3 month time period."/>
                            </div>
                            <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo $formatted_visit_duration_3months; ?>
                            </span>
                            <span class="ga-header-value-change change-<?php echo $change_diff_visitduration; ?>">
                                <?php echo $change_icon_visitduration; ?> <?php echo $formatted_diff_visit_duration; ?>
                            </span>
                            </div>
                        </td>
                        <td class="ga-header-table-cell">
                            <div class="ga-header-title">
                                Bounce Rate
                                <img src="<?php echo GANALYTICS_URL; ?>/assets/img/help-icon.png" class="tooltip"
                                     title="Percentage of bounce rate during last 3 months time period. The change shows the difference compared to previous 3 month time period."/>
                            </div>
                            <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo $formatted_bouncerate_3months; ?>
                            </span>
                            <span class="ga-header-value-change">
                               <?php echo $change_icon_bouncerate; ?> <?php echo $formatted_diff_bouncerate; ?>
                            </span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="ga-visits-graph-wrapper">
                <?php
                // Getting values needed for visits chart
                $results_visits_graph = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $date_3months_start,
                    $date_3months_end,
                    'ga:visits',
                    array(
                        'dimensions' => 'ga:date'
                    ));

                ?>
                <div id="visits_chart_div" style="width: 100%; height: 250px;"></div>
                <script type="text/javascript">
                    google.load("visualization", "1", {packages:["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Date', 'Visits']
                            <?php
                            foreach($results_visits_graph->getRows() as $key=>$row){
                                $year = substr($row[0],0,4);
                                $month = substr($row[0],4,2);
                                $day = substr($row[0],6,2);
                                $date = date('M j, Y',strtotime("$day.$month.$year"));
                                echo ",['$date',{$row[1]}]";
                            }
                            ?>
                        ]);

                        var options = {
                            chartArea : {width: "90%"},
                            fontSize : 11,
                            hAxis: {
                                showTextEvery: 30,
                                format:'MMM d, y'
                            },
                            legend : {position: 'none'},
                            title: 'Visits: Last 3 Months',
                            vAxis:{
                                gridlines:{
                                    count : 5,
                                    color : '#eee'
                                },
                                minValue : 0,
                                baselineColor: "#999"
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('visits_chart_div'));
                        chart.draw(data, options);
                    }
                </script>
            </div>
            <div class="top-list-wrapper">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <td style="width: 33%; vertical-align: top;">
                            <strong>Top 5 Pages</strong>
                            <?php
                            $results_top_pages = $analytics->data_ga->get(
                                'ga:'.$ganalytics_settings['ga_profile_id'],
                                $date_3months_start,
                                $date_3months_end,
                                'ga:pageviews',
                                array(
                                    'dimensions' => 'ga:pageTitle,ga:pagePath',
                                    'sort' => '-ga:pageviews',
                                    'max-results' => 5
                                ));
                            if(is_array($results_top_pages->getRows())){
                                echo '<ol>';
                                foreach($results_top_pages->getRows() as $top_page){
                                    echo '<li>';
                                    echo '<a href="'.$blog_domain_property->websiteUrl.$top_page[1].'" target="_blank">'.$top_page[0].'</a> ';
                                    echo '<span style="color: #aaa;"> - '.$top_page[2].' Views</span>';
                                    echo '</li>';
                                }
                                echo '</ol>';}
                            else{
                                echo '<div>No Pages Available</div>';
                            }
                            ?>
                        </td>
                        <td style="width: 34%; vertical-align: top;">
                            <strong>Top 5 Referral Sites</strong>
                            <?php
                            $results_top_referrals = $analytics->data_ga->get(
                                'ga:'.$ganalytics_settings['ga_profile_id'],
                                $date_3months_start,
                                $date_3months_end,
                                'ga:visits',
                                array(
                                    'dimensions' => 'ga:source',
                                    'filters' => 'ga:medium==referral',
                                    'sort' => '-ga:visits',
                                    'max-results' => 5
                                ));
                            if(is_array($results_top_referrals->getRows())){
                                echo '<ol>';
                                foreach($results_top_referrals->getRows() as $top_referral){
                                    echo '<li>';
                                    echo '<a href="http://'.$top_referral[0].'" target="_blank">'.$top_referral[0].'</a> ';
                                    echo '<span style="color: #aaa;"> - '.$top_referral[1].' Visits</span>';
                                    echo '</li>';
                                }
                                echo '</ol>';
                            }else{
                                echo '<div>No Referrals Available</div>';
                            }
                            ?>
                        </td>
                        <td style="width: 33%; vertical-align: top;">
                            <strong>Top 5 Keywords</strong>
                            <?php
                            $results_top_keywords = $analytics->data_ga->get(
                                'ga:'.$ganalytics_settings['ga_profile_id'],
                                $date_3months_start,
                                $date_3months_end,
                                'ga:visits',
                                array(
                                    'dimensions' => 'ga:keyword',
                                    'sort' => '-ga:visits',
                                    'max-results' => 5
                                ));
                            if(is_array($results_top_keywords->getRows())){
                                echo '<ol>';
                                foreach($results_top_keywords->getRows() as $top_keyword){
                                    echo '<li>';
                                    echo $top_keyword[0];
                                    echo '<span style="color: #aaa;"> - '.$top_keyword[1].' Visits</span>';
                                    echo '</li>';
                                }
                                echo '</ol>';
                            }
                            else{
                                echo '<div>No Keywords Available</div>';
                            }
                            ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php
        }
        catch(Exception $e){
            ?>
            <div class="updated below-h2">
                <p>
                    Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $redirect_url; ?>">settings</a> page to check connection status.
                </p>
            </div>
        <?php
        }
    }else{
        ?>
        <div class="updated below-h2">
            <p>
                Please update <a href="<?php echo $redirect_url; ?>">gAnalytics Settings</a>. Once you are connected, you can view your important
                Google Analytics statistics here.
            </p>
        </div>

    <?php
    }
    ?>
    </div>
<?php
}
catch(Exception $e){
    ?>
    <div class="updated below-h2" style="margin-left: 0px;">
        <p>
            Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $redirect_url; ?>">settings</a> page to check connection status.
        </p>
    </div>
<?php
}
?>