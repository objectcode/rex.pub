<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.5
 * @copyright (c) 2013 cube3x
 *
 * This file shows detailed analytics report of a page
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
set_time_limit(0);

//plugin page urls
$report_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_report';
$settings_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';

// Check for single or all posts
$show_all_posts = true;
$current_post_id = 0;
if(isset($_GET['post_id']) && $_GET['post_id'] > 0){
    $show_all_posts = false;
    $current_post_id = $_GET['post_id'];
}

$duration = "1month";
if(isset($_GET['duration'])){
    $duration = $_GET['duration'];
}

// Check search term
$search_term = '';
if(isset($_GET['s'])){
    $search_term = $_GET['s'];
}

// Check posts or pages
$post_type = "post";
if(isset($_GET['type'])){
    $post_type = $_GET['type'];
}

// Get page number
$current_page_number = 1;
if(isset($_GET['paged'])){
    $current_page_number = $_GET['paged'];
}

// Query to fetch posts or pages
if($show_all_posts){
    $args = array(
        'post_type' => $post_type,
        's' => $search_term,
        'post_status' => array( 'publish', 'draft', 'pending','private' ),
        'posts_per_page' => 10,
        'paged' => $current_page_number
    );
    $query = new WP_Query($args);
    $title_array = array();
    $permalink_array = array();
    while ($query->have_posts()) : $query->the_post();
        array_push($title_array, get_the_title());
        array_push($permalink_array, get_permalink());
    endwhile;
    wp_reset_postdata();

    /**** setting pagination ****/

// Getting total number of published posts
    $total_number_of_posts = $query->found_posts;

//Finding number of pages. 20 items in one page.
    $number_of_pages =  $query->max_num_pages;

//Next and prev page numbers.
    $next_page = 0;
    if($current_page_number < $number_of_pages){
        $next_page = $current_page_number+1;
    }
    $prev_page = 0;
    if($current_page_number > 1){
        $prev_page = $current_page_number-1;
    }

//Preparing navigation urls
    $first_page_url = '';
    if($current_page_number != 1){
        $first_page_url = $report_url."&type=$post_type&s=$search_term&duration=$duration";
    }

    $last_page_url = '';
    if($current_page_number != $number_of_pages){
        $last_page_url =  $report_url."&type=$post_type&s=$search_term&duration=$duration&paged=$number_of_pages";
    }

    $prev_page_url = '';
    if($prev_page != 0){
        if($prev_page == 1){
            $prev_page_url = $first_page_url;
        }else{
            $prev_page_url = $report_url."&type=$post_type&s=$search_term&duration=$duration&paged=$prev_page";
        }
    }

    $next_page_url = '';
    if($next_page != 0){
        $next_page_url = $report_url."&type=$post_type&s=$search_term&duration=$duration&paged=$next_page";
    }
    /******** End of pagination **********/
}

/*********** Getting dates for duration *************/
$timestamp_yesterday = time() - 86400;

$timestamp_1week_start  =   $timestamp_yesterday    - 604800;

$timestamp_1month_start =   $timestamp_yesterday    - 2592000;

$timestamp_3month_start =   $timestamp_yesterday    - 7776000;

$timestamp_6month_start =   $timestamp_yesterday    - 15552000;

$timestamp_1year_start  =   $timestamp_yesterday    - 31536000;

$date_yesterday = date('Y-m-d',$timestamp_yesterday);

$date_1week_start  =   date('Y-m-d',$timestamp_1week_start);

$date_1month_start =   date('Y-m-d',$timestamp_1month_start);

$date_3month_start =   date('Y-m-d',$timestamp_3month_start);

$date_6month_start =   date('Y-m-d',$timestamp_6month_start);

$date_1year_start  =   date('Y-m-d',$timestamp_1year_start);
/*********** End of Getting Dates **************/

/************** Selecting start and end date ***************/
$report_date_start  = '';
$report_date_end    = $date_yesterday;
switch($duration){
    case "1week":
        $report_date_start  = $date_1week_start;
        break;
    case "1month":
        $report_date_start  = $date_1month_start;
        break;
    case "3months":
        $report_date_start  = $date_3month_start;
        break;
    case "6months":
        $report_date_start  = $date_6month_start;
        break;
    case "1year":
        $report_date_start  = $date_1year_start;
        break;
    default:
        break;
}
/************ End of selecting start and end date **********/

require_once GANALYTICS_PATH.'api.php';
if($access_token){
    $client->setAccessToken($access_token);
}
?>
<div class='wrap ganalytics report'>
<div class="icon32 ga-header-app-icon"></div>
<h2>gAnalytics Report
    <?php
    if($access_token){
        ?>
        <select class="ga-report-time-period">
            <option value="1week" <?php if($duration == "1week") echo 'selected'; ?>>1 Week (7 Days)</option>
            <option value="1month" <?php if($duration == "1month") echo 'selected'; ?>>1 Month (30 Days)</option>
            <option value="3months" <?php if($duration == "3months") echo 'selected'; ?>>3 Months (90 Days)</option>
            <option value="6months" <?php if($duration == "6months") echo 'selected'; ?>>6 Months (180 Days)</option>
            <option value="1year" <?php if($duration == "1year") echo 'selected'; ?>>1 Year (365 Days)</option>
        </select>
        <span class="ga-report-time-label">
                <?php echo date('M j, Y',strtotime($report_date_start));?> - <?php echo date('M j, Y',strtotime($report_date_end));?>
            </span>
    <?php
    }
    ?>
    <script type="text/javascript">
        jQuery(function ($) {
            $(document).ready(function(){
                $(".ga-report-time-period").change(function(){
                    var duration = $(this).val();
                    <?php
                    if($show_all_posts){
                    ?>
                    window.location.href = "<?php echo $report_url; ?>&duration="+duration+"<?php echo "&type=$post_type&s=$search_term&paged=$current_page_number" ?>";
                    <?php
                    }else{
                    ?>
                    window.location.href = "<?php echo $report_url; ?>&duration="+duration+"<?php echo "&post_id=$current_post_id" ?>";
                    <?php
                    }
                    ?>
                });
            });
        });
    </script>
</h2>
<?php
try{
    if($access_token && $show_all_posts){
        ?>
        <div class="ga-report-block-wrapper">
            <form>
                <input type="hidden" name="page" value="ga_report"/>
                <input type="hidden" name="duration" value="<?php echo $duration; ?>"/>
                <div class="tablenav top">

                    <!-- Post type selection -->
                    <div class="alignleft actions">
                        <select name="type" id="report-post-type">
                            <option value="post" <?php if($post_type == 'post') echo 'selected'; ?>>Posts</option>
                            <option value="page" <?php if($post_type == 'page') echo 'selected'; ?>>Pages</option>
                        </select>
                        <input type="submit" name="" id="doaction" class="button action" value="Apply">
                    </div>
                    <script type="text/javascript">
                        jQuery(function ($) {
                            $(document).ready(function(){
                                $("#report-post-type").change(function(){
                                    var post_type = $(this).val();
                                    $('input[name="paged"]').val(1);
                                });
                            });
                        });
                    </script>

                    <!-- Search box -->
                    <div class="alignleft actions">
                        <p class="search-box">
                            <label class="screen-reader-text" for="post-search-input">Search Pages:</label>
                            <input type="search" id="post-search-input" name="s" value="<?php echo $search_term; ?>">
                            <input type="submit" name="" id="search-submit" class="button" value="Search Pages">
                        </p>
                    </div>

                    <!-- Pagination -->
                    <div class="tablenav-pages">
                        <span class="displaying-num"><?php echo $total_number_of_posts; ?> items</span>
<span class="pagination-links"><a class="first-page <?php if(!$first_page_url) echo 'disabled'; ?>" title="Go to the first page" href="<?php echo $first_page_url; ?>">&laquo;</a>
<a class="prev-page <?php if(!$prev_page_url) echo 'disabled'; ?>" title="Go to the previous page" href="<?php echo $prev_page_url; ?>">&lsaquo;</a>
<span class="paging-input"><input class="current-page" title="Current page" type="text" name="paged" value="<?php echo $current_page_number; ?>" size="2"> of <span class="total-pages"><?php echo $number_of_pages; ?></span></span>
<a class="next-page <?php if(!$next_page_url) echo 'disabled'; ?>" title="Go to the next page" href="<?php echo $next_page_url; ?>">&rsaquo;</a>
<a class="last-page <?php if(!$last_page_url) echo 'disabled'; ?>" title="Go to the last page" href="<?php echo $last_page_url; ?>">&raquo;</a></span>
                    </div>
                </div>
                <?php
                //prepare filter string
                $url_path_array = array();
                $filter_path_array = array();
                if(is_array($permalink_array)){
                    foreach($permalink_array as $link){
                        $parsed_url = parse_url($link);
                        $url_path = '';
                        if(isset($parsed_url["path"]))
                            $url_path .= $parsed_url["path"];
                        else
                            $url_path .= '/';
                        if(isset($parsed_url["query"]))
                            $url_path .=  '?'.$parsed_url["query"];
                        array_push($url_path_array, $url_path);
                        $filter_path = 'ga:pagepath=='.$url_path;
                        array_push($filter_path_array, $filter_path);
                    }
                }
                $filter_string = implode(',', $filter_path_array);

                //request analytics data
                $report_data = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $report_date_start,
                    $report_date_end,
                    'ga:pageviews,ga:avgTimeOnPage,ga:entranceBounceRate,ga:organicSearches',
                    array(
                        'dimensions' => 'ga:pagePath',
                        'max-results' => 20,
                        'filters' => $filter_string
                    ));
                ?>

                <table class="widefat ga-report-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Page Views</th>
                        <th>Avg. Time On Page</th>
                        <th>Bounce Rate</th>
                        <th>Search Traffic</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Page Views</th>
                        <th>Avg. Time On Page</th>
                        <th>Bounce Rate</th>
                        <th>Search Traffic</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    if(is_array($report_data->getRows())){
                        foreach($url_path_array as $url_key => $path){
                            foreach($report_data->getRows() as $key=>$report){
                                if($report[0] == $path){
                                    ?>
                                    <tr>
                                        <td><?php echo '<a href="'.$permalink_array[$url_key].'" target="_blank">'.$title_array[$url_key].'</a>'; ?></td>
                                        <td><?php echo number_format((int)$report[1]); ?></td>
                                        <td><?php echo gmdate("H:i:s",(int)$report[2]); ?></td>
                                        <td><?php echo round((float)$report[3],2); ?>%</td>
                                        <td><?php echo number_format((int)$report[4]); ?></td>
                                    </tr>
                                    <?php
                                    break;
                                }
                            }
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </form>
        </div>
    <?php
    }
    else if($current_post_id > 0){
        ?>

        <?php
        // Output Variables
        $page_visits = 0;
        $page_views = 0;
        $organic_visits = 0;
        $referral_visits = 0;

        $post_object = get_post($current_post_id);
        $post_title = $post_object->post_title;
        $link = get_permalink( $current_post_id );
        $url_path = '';
        $parsed_url = parse_url($link);
        if(isset($parsed_url["path"]))
            $url_path .= $parsed_url["path"];
        else
            $url_path .= '/';
        if(isset($parsed_url["query"]))
            $url_path .=  '?'.$parsed_url["query"];
        $filter_string = 'ga:pagepath=='.$url_path;

        //request analytics data
        $report_data = $analytics->data_ga->get(
            'ga:'.$ganalytics_settings['ga_profile_id'],
            $report_date_start,
            $report_date_end,
            'ga:visits,ga:pageviews',
            array(
                'filters' => $filter_string
            ));
        if(is_array($report_data->getRows())){
            foreach($report_data->getRows() as $key=>$report){
                $page_visits = $report[0];
                $page_views = $report[1];
            }
        }
        $report_data = $analytics->data_ga->get(
            'ga:'.$ganalytics_settings['ga_profile_id'],
            $report_date_start,
            $report_date_end,
            'ga:visits',
            array(
                'dimensions' => 'ga:medium',
                'filters' => $filter_string
            ));

        if(is_array($report_data->getRows())){
            foreach($report_data->getRows() as $key=>$report){
                if($report[0]=="organic")
                    $organic_visits = $report[1];
                else if($report[0]=="referral")
                    $referral_visits = $report[1];
            }
        }
        ?>
        <div class="ga-header-block-wrapper">
            <table class="form-table single-post-meta">
                <tbody>
                <tr class="post-title">
                    <td class="col1">&nbsp;&nbsp;&nbsp;Title :</td>
                    <td class="col2"><?php echo $post_title; ?></td>
                </tr>
                <tr class="post-url">
                    <td class="col1">&nbsp;&nbsp;&nbsp;URL :</td>
                    <td class="col2"><?php echo $link; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ga-header-block-wrapper">
            <table class="form-table">
                <tbody>
                <tr>
                    <td class="ga-header-table-cell">
                        <div class="ga-header-title">
                            Page Visits
                        </div>
                        <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo number_format((int)$page_visits); ?>
                            </span>
                    </td>
                    <td class="ga-header-table-cell">
                        <div class="ga-header-title">
                            Page Views
                        </div>
                        <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo number_format((int)$page_views); ?>
                            </span>
                        </div>
                    </td>
                    <td class="ga-header-table-cell">
                        <div class="ga-header-title">
                            Organic Visits
                        </div>
                        <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo number_format((int)$organic_visits); ?>
                            </span>
                        </div>
                    </td>
                    <td class="ga-header-table-cell">
                        <div class="ga-header-title">
                            Referral Visits
                        </div>
                        <div class="ga-header-value">
                            <span class="ga-header-value-current">
                                <?php echo number_format((int)$referral_visits); ?>
                            </span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ga-visits-graph-wrapper">
            <?php
            // Getting values needed for visits chart
            $results_visits_graph = $analytics->data_ga->get(
                'ga:'.$ganalytics_settings['ga_profile_id'],
                $report_date_start,
                $report_date_end,
                'ga:visits',
                array(
                    'dimensions' => 'ga:date',
                    'filters' => $filter_string
                ));

            ?>
            <div id="visits_chart_div" style="width: 100%; height: 250px;"></div>
            <script type="text/javascript">
                google.load("visualization", "1", {packages:["corechart"]});
                google.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Date', 'Visits']
                        <?php
                        foreach($results_visits_graph->getRows() as $key=>$row){
                            $year = substr($row[0],0,4);
                            $month = substr($row[0],4,2);
                            $day = substr($row[0],6,2);
                            $date = date('M j, Y',strtotime("$day.$month.$year"));
                            echo ",['$date',{$row[1]}]";
                        }
                        ?>
                    ]);

                    var options = {
                        chartArea : {width: "90%"},
                        fontSize : 11,
                        hAxis: {
                            showTextEvery: 30,
                            format:'MMM d, y'
                        },
                        legend : {position: 'none'},
                        title: 'Visits',
                        vAxis:{
                            gridlines:{
                                count : 5,
                                color : '#eee'
                            },
                            minValue : 0,
                            baselineColor: "#999"
                        }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('visits_chart_div'));
                    chart.draw(data, options);
                }
            </script>
        </div>
        <div class="ga-visits-graph-wrapper">
            <?php
            // Getting values needed for visits chart
            $results_visits_graph = $analytics->data_ga->get(
                'ga:'.$ganalytics_settings['ga_profile_id'],
                $report_date_start,
                $report_date_end,
                'ga:organicSearches',
                array(
                    'dimensions' => 'ga:date',
                    'filters' => $filter_string
                ));

            ?>
            <div id="page_views_chart_div" style="width: 100%; height: 250px;"></div>
            <script type="text/javascript">
                google.load("visualization", "1", {packages:["corechart"]});
                google.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Date', 'Search Traffic']
                        <?php
                        foreach($results_visits_graph->getRows() as $key=>$row){
                            $year = substr($row[0],0,4);
                            $month = substr($row[0],4,2);
                            $day = substr($row[0],6,2);
                            $date = date('M j, Y',strtotime("$day.$month.$year"));
                            echo ",['$date',{$row[1]}]";
                        }
                        ?>
                    ]);

                    var options = {
                        chartArea : {width: "90%"},
                        fontSize : 11,
                        hAxis: {
                            showTextEvery: 30,
                            format:'MMM d, y'
                        },
                        legend : {position: 'none'},
                        title: 'Organic Search Visits',
                        vAxis:{
                            gridlines:{
                                count : 5,
                                color : '#eee'
                            },
                            minValue : 0,
                            baselineColor: "#999"
                        }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('page_views_chart_div'));
                    chart.draw(data, options);
                }
            </script>
        </div>
    <?php
    }
    else{
        ?>
        <div class="updated below-h2">
            <p>
                Please update <a href="<?php echo $settings_url; ?>">gAnalytics Settings</a>. Once you are connected, you can view your important
                Google Analytics statistics here.
            </p>
        </div>
    <?php
    }
}catch(Exception $e){
    ?>
    <div class="updated below-h2" style="margin-left: 0px;">
        <p>
            Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $settings_url; ?>">settings</a> page to check connection status.
        </p>
    </div>
<?php
}
?>
</div>