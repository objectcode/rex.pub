<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.5
 * @copyright (c) 2013 cube3x
 *
 * This file shows Monthly report of the site
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
set_time_limit(0);

//plugin page urls
$settings_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_settings';

require_once GANALYTICS_PATH.'api.php';

if($access_token){
    $client->setAccessToken($access_token);
}
/******* End of setting google analytics api sdk ********/

/******** Reading Google Analytics setup date *********/
$created_time = $ganalytics_settings['ga_active_web_property']->created;
$created_time_display = date('d M, Y',strtotime($created_time));
$created_time_api = date('Y-m-d',strtotime($created_time));
/****** End of reading Google Analytics Setup date *******/
?>
<div class='wrap ganalytics monthly-report'>
    <div class="icon32 ga-header-app-icon"></div>
    <h2>gAnalytics Monthly Report
        <?php
        if($access_token){
            ?>
            <span class="ga-monthly-report-start-date">
                Google Analytics Setup Date - <strong><?php echo $created_time_display; ?></strong>
            </span>
            <?php
        }
        ?>
    </h2>
    <?php
    try{
        if($access_token){
            ?>
            <div class="ga-monthly-report-block-wrapper">
                <?php
                //request analytics data
                $monthly_report_data = $analytics->data_ga->get(
                    'ga:'.$ganalytics_settings['ga_profile_id'],
                    $created_time_api,
                    date('Y-m-d'),
                    'ga:visits,ga:pageviews,ga:organicSearches,ga:avgTimeOnSite,ga:entranceBounceRate',
                    array(
                        'dimensions' => 'ga:year,ga:month'
                    ));
                ?>
                <table class="widefat ga-monthly-report-table">
                    <thead>
                    <tr>
                        <th>Month</th>
                        <th>Visits</th>
                        <th>Page Views</th>
                        <th>Organic Search Visits</th>
                        <th>Average Time On Site</th>
                        <th>Bounce Rate</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Month</th>
                        <th>Visits</th>
                        <th>Page Views</th>
                        <th>Organic Search Visits</th>
                        <th>Average Time On Site</th>
                        <th>Bounce Rate</th>
                    </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        if(is_array($monthly_report_data->getRows())){
                            foreach($monthly_report_data->getRows() as $key=>$report){
                                ?>
                            <tr>
                                <td><?php echo $report[0].' - '.date('F', mktime(0, 0, 0, $report[1], 1, 2000));; ?></td>
                                <td><?php echo number_format((int)$report[2]); ?></td>
                                <td><?php echo number_format((int)$report[3]); ?></td>
                                <td><?php echo number_format((int)$report[4]); ?></td>
                                <td><?php echo gmdate("H:i:s",(int)$report[5]); ?></td>
                                <td><?php echo round((float)$report[6],2); ?>%</td>
                            </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <pre>
            </div>
            <?php
        }
        else{
            ?>
            <div class="updated below-h2">
                <p>
                    Please update <a href="<?php echo $settings_url; ?>">gAnalytics Settings</a>. Once you are connected, you can view your important
                    Google Analytics statistics here.
                </p>
            </div>
            <?php
        }
    }catch(Exception $e){
        ?>
        <div class="updated below-h2" style="margin-left: 0px;">
            <p>
                Plugin could not connect to Google Analytics. Please visit <a href="<?php echo $settings_url; ?>">settings</a> page to check connection status.
            </p>
        </div>
        <?php
    }
    ?>
</div>