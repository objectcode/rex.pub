<?php
/*
* Plugin Name: gAnalytics
* Version: 2.8
* Plugin URI: http://cube3x.com/ganalytics-wordpress-plugin/
* Description: Google Analytics Tuned For WordPress
* Author: CUBE3X
* Author URI: http://cube3x.com
*/
?>
<?php
include 'config.php';
include 'inc/functions.php';
include 'widgets/widgets.php';
include 'shortcodes/shortcodes.php';

// Plugin activation
function fn_ganalyitcs_activate() {
    $ganalytics_settings = array(
        'use_own_api' => false,
        'google_auth_code' => '',
        'google_api_key' => 'AIzaSyCCrO5rIlk2DKjj7ZGulhlmHNlab6gJi0k',
        'google_client_id' => '987871093172-f7v9tra8u3mevo38o5lhutk0vo7gg1n8.apps.googleusercontent.com',
        'google_client_secret' => 'Qw-S1iXB9tLXUqPoWlnYyJU8',
        'google_access_token' => '',
        'ga_active_web_property' => '',
        'ga_profile_id' => '',
        'version' => GANALYTICS_VERSION
    );
    update_option( 'ganalytics_settings', $ganalytics_settings );

    $ganalytics_options = array(
        'request_interval' => 12,
        'next_request_time' => time()+43200,
        'widget_site_visitors_total_visitors' => 0,
        'widget_top_countries_response' => array(),
        'widget_top_countries_total_visitors' => 0,
        'widget_trending_posts_day' => array(),
        'widget_trending_posts_week' => array(),
        'widget_trending_posts_month' => array(),
        'widget_trending_posts_alltime' => array()
    );
    update_option( 'ganalytics_options', $ganalytics_options );

    //cron job
    wp_schedule_event( time(), 'hourly', 'ga_hourly_event_hook' );
}
register_activation_hook( __FILE__, 'fn_ganalyitcs_activate' );

// Plugin deactivation
function fn_ganalyitcs_deactivate() {
    /*Deleting all details is disabled for now. Because the admin have to re-enter
    all details again after reactivating. */
    //delete_option( 'ganalytics_settings' );
    wp_clear_scheduled_hook( 'ga_hourly_event_hook' );
}
register_deactivation_hook( __FILE__, 'fn_ganalyitcs_deactivate' );
?>