<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.8
 * @copyright (c) 2013 cube3x
 *
 * Defines variables required by the plugin
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
define("GANALYTICS_PATH", plugin_dir_path( __FILE__ ));
define("GANALYTICS_URL", plugins_url('',__FILE__));
define("GANALYTICS_VERSION", "2.8");
?>