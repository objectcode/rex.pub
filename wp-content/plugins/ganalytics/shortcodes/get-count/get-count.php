<?php
	
	add_shortcode('show_visitors_count','show_total_visitors');

    function show_total_visitors($attr){
    	require_once GANALYTICS_PATH.'api.php';

        $ganalytics_options = get_option('ganalytics_options');
        $ganalytics_settings = get_option('ganalytics_settings');
        $total_site_visitors = $ganalytics_options['widget_site_visitors_total_visitors'];
        $display_total_site_visitors = number_format($total_site_visitors);
        $total_views = 0;

        $date_all_start = '2005-01-01'; //Apparently this is the date when GA started, they don't have any data b/f the date.
        $timestamp_all_end = time(); //Till date :: we are going for the total views :)
        $date_all_end = date('Y-m-d',$timestamp_all_end);
        
        fn_ga_hourly(false,'widget-site-visitors');

        $options = shortcode_atts( array(
		        'get' => 'all',
		    	), $attr );

        if($options['get'] == 'all' ) {
        	$total_views = $display_total_site_visitors;

        } elseif($options['get'] == 'single') {
        	$results_single_page_count = $analytics->data_ga->get(
                                'ga:'.$ganalytics_settings['ga_profile_id'],
                                $date_all_start,
                                $date_all_end,
                                'ga:pageviews',
                                array(
                                    'dimensions' => 'ga:pageTitle,ga:pagePath',
                                    'metrics' => 'ga:pageviews',
                                    'sort' => '-ga:pageviews',
                                    'filters'	=>	'ga:pagePath==' . $_SERVER['REQUEST_URI'],
                                    'max-results' => 1,
                                ));
	        if(is_array($results_single_page_count->getRows())){
	            foreach($results_single_page_count->getRows() as $single_page){
	            	$total_views = $single_page[2];
	            }
	        }
        }

        return $total_views;
     }