<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.7
 * @copyright (c) 2013 cube3x
 *
 * This file defines the functions needed by gAnalytics plugin.
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
// Add gAnalytics Menu Items
add_action( 'admin_menu', 'fn_ganalytics_menu_items' );

function fn_ganalytics_menu_items(){
    add_menu_page( 'gAnalytics', 'gAnalytics', 'manage_options', 'ga_dashboard', 'fn_ga_dashboard_page', GANALYTICS_URL.'/assets/img/ganalytics-icon.png');
    add_submenu_page( 'ga_dashboard', 'gAnalytics Compare', 'gA Compare', 'manage_options', 'ga_compare', 'fn_ga_compare_page' );
    add_submenu_page( 'ga_dashboard', 'gAnalytics Report', 'gA Report', 'manage_options', 'ga_report', 'fn_ga_report_page' );
    add_submenu_page( 'ga_dashboard', 'gAnalytics Monthly Report', 'Monthly Report', 'manage_options', 'ga_monthly_report', 'fn_ga_monthly_report_page' );
    add_submenu_page( 'ga_dashboard', 'gAnalytics Mobile Devices', 'Mobile Devices', 'manage_options', 'ga_mobile_devices', 'fn_ga_mobile_devices_page' );
    add_submenu_page( 'ga_dashboard', 'gAnalytics Settings', 'Settings', 'manage_options', 'ga_settings', 'fn_ga_settings_page' );
    add_submenu_page( 'ga_dashboard', 'gAnalytics Documentation', 'Documentation', 'manage_options', 'ga_docs', 'fn_ga_docs_page' );

    //Rename first gAnalytics sub menu
    global $submenu;
    if ( isset( $submenu['ga_dashboard'] ) )
        $submenu['ga_dashboard'][0][0] = 'Dashboard';

}
function fn_ga_dashboard_page(){
    if ( isset( $_GET['page'] ) && 'ga_dashboard' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/dashboard.php');
}
function fn_ga_settings_page(){
    if ( isset( $_GET['page'] ) && 'ga_settings' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/settings.php');
}
function fn_ga_compare_page(){
    if ( isset( $_GET['page'] ) && 'ga_compare' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/compare.php');
}
function fn_ga_report_page(){
    if ( isset( $_GET['page'] ) && 'ga_report' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/report.php');
}
function fn_ga_monthly_report_page(){
    if ( isset( $_GET['page'] ) && 'ga_monthly_report' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/monthly-report.php');
}
function fn_ga_mobile_devices_page(){
    if ( isset( $_GET['page'] ) && 'ga_mobile_devices' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/mobile-devices.php');
}
function fn_ga_docs_page(){
    if ( isset( $_GET['page'] ) && 'ga_docs' == $_GET['page'] )
        include(GANALYTICS_PATH.'pages/documentation.php');
}

// Admin css insertion
add_action( 'admin_enqueue_scripts', 'fn_ga_admin_reference' );
function fn_ga_admin_reference(){
    wp_enqueue_style( 'ganalytics-admin-style', GANALYTICS_URL.'/assets/css/ganalytics-admin.css', array(),GANALYTICS_VERSION);
    wp_enqueue_style( 'ganalytics-tooltip-style', GANALYTICS_URL.'/assets/inc/tooltipster/tooltipster.css');
    wp_enqueue_style( 'ganalytics-tooltip-style-light', GANALYTICS_URL.'/assets/inc/tooltipster/themes/tooltipster-light.css');

    wp_enqueue_script('google-visualization-chart', 'https://www.google.com/jsapi');
    wp_enqueue_script('ganalytics-tooltip-script', GANALYTICS_URL.'/assets/inc/tooltipster/jquery.tooltipster.min.js', array('jquery'));
    wp_enqueue_script('ganalytics-admin-script', GANALYTICS_URL.'/assets/js/ganalytics-admin.js', array('ganalytics-tooltip-script'));
}

//function to return country code
function fn_ga_get_country_code($country_name){
    $countrycodes = array(
        "Afghanistan"=>"AF",
        "\xc3\x85land Islands"=>"AX",
        "Albania"=>"AL",
        "Algeria"=>"DZ",
        "American Samoa"=>"AS",
        "Andorra"=>"AD",
        "Angola"=>"AO",
        "Anguilla"=>"AI",
        "Antarctica"=>"AQ",
        "Antigua and Barbuda"=>"AG",
        "Argentina"=>"AR",
        "Armenia"=>"AM",
        "Aruba"=>"AW",
        "Australia"=>"AU",
        "Austria"=>"AT",
        "Azerbaijan"=>"AZ",
        "Bahamas"=>"BS",
        "Bahrain"=>"BH",
        "Bangladesh"=>"BD",
        "Barbados"=>"BB",
        "Belarus"=>"BY",
        "Belgium"=>"BE",
        "Belize"=>"BZ",
        "Benin"=>"BJ",
        "Bermuda"=>"BM",
        "Bhutan"=>"BT",
        "Bolivia"=>"BO",
        "Bonaire, Sint Eustatius and Saba"=>"BQ",
        "Bosnia and Herzegovina"=>"BA",
        "Botswana"=>"BW",
        "Bouvet Island"=>"BV",
        "Brazil"=>"BR",
        "British Indian Ocean Territory"=>"IO",
        "Brunei"=>"BN",
        "Bulgaria"=>"BG",
        "Burkina Faso"=>"BF",
        "Burundi"=>"BI",
        "Cambodia"=>"KH",
        "Cameroon"=>"CM",
        "Canada"=>"CA",
        "Cape Verde"=>"CV",
        "Cayman Islands"=>"KY",
        "Central African Republic"=>"CF",
        "Chad"=>"TD",
        "Chile"=>"CL",
        "China"=>"CN",
        "Christmas Island"=>"CX",
        "Cocos (Keeling) Islands"=>"CC",
        "Colombia"=>"CO",
        "Comoros"=>"KM",
        "Congo"=>"CG",
        "Congo, The Democratic Republic of the"=>"CD",
        "Cook Islands"=>"CK",
        "Costa Rica"=>"CR",
        "C\xc3\xb4te d'Ivoire"=>"CI",
        "Croatia"=>"HR",
        "Cuba"=>"CU",
        "Cura\xc3\xa7ao"=>"CW",
        "Cyprus"=>"CY",
        "Czech Republic"=>"CZ",
        "Denmark"=>"DK",
        "Djibouti"=>"DJ",
        "Dominica"=>"DM",
        "Dominican Republic"=>"DO",
        "Ecuador"=>"EC",
        "Egypt"=>"EG",
        "El Salvador"=>"SV",
        "Equatorial Guinea"=>"GQ",
        "Eritrea"=>"ER",
        "Estonia"=>"EE",
        "Ethiopia"=>"ET",
        "Falkland Islands (Malvinas)"=>"FK",
        "Faroe Islands"=>"FO",
        "Fiji"=>"FJ",
        "Finland"=>"FI",
        "France"=>"FR",
        "French Guiana"=>"GF",
        "French Polynesia"=>"PF",
        "French Southern Territories"=>"TF",
        "Gabon"=>"GA",
        "Gambia"=>"GM",
        "Georgia"=>"GE",
        "Germany"=>"DE",
        "Ghana"=>"GH",
        "Gibraltar"=>"GI",
        "Greece"=>"GR",
        "Greenland"=>"GL",
        "Grenada"=>"GD",
        "Guadeloupe"=>"GP",
        "Guam"=>"GU",
        "Guatemala"=>"GT",
        "Guernsey"=>"GG",
        "Guinea"=>"GN",
        "Guinea-Bissau"=>"GW",
        "Guyana"=>"GY",
        "Haiti"=>"HT",
        "Heard Island and McDonald Islands"=>"HM",
        "Holy See (Vatican City State)"=>"VA",
        "Honduras"=>"HN",
        "Hong Kong"=>"HK",
        "Hungary"=>"HU",
        "Iceland"=>"IS",
        "India"=>"IN",
        "Indonesia"=>"ID",
        "Iran"=>"IR",
        "Iraq"=>"IQ",
        "Ireland"=>"IE",
        "Israel"=>"IL",
        "Italy"=>"IT",
        "Jamaica"=>"JM",
        "Japan"=>"JP",
        "Jersey"=>"JE",
        "Jordan"=>"JO",
        "Kazakhstan"=>"KZ",
        "Kenya"=>"KE",
        "Kiribati"=>"KI",
        "Korea, Democratic People's Republic of"=>"KP",
        "Korea, Republic of"=>"KR",
        "Kuwait"=>"KW",
        "Kyrgyzstan"=>"KG",
        "Lao People's Democratic Republic"=>"LA",
        "Latvia"=>"LV",
        "Lebanon"=>"LB",
        "Lesotho"=>"LS",
        "Liberia"=>"LR",
        "Libya"=>"LY",
        "Liechtenstein"=>"LI",
        "Lithuania"=>"LT",
        "Luxembourg"=>"LU",
        "Macau"=>"MO",
        "Macedonia [FYROM]"=>"MK",
        "Madagascar"=>"MG",
        "Malawi"=>"MW",
        "Malaysia"=>"MY",
        "Maldives"=>"MV",
        "Mali"=>"ML",
        "Malta"=>"MT",
        "Marshall Islands"=>"MH",
        "Martinique"=>"MQ",
        "Mauritania"=>"MR",
        "Mauritius"=>"MU",
        "Mayotte"=>"YT",
        "Mexico"=>"MX",
        "Micronesia, Federated States of"=>"FM",
        "Moldova"=>"MD",
        "Monaco"=>"MC",
        "Mongolia"=>"MN",
        "Montenegro"=>"ME",
        "Montserrat"=>"MS",
        "Morocco"=>"MA",
        "Mozambique"=>"MZ",
        "Myanmar [Burma]"=>"MM",
        "Namibia"=>"NA",
        "Nauru"=>"NR",
        "Nepal"=>"NP",
        "Netherlands"=>"NL",
        "New Caledonia"=>"NC",
        "New Zealand"=>"NZ",
        "Nicaragua"=>"NI",
        "Niger"=>"NE",
        "Nigeria"=>"NG",
        "Niue"=>"NU",
        "Norfolk Island"=>"NF",
        "Northern Mariana Islands"=>"MP",
        "Norway"=>"NO",
        "Oman"=>"OM",
        "Pakistan"=>"PK",
        "Palau"=>"PW",
        "Palestine"=>"PS",
        "Panama"=>"PA",
        "Papua New Guinea"=>"PG",
        "Paraguay"=>"PY",
        "Peru"=>"PE",
        "Philippines"=>"PH",
        "Pitcairn"=>"PN",
        "Poland"=>"PL",
        "Portugal"=>"PT",
        "Puerto Rico"=>"PR",
        "Qatar"=>"QA",
        "R\xc3\xa9union"=>"RE",
        "Romania"=>"RO",
        "Russia" => "RU",
        "Russian Federation"=>"RU",
        "Rwanda"=>"RW",
        "Saint Barth\xc3\xa9lemy"=>"BL",
        "Saint Helena, Ascension and Tristan Da Cunha"=>"SH",
        "Saint Kitts and Nevis"=>"KN",
        "Saint Lucia"=>"LC",
        "Saint Martin (French part)"=>"MF",
        "Saint Pierre and Miquelon"=>"PM",
        "Saint Vincent and the Grenadines"=>"VC",
        "Samoa"=>"WS",
        "San Marino"=>"SM",
        "Sao Tome and Principe"=>"ST",
        "Saudi Arabia"=>"SA",
        "Senegal"=>"SN",
        "Serbia"=>"RS",
        "Seychelles"=>"SC",
        "Sierra Leone"=>"SL",
        "Singapore"=>"SG",
        "Sint Maarten (Dutch part)"=>"SX",
        "Slovakia"=>"SK",
        "Slovenia"=>"SI",
        "Solomon Islands"=>"SB",
        "Somalia"=>"SO",
        "South Africa"=>"ZA",
        "South Georgia and the South Sandwich Islands"=>"GS",
        "South Korea"=>"KR",
        "South Sudan"=>"SS",
        "Spain"=>"ES",
        "Sri Lanka"=>"LK",
        "Sudan"=>"SD",
        "Suriname"=>"SR",
        "Svalbard and Jan Mayen"=>"SJ",
        "Swaziland"=>"SZ",
        "Sweden"=>"SE",
        "Switzerland"=>"CH",
        "Syrian Arab Republic"=>"SY",
        "Taiwan, Province of China"=>"TW",
        "Taiwan"=>"TW",
        "Tajikistan"=>"TJ",
        "Tanzania, United Republic of"=>"TZ",
        "Thailand"=>"TH",
        "Timor-Leste"=>"TL",
        "Togo"=>"TG",
        "Tokelau"=>"TK",
        "Tonga"=>"TO",
        "Trinidad and Tobago"=>"TT",
        "Tunisia"=>"TN",
        "Turkey"=>"TR",
        "Turkmenistan"=>"TM",
        "Turks and Caicos Islands"=>"TC",
        "Tuvalu"=>"TV",
        "Uganda"=>"UG",
        "Ukraine"=>"UA",
        "United Arab Emirates"=>"AE",
        "United Kingdom"=>"GB",
        "United States"=>"US",
        "United States Minor Outlying Islands"=>"UM",
        "Uruguay"=>"UY",
        "Uzbekistan"=>"UZ",
        "Vanuatu"=>"VU",
        "Venezuela"=>"VE",
        "Vietnam"=>"VN",
        "Virgin Islands, British"=>"VG",
        "U.S. Virgin Islands"=>"VI",
        "Wallis and Futuna"=>"WF",
        "Western Sahara"=>"EH",
        "Yemen"=>"YE",
        "Zambia"=>"ZM",
        "Zimbabwe"=>"ZW"
    );
    return $countrycodes[$country_name];
}

function fn_ga_get_browser_image($browser_name){
    $browser_images = array(
        "Chrome" => "chrome.png",
        "Firefox" => "firefox.png",
        "Safari" => "safari.png",
        "Internet Explorer" => "ie.png",
        "Opera" => "opera.png"
    );
    if($browser_images[$browser_name]){
        return $browser_images[$browser_name];
    }
    else{
        return 'web.png';
    }
}

//Hook to add link to edit post page
add_filter( 'post_row_actions', 'report_post_link', 10, 2 );
function report_post_link( $actions, WP_Post $post ) {
    if ( 'post' == $post->post_type){
        $report_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_report&post_id='.$post->ID;
        $actions['ga_report'] = '<a href="'.$report_url.'">gAnalytics Report</a>';
    }

    return $actions;
}

add_filter( 'page_row_actions', 'report_page_link', 10, 2 );
function report_page_link( $actions, WP_Post $post ) {
    if ( 'page' == $post->post_type){
        $report_url = get_bloginfo('wpurl').'/wp-admin/admin.php?page=ga_report&post_id='.$post->ID;
        $actions['ga_report'] = '<a href="'.$report_url.'">gAnalytics Report</a>';
    }

    return $actions;
}

// Hourly job
add_action( 'ga_hourly_event_hook', 'fn_ga_hourly' );
function fn_ga_hourly($bypass = false, $module = "any", $args=array()) {
    include GANALYTICS_PATH.'inc/ga-api-setup-include.php';
    $ganalytics_options = get_option('ganalytics_options');
    if($access_token){
        if($ganalytics_options['next_request_time'] <= time() || $bypass){
            $ganalytics_options['next_request_time'] = time() + ($ganalytics_options['request_interval']*3600);
            switch($module){
                case "widget-site-visitors":
                    $total_visitors_data = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api, date('Y-m-d'), 'ga:visits');
                    if(is_array($total_visitors_data->getRows())){
                        foreach($total_visitors_data->getRows() as $key=>$visits){
                            $ganalytics_options['widget_site_visitors_total_visitors'] = $visits[0];
                        }
                    }
                    break;
                case "widget-top-countries":
                    $results_top_countries = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api,
                        date("Y-m-d"),
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:country',
                            'sort' => '-ga:visits'
                        ));
                    if(is_array($results_top_countries->getRows())){
                        $ganalytics_options['widget_top_countries_response'] = $results_top_countries->getRows();
                    }
                    $ganalytics_options['widget_top_countries_total_visitors'] = (int)$results_top_countries->totalsForAllResults["ga:visits"];
                    break;
                case "widget-trending-posts":
                    $blog_url = get_bloginfo('url');
                    $count_posts = wp_count_posts();
                    $published_posts = $count_posts->publish;
                    $args = array('numberposts' =>$published_posts);
                    $myposts = get_posts($args );
                    $permalink_array = array();
                    $id_array = array();
                    foreach( $myposts as $post ) :
                        setup_postdata($post);
                        array_push($permalink_array, get_permalink($post->ID));
                        array_push($id_array,$post->ID);
                    endforeach;

                    $trending_pages_day = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-1 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_day->getRows())){
                        $count = 0;
                        $trending_posts_day_array = array();
                        foreach($trending_pages_day->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_day_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_day'] = $trending_posts_day_array;
                    }

                    $trending_pages_week = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-8 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_week->getRows())){
                        $count = 0;
                        $trending_posts_week_array = array();
                        foreach($trending_pages_week->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_week_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_week'] = $trending_posts_week_array;
                    }

                    $trending_pages_month = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-31 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_month->getRows())){
                        $count = 0;
                        $trending_posts_month_array = array();
                        foreach($trending_pages_month->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_month_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_month'] = $trending_posts_month_array;
                    }

                    $trending_pages_alltime = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api, date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_alltime->getRows())){
                        $count = 0;
                        $trending_posts_alltime_array = array();
                        foreach($trending_pages_alltime->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_alltime_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_alltime'] = $trending_posts_alltime_array;
                    }
                    break;
                case "any":
                    // site visitors widget
                    $total_visitors_data = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api, date('Y-m-d'), 'ga:visits');
                    if(is_array($total_visitors_data->getRows())){
                        foreach($total_visitors_data->getRows() as $key=>$visits){
                            $ganalytics_options['widget_site_visitors_total_visitors'] = $visits[0];
                        }
                    }

                    // top countries widget
                    $results_top_countries = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api,
                        date("Y-m-d"),
                        'ga:visits',
                        array(
                            'dimensions' => 'ga:country',
                            'sort' => '-ga:visits'
                        ));
                    if(is_array($results_top_countries->getRows())){
                        $ganalytics_options['widget_top_countries_response'] = $results_top_countries->getRows();
                    }
                    $ganalytics_options['widget_top_countries_total_visitors'] = (int)$results_top_countries->totalsForAllResults["ga:visits"];

                    //trending posts
                    $blog_url = get_bloginfo('url');
                    $count_posts = wp_count_posts();
                    $published_posts = $count_posts->publish;
                    $args = array('numberposts' =>$published_posts);
                    $myposts = get_posts($args );
                    $permalink_array = array();
                    $id_array = array();
                    foreach( $myposts as $post ) :
                        setup_postdata($post);
                        array_push($permalink_array, get_permalink($post->ID));
                        array_push($id_array,$post->ID);
                    endforeach;
                    wp_reset_postdata();

                    $trending_pages_day = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-1 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));
                    if(is_array($trending_pages_day->getRows())){
                        $count = 0;
                        $trending_posts_day_array = array();
                        foreach($trending_pages_day->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_day_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_day'] = $trending_posts_day_array;
                    }

                    $trending_pages_week = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-8 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_week->getRows())){
                        $count = 0;
                        $trending_posts_week_array = array();
                        foreach($trending_pages_week->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_week_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_week'] = $trending_posts_week_array;
                    }

                    $trending_pages_month = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        date('Y-m-d',strtotime("-31 days")), date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_month->getRows())){
                        $count = 0;
                        $trending_posts_month_array = array();
                        foreach($trending_pages_month->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_month_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_month'] = $trending_posts_month_array;
                    }

                    $trending_pages_alltime = $analytics->data_ga->get(
                        'ga:'.$ganalytics_settings['ga_profile_id'],
                        $created_time_api, date('Y-m-d',strtotime("-1 days")), 'ga:sessions,ga:pageviews',
                        array(
                            'dimensions' => 'ga:pagePath',
                            'sort' => '-ga:pageviews'
                        ));

                    if(is_array($trending_pages_alltime->getRows())){
                        $count = 0;
                        $trending_posts_alltime_array = array();
                        foreach($trending_pages_alltime->getRows() as $key=>$row){
                            if(in_array($blog_url.$row[0],$permalink_array)){
                                $postkey = array_search($blog_url.$row[0],$permalink_array);
                                $count++;
                                if($count > 20)
                                    break;
                                $data = array();
                                $data['id']=$id_array[$postkey];
                                $data['visits']=$row[1];
                                $data['pageviews']=$row[2];
                                array_push($trending_posts_alltime_array,$data);
                            }
                        }
                        $ganalytics_options['widget_trending_posts_alltime'] = $trending_posts_alltime_array;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    update_option( 'ganalytics_options', $ganalytics_options );
}
?>