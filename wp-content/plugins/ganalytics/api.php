<?php
/**
 * gAnalytics Wordpress Plugin
 * @version 2.6.2
 * @copyright (c) 2013 cube3x
 *
 * module to call Google SDK
 * Author Home Page: http://cube3x.com
 *
 **/
?>
<?php
set_time_limit(0);

require_once GANALYTICS_PATH.'ga-sdk/Google_Client.php';
require_once GANALYTICS_PATH.'ga-sdk/contrib/Google_AnalyticsService.php';
$client = new Google_Client();
$client->setApplicationName('gAnalytics');
$client->setAccessType('offline');
$client->setUseObjects(true);

// Reading plugin settings from option variable
$ganalytics_settings = get_option('ganalytics_settings');

$client->setClientId($ganalytics_settings['google_client_id']);
$client->setClientSecret($ganalytics_settings['google_client_secret']);
$client->setDeveloperKey($ganalytics_settings['google_api_key']);
$client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');

$analytics = new Google_AnalyticsService($client);

// Setting Access Token
$access_token = $ganalytics_settings['google_access_token'];
?>