<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_5544d8a4b84a4',
	'title' => __('Settings', $this->textDomain),
	'fields' => array (
        
        array (
			'key' => 'field_5554ae984791b',
			'label' => __('Purchase Code', $this->textDomain),
			'name' => $this->slug.'_purchase_code',
			'prefix' => '',
			'type' => 'text',
			'instructions' => __('Add your purchase code to get automatic updates to the plugin.', $this->textDomain),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
      
		array (
			'key' => 'field_5544d8fa32d3c',
			'label' => __('White-label the Plugin', $this->textDomain),
			'name' => $this->slug.'_whitelabel',
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => __('Checking this box will remove the branding elements of the plugin, like the custom header of the pages and the about page. 
<br><br>
This can also be achieved by adding <code>add_filter(\'wpamm/settings/whitelabel\', \'__return_true\');</code> to your functions.php.', $this->textDomain),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => __('White-label the Plugin.', $this->textDomain),
			'default_value' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-settings',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;