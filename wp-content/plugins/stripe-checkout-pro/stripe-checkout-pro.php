<?php

/**
 * Stripe Pro
 *
 * @package   SC Pro
 * @author    Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 * @license   GPL-2.0+
 * @link      http://wpstripe.net
 * @copyright 2014 Phil Derksen
 *
 * @wordpress-plugin
 * Plugin Name: Stripe Pro
 * Plugin URI: http://wpstripe.net
 * Description: Add a Stripe Checkout form overlay to your site in minutes using shortcodes. **Pro Version**
 * Version: 2.0.7
 * Author: Phil Derksen
 * Author URI: http://philderksen.com
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: sc
 * Domain Path: /languages/
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'SC_MAIN_FILE' ) ) {
	define( 'SC_MAIN_FILE', __FILE__ );
}

if( ! defined( 'SC_PATH' ) ) {
	define( 'SC_PATH', plugin_dir_path( __FILE__ ) );
}

if( ! defined( 'SC_URL' ) ) {
	define( 'SC_URL', plugins_url( '', __FILE__ ) . '/' );
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Check for Stripe Checkout Lite or old "basic" add-ons.
// If active then make sure to 'die' otherwise a fatal error will stop everything.
if( is_plugin_active( 'stripe/stripe-checkout.php' ) || is_plugin_active( 'stripe-coupons/stripe-coupons.php' ) 
		|| is_plugin_active( 'stripe-custom-fields/stripe-custom-fields.php' ) || is_plugin_active( 'stripe-user-entered-amount/stripe-user-entered-amount.php' ) ) {
	wp_die( sprintf( __(  'Simple Stripe Checkout (Lite) and any old add-ons must be deactivated before you can activate Pro.' .
					' <a href="%s">Return to Plugins</a>.', 'sc_coup' ), get_admin_url( '', 'plugins.php' ) ) );
} 

if( ! class_exists( 'Stripe_Checkout' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'class-stripe-checkout.php' );
}

require_once( plugin_dir_path( __FILE__ ) . 'class-stripe-checkout-pro.php' );


// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'Stripe_Checkout', 'activate' ) );

Stripe_Checkout_Pro::get_instance();
