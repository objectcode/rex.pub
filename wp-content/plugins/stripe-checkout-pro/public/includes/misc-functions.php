<?php

/**
 * Misc plugin functions
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Common method to set Stripe API key from options.
 *
 * @since 2.0.7
 */
function sc_set_stripe_key() {
	global $sc_options;
	$key = '';

	// Check first if in live or test mode.
	if( ! empty( $sc_options['enable_live_key'] ) && $sc_options['enable_live_key'] == 1 && $test_mode != 'true' ) {
		$key = ( ! empty( $sc_options['live_secret_key'] ) ? $sc_options['live_secret_key'] : '' );
	} else {
		$key = ( ! empty( $sc_options['test_secret_key'] ) ? $sc_options['test_secret_key'] : '' );
	}

	Stripe::setApiKey( $key );
}

/**
 * Function that will actually charge the customers credit card
 * 
 * @since 2.0.0
 */
function sc_charge_card() {
	if( isset( $_POST['stripeToken'] ) ) {
		
		if( ! class_exists( 'Stripe' ) ) {
			require_once( SC_PLUGIN_DIR . 'libraries/stripe-php/Stripe.php' );
		}
		
		global $sc_options;
		
		// Set redirect
		$redirect      = $_POST['sc-redirect'];
		$fail_redirect = $_POST['sc-redirect-fail'];
		$failed = null;
		
		$message = '';
		
		// Get the credit card details submitted by the form
		$token                 = $_POST['stripeToken'];
		$amount                = $_POST['sc-amount'];
		$description           = $_POST['sc-description'];
		$store_name            = $_POST['sc-name'];
		$currency              = $_POST['sc-currency'];
		$sub                   = ( isset( $_POST['sc_sub_id'] ) ); 
		$interval              = ( isset( $_POST['sc_sub_interval'] ) ? $_POST['sc_sub_interval'] : 'month' );
		$interval_count        = ( isset( $_POST['sc_sub_interval_count'] ) ? $_POST['sc_sub_interval_count'] : 1 );
		$statement_description = ( isset( $_POST['sc_sub_statement_description'] ) ? $_POST['sc_sub_statement_description'] : '' );
		
		if( $sub ) {
			$sub = ( ! empty( $_POST['sc_sub_id'] ) ? $_POST['sc_sub_id'] : 'custom' );
		}
		
		sc_set_stripe_key();
		
		$meta = array();
		
		$meta = apply_filters( 'sc_meta_values', $meta );
		
		$amount = apply_filters( 'sc_charge_amount', $amount );
		
		if( ! empty( $sub ) ) {
			
			try {
				
				if( $sub == 'custom' ) {
					
					$timestamp = time();
					
					$plan_id = $_POST['stripeEmail'] . '_' . $amount . '_' . $timestamp;
					
					// Create a plan
					$plan_args = array( 
							'amount'                => $amount,
							'interval'              => $interval,
							'name'                  => $plan_id,
							'currency'              => $currency,
							'id'                    => $plan_id,
							'interval_count'        => $interval_count
						);
					
					if( ! empty( $statement_description ) ) {
						$plan_args['statement_description'] = $statement_description;
					}
					
					$new_plan = Stripe_Plan::create( $plan_args );
					
					// Create a customer and charge
					$new_customer = Stripe_Customer::create( array( 
							'email'    => $_POST['stripeEmail'],
							'card'     => $token,
							'plan'     => $plan_id,
							'metadata' => $meta
						));
					
				} else {
					
					// Create new customer 
					$cust_args = array( 
							'email' => $_POST['stripeEmail'],
							'card'  => $token,
							'plan' => $sub,
							'metadata' => $meta
						);
					
					if( ! empty( $coupon ) ) {
						$cust_args['coupon'] = $coupon;
					}
					
					$new_customer = Stripe_Customer::create( $cust_args );
	
					// Set currency based on sub
					$plan = Stripe_Plan::retrieve( $sub );
					
					//echo $subscription . '<Br>';
					$currency = strtoupper( $plan->currency );
					
				}
				
				// We want to add the meta data and description to the actual charge so that users can still view the meta sent with a subscription + custom fields
				// the same way that they would normally view it without subscriptions installed.
				// We need the steps below to do this

				// First we get the latest invoice based on the customer ID
				$invoice = Stripe_Invoice::all( array(
					'customer' => $new_customer->id,
					'limit' => 1 )
				 );

				// Now that we have the invoice object we can get the charge ID
				$inv_charge = $invoice->data[0]->charge;

				// Finally, with the charge ID we can update the specific charge and inject our meta data sent from Stripe Custom Fields
				$ch = Stripe_Charge::retrieve( $inv_charge );
				$ch->metadata = $meta;

				if( ! empty( $description ) ) {
					$ch->description = $description;
				}

				$ch->save();
				
				$query_args = array( 'charge' => $ch->id, 'store_name' => urlencode( $store_name ) );
				
				$failed = false;
				
			} catch(Stripe_CardError $e) {

				$redirect = $fail_redirect;

				$failed = true;

				//$e = $e->getJsonBody();

				$query_args = array( 'sub' => true, 'charge_failed' => true );
				
			} catch (Stripe_AuthenticationError $e) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				
				$redirect = $fail_redirect;

				$failed = true;

				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Stripe_ApiConnectionError $e) {
				// Network communication with Stripe failed
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Stripe_Error $e) {
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
			}
			
		} else {
			
			
			
			// Create the charge on Stripe's servers - this will charge the user's default card
			try {
				// Create new customer 
				$new_customer = Stripe_Customer::create( array( 
						'email' => $_POST['stripeEmail'],
						'card'  => $token
					));
				
				$charge_args = array(
						'amount'      => $amount, // amount in cents, again
						'currency'    => $currency,
						'customer'    => $new_customer['id'],
						'metadata'    => $meta
					);
				
				if( ! empty( $description ) ) {
					$charge_args['description'] = $description;
				}
				
				$charge = Stripe_Charge::create( $charge_args );

				$query_args = array( 'charge' => $charge->id, 'store_name' => urlencode( $store_name ) );

				$failed = false;
				
			} catch(Stripe_CardError $e) {
				
				$redirect = $fail_redirect;

				$failed = true;

				$e = $e->getJsonBody();
				
				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Stripe_AuthenticationError $e) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				
				$redirect = $fail_redirect;

				$failed = true;

				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Stripe_ApiConnectionError $e) {
				// Network communication with Stripe failed
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Stripe_Error $e) {
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
				
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				
				$redirect = $fail_redirect;

				$failed = true;
				
				$e = $e->getJsonBody();

				$query_args = array( 'charge' => $e['error']['charge'], 'charge_failed' => true );
			}
		}
		
		unset( $_POST['stripeToken'] );
		
		do_action( 'sc_redirect_before' );
		
		wp_redirect( add_query_arg( $query_args, apply_filters( 'sc_redirect', $redirect, $failed ) ) );
		
		do_action( 'sc_redirect_after' );
		
		exit;
	}
}

function sc_print_errors( $err = array() ) {
	
	$message = '';
	
	if( current_user_can( 'manage_options' ) ) {
		foreach( $err as $k => $v ) {
			$message  = '<h6>' . $k . ': ' . $v . '</h6>';
		}
	} else {
		$message = '<h6>' . __( 'An error has occurred. If the problem persists, please contact a site administrator.', 'sc' ) . '</h6>';
	}
	
	return apply_filters( 'sc_error_message', $message );
}

// We only want to run the charge if the Token is set
if( isset( $_POST['stripeToken'] ) ) {
	add_action( 'init', 'sc_charge_card' );
}

/*
 * Function to show the payment details after the purchase
 * 
 * @since 2.0.0
 */
function sc_show_payment_details( $content ) {
	
	$html = '';

	sc_set_stripe_key();
	
	// If a subscription fail happened let's check now
	if( isset( $_GET['sub'] ) ) {
		
		$html  = '<div class="sc-payment-details-wrap sc-payment-details-error">';
		$html .= __( 'Sorry, but your payment was not processed.', 'sc' );
		$html .= '<br><br>' . __( 'If the problem persists please contact the site administrator.', 'sc' );
		$html .= '</div>';
		
		return apply_filters( 'sc_payment_details_error', $html ) . $content;
	}

	// Successful charge output.
	if ( isset( $_GET['charge'] )  && ! isset( $_GET['charge_failed'] ) ) {

		$charge_id = esc_html( $_GET['charge'] );

		// https://stripe.com/docs/api/php#charges
		$charge_response = Stripe_Charge::retrieve( $charge_id );

		$html = '<div class="sc-payment-details-wrap">';
		
		$html .= '<p>' . __( 'Congratulations. Your payment went through!', 'sc' ) . '</p>' . "\n";
		
		if( ! empty( $charge_response->description ) ) {
			$html .= '<p>' . __( "Here's what you bought:", 'sc' ) . '</p>';
			$html .= $charge_response->description . '<br>' . "\n";
		}
		
		if ( isset( $_GET['store_name'] ) && ! empty( $_GET['store_name'] ) ) {
			$html .= 'From: ' . esc_html( $_GET['store_name'] ) . '<br/>' . "\n";
		}
		
		$html .= '<br><strong>' . __( 'Total Paid: ', 'sc' ) . sc_stripe_to_formatted_amount( $charge_response->amount, $charge_response->currency ) . ' ' . 
				strtoupper( $charge_response->currency ) . '</strong>' . "\n";
		
		$html .= '<p>' . sprintf( __( 'Your transaction ID is: %s', 'sc' ), $charge_id ) . '</p>';
		
		$html .= '</div>';
		
		
		return apply_filters( 'sc_payment_details', $html, $charge_response ) . $content;

	} elseif ( isset( $_GET['charge_failed'] ) ) {
		
		$charge_id = esc_html( $_GET['charge'] );
		
		$charge = Stripe_Charge::retrieve( $charge_id );
		
		$html  = '<div class="sc-payment-details-wrap sc-payment-details-error">';
		$html .= __( 'Sorry, but your payment was not processed.', 'sc' );
		$html .= '<br><br>' . __( 'Reason for failure: ', 'sc' ) . $charge->failure_message;
		$html .= '<br><br>' . sprintf( __( 'Transaction ID: %s', 'sc' ), $charge_id );
		$html .= '</div>';
		
		return apply_filters( 'sc_payment_details_error', $html ) . $content;

	} 
	
	return $content;
}
add_filter( 'the_content', 'sc_show_payment_details' );

/**
 * Convert amount opposite of sc_decimal_to_stripe_amount().
 * Needed for Stripe's calculated amount. Don't convert if using zero-decimal currency.
 *
 * @since 2.0.0
 */
function sc_stripe_to_decimal_amount( $amount, $currency ) {

	if ( !sc_is_zero_decimal_currency( $currency) ) {
		// Always round to 2 decimals.
		$amount = round( $amount / 100, 2 );
	}

	return $amount;
}

/**
 * Format Stripe (non-decimal) amount for screen.
 *
 * @since 2.0.0
 */
function sc_stripe_to_formatted_amount( $amount, $currency ) {

	// First convert to decimal if needed.
	$amount = sc_stripe_to_decimal_amount( $amount, $currency );

	// Use 2 decimals unless zero-decimal currency.
	$formatted_amount = number_format_i18n( $amount, ( sc_is_zero_decimal_currency( $currency ) ? 0 : 2 ) );

	return $formatted_amount;
}

/**
 * List of zero-decimal currencies according to Stripe.
 * Needed for PHP and JS.
 * See: https://support.stripe.com/questions/which-zero-decimal-currencies-does-stripe-support
 *
 * @since 2.0.0
 */
function sc_zero_decimal_currencies() {
	return array( 'BIF', 'CLP', 'DJF', 'GNF', 'JPY', 'KMF', 'KRW', 'MGA', 'PYG', 'RWF', 'VUV', 'XAF', 'XOF', 'XPF' );
}

/**
 * Check if currency is zero-decimal.
 *
 * @since 2.0.0
 */
function sc_is_zero_decimal_currency( $currency ) {
	return in_array( strtoupper( $currency ), sc_zero_decimal_currencies() );
}

/**
 * Check if the [stripe] shortcode exists on this page
 * 
 * @since 2.0.0
 */
function sc_has_shortcode() {
	global $post;
	
	// Currently ( 5/8/2014 ) the has_shortcode() function will not find a 
	// nested shortcode. This seems to do the trick currently, will switch if 
	// has_shortcode() gets updated. -NY
	if ( strpos( $post->post_content, '[stripe' ) !== false ) {
		return true;
	}
	
	return false;
}

/**
 * Since Stripe does not deal with Shipping information we will add it as meta to pass to the Dashboard
 * 
 * @since 2.0.0
 */
function sc_add_shipping_meta( $meta ) {
	if( isset( $_POST['sc-shipping-name'] ) ) {
		
		// Add Shipping Name as an item
		$meta['Shipping Name']    = $_POST['sc-shipping-name'];
		
		// Show address on two lines: Address 1 and Address 2 in Stripe dashboard -> payments 
		$meta['Shipping Address 1'] = $_POST['sc-shipping-address'];
		$meta['Shipping Address 2'] = $_POST['sc-shipping-zip'] . ', ' . $_POST['sc-shipping-city'] . ', ' . $_POST['sc-shipping-state'] . ', ' . $_POST['sc-shipping-country'];
	}
	
	return $meta;
}
add_filter( 'sc_meta_values', 'sc_add_shipping_meta' );

/**
 * Google Analytics campaign URL.
 *
 * @since   2.0.0
 *
 * @param   string  $base_url Plain URL to navigate to
 * @param   string  $source   GA "source" tracking value
 * @param   string  $medium   GA "medium" tracking value
 * @param   string  $campaign GA "campaign" tracking value
 * @return  string  $url      Full Google Analytics campaign URL
 */
function sc_ga_campaign_url( $base_url, $source, $medium, $campaign ) {
	// $medium examples: 'sidebar_link', 'banner_image'

	$url = add_query_arg( array(
		'utm_source'   => $source,
		'utm_medium'   => $medium,
		'utm_campaign' => $campaign
	), $base_url );

	return $url;
}

/**
 * Disables opengraph tags to avoid conflicts with WP SEO by Yoast
 *
 * @since 2.0.0
 */
function sc_disable_seo_og() {
	
	$sc_payment_details = Stripe_Checkout_Pro::get_instance()->session->get( 'sc_payment_details' );
	
	if ( $sc_payment_details['show'] == true ) {
		remove_action( 'template_redirect', 'wpseo_frontend_head_init', 999 );
	}
}

function sc_license_settings( $settings ) {
	
	$settings['licenses']['note'] = array(
			'id'   => 'note',
			'name' => '',
			'desc' => '<p class="description">' . __( 'These license keys are used for access to automatic upgrades and support.', 'sc' ) . '</p>',
			'type' => 'section'
	);

	$settings['licenses']['sc_license_key'] = array(
			'id'   => 'sc_license_key',
			'name' => __( 'License Key', 'sc' ),
			'desc' => '',
			'type' => 'license',
			'size' => 'regular-text',
			'product' => 'Stripe Checkout Pro'
	);

	return $settings;
}
add_filter( 'sc_settings', 'sc_license_settings' );

function sc_check_license( $license, $item ) {
	
return true;	
}

function sc_activate_license() {
	$sc_licenses = get_option( 'sc_licenses' );
	
	$current_license = $_POST['license'];
	$item            = $_POST['item'];
	$action          = $_POST['sc_action'];
	$id              = $_POST['id'];
	
	// Need to trim the id of the excess stuff so we can update our option later
	$length = strpos( $id, ']' ) - strpos( $id, '[' );
	$id = substr( $id, strpos( $id, '[' ) + 1, $length - 1 );
	
	// Do activation
	$activate_params = array(
		'edd_action' => $action,
		'license'    => $current_license,
		'item_name'  => urlencode( $item ),
		'url' => home_url()
	);

	$response = wp_remote_get( add_query_arg( $activate_params, SC_EDD_SL_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

	if( is_wp_error( $response ) )
	{
		echo 'ERROR';
		
		die();
	}
	
	$activate_data = json_decode( wp_remote_retrieve_body( $response ) );
	
	if( $activate_data->license == 'valid' ) {
		$sc_licenses[$item] = 'valid';
		
		$sc_settings_licenses = get_option( 'sc_settings_licenses' );
		
		$sc_settings_licenses[$id] = $current_license;
		
		update_option( 'sc_settings_licenses', $sc_settings_licenses );
		
		
	} else if( $activate_data->license == 'deactivated' ) {
		$sc_licenses[$item] = 'deactivated';
	} else {
		$sc_licenses[$item] = 'invalid';
	}
	
	update_option( 'sc_licenses', $sc_licenses );
	
	//echo '<pre>' . print_r( $sc_licenses, true ) . '</pre>';
	
	echo $activate_data->license;
	
	

	//echo "Licesne: $license, Item: $item";
	
	die();
}
add_action( 'wp_ajax_sc_activate_license', 'sc_activate_license' );

/**
 * Function to handle AJAX request for coupon check
 * 
 * @since 2.0.0
 */
function sc_coup_ajax_check() {

	global $sc_options;

	$json = '';
	$code = $_POST['coupon'];
	$amount = $_POST['amount'];

	$json['coupon']['code'] = $code;


	// Check what mode we are in
	if( ! empty( $sc_options['enable_live_key'] ) && $sc_options['enable_live_key'] == 1 ) {
		$key = ( ! empty( $sc_options['live_secret_key'] ) ? $sc_options['live_secret_key'] : '' );
	} else {
		$key = ( ! empty( $sc_options['test_secret_key'] ) ? $sc_options['test_secret_key'] : '' );
	}

	// Set key
	Stripe::setApiKey( $key );

	try {
		$coupon = Stripe_Coupon::retrieve( trim( $code ) );

		if( ! empty( $coupon->percent_off ) ) {
			$json['coupon']['amountOff'] = $coupon->percent_off;
			$json['coupon']['type'] = 'percent';

			if( $coupon->percent_off == 100 ) {
				$amount = 0;
			} else {
				$amount = round( ( $amount * ( ( 100 - $coupon->percent_off ) / 100 ) ) );
			}
		} else if( ! empty( $coupon->amount_off ) ) {
			$json['coupon']['amountOff'] = $coupon->amount_off;
			$json['coupon']['type'] = 'amount';

			$amount = $amount - $coupon->amount_off;

			if( $amount < 0 ) {
				$amount = 0;
			}
		}

		// Set message to amount now before checking for errors
		$json['success'] = true;
		$json['message'] = $amount;

		if( $amount < 50 ) {
			$json['success'] = false;
			$json['message'] = 'Coupon entered puts the total below the required minimum amount.';
		}

	} catch (Exception $e) {
		// an exception was caught, so the code is invalid
		$json['success'] = false;
		$json['message'] = 'Invalid coupon code.';
	}
		
	// Return as JSON
	echo json_encode( $json );
	
	die();
}
add_action( 'wp_ajax_sc_coup_get_coupon', 'sc_coup_ajax_check' );
add_action( 'wp_ajax_nopriv_sc_coup_get_coupon', 'sc_coup_ajax_check' );

/**
 * Function to handle adding the coupon as meta data in Stripe Dashboard
 * 
 * @since 2.0.0
 */
function sc_coup_add_coupon_meta( $meta ) {
	if( isset( $_POST['sc_coup_coupon_code'] ) && ! empty( $_POST['sc_coup_coupon_code'] ) ) {
		$meta['Coupon Code'] = $_POST['sc_coup_coupon_code'];
	}
	
	return $meta;
}
add_filter( 'sc_meta_values', 'sc_coup_add_coupon_meta' );


/**
 * Check if the [stripe] shortcode exists on this page
 * 
 * @since 2.0.0
 */
function sc_coup_has_shortcode( $content = null ) {
	
	if( ! $content ) {
		global $post;
		$content = $post->post_content;
	}
	
	// Currently ( 5/8/2014 ) the has_shortcode() function will not find a 
	// nested shortcode. This seems to do the trick currently, will switch if 
	// has_shortcode() gets updated. -NY
	if ( strpos( $content, '[stripe_coupon' ) !== false ) {
		return true;
	}
	
	return false;
}



/**********************************************************************************************************************
 * Stripe Custom Fields
 *
 * @package SC_CF
 * @author  Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 */

/*
 * Send post meta
 * 
 * @since 2.0.0
 */
function sc_cf_checkout_meta( $meta ) {
	if( isset( $_POST['sc_form_field'] ) ) {
		foreach( $_POST['sc_form_field'] as $k => $v ) {
			$meta[$k] = $v;
		}
	}
	
	return $meta;
}
add_filter( 'sc_meta_values', 'sc_cf_checkout_meta' );


/**
 * Check if the [stripe_amount] shortcode exists on this page
 * 
 * @since 2.0.0
 */
function is_sc_cf_date_active( $content = null ) {
	
	if( ! $content ) {
		global $post;
		
		$content = $post->post_content;
	}
	
	// Currently ( 5/8/2014 ) the has_shortcode() function will not find a 
	// nested shortcode. This seems to do the trick currently, will switch if 
	// has_shortcode() gets updated. -NY
	if ( strpos( $content, '[stripe_date' ) ) {
		return true;
	}
	
	return false;
}


/**
 * Check if any of the shortcode are being displayed
 * 
 * @since 2.0.0
 */
function is_sc_cf_shortcode_active( $content = null ) {
	
	if( ! $content ) {
		global $post;
		
		$content = $post->post_content;
	}
	
	
	if( strpos( $content, '[stripe_text' ) ||
		strpos( $content, '[stripe_checkbox' ) ||
		strpos( $content, '[stripe_number' ) ||
		strpos( $content, '[stripe_date' ) ) {
		
			return true;
	}
	
	
	return false;
}


/**************************************************************************************************************
 * Stripe User Entered Amount misc functions
 *
 * @package SC_UEA
 * @author  Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 */

/**
 * Check if the [stripe_amount] shortcode exists on this page
 * 
 * @since 2.0.0
 */
function sc_uea_is_active( $content = null ) {
	
	if( ! $content ) {
		global $post;
		
		$content = $post->post_content;
	}
	
	// Currently ( 5/8/2014 ) the has_shortcode() function will not find a 
	// nested shortcode. This seems to do the trick currently, will switch if 
	// has_shortcode() gets updated. -NY
	if ( strpos( $content, '[stripe_amount' ) ) {
		return true;
	}
	
	return false;
}

/**
 * This function hooks into the Stipe Checkout attributes and checks to see if an amount attribute is added
 * If an amount attribute is not added then we set it to 50 (the lowest possible) so that it will still show the button
 * and our UEA plugin will be able to run.
 * 
 * @since 2.0.0
 */
function sc_uea_set_minimum( $out, $pairs, $atts ) {
	
	if( empty( $out['amount'] ) ) {
		$out['amount'] = 50;
	}
	
   return $out;
}
add_filter( 'shortcode_atts_stripe', 'sc_uea_set_minimum', 10, 3 );


