=== Stripe Checkout Pro ===
Contributors: pderksen, nickyoung87
Requires at least: 3.7.4
Tested up to: 4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Add a highly optimized Stripe Checkout form overlay to your site in a few simple steps. **Pro Version**

== Changelog ==

= 2.0.7 =

* Allow display of more charge details on the payment success page. This is made possible by utilizing the Stripe charge ID to retrieve the entire charge object via the Stripe API.
* Renamed a function for better compatibility with themes and other plugins.
* Improved messaging for minimum required amount by Stripe (50 units).

= 2.0.6 =

* Updated to be compatible with Stripe Subscriptions 1.0.5.
* Updated to most recent Stripe PHP library (v1.17.2).

= 2.0.5 =

* Payment button style setting and shortcode added.
* Updated 3rd party JS/CSS libraries: Moment, Parlsey, Bootstrap Switch.

= 2.0.4 =

* Added drop-down custom field shortcode: [stripe_dropdown]
* Added radio button custom field shortcode: [stripe_radio]
* Added test_mode attribute to specify test mode per form.
* Added option to save settings when uninstalling.
* Special characters now encoded properly for overlay form.
* Fixed bug with description not being added to Stripe dashboard.
* Better error handling for Stripe API Requests.
* Better admin-only notices for invalid shortcode combinations.

= 2.0.3 =

* Fixed compatibility issue with Pro shortcodes used alongside add-ons.

= 2.0.2 =

* Fixed bug with update check when other plugins are using the same EDD software licensing library.
* Add-on framework updates.

= 2.0.1 =

* Fixed bug where coupon code Apply button was firing off overlay.

= 2.0.0 =

* Initial plugin conversion from add-on model used previously.
* Updated to most recent Stripe PHP library (v1.17.1).
* Improved license key validation checks and messaging.
* Fixed validation issue with custom text fields.
