<?php
/**
 * Plugin shortcode functions
 *
 * @package SC
 * @author  Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Function to process the [stripe] shortcode
 * 
 * @since 2.0.0
 */
function sc_stripe_shortcode_pro( $attr, $content = null ) {
	
	global $sc_options, $sc_script_options, $script_vars;
	
	STATIC $uid = 1;
	
	extract( shortcode_atts( array(
					'name'                  => ( ! empty( $sc_options['name'] ) ? $sc_options['name'] : get_bloginfo( 'title' ) ),
					'description'           => '',
					'amount'                => '',
					'image_url'             => '',
					'currency'              => ( ! empty( $sc_options['currency'] ) ? $sc_options['currency'] : 'USD' ),
					'checkout_button_label' => '',
					'billing'               => '',    // true or false
					'shipping'              => '',    // true or false
					'payment_button_label'  => ( ! empty( $sc_options['payment_button_label'] ) ? $sc_options['payment_button_label'] : __( 'Pay with Card', 'sc' ) ),
					'enable_remember'       => '',    // true or false
					'success_redirect_url'  => ( ! empty( $sc_options['success_redirect_url'] ) ? $sc_options['success_redirect_url'] : get_permalink() ),
					'failure_redirect_url'  => ( ! empty( $sc_options['failure_redirect_url'] ) ? $sc_options['failure_redirect_url'] : get_permalink() ),
					'prefill_email'         => 'false',
					'verify_zip'            => ( ! empty( $sc_options['verify_zip'] ) ? 'true' : 'false' ),
					'payment_button_style'  => ( ! empty( $sc_options['payment_button_style'] ) && $sc_options['payment_button_style'] == 'none' ? 'none' : '' ),
					'test_mode'             => 'false'
				), $attr, 'stripe' ) );
	
	if( $content != null ) {
		if( sc_uea_is_active( $content ) && sc_coup_has_shortcode( $content ) ) {
			if( current_user_can( 'manage_options' ) ) {
				return '<h6>' . __( 'You cannot include a user-entered amount field and coupon code field in the same form.', 'sc' ) . '</h6>';
			}

			return;
		}
	}
	
	// Check if in test mode or live mode
	if( ! empty( $sc_options['enable_live_key'] ) && $sc_options['enable_live_key'] == 1 && $test_mode != 'true' ) {
		$data_key = ( ! empty( $sc_options['live_publish_key'] ) ? $sc_options['live_publish_key'] : '' );
		
		if( empty( $sc_options['live_secret_key'] ) ) {
			$data_key = '';
		}
	} else {
		$data_key = ( ! empty( $sc_options['test_publish_key'] ) ? $sc_options['test_publish_key'] : '' );
		
		if( empty( $sc_options['test_secret_key'] ) ) {
			$data_key = '';
		}
	}
	
	if( empty( $data_key ) ) {
		
		if( current_user_can( 'manage_options' ) ) {
			return '<h6>' . __( 'You must enter your API keys before the Stripe button will show up here.', 'sc' ) . '</h6>';
		}
		
		return '';
	}
	
	if( ! empty( $prefill_email ) && $prefill_email !== 'false' ) {
		// Get current logged in user email
		if( is_user_logged_in() ) {
			$prefill_email = get_userdata( get_current_user_id() )->user_email;
		} else { 
			$prefill_email = 'false';
		}
	}
	
	// Save all of our options to an array so others can run them through a filter if they need to
	$sc_script_options = array( 
		'script' => array(
			'key'                  => $data_key,
			'name'                 => html_entity_decode( $name ),
			'description'          => html_entity_decode( $description ),
			'amount'               => $amount,
			'image'                => $image_url,
			'currency'             => strtoupper( $currency ),
			'panel-label'          => html_entity_decode( $checkout_button_label ),
			'billing-address'      => $billing,
			'shipping-address'     => $shipping,
			'label'                => html_entity_decode( $payment_button_label ),
			'allow-remember-me'    => $enable_remember,
			'email'                => $prefill_email,
			'verify_zip'           => $verify_zip
		),
		'other' => array(
			'success-redirect-url' => $success_redirect_url,
			'failure-redirect-url' => $failure_redirect_url
		)
	);
	
	$sc_script_options = apply_filters( 'sc_modify_script_options', $sc_script_options );
	
	// Set our global array based on the uid so we can make sure each button/form is unique
	$script_vars[$uid] = array(
			'key'             => ( ! empty( $sc_script_options['script']['key'] ) ? $sc_script_options['script']['key'] : ( ! empty( $sc_options['key'] ) ? $sc_options['key'] : -1 ) ),
			'name'            => ( ! empty( $sc_script_options['script']['name'] ) ? $sc_script_options['script']['name'] : ( ! empty( $sc_options['name'] ) ? $sc_options['name'] : -1 ) ),
			'description'     => ( ! empty( $sc_script_options['script']['description'] ) ? $sc_script_options['script']['description'] : ( ! empty( $sc_options['description'] ) ? $sc_options['description'] : -1 ) ),
			'amount'          => ( ! empty( $sc_script_options['script']['amount'] ) ? $sc_script_options['script']['amount'] : ( ! empty( $sc_options['amount'] ) ? $sc_options['amount'] : -1 ) ),
			'image'           => ( ! empty( $sc_script_options['script']['image'] ) ? $sc_script_options['script']['image'] : ( ! empty( $sc_options['image_url'] ) ? $sc_options['image_url'] : -1 ) ),
			'currency'        => ( ! empty( $sc_script_options['script']['currency'] ) ? $sc_script_options['script']['currency'] : ( ! empty( $sc_options['currency'] ) ? $sc_options['currency'] : -1 ) ),
			'panelLabel'      => ( ! empty( $sc_script_options['script']['panel-label'] ) ? $sc_script_options['script']['panel-label'] : ( ! empty( $sc_options['checkout_button_label'] ) ? $sc_options['checkout_button_label'] : -1 ) ),
			'billingAddress'  => ( ! empty( $sc_script_options['script']['billing-address'] ) ? $sc_script_options['script']['billing-address'] : ( ! empty( $sc_options['billing'] ) ? $sc_options['billing'] : -1 ) ),
			'shippingAddress' => ( ! empty( $sc_script_options['script']['shipping-address'] ) ? $sc_script_options['script']['shipping-address'] : ( ! empty( $sc_options['shipping'] ) ? $sc_options['shipping'] : -1 ) ),
			'allowRememberMe' => ( ! empty( $sc_script_options['script']['allow-remember-me'] ) ? $sc_script_options['script']['allow-remember-me'] : ( ! empty( $sc_options['enable_remember'] ) ? $sc_options['enable_remember'] : -1 ) ),
			'email'           => ( ! empty( $sc_script_options['script']['email'] ) && ! ( $sc_script_options['script']['email'] === 'false' ) ? $sc_script_options['script']['email'] : -1 ),
			'zipCode'         => ( ! empty( $sc_script_options['script']['verify_zip'] ) && ! ( $sc_script_options['script']['verify_zip'] === 'false' ) ? $sc_script_options['script']['verify_zip'] : -1 )
	);

	// Reference for Stripe's zero-decimal currencies in JS.
	$script_vars['zero_decimal_currencies'] = sc_zero_decimal_currencies();
	
	$name                 = $sc_script_options['script']['name'];
	$description          = $sc_script_options['script']['description'];
	$amount               = $sc_script_options['script']['amount'];
	$success_redirect_url = $sc_script_options['other']['success-redirect-url'];
	$failure_redirect_url = $sc_script_options['other']['failure-redirect-url'];
	$currency             = $sc_script_options['script']['currency'];

	//Add Parsley JS form validation attribute here.
	$html  = '<form id="sc_checkout_form_' . $uid . '" method="POST" action="" data-sc-id="' . $uid . '" class="sc-checkout-form" ';
	$html .= 'data-parsley-validate>';

	$content = sc_parse_shortcode_content( $content );
	
	$html .= apply_filters( 'sc_shortcode_content', $content );
	
	$html .= '<input type="hidden" name="sc-name" value="' . esc_attr( $name ) . '" />';
	$html .= '<input type="hidden" name="sc-description" value="' . esc_attr( $description ) . '" />';
	$html .= '<input type="hidden" name="sc-amount" class="sc_amount" value="" />';
	$html .= '<input type="hidden" name="sc-redirect" value="' . esc_attr( ( ! empty( $success_redirect_url ) ? $success_redirect_url : get_permalink() ) ) . '" />';
	$html .= '<input type="hidden" name="sc-redirect-fail" value="' . esc_attr( ( ! empty( $failure_redirect_url ) ? $failure_redirect_url : get_permalink() ) ) . '" />';
	$html .= '<input type="hidden" name="sc-currency" value="' . esc_attr( $currency ) . '" />';
	$html .= '<input type="hidden" name="stripeToken" value="" class="sc_stripeToken" />';
	$html .= '<input type="hidden" name="stripeEmail" value="" class="sc_stripeEmail" />';
	
	if( $test_mode == 'true' ) {
		$html .= '<input type="hidden" name="sc_test_mode" value="true" />'; 
	}
	
	// Add shipping information fields if it is enabled
	if( $shipping === 'true' ) {
		$html .= '<input type="hidden" name="sc-shipping-name" class="sc-shipping-name" value="" />';
		$html .= '<input type="hidden" name="sc-shipping-country" class="sc-shipping-country" value="" />';
		$html .= '<input type="hidden" name="sc-shipping-zip" class="sc-shipping-zip" value="" />';
		$html .= '<input type="hidden" name="sc-shipping-state" class="sc-shipping-state" value="" />';
		$html .= '<input type="hidden" name="sc-shipping-address" class="sc-shipping-address" value="" />';
		$html .= '<input type="hidden" name="sc-shipping-city" class="sc-shipping-city" value="" />';
	}

	// Payment button defaults to built-in Stripe class "stripe-button-el" unless set to "none".
	$html .= '<button class="sc-payment-btn' . ( $payment_button_style == 'none' ? '' : ' stripe-button-el' ) . '"><span>' . $payment_button_label . '</span></button>';
	$html .= '</form>';

	// Increment static uid counter
	$uid++;

	//Stripe minimum amount allowed.
	$stripe_minimum_amount = 50;

	if( ( empty( $amount ) || $amount < $stripe_minimum_amount ) || ! isset( $amount ) ) {

		if( current_user_can( 'manage_options' ) ) {
			$html =  '<h6>';
			$html .= __( 'Stripe checkout requires an amount of ', 'sc' ) . $stripe_minimum_amount;
			$html .= ' (' . sc_stripe_to_formatted_amount( $stripe_minimum_amount, $currency ) . ' ' . $currency . ')';
			$html .= __( ' or larger.', 'sc' );
			$html .= '</h6>';

			return $html;
		}
		
		return '';
		
	} else if( ! isset( $_GET['charge'] ) ) {
		return $html;
	}
	
	return '';

}
remove_shortcode( 'stripe' );
add_shortcode( 'stripe', 'sc_stripe_shortcode_pro' );


/**
 * Function to process [stripe_total] shortcode
 * 
 * 
 * @since 2.0.0
 */
function sc_stripe_total( $attr ) {
	
	global $sc_options, $sc_script_options;
	
	extract( shortcode_atts( array(
					'label' => ( ! empty( $sc_options['stripe_total_label'] ) ? $sc_options['stripe_total_label'] : __( 'Total Amount:', 'sc' ) )
				), $attr, 'stripe_total' ) );

	$currency = strtoupper( $sc_script_options['script']['currency'] );
	$stripe_amount = $sc_script_options['script']['amount'];

	$html  = '<div class="sc-form-group">';
	$html .= $label . ' ';
	$html .= '<span class="sc-total-amount">';

	// USD only: Show dollar sign on left of amount.
	if ( $currency === 'USD' ) {
		$html .= '$';
	}

	$html .= sc_stripe_to_formatted_amount( $stripe_amount, $currency );

	// Non-USD: Show currency on right of amount.
	if ( $currency !== 'USD' ) {
		$html .= ' ' . $currency;
	}

	$html .= '</span>'; //sc-total-amount
	$html .= '</div>'; //sc-form-group

	return $html;
}
add_shortcode( 'stripe_total', 'sc_stripe_total' );

/**
 * Function to remove the annoying <br> and <p> tags from wpautop inside the shortcode
 * 
 * Found this function here: http://charlesforster.com/shortcodes-and-line-breaks-in-wordpress/
 * 
 * @since 2.0.0
 */
function sc_parse_shortcode_content( $content ) {
 
    // Parse nested shortcodes and add formatting.
    $content = trim( do_shortcode( $content ) ); 
 
    // Remove '</p>' from the start of the string.
    if ( substr( $content, 0, 4 ) == '</p>' ) 
        $content = substr( $content, 4 ); 
 
    // Remove '<p>' from the end of the string.
    if ( substr( $content, -3, 3 ) == '<p>' ) 
        $content = substr( $content, 0, -3 ); 
 
    // Remove any instances of '<p></p>'.
    $content = str_replace( array( '<p></p>' ), '', $content ); 
	
	$content = str_replace( array( '<br>', '<br />' ), '', $content );
 
    return $content; 
} 

/**
 * Render code for [stripe_coupon]
 * 
 * @since 2.0.0
 */
function sc_coup_stripe_coupon( $attr ) {
	global $sc_options;
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
		'label'              => ( ! empty( $sc_options['sc_coup_label'] ) ? $sc_options['sc_coup_label'] : '' ),
		'placeholder'        => '',
		'apply_button_style' => ( ! empty( $sc_options['sc_coup_apply_button_style'] ) && $sc_options['sc_coup_apply_button_style'] == 'stripe' ? 'stripe' : '' )
	), $attr, 'stripe_coupon' ) );
	
	$html  = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label for="sc-coup-coupon-' . $counter . '">' . $label . '</label>' : '' );
	$html .= '<div class="sc-coup-coupon-container">';
    $html .= '<input type="text" class="sc-form-control sc-coup-coupon" id="sc-coup-coupon-' . $counter . '" name="sc_coup_coupon" placeholder="' . esc_attr( $placeholder ) . '" ';

	// Make Parsley JS validation ignore this field entirely.
	$html .= 'data-parsley-ui-enabled="false">';

	// Store valid coupon code in hidden field.
	$html .= '<input type="hidden" class="sc-coup-coupon-code" name="sc_coup_coupon_code" />';

	// Apply button (using "stripe" style if indicated).
	$html .= '<button class="sc-coup-apply-btn' . ( $apply_button_style == 'stripe' ? ' stripe-button-el' : '' ) . '"><span>Apply</span></button>';

	$html .= '</div>'; //sc-coup-coupon-container

	// Loading indicator and validation message.
	$html .= '<div class="sc-coup-loading"><img src="' . SC_URL . 'assets/loading.gif" /></div>';
	$html .= '<div class="sc-coup-validation-message"></div>';

	// Success message and removal link.
	$html .= '<div class="sc-coup-success-row">';
	$html .= '<span class="sc-coup-success-message"></span>';
	$html .= ' <span class="sc-coup-remove-coupon">(<a href="#">remove</a>)</span>';
	$html .= '</div>'; //sc-coup-success-row

	$html .= '</div>'; //sc-form-group

	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_coupon', 'sc_coup_stripe_coupon' );




/**************************************************************************************************************
 * Stripe Custom Fields shortcode declarations
 *
 * @package SC_CF
 * @author  Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 */

/**
 * Shortcode to output a custom text field
 * 
 * @since 2.0.0
 */
function sc_cf_text( $attr ) {
	
	STATIC $counter = 1;
			
	extract( shortcode_atts( array(
					'id'          => '',
					'label'       => '',
					'placeholder' => '',
					'required'    => 'false',
					'default'     => '',
					'multiline'   => 'false',
					'rows'        => '5'
				), $attr, 'stripe_text' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_text_' . $counter;
	}
	
	$html  = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label for="' . esc_attr( $id ) . '">' . $label . '</label>' : '' );
	
	if( $multiline === 'true' ) {
		$html .= '<textarea rows="' . esc_attr( $rows ) . '" class="sc-form-control sc-cf-textarea" id="' . esc_attr( $id ) . '" ' .
		         'name="sc_form_field[' . $id . ']" placeholder="' . esc_attr( $placeholder ) . '" ' . ( $required === 'true' ? 'required' : '' ) . '>' .
		         esc_textarea( $default ) . '</textarea>';
	} else {
		$html .= '<input type="text" value="' . esc_attr( $default ) . '" class="sc-form-control sc-cf-text" id="' . esc_attr( $id ) . '" ' .
		         'name="sc_form_field[' . $id . ']" placeholder="' . esc_attr( $placeholder ) . '" ' . ( $required === 'true' ? 'required' : '' ) . '>';
	}
	
	$html .= '</div>'; //sc-form-group
	
	// Increment static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_text', 'sc_cf_text' );

/**
 * Shortcode to output a date field
 * 
 * @since 2.0.0
 */
function sc_cf_date( $attr ) {
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'id'          => '',
					'label'       => '',
					'placeholder' => '',
					'required'    => 'false',
					'default'     => ''
				), $attr, 'stripe_date' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_date_' . $counter;
	}
	
	$html  = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label for="' . esc_attr( $id ) . '">' . $label . '</label>' : '' );

	// Include inline Parsley JS validation data attributes.
	// Parsley doesn't have date validation built-in, so add as custom validator using Moment JS.
	$html .= '<input type="text" value="' . esc_attr( $default ) . '" class="sc-form-control sc-cf-date" name="sc_form_field[' . $id . ']" ';
	$html .= 'id="' . esc_attr( $id ) . '" value="' . esc_attr( $default ) . '" placeholder="' . esc_attr( $placeholder ) . '" ';
	$html .= ( ( $required === 'true') ? 'required' : '' ) . ' data-parsley-required-message="Please select a date.">';
	//$html .= 'data-parsley-date="true" data-parsley-date-message="Please select a valid date.">';

	$html .= '</div>'; //sc-form-group

	// Increment static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_date', 'sc_cf_date' );


/**
 * Shortcode to output a checkbox
 * 
 * @since 2.0.0
 */

function sc_cf_checkbox( $attr ) {
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'id'       => '',
					'label'    => '',
					'required' => 'false',
					'default'  => 'false'
				), $attr, 'stripe_date' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_checkbox_' . $counter;
	}
	
	$checked  = ( ( $default === 'true' || $default === 'checked' ) ? 'checked' : '' );
	
	$html  = '<div class="sc-form-group">';

	// Put <input type="checkbox"> inside of <lable> like Bootstrap 3.
	$html .= '<label>';

	$html .= '<input type="checkbox" id="' . esc_attr( $id ) . '" class="sc-cf-checkbox" name="sc_form_field[' . esc_attr( $id ) . ']" ';
	$html .= ( ( $required === 'true' ) ? 'required' : '' ) . ' ' . $checked . ' value="Yes" ';

	// Point to custom container for errors as checkbox fields aren't automatically placing it in the right place.
	$html .= 'data-parsley-errors-container="#sc_cf_checkbox_error_' . $counter . '">';

	// Actual label text.
	$html .= $label;

	$html .= '</label>';

	// Hidden field to hold a value to pass to Stripe payment record.
	$html .= '<input type="hidden" id="' . esc_attr( $id ) . '_hidden" class="sc-cf-checkbox-hidden" name="sc_form_field[' . esc_attr( $id ) . ']" value="No">';

	// Custom validation errors container for checkbox fields.
	// Needs counter ID specificity to match input above.
	$html .= '<div id="sc_cf_checkbox_error_' . $counter . '"></div>';

	$html .= '</div>'; //sc-form-group

	// Incrememnt static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_checkbox', 'sc_cf_checkbox' );


/**
 * Shortcode to output a number box
 * 
 * @since 2.0.0
 */
function sc_cf_number( $attr ) {
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'id'          => '',
					'label'       => '',
					'required'    => 'false',
					'placeholder' => '',
					'default'     => '',
					'min'         => '',
					'max'         => '',
					'step'        => ''
				), $attr, 'stripe_date' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_number_' . $counter;
	}
	
	$min = ( ! empty( $min ) ? 'min="' . $min . '" ' : '' );
	$max = ( ! empty( $max ) ? 'max="' . $max . '" ' : '' );
	$step = ( ! empty( $step ) ? 'step="' . $step . '" ' : '' );
	
	$html  = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label for="' . esc_attr( $id ) . '">' . $label . '</label>' : '' );

	// No Parsley JS number validation yet as HTML5 number type takes care of it.
    $html .= '<input type="number" class="sc-form-control sc-cf-number" id="' . esc_attr( $id ) . '" name="sc_form_field[' . $id . ']" ';
	$html .= 'placeholder="' . esc_attr( $placeholder ) . '" value="' . esc_attr( $default ) . '" ';
	$html .= $min . $max . $step . ( ( $required === 'true' ) ? 'required' : '' ) . '>';

	$html .= '</div>'; //sc-form-group
	
	// Incrememnt static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_number', 'sc_cf_number' );


/**************************************************************************************************************
 * Stripe User Entered Amount shortcode declarations
 *
 * @package SC_UEA
 * @author  Phil Derksen <pderksen@gmail.com>, Nick Young <mycorpweb@gmail.com>
 */
/**
 * Function to add the custom user amount textbox via shortcode
 * 
 * @since 2.0.0
 */
function sc_uea_amount( $attr ) {
	global $sc_script_options, $sc_options;
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'label'       => ( ! empty( $sc_options['sc_uea_label'] ) ? $sc_options['sc_uea_label'] : '' ),
					'placeholder' => '',
					'default'     => ''
				), $attr, 'stripe_amount' ) );

	$currency = strtoupper( $sc_script_options['script']['currency'] );

	$html  = '';
	$html .= '<div class="sc-form-group">';

	$html .= ( !empty( $label ) ? '<label for="sc_uea_custom_amount_' . $counter . '">' . $label . '</label>' : '' );
	$html .= '<div class="sc-uea-container">';

	// USD only: Show dollar sign on left of input.
	if ( $currency === 'USD' ) {
		$html .= '$ ';
	}

	//Stripe minimum amount allowed.
	$stripe_minimum_amount = 50;

	//Get amount to validate based on currency.
	$converted_minimum_amount = sc_stripe_to_decimal_amount( $stripe_minimum_amount, $currency );

	// Construct amount validation message.
	$minimum_amount_validation_msg = 'Please enter an amount equal to or more than ';

	// USD only: Show "50 cents" instead of "50" + currency code.
	// Non-USD: Format and show currency code on right.
	if ( $currency === 'USD' ) {
		$minimum_amount_validation_msg .= $stripe_minimum_amount . ' cents.';
	} else {
		// Format number with decimals depending on zero-decimal status.
		$minimum_amount_validation_msg .= sc_stripe_to_formatted_amount( $stripe_minimum_amount, $currency ) . ' ' . $currency . '.';
	}

	// Include inline Parsley JS validation data attributes.
	// http://parsleyjs.org/doc/index.html#psly-validators-list
    $html .= '<input type="text" class="sc-form-control sc-uea-custom-amount" name="sc_uea_custom_amount" ';
	$html .= 'id="sc_uea_custom_amount_' . $counter . '" value="' . esc_attr( $default ) . '" placeholder="' . esc_attr( $placeholder ) . '" ';
	$html .= 'required data-parsley-required-message="Please enter an amount." ';
	$html .= 'data-parsley-type="number" data-parsley-type-message="Please enter a valid amount." ';
	$html .= 'data-parsley-min="' . $converted_minimum_amount . '" data-parsley-min-message="' . $minimum_amount_validation_msg . '" ';

	// Point to custom container for errors so we can place the non-USD currencies on the right of the input box.
	$html .= 'data-parsley-errors-container="#sc_uea_custom_amount_errors_' . $counter . '">';

	// Non-USD: Show currency on right of input box.
	if ( $currency !== 'USD' ) {
		$html .= ' <span class="sc-uea-currency">' . $currency . '</span>';
	}

	// Custom validation errors container for UEA.
	// Needs counter ID specificity to match input above.
	$html .= '<div id="sc_uea_custom_amount_errors_' . $counter . '"></div>';

	$html .= '</div>'; //sc-uea-container
	$html .= '</div>'; //sc-form-group

	$counter++;
	
	if( ! isset( $_GET['charge'] ) ) {
		return apply_filters( 'sc_uea_shortcode_html', $html );
	}
	
	return '';
	
}
add_shortcode( 'stripe_amount', 'sc_uea_amount' );


/**
 * Shortcode to output a dropdown list
 * 
 * @since 2.0.0
 */
function sc_cf_dropdown( $attr ) {
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'id'          => '',
					'label'       => '',
					'default'     => '',
					'options'     => ''
				), $attr, 'stripe_dropdown' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_select_' . $counter;
	}
	
	$options = explode( ',', $options );
	
	$html  = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label for="' . esc_attr( $id ) . '">' . $label . '</label>' : '' );
	$html .= '<select class="sc-form-control sc-cf-dropdown" id="' . esc_attr( $id ) . '" name="sc_form_field[' . esc_attr( $id ) . ']">';
	
	$i = 1;
	foreach( $options as $option ) {
		
		$option = trim( $option );
		
		$html .= '<option ' . ( $default == $option ? 'selected' : '' ) . '>' . $option . '</option>';
		$i++;
	}
	
	$html .= '</select>';
	$html .= '</div>'; //sc-form-group
	
	// Incrememnt static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_dropdown', 'sc_cf_dropdown' );

/**
 * Shortcode to output a number box
 * 
 * @since 2.0.0
 */
function sc_cf_radio( $attr ) {
	
	STATIC $counter = 1;
	
	extract( shortcode_atts( array(
					'id'          => '',
					'label'       => '',
					'default'     => '',
					'options'     => ''
				), $attr, 'stripe_radio' ) );
	
	// Check for ID and if it doesn't exist then we will make our own
	if( $id == '' ) {
		$id = 'sc_cf_radio_' . $counter;
	}
	
	$options = explode( ',', $options );
	
	$html = '<div class="sc-form-group">';

    $html .= ( ! empty( $label ) ? '<label>' . $label . '</label>' : '' );

	$html .= '<div class="sc-radio-group">';
	
	$i = 1;
	foreach( $options as $option ) {
		
		$option = trim( $option );
		
		if( empty( $default ) ) {
			$default = $option;
		}
		
		//$html .= '<input type="radio" value="' . esc_attr( $option ) . '" name="sc_form_field[' . esc_attr( $id ) . ']" class="' . 
		//		esc_attr( $id ) . '_' . $i . '" ' . ( $default == $option ? 'checked' : '' ) . '>' . $option . '<br>';
		
		$html .= '<label title="' . esc_attr( $option ) . '">';
		$html .= '<input type="radio" name="sc_form_field[' . esc_attr( $id ) . ']" value="' . esc_attr( $option ) . '" ' . ( $default == $option ? 'checked' : '' ) . 
				' class="' . esc_attr( $id ) . '_' . $i . '" data-parsley-errors-container=".sc-form-group">';
		$html .= '<span>' . $option . '</span>';
		$html .= '</label>';
		
		$i++;
	}
	
	$html .= '</div>'; //sc-radio-group
	$html .= '</div>'; //sc-form-group
	
	// Incrememnt static counter
	$counter++;
	
	return $html;
}
add_shortcode( 'stripe_radio', 'sc_cf_radio' );

