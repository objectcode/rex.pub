<?php

/**
 * Plugin Name: Google Profile Avatars
 * Plugin URI: http://wp-glogin.com/google-profile-avatars
 * Description: Display users' Google profile image as their avatar in WordPress blog posts and admin. Extends Google Apps Login plugin so no extra auth is required.
 * Version: 1.1
 * Author: Dan Lester
 * Author URI: http://wp-glogin.com/
 * Network: true
 * License: Premium paid
 * 
 * Do not copy, modify, or redistribute without authorization from author Lesterland Ltd (contact@wp-glogin.com)
 * 
 * You need to have purchased a license to install this software on one website.
 * 
 * You are not authorized to use, modify, or distribute this software beyond the single site license that you
 * have purchased.
 * 
 * You must not remove or alter any copyright notices on any and all copies of this software.
 * 
 * This software is NOT licensed under one of the public "open source" licenses you may be used to on the web.
 * 
 * For full license details, and to understand your rights, please refer to the agreement you made when you purchased it 
 * from our website at https://wp-glogin.com/
 * 
 * THIS SOFTWARE IS SUPPLIED "AS-IS" AND THE LIABILITY OF THE AUTHOR IS STRICTLY LIMITED TO THE PURCHASE PRICE YOU PAID 
 * FOR YOUR LICENSE.
 * 
 * Please report violations to contact@wp-glogin.com
 * 
 * Copyright Lesterland Ltd, registered company in the UK number 08553880
 * 
 */


class google_profile_avatars {
	
	// Singleton
	private static $instance = null;
	
	public static function get_instance() {
		if (null == self::$instance) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	protected function __construct() {
		$this->add_actions();
	}
	
	protected function add_actions() {
		add_action('gal_user_loggedin', Array($this, 'gpa_user_loggedin'), 10, 5);
		add_filter('get_avatar', Array($this, 'gpa_get_avatar'), 10, 5); 
		
		if (is_admin()) {
			add_action( 'admin_init', Array($this, 'gdm_admin_init'), 5, 0 );
			add_filter( 'gal_avatar_source_desc', Array($this, 'gpa_avatar_source_desc'), 10, 2);
		}

		// BuddyPress filters
		add_filter('bp_core_fetch_avatar', Array($this, 'gpabp_core_fetch_avatar'), 10, 9);
		add_filter('bp_core_fetch_avatar_url', Array($this, 'gpabp_core_fetch_avatar_url'), 10, 2);
	}
	
	public function gpa_user_loggedin($wp_user, $google_userinfo, $wp_userisnew, $google_client, $google_oauth2service) {
		// Save picture URL
		if (property_exists($google_userinfo, 'picture') && $google_userinfo->picture != '') {
			update_user_meta($wp_user->ID, 'gpa_user_avatar', $google_userinfo->picture);
		}
	}
	
	public function gpa_get_avatar($avatar, $id_or_email, $size, $default, $alt) {
		
		if (is_object($id_or_email)) {
			// Comment or post
			$id_or_email = (int) $id_or_email->user_id;
		} else if (is_string($id_or_email)) { 
			// Try email
			$user = get_user_by('email',$id_or_email);
			if ($user) {
				$id_or_email = $user->ID;
			}
		}
		
		if (is_numeric($id_or_email)) {
			$google_picture = get_user_meta($id_or_email, 'gpa_user_avatar', true);
			if ($google_picture) {
				$safe_alt = false === $alt ? '' : esc_attr( $alt );
				$avatar = "<img alt='{$safe_alt}' src='{$google_picture}' class='avatar avatar-{$size} photo avatar-default' height='{$size}' width='{$size}' />";
			}
		}
		return $avatar;
	}
	
	public function gdm_admin_init() {
		// Check Google Apps Login is configured - display warnings if not
		if (apply_filters('gal_get_clientid', '') == '') {
			add_action('admin_notices', Array($this, 'gpa_admin_auth_message'));
			if (is_multisite()) {
				add_action('network_admin_notices', Array($this, 'gpa_admin_auth_message'));
			}
		}
	}
	
	public function gpa_admin_auth_message() {
		?>
		<div class="error">
        	<p>You will need to install and configure 
        		<a href="http://wp-glogin.com/?utm_source=Admin%20Configmsg&utm_medium=plugin&utm_campaign=Avatars" 
        		target="_blank">Google Apps Login</a>  
        		plugin in order for Google Profile Avatars to work. (Requires v2.4+ of Free or Professional)
        	</p>
    	</div> <?php
	}

	public function gpa_avatar_source_desc($avatar_source_desc, $wp_user) {
		if (is_object($wp_user) && property_exists($wp_user, 'ID')) {
			$google_picture = get_user_meta($wp_user->ID, 'gpa_user_avatar', true);
			if ($google_picture) {
				$avatar_source_desc = 'Currently using your Google account profile photo - change <a href="https://www.google.com/settings">here</a>. '
 							.'You will then need to log out of this site, then log in again using the \'Login with Google\' button.';
			}
			else {
				$avatar_source_desc = 'Currently using default <a href="http://gravatar.com/">Gravatar</a> photo.'
 						.' To use your Google account profile photo instead, log out, then log in again using the \'Login with Google\' button.';
			}
		}
		
		return $avatar_source_desc;
	}

	// BuddyPress filters
	public function gpabp_core_fetch_avatar($gravatar_html, $params, $item_id, $avatar_dir, $css_id, $html_width, $html_height, $avatar_folder_url, $avatar_folder_dir) {
		$google_picture = $this->gpabp_get_user_image_url($item_id);
		if ($google_picture) {
			$gravatar_html = preg_replace('~img src=\"[^\"]+\"~', 'img src="'.$google_picture.'"', $gravatar_html);
		}
		return $gravatar_html;
	}

	public function gpabp_core_fetch_avatar_url($gravatar_url, $params) {
		if (isset($params['item_id'])) {
			$google_picture = $this->gpabp_get_user_image_url($params['item_id']);
			if ($google_picture) {
				$gravatar_url = preg_replace('~[^\"]+~', $google_picture, $gravatar_url);
			}
		}
		return $gravatar_url;
	}

	protected function gpabp_get_user_image_url($id_or_email) {
		if (is_numeric($id_or_email)) {
			$google_picture = get_user_meta($id_or_email, 'gpa_user_avatar', true);
			if ($google_picture) {
				return $google_picture;
			}
		}
		return '';
	}

}


// Global accessor function to singleton
function GoogleProfileAvatar() {
	return google_profile_avatars::get_instance();
}

// Initialise at least once
GoogleProfileAvatar();

?>
