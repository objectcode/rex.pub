<?php
class Adp_Requirements_Error
{
	public static function init()
	{
		if (current_user_can('activate_plugins'))
		{
			add_action('admin_notices', array(__CLASS__, 'notice'));
		}
	}

	public static function notice()
	{
		printf('<div class="error"><p>Analytics Dashboard plugin isn\'t working correctly. Your PHP version is %s, while minimum 5.3 is required.</p></div>', PHP_VERSION);
	}
}