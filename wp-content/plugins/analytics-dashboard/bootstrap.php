<?php
/*
Plugin Name: Analytics Dashboard
Plugin URI:  http://analytics-dashboard.co
Description: Analytics Dashboard displays in one place all essential metrics and statistics of your blog, Twitter Profile and Facebook Fan Page. It allows you to monitor posts & comments statistics, authors activity and social media popularity.
Version:     1.0.1
Author:      Woj
Author URI:  mailto:woj@analytics-dashboard.co
*/

// Direct access is forbidden
if (!defined('WPINC'))
{
	exit;
}

// Check required PHP version
if (version_compare(PHP_VERSION, '5.3', '<'))
{
	require_once dirname( __FILE__ ) . '/requirements-error.php';
	add_action('admin_init', array('Adp_Requirements_Error', 'init'));
}
else  // Load plugin only when requirements are met
{

	// Require main plugin files
	require_once (plugin_dir_path( __FILE__ ) . 'plugin/cron_actions.php');
	require_once (plugin_dir_path( __FILE__ ) . 'plugin/analytics_dashboard.php');

	// Ensure the main class exists
	if (class_exists('Analytics_Dashboard'))
	{

		// Hook activating plugin
		function adp_activate()
		{
			Analytics_Dashboard::activate();
		}

		// Hook deactivating plugin
		function adp_deactivate()
		{
			Analytics_Dashboard::deactivate();
		}	

		// Initialize the plugin
		function adp_init()
		{
			$plugin = new Analytics_Dashboard(plugin_dir_path(__FILE__), plugins_url('', __FILE__));
			$plugin->init();
		}

		// Init plugin and (de)activation hooks
		adp_init();
		register_activation_hook(__FILE__, 'adp_activate');
		register_deactivation_hook( __FILE__, 'adp_deactivate');
	}
}