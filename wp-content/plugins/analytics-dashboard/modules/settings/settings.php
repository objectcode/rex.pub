<?php
/**
 * 
 * Settings module, display and save settings form
 * 
 */
class Adp_Settings extends Adp_Module_Admin implements Adp_Module_Interface
{
	/**
	 * Called on module initialization
	 */
	public function init()
	{		
		// Register page in Wordpress
		$this->register_page(array(
			'parent' => 'options-general.php',
			'title' => 'Analytics Dashboard Settings',
			'menu' => 'Analytics Dashboard',
			'capability' => 'manage_options'
		));

		// Register Wordpress settings
		add_action('admin_init', array($this, 'init_settings'));

		// Add link to settings directly on Plugins page
		add_filter('plugin_action_links_analytics-dashboard/bootstrap.php', array($this, 'plugin_settings_link'));

	}	

	/**
	 * Initialize settings fields in Wordpress
	 */
	public function init_settings()
	{
		// Register setting group
		register_setting('adp_settings', 'adp_settings', array($this, 'sanitize_setting'));

		// Register section for social media profiles
		add_settings_section('adp_settings_social', 'Social media profiles', array($this, 'section_social_callback'), 'adp_settings');

		// Twitter username
		add_settings_field('twitter_username', 'Twitter username', array($this, 'render_setting'), 'adp_settings', 'adp_settings_social', array('id'=>'twitter_username', 'desc'=>'Just the profile username (without https://twitter.com/). Example: WordPress'));

		// Facebook (Fan Page) username
		add_settings_field('facebook_username', 'Facebook username', array($this, 'render_setting'), 'adp_settings', 'adp_settings_social', array('id'=>'facebook_username', 'desc'=>'Just the Fan Page name (without https://facebook.com/). Example: WordPress'));

		// Register section for Facebook API
		add_settings_section('adp_settings_facebook_api', 'Facebook API settings', array($this, 'section_facebook_api_callback'), 'adp_settings');		

		// Facebook App ID
		add_settings_field('facebook_app_id', 'Facebook App ID', array($this, 'render_setting'), 'adp_settings', 'adp_settings_facebook_api', array('id'=>'facebook_app_id', 'desc'=>''));

		// Facebook App Secret
		add_settings_field('facebook_app_secret', 'Facebook App Secret', array($this, 'render_setting'), 'adp_settings', 'adp_settings_facebook_api', array('id'=>'facebook_app_secret', 'desc'=>''));

	}

	/**
	 * Render form for settings field
	 */
	public function render_setting($args)
	{
		extract($args);

		// Read current option
		$options = get_option('adp_settings');

		if (!isset($options[$id]))
		{
			$options[$id] = '';
		}

		// Display input
		echo "<input class='regular-text' type='text' id='label_$id' name='adp_settings[" . $id . "]' value='".esc_attr($options[$id])."' />";  
		echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
	}

	/**
	 * Add link to settings page
	 */
	public function plugin_settings_link($links)
	{
		$link = '<a href="options-general.php?page=analytics-dashboard">Settings</a>'; 
  		array_unshift($links, $link); 
  		return $links; 
	}

	/**
	 * Callback function called by Wordpress when sending form
	 * We delete previous stats when username has changed
	 */
	public function sanitize_setting($data)
	{
		
		// Take the last part after slash (get rid of twitter.com/ or facebook.com/)
		$data['twitter_username'] = explode('/', $data['twitter_username']);
		$data['twitter_username'] = end($data['twitter_username']);
		$data['facebook_username'] = explode('/', $data['facebook_username']);
		$data['facebook_username'] = end($data['facebook_username']);

		// Replace non-supported characters
		$data['twitter_username'] = preg_replace("/[^a-zA-Z0-9_\-\.]/", "", $data['twitter_username']);
		$data['facebook_username'] = preg_replace("/[^a-zA-Z0-9_\-\.]/", "", $data['facebook_username']);

		// Trim user values
		$data['twitter_username'] = trim($data['twitter_username']);
		$data['facebook_username'] = trim($data['facebook_username']);

		// Read current options
		$options = get_option('adp_settings');
		$twitter_username = (!isset($options['twitter_username']) || empty($options['twitter_username'])) ? false : $options['twitter_username'];
		$facebook_username = (!isset($options['facebook_username']) || empty($options['facebook_username'])) ? false : $options['facebook_username'];

		// If Twitter username has changed
		if ($twitter_username != $data['twitter_username'])
		{
			$twitter = new Adp_Twitter();
			$twitter->reset_stats();

			if (!empty($data['twitter_username']))
			{
				$twitter->set_username($data['twitter_username']);
				$twitter->update_today_stats();
			}
		}

		// If Facebook username has changed
		if ($facebook_username != $data['facebook_username'])
		{
			$facebook = new Adp_Facebook();
			$facebook->reset_stats();
			if (!empty($data['facebook_username']))
			{						
				$facebook->set_username($data['facebook_username']);
				$facebook->update_today_stats();
			}
		}		

		return $data;
	}

	/**
	 * Called when module page is being accessed
	 */
	public function load()
	{
		// Nothing here - Form handling is done by Wordpress itself
		// See view file for more details
	}

	/**
	 * Called when module page is being rendered
	 */	
	public function render()
	{

		// Prepare data which will be passed to the view
		$view_data = array(
			'title' => $this->options['title'],
			'view' => $this->options['module'].'/'.$this->options['module'].'.view.php',
		);

		extract($view_data);
		include($this->options['path'] . 'views/layout.php');	
	}		

	/**
	 * Responsible for displaying Facebook API section
	 */	
	public function section_social_callback($args)
	{
		echo 'Please notice that changing Twitter or Facebook usernames will reset existing statistics data. For accurate results data should be gathered for minimum 1 week. Plugin automatically updates statistics 2 times per day.';
	}	

	/**
	 * Responsible for displaying Facebook API section
	 */	
	public function section_facebook_api_callback($args)
	{
		echo 'In order to track Fan Page statistics, it\'s required to have an app registered in Facebook API. You can use existing app or create new one using <a href="https://developers.facebook.com/quickstarts/?platform=web" target="blank">this url</a> (use "Skip and Create App ID" button).';
	}
}