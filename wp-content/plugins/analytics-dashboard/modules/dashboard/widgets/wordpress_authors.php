<?php
/**
 * 
 * Widget displaying top authors
 *  
 */
class Adp_Widget_Wordpress_Authors extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Top Authors';

	/**
	 * Function called when widget is initialized
	 */	
	public function init()
	{
		$this->assign('data', $this->generate_data());
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		global $wpdb;

		// Get authors posts count data from Wordpress DB
		$query = $wpdb->prepare(
			'SELECT COUNT(*) as posts_count, post_author, u.user_email, u.display_name, u.user_nicename FROM '.$wpdb->posts.' p
			LEFT JOIN '.$wpdb->users.' u ON p.post_author = u.ID
			WHERE post_type="post"
			AND post_status="publish"
			AND DATE(post_date)>="%s"
			AND DATE(post_date)<="%s"
			GROUP BY post_author
			ORDER BY posts_count DESC LIMIT 5',
			$this->date_range->start->format('Y-m-d'),
			$this->date_range->end->format('Y-m-d')
		);

		$results = $wpdb->get_results($query);

		// Error while executing query
		if (!$results)
		{
			return;
		}

		// Prepare all data used by the widget
		$data = array();
		foreach ($results as $user)
		{
			// Add URL to author's profile
			$user->url = get_author_posts_url($user->post_author, $user->user_nicename);
		}

		$data['users'] = $results;	
		return $data;
	}	
}