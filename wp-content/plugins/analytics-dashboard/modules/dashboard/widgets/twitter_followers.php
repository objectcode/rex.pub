<?php
/**
 * 
 * Widget displaying Twitter followers statistics
 *  
 */
class Adp_Widget_Twitter_Followers extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Twitter Followers';

	/**
	 * Function called when widget is initialized
	 */
	public function init()
	{
		$twitter = new Adp_Twitter();
		if ($twitter->get_username())
		{
			$this->assign('data', $this->generate_data());
		}		
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		// Prepare empty data for chart
		$dates = array();
		foreach ( $this->date_range->period as $day )
		{
			$dates[] = $day->format("Y-m-d");
		}

		// Load Twitter data
		$twitter = new Adp_Twitter();
		$stats = $twitter->get_stats();

		// Populate chart's data with followers count
		$values = array();
		foreach ($stats as $key => $row)
		{
			$values[$key] = $row;
		}		

		// Prepare data for the chart
		$chart_data = array();
		foreach ($dates as $day)
		{	
			$day = new DateTime($day);
			$chart_data['labels'][] = $day->format("d.m");
			$chart_data['series'][0][] = (int) (isset($values[$day->format("Y-m-d")]['followers'])) ? $values[$day->format("Y-m-d")]['followers'] : 0;
		}

		// Check if followers count in selected period is > 0 (if we have any data)
		if (array_sum($chart_data['series'][0]) == 0)
		{
			return false;
		}

		// The newest data to show
		$today_stats = end($stats);

		// We want to compare the newest data with the one from end of previous period
		$previous_day = $this->date_range->previous_end->format('Y-m-d');

		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		$data['followers'] = (int) $today_stats['followers'];
		$data['following'] = (int) $today_stats['following'];
		$data['previous_followers'] = (int) (isset($values[$previous_day]['followers'])) ? $values[$previous_day]['followers'] : 0;	
		$data['previous_following'] = (int) (isset($values[$previous_day]['following'])) ? $values[$previous_day]['following'] : 0;	
		$data['followers_diff'] = $data['followers'] - $data['previous_followers'];
		$data['following_diff'] = $data['following'] - $data['previous_following'];
		$data['twitter_profile'] = 'http://twitter.com/'.$twitter->get_username();

		return $data;
	}

}