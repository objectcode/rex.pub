<?php
/**
 * 
 * Widget displaying Fan Page likes statistics
 *  
 */
class Adp_Widget_Facebook_Likes extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Fan Page likes';

	/**
	 * Function called when widget is initialized
	 */
	public function init()
	{
		$facebook = new Adp_Facebook();
		if ($facebook->get_username())
		{
			$this->assign('data', $this->generate_data());
		}		
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		// Prepare empty data for chart
		$dates = array();
		foreach ( $this->date_range->period as $day )
		{
			$dates[] = $day->format("Y-m-d");
		}

		// Load Facebook data
		$facebook = new Adp_Facebook();
		$stats = $facebook->get_stats();

		// Populate chart's data with likes count
		$values = array();
		foreach ($stats as $key => $row)
		{
			$values[$key] = $row;
		}		

		// Prepare data for the chart
		$chart_data = array();
		foreach ($dates as $day)
		{	
			$day = new DateTime($day);
			$chart_data['labels'][] = $day->format("d.m");
			$chart_data['series'][0][] = (int) (isset($values[$day->format("Y-m-d")]['likes'])) ? $values[$day->format("Y-m-d")]['likes'] : 0;
		}

		// Check if likes count in selected period is > 0 (if we have any data already)
		if (array_sum($chart_data['series'][0]) == 0)
		{
			return false;
		}

		// The newest data to show
		$today_stats = end($stats);

		// We want to compare the newest data with the one from end of previous period
		$previous_day = $this->date_range->previous_end->format('Y-m-d');

		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		$data['likes'] = (int) $today_stats['likes'];
		$data['talking'] = (int) $today_stats['talking'];
		$data['previous_likes'] = (int) (isset($values[$previous_day]['likes'])) ? $values[$previous_day]['likes'] : 0;	
		$data['previous_talking'] = (int) (isset($values[$previous_day]['talking'])) ? $values[$previous_day]['talking'] : 0;	
		$data['likes_diff'] = $data['likes'] - $data['previous_likes'];
		$data['talking_diff'] = $data['talking'] - $data['previous_talking'];
		$data['facebook_profile'] = 'https://www.facebook.com/'.$facebook->get_username();

		return $data;
	}

}