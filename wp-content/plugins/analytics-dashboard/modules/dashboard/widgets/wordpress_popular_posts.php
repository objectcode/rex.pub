<?php
/**
 * 
 * Widget displaying popular posts (most commented ones)
 *  
 */
class Adp_Widget_Wordpress_Popular_Posts extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Most commented posts';

	/**
	 * Function called when widget is initialized
	 */	
	public function init()
	{
		$this->assign('data', $this->generate_data());
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		global $wpdb;

		// Get posts count data from Wordpress DB
		$query = $wpdb->prepare(
			'SELECT ID as id, post_title as title, comment_count as comments FROM '.$wpdb->posts.' WHERE post_type="post" AND post_status="publish"
			AND DATE(post_date)>="%s"
			AND DATE(post_date)<="%s"
			AND comment_count > 0
			ORDER BY comment_count DESC, post_date DESC LIMIT 5',
			$this->date_range->start->format('Y-m-d'),
			$this->date_range->end->format('Y-m-d')
		);

		// Prepare all data used by the widget
		$data = array();
		$data['posts'] = $wpdb->get_results($query);

		foreach ($data['posts'] as $post)
		{
			// Add post's permalink
			$post->url = get_permalink($post->id);

			// Add post's thumbnail
			$post->thumb = get_the_post_thumbnail($post->id, 'thumbnail');
		}

		return $data;
	}

}