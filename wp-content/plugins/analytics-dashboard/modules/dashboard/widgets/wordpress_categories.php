<?php
/**
 * 
 * Widget displaying categories with most posts
 *  
 */
class Adp_Widget_Wordpress_Categories extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Top Categories';

	/**
	 * Function called when widget is initialized
	 */
	public function init()
	{
		$this->assign('data', $this->generate_data());
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		global $wpdb;
		
		// Get posts count data from Wordpress DB
		$query = $wpdb->prepare(
			'SELECT COUNT(*) as posts_count, c.name, c.slug FROM '.$wpdb->term_relationships.' r
				LEFT JOIN '.$wpdb->term_taxonomy.' t ON r.term_taxonomy_id = t.term_taxonomy_id
				LEFT JOIN '.$wpdb->posts.' p ON r.object_id = p.ID
				LEFT JOIN '.$wpdb->terms.' c ON t.term_id = c.term_id
				WHERE t.taxonomy = "category"
				AND post_type="post"
				AND post_status="publish"
				AND DATE(post_date)>="%s" AND DATE(post_date)<="%s"
				GROUP BY c.term_id
				ORDER BY posts_count DESC, c.name ASC
				LIMIT 8',
			$this->date_range->start->format('Y-m-d'),
			$this->date_range->end->format('Y-m-d')
		);
		$results = $wpdb->get_results($query);

		// Error while executing query
		if (!$results)
		{
			return;
		}		

		// Prepare data for the chart
		$chart_data = array();
		foreach ($results as $row)
		{	
			$chart_data['labels'][] = (strlen($row->name) > 15 ) ? substr($row->name, 0, 15).'...' : $row->name;
			$chart_data['series'][0][] = (int) $row->posts_count;
		}

		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		$data['categories_count'] = count($results);

		return $data;
	}

}