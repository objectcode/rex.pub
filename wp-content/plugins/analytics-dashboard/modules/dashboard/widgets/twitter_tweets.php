<?php
/**
 * 
 * Widget displaying Tweets statistics
 *  
 */
class Adp_Widget_Twitter_Tweets extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Tweets';

	/**
	 * Function called when widget is initialized
	 */
	public function init()
	{
		$twitter = new Adp_Twitter();
		if ($twitter->get_username())
		{
			$this->assign('data', $this->generate_data());
		}		
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		// Load Twitter data
		$twitter = new Adp_Twitter();
		$stats = $twitter->get_stats();

		if (!isset($stats) || empty($stats) || count($stats)<2)
		{
			return array();
		}

		// Prepare empty data for chart
		$dates = array();
		foreach ( $this->date_range->period as $day )
		{
			$dates[] = $day->format("Y-m-d");
		}


		// We need additional (previous) 1 day to calculate difference for 1st day in range
		$previous_day = clone $this->date_range->start;
		$previous_day->modify('-1 day');
		$previous_tweets = (isset($stats[$previous_day->format('Y-m-d')])) ? $stats[$previous_day->format('Y-m-d')]['tweets'] : false;

		// Populate chart's data with followers count
		$values = array();
		foreach ($stats as $key => $row)
		{
			if ($previous_tweets !== false) // we have data from previous day
			{
				$values[$key] = $row['tweets'] - $previous_tweets;
			}

			$previous_tweets = $row['tweets'];
		}		

		// Prepare data for the chart
		$chart_data = array();
		foreach ($dates as $day)
		{	
			$day = new DateTime($day);
			$chart_data['labels'][] = $day->format("d.m");
			$chart_data['series'][0][] = (int) (isset($values[$day->format("Y-m-d")])) ? $values[$day->format("Y-m-d")] : 0;
		}

		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		return $data;
	}

}