<?php
/**
 * 
 * Widget displaying posts statistics
 *  
 */
class Adp_Widget_Wordpress_Posts extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Posts';

	/**
	 * Function called when widget is initialized
	 */	
	public function init()
	{
		$this->assign('data', $this->generate_data());
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		global $wpdb;

		// Get posts count data from Wordpress DB
		$query = $wpdb->prepare(
			'SELECT COUNT(*) as posts_count, DATE(post_date) as date FROM '.$wpdb->posts.' WHERE post_type="post" AND post_status="publish"
			AND DATE(post_date)>="%s"
			AND DATE(post_date)<="%s"
			GROUP BY date
			ORDER BY date ASC',
			$this->date_range->previous_start->format('Y-m-d'),
			$this->date_range->end->format('Y-m-d')
		);

		$results = $wpdb->get_results($query);

		// Error while executing query
		if (!$results)
		{
			// Return empty data			
			$data = array();
			$data['total'] = 0;
			$data['average'] = 0;
			$data['previous_total'] = 0;
			$data['previous_average'] = 0;
			$data['total_diff'] = 0;
			$data['average_diff'] = 0;
			return $data;
		}
		
		// Prepare empty data for chart
		$dates = array();
		foreach ( $this->date_range->period as $day )
		{
			$dates[] = $day->format("Y-m-d");
		}

		// Populate chart's data with posts count
		$values = array();
		foreach($results as $row)
		{
			$values[$row->date] =  (int) $row->posts_count;
		}

		// Prepare data for the chart
		$chart_data = array();
		foreach ($dates as $day)
		{	
			$day = new DateTime($day);
			$previous_day = clone $day;
			$previous_day = $previous_day->modify('-'.$this->date_range->days.' days');
			$chart_data['labels'][] = ($this->date_range->days <= 7) ? mb_strtoupper($day->format("D")) : $day->format("d.m");
			$chart_data['series'][0][] = (int) (isset($values[$previous_day->format("Y-m-d")])) ? $values[$previous_day->format("Y-m-d")] : 0 ;
			$chart_data['series'][1][] = (int) (isset($values[$day->format("Y-m-d")])) ? $values[$day->format("Y-m-d")] : 0;
		}

		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		$data['total'] = array_sum($chart_data['series'][1]);
		$data['average'] = round($data['total'] / $this->date_range->days);
		$data['previous_total'] = array_sum($chart_data['series'][0]);
		$data['previous_average'] = round($data['previous_total'] / $this->date_range->days);
		$data['total_diff'] = $data['total'] - $data['previous_total'];
		$data['average_diff'] = $data['average'] - $data['previous_average'];

		return $data;
	}

}