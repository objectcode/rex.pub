<?php
/**
 * 
 * Widget displaying comments statistics
 *  
 */
class Adp_Widget_Wordpress_Comments extends Adp_Widget
{
	/**
	 * Widget's title
	 */
	public $title = 'Comments';

	/**
	 * Function called when widget is initialized
	 */
	public function init()
	{
		$this->assign('data', $this->generate_data());
	}

	/**
	 * Prepare data required by the widget
	 */
	private function generate_data()
	{
		global $wpdb;

		// Get comments count data from Wordpress DB
		$query = $wpdb->prepare(
			'SELECT COUNT(*) as comments_count, DATE(comment_date) as date FROM '.$wpdb->comments.' WHERE comment_approved="1"
			AND DATE(comment_date)>="%s"
			AND DATE(comment_date)<="%s"
			GROUP BY date
			ORDER BY date ASC',
			$this->date_range->start->format('Y-m-d'),
			$this->date_range->end->format('Y-m-d')
		);
		$results = $wpdb->get_results($query);

		// Error while executing query
		if (!$results)
		{
			// Return empty data
			$data = array();
			$data['total'] = 0;
			$data['average'] = 0;
			return $data;
		}
		
		// Prepare empty data for chart
		$dates = array();
		foreach ( $this->date_range->period as $day )
		{
			$dates[] = $day->format("Y-m-d");
		}

		// Populate chart's data with comments count
		$values = array();
		foreach($results as $row)
		{
			$values[$row->date] =  (int) $row->comments_count;
		}

		// Prepare data for the chart
		$chart_data = array();
		foreach ($dates as $key => $day)
		{	
			$day = new DateTime($day);
			$chart_data['labels'][] = $key;
			$chart_data['series'][0][] = (int) (isset($values[$day->format("Y-m-d")])) ? $values[$day->format("Y-m-d")]: 0;
		}

		// Count spam comments
		$spam_count = (int) $wpdb->get_var(
			$wpdb->prepare(
				'SELECT COUNT(*) FROM '.$wpdb->comments.' WHERE comment_approved="spam" AND DATE(comment_date)>="%s" AND DATE(comment_date)<="%s"',
				$this->date_range->start->format('Y-m-d'),
				$this->date_range->end->format('Y-m-d')
			)
		);
	
		// Prepare all data used by the widget
		$data = array();
		$data['chart'] = $chart_data;
		$data['total'] = (int) array_sum($chart_data['series'][0]);
		$data['average'] = (int) round($data['total'] / $this->date_range->days);
		$data['spam'] = (int) $spam_count;
		$data['approved_rate'] = (int) round(100 * $data['total'] / ($data['total']+$data['spam']));
		$data['spam_rate'] = (int) 100-$data['approved_rate'];
		
		return $data;
	}
}