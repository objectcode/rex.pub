<?php
include 'date_range.php';
include 'twitter.php';
include 'facebook.php';
include 'dashboard_widget.php';

/**
 * 
 * Dashboard module, displaying all statistics
 * 
 */
class Adp_Dashboard extends Adp_Module_Admin implements Adp_Module_Interface
{
	/**
	 * Collection of dashboard widgets
	 */
	private $widgets = array();

	/**
	 * Date Range object
	 */
	private $date_range;
	
	/**
	 * Called on module initialization
	 */
	public function init()
	{		
		$this->date_range = new Adp_Date_Range();

		// Register page in Wordpress
		$this->register_page(array(
			'parent' => 'index.php',
			'title' => 'Analytics Dashboard',
			'menu' => 'Analytics',
			'capability' => 'publish_posts'
		));

		// Include all dashboard widgets
		$this->register_widgets();

		// Register screen options for date range
		$this->date_range->register();

	}

	/**
	 * Called when module page is being accessed
	 */
	public function load()
	{
		// Load Wordpress JS script for boxed layout		
		$this->enqueue_script('postbox');

		// Load library for charts (chartist)
		$this->enqueue_script('chartist', '//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js');
		$this->enqueue_style('chartist', '//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css');

		// Load Dashboard JS script
		$this->enqueue_script('adp_dashboard', 'dashboard.js', array('jquery', 'postbox'));

		// Load font with icons (font-awesome)
		$this->enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

		// Load Dashboard CSS styles
		$this->enqueue_style('analytics-dashboard', 'analytics-dashboard.css');

		// Show Wordpress Screen Options for layout configuration
		add_screen_option('layout_columns', array('max' => 2, 'default' => 2));

		// Show Wordpress Screen Options for date range configuration
		$this->date_range->display();

		// Register box for each registered widget
		add_action('add_meta_boxes', function() {
			foreach ($this->widgets as $key => $widget)
			{
				add_meta_box('Adp_widget_'.$key, $widget->getTitle(), array($widget, 'render'), null, (($key%2==0) ? 'normal' : 'side') , 'high');
			}
		});
    	
    	// Run WP boxed layout action
    	do_action('add_meta_boxes', null, null);
	}

	/**
	 * Called when module page is being rendered
	 */	
	public function render()
	{

		// Prepare data which will be passed to the view
		$view_data = array(
			'title' => $this->options['title'],
			'date_range' => $this->date_range->get(),
			'view' => $this->options['module'].'/'.$this->options['module'].'.view.php',
		);

		extract($view_data);
		include($this->options['path'] . 'views/layout.php');	
	}	

	/**
	 * Register all widgets
	 */
	private function register_widgets()
	{
		// Scan directory and include all widgets
		$widgets_list_file = dirname(__FILE__) . '/widgets.php';
		
		if (!file_exists($widgets_list_file))
		{
			return false;
		}

		include($widgets_list_file);

		$widgets_path = dirname(__FILE__).'/widgets/';

		foreach ($widgets as $widget)
		{
   			// Include widget file
       		include($widgets_path . $widget);

       		// Initialize object and save it in the container
       		$class_name = $this->guess_widget_class_name($widget);        		
       		$widget_obj = new $class_name($this->options['path'], $widget);
       		if ($widget_obj instanceof Adp_Widget)
       		{
       			$this->widgets[] = $widget_obj;
       		}
		}
	}

	/**
	 * Guess widget class name based on its filename
	 */
	private function guess_widget_class_name($file_name)
	{
		$file_name = str_replace(array('.php', '_'), array('', ' '), $file_name);
		$file_name = ucwords($file_name);
		$class_name = 'Adp_Widget_';
		$class_name .= str_replace(' ', '_', $file_name);
		return $class_name;
	}
	
}