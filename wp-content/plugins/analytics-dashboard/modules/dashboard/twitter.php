<?php 
/**
 * 
 * Class responsible for loading basic stats for Twitter profiles
 * Uses YQL from Yahoo
 * 
 */
class Adp_Twitter
{
	/**
	 * Key in wp_options table where data is stored
	 */
	private $option_name = 'adp_twitter_stats';

	/**
	 * Twitter profile username
	 */			
	private $username;

	public function __construct()
	{
		// Get current username from settings
		$options = get_option('adp_settings');
		$this->username = (!isset($options['twitter_username']) || empty($options['twitter_username'])) ? false : $options['twitter_username'];
	}

	/**
	 * Get username
	 */
	public function get_username()
	{
		return $this->username;
	}

	/**
	 * Set username
	 */
	public function set_username($username)
	{
		$this->username = $username;
	}	

	/**
	 * Make request to get current stats
	 */
	public function load_current_stats()
	{
		if ($this->username === false) return false;

		// Twitter profile URL
		$profile_url = sprintf('https://twitter.com/%s', $this->username);

		// YQL query to get data (https://developer.yahoo.com/yql/)
		$yql = 'SELECT title from html where url="'.$profile_url.'" AND
				xpath="//li[contains(@class,\'ProfileNav-item--followers\')]/a|
					   //li[contains(@class,\'ProfileNav-item--following\')]/a|
				       //li[contains(@class,\'ProfileNav-item--tweets\')]/a"';

		// Specify YQL URL - with query and JSON format in response
		$url = 'http://query.yahooapis.com/v1/public/yql?q='.urlencode($yql).'&format=json';

		// Get data from the YQL url
		$response = wp_remote_get($url); 
		
		// Connection error
		if (is_wp_error($response))
		{
			return false;
		}

		// Decode data
  		$data = json_decode($response['body'], true);

  		// Data doesn't contain valid results
  		if (!isset($data['query']['results']))
  		{
  			return false;
  		}

  		// Parse data and match with columns
		$keys = array('tweets', 'following', 'followers');
		$values = array();

		foreach ($data['query']['results']['a'] as $stat)
		{
			// We want to remove all dots and spaces from values
			$values[] = (int) preg_replace('/\D+/', '', $stat['title']);
		}

		$stats = array_combine($keys, $values);

		// Return data
		return $stats;
	}

	/**
	 * Update stats for current day
	 */
	public function update_today_stats()
	{
		// Delete cache  to assure we operate on latest data
		wp_cache_delete($this->option_name, 'options');

		// Get current local date
		$today = date('Y-m-d', current_time('timestamp'));

		// Load existing stats
		$stats = $this->get_stats();

		// Get new stats for today
		$today_stats = $this->load_current_stats();

		// In case of error - do not update anything
		if  ($today_stats == false) return false;

		// Add today's stats to previous ones
		$stats[$today] = $today_stats;

		// Save new stats
		$this->update_stats($stats);
	}

	/**
	 * Get stats from database
	 */
	public function get_stats()
	{
		// Load data from storage
		$stats = get_option($this->option_name);

		// Return stats from storage
		return json_decode($stats, true);
	}

	/**
	 * Update stats in database
	 */
	public function update_stats($stats)
	{
		// Sort stats by date 
		ksort($stats);

		// Limit entries to last 200 days
		$stats = array_slice($stats, -200);

		// Save data
		update_option($this->option_name, json_encode($stats));
	}

	/**
	 * Delete stats in database
	 */
	public function reset_stats()
	{
		update_option($this->option_name, json_encode(array()));
	}
}