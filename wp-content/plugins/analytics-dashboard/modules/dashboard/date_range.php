<?php
/**
 * 
 * Class responsible for date range selection
 * 
 */
class Adp_Date_Range
{

	/**
	 * Available date ranges
	 */
	private $ranges = array(
		'7'=>'Last 7 days',
		'30'=>'Last 30 days',
		'week'=>'Last week',
		'month'=>'Last month'
	);

	/**
	 * Register screen option setting and save it value while form is being sent
	 */
	public function register()
	{
		add_filter('set-screen-option', function ($status, $option, $value)
		{
			// If user is saving this specific option
			if ('adp_date_range' == $option)
			{
				if (isset($this->ranges[$_POST['adp_date_range']]))
				{
					$value = $_POST['adp_date_range'];
				}
			}
			return $value;
		}, 11, 3);
	}	

	/**
	 * Display form for selecting date range for user
	 */
	public function display()
	{
		add_filter('screen_settings', function ($status, $args)
		{
    		$return = $status;

	        if ($args->base == 'dashboard_page_analytics')
	        {    
	            $button = get_submit_button('Apply', 'button', 'screen-options-apply', false);
	            $return .= "<h5>Date range</h5>
	            <input type='hidden' name='wp_screen_options[option]' value='adp_date_range'>
	            <input type='hidden' name='wp_screen_options[value]' value='yes'>
	            <select name='adp_date_range'>";

	            foreach ($this->ranges as $key=>$value)
	            {
	            	$selected = ($this->get_current_option() == $key) ? ' selected="selected"' : '';
	            	$return .= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
	            }
					
	            $return .= "</select>";
	            $return .= $button;
	        }

	        return $return;
		}, 10, 2);

	}

	/**
	 * Read current date range setting for logged user
	 */
	private function get_current_option()
	{
		return get_user_meta(get_current_user_id(), 'adp_date_range', true);
	}

	/**
	 * Get start date, end date and period for selected by user date range
	 */
	public function get()
	{
		// Get current setting for the logged user
		$user_range = $this->get_current_option();
		
		// Prepare start and end dates in local timezone
		$start = DateTime::createFromFormat('U', current_time('timestamp'));
		$end = DateTime::createFromFormat('U', current_time('timestamp'));

		// Based on user's setting determine start and end dates
		switch ($user_range) 
		{

			// Last week (Monday-Sunday)
			case 'week':	$start->modify('Monday last week');			
							$end->modify('Sunday last week');
							break;

			// Last month (01 - 28/29/30/31 of previous month)
			case 'month':	$end->modify('last day of last month');
							$start->modify('first day of last month');
							break;

			// Last 30 days (today and previous 29 days)
			case 30:		$start->modify('-29 days');
							break;

			// Last 7 days (today and previous 6 days)
			default: 	
							$start->modify('-6 days');
		}		

		// Include the last day in period (we want to see today in stats)
		$period_end = clone $end;
		$period_end->modify("+1 day");

		// Prepare object to return
		$range = new StdClass();
		$range->start = $start;
		$range->end = $end;
		$range->period = new DatePeriod($start, DateInterval::createFromDateString('1 day'), $period_end);
		$range->days = iterator_count($range->period);

		// Calculate previous date range for comparisons
		$range->previous_start = clone $start;
		$range->previous_start->modify('-'.$range->days.' days');
		$range->previous_end = clone $end;
		$range->previous_end->modify('-'.$range->days.' days');

		// Return date_range as an object
		return $range;
	}
}