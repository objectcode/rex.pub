<?php 
/**
 * 
 * Class responsible for loading basic stats for Twitter profiles
 * Uses Facebook Graph
 * 
 */
class Adp_Facebook
{
	/**
	 * Key in wp_options table where data is stored
	 */	
	private $option_name = 'adp_facebook_stats';

	/**
	 * Facebook Fan Page username
	 */		
	private $username;

	/**
	 * Facebook App ID
	 */		
	private $app_id;

	/**
	 * Facebook App Secret
	 */		
	private $app_secret;

	public function __construct()
	{
		// Get current username from settings
		$options = get_option('adp_settings');
		$this->username = (!isset($options['facebook_username']) || empty($options['facebook_username'])) ? false : $options['facebook_username'];		
		$this->app_id = (!isset($options['facebook_app_id']) || empty($options['facebook_app_id'])) ? false : $options['facebook_app_id'];		
		$this->app_secret = (!isset($options['facebook_app_secret']) || empty($options['facebook_app_secret'])) ? false : $options['facebook_app_secret'];		
	}

	/**
	 * Get username
	 */
	public function get_username()
	{
		return $this->username;
	}	

	/**
	 * Set username
	 */
	public function set_username($username)
	{
		$this->username = $username;
	}		

	/**
	 * Make request to get current stats
	 */
	public function load_current_stats()
	{
		// Check if we have username and Facebook App data
		if (!$this->username || !$this->app_id || !$this->app_secret) 
		{
			return false;
		}

		// Facebook profile Graph URL
		$url = sprintf('https://graph.facebook.com/%s?access_token=%s|%s', $this->username, $this->app_id, $this->app_secret);
		// Get data from the YQL url
		$response = wp_remote_get($url); 
		
		// Connection error
		if (is_wp_error($response))
		{
			return false;
		}

		// Decode data
  		$data = json_decode($response['body'], true);

		// Data doesn't contain valid results
  		if (!is_array($data))
  		{
  			return false;
  		}

  		// Get only the data which we need
		$stats = array();
		$stats['likes']	= (int) $data['likes'];
		$stats['talking'] = (int) $data['talking_about_count'];

		// Return data
		return $stats;
	}


	/**
	 * Update stats for current day
	 */
	public function update_today_stats()
	{
		// Delete cache  to assure we operate on latest data
		wp_cache_delete($this->option_name, 'options');

		// Get current local date
		$today = date('Y-m-d', current_time('timestamp'));

		// Load existing stats
		$stats = $this->get_stats();

		// Get new stats for today
		$today_stats = $this->load_current_stats();

		// In case of error - do not update anything
		if  ($today_stats == false) return false;

		// Add today's stats to previous ones
		$stats[$today] = $today_stats;

		// Save new stats
		$this->update_stats($stats);
	}

	/**
	 * Get stats from database
	 */
	public function get_stats()
	{
		// Load data from storage
		$stats = get_option($this->option_name);

		// Return stats from storage
		return json_decode($stats, true);
	}

	/**
	 * Update stats in database
	 */
	public function update_stats($stats)
	{
		// Sort stats by date 
		ksort($stats);

		// Limit entries to last 200 days
		$stats = array_slice($stats, -200);
		
		// Save data
		update_option($this->option_name, json_encode($stats));
	}

	/**
	 * Delete stats in database
	 */
	public function reset_stats()
	{
		update_option($this->option_name, json_encode(array()));
	}	
}