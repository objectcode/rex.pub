<?php
/**
 * 
 * Parent abstract class for all widgets
 * 
 */

abstract class Adp_Widget
{
	/**
	 * Collection of variables passed to view
	 */
	private $view_variables = array();

	/**
	 * Base path
	 */
	private $path;

	/**
 	 * Filename of the widget
	 */
	private $filename;

	/**
	 * Object responsible for date selection
	 */
	protected $date_range;

	/**
	 * Title of the widget
	 */
	protected $title = 'Widget';

	/**
	 * Initializable function of the widget
	 */ 
	abstract public function init();

	public function __construct($path, $filename)
	{
		$this->path = $path;
		$this->filename = $filename;
	}

	/**
	 * Function loaded while rendering widget (by Wordpress functions)
	 */
	public function render() 
	{

		// Store current date range object
		$Adp_Date_Range = new Adp_Date_Range();
		$this->date_range = $Adp_Date_Range->get();
		$this->assign('date_range', $this->date_range);

		// Initialize widget
		$this->init();

		// Get path to view for the widget
		$view_path = $this->path .'views/dashboard/widgets/'.str_replace('.php', '.view.php', $this->filename);

		// Check whether view file exists
		if (!file_exists(($view_path)))
		{
			return false;
		}

		// Display widget with variables passed by widget to view
		extract($this->view_variables);
		include($view_path);
	}

	/**
	 * Return widget's title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Assign variable to the widget's view
	 */ 
	protected function assign($key, $value)
	{
		$this->view_variables[$key] = $value;
	}

}