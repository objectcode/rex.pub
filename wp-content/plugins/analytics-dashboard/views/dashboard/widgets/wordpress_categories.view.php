<div id="wordpress-categories-widget" class="adp_widget">		
	<?php if (isset($data['chart'])) : ?>
	<div id="categories_chart" class="ct-chart"></div>
	<?php else: ?>
	No posts were published during selected period.
	<?php endif; ?>
</div>

<?php if (isset($data['chart'])) : ?>
<script>
jQuery(document).on('ready', function($) {

	new Chartist.Bar('#categories_chart', <?php echo json_encode($data['chart']) ?>, {
		height: '<?php echo 40 + (32*$data['categories_count'])?>px',
		fullWidth: true,
		chartPadding: 0,
		horizontalBars: true,
		reverseData: true,
		axisY: {
		  offset: 120,
		  showGrid: false,
		  labelOffset: {
      		x: 0,
      		y: 9
    	  },
		},
		axisX: {
			scaleMinSpace: 40,
			labelInterpolationFnc: function(value, index) {return value % 1 === 0 ? value : null;},
		}		
	});

});
</script>
<?php endif; ?>