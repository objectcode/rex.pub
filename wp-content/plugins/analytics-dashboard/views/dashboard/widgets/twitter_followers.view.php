<div id="twitter-followers-widget" class="adp_widget">
	<?php if (!isset($data)) : ?>

		Please provide Twitter username in <a href="options-general.php?page=analytics-dashboard">plugin settings page</a>.

	<?php else: ?>

		<?php if ($data['chart']) : ?>
		<div id="followers_chart" class="ct-chart"></div>

		<ul>
			<li class="box box-with-icon box-50">
				<span class="icon"><a href="<?php echo $data['twitter_profile'] ?>" target="blank"><i class="fa fa-twitter"></i></a></span>
				<div>
					<strong class="big-number"><?php echo$data['followers']?> <?php echo display_change($data['followers_diff']) ?></strong>
					<span class="label uppercase">Followers</span>
				</div>			
			</li><li class="box box-with-icon box-50">
				<span class="icon"><i class="fa fa-user"></i></span>
				<div>
					<strong class="big-number"><?php echo$data['following']?> <?php echo display_change($data['following_diff']) ?></strong>
					<span class="label uppercase">Following</span>
				</div>
			</li>
		</ul>

		<?php else : ?>
		No data for selected period.
		<?php endif; ?>
	<?php endif; ?>

</div>


<?php if (isset($data['chart'])) : ?>
<script>
jQuery(document).on('ready', function($) {

	new Chartist.Line('#followers_chart', <?php echo json_encode($data['chart']) ?>, {
		height: 220,
		showArea: true,
		fullWidth: true,
		chartPadding: 5,
		showPoint: false,
		showLabel: true,
		lineSmooth: Chartist.Interpolation.simple({
    		divisor: 2
  		}),		
		axisX: {
			showGrid: <?php echo ($date_range->days > 7) ? 'false' : 'true' ?>,
			labelOffset: {
		      x: -25,
		      y: 10,
    		},
    		<?php
    			if ($date_range->days > 7): 
    				echo 'labelInterpolationFnc: function(value, index) {return index % 3 === 0 ? value : null;}';
    			endif;
    		?>
		},
		axisY: {
			labelInterpolationFnc: function(value, index) {return value % 1 === 0 ? value : null;},
		}
	});
});
</script>
<?php endif; ?>