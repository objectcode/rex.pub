<div id="wordpress-comments-widget" class="adp_widget">		

	<div class="col">
		<strong class="big-number"><?php echo $data['total'] ?></strong><br>
		<span class="label uppercase">Comments</span>
	</div>

	<div class="col col-2">
		<?php if (isset($data['chart'])) : ?>
		<div id="comments_chart" class="ct-chart"></div>
		<?php endif; ?>
	</div>

	<hr>

	<div class="col">
		<strong class="big-number"><?php echo $data['average'] ?></strong><br>
		<span class="label uppercase">Comments / day</span>

	</div>

	<div class="col">	
		<?php if ($data['total'] > 0) : ?>
		<div id="comments_chart_approved" class="ct-chart circle">
			<div class="value" ><?php echo $data['approved_rate'] ?>%</div>
		</div>
		<span class="label uppercase">approved</span>
		<?php endif; ?>
	</div>

	<div class="col">
		<?php if ($data['total'] > 0) : ?>
		<div id="comments_chart_spam" class="ct-chart circle">
			<div class="value"><?php echo $data['spam_rate'] ?>%</div>
		</div>
		<span class="label uppercase">spam</span>
		<?php endif; ?>
	</div>

</div>


<script>
jQuery(document).on('ready', function($) {
	<?php if ($data['chart']) : ?>

	new Chartist.Line('#comments_chart', <?php echo json_encode($data['chart']) ?>, {
		height: '110px',
		fullWidth: true,
		chartPadding: 0,
		showPoint: false,
		showLabel: false,
		showLine: true,
		lineSmooth: false,
		showArea: true,
		axisX: {
			offset: 0,
			showGrid: false,
			showLabel: false,
		},
		axisY: {
			offset: 0,
			showGrid: false,
			showLabel: false,
		},
	});

	<?php endif; ?>

	<?php if ($data['total']>0) : ?>
	var donutOptions = {
	  donut: true,
	  height: '100px',
	  width: '100px',
	  donutWidth: 13,
	  startAngle: 15,
	  showLabel: false,    		
	};
	
	new Chartist.Pie('#comments_chart_approved', {
	  	series: [<?php echo $data['total'] ?>, <?php echo $data['spam'] ?>]
	  }, donutOptions
	);

	new Chartist.Pie('#comments_chart_spam', {
		series: [<?php echo $data['spam'] ?>, <?php echo $data['total'] ?>]
	  }, donutOptions);	
	<?php endif; ?>
});
</script>