<div id="wordpress-popular-posts-widget" class="adp_widget">
<?php if (isset($data['posts']) && is_array($data['posts']) && count($data['posts'])>0) : ?>
	<table>
	<?php foreach ($data['posts'] as $key => $post): ?>	
		<tr>
			<td class="thumb"><?php echo ($post->thumb) ? $post->thumb : '<span><i class="fa fa-camera"></i></span>' ?></td>
			<td class="title">
				<a href="<?php echo $post->url ?>" target="new" class="name"><?php echo $post->title ?></a>
			</td>
			<td class="count">
				<?php
					$transform = 'transform: rotate('.round($post->comments / $data['posts'][0]->comments / 2, 2).'turn);';
				?>
				<div class="gauge"><span><?php echo $post->comments ?></span><div class="meter" style="<?php echo$transform?> -ms-<?php echo$transform?> -webkit-<?php echo$transform?>"></div></div>
			</td>			
		</tr>
	<?php endforeach; ?>
	</table>
<?php else : ?>
No comments were published during selected period.
<?php endif ?>

</div>