<div id="wordpress-posts-widget" class="adp_widget">	
	<?php if (isset($data['chart'])) : ?>
	<div id="posts_chart" class="ct-chart"></div>
	

	<div class="summary">
		<ul>
			<li class="legend">
				<div class="label serie-b">Current period</div>
				<div class="label serie-a">Previous period</div>
			</li><li class="box box-with-icon">
				<span class="icon"><i class="fa fa-file-o"></i></span>
				<div>
					<strong class="big-number"><?php echo $data['total'] ?> <?php echo display_change($data['total_diff']) ?></strong>
					<span class="label uppercase">Total Posts</span>
				</div>
			</li><li class="box box-with-icon">
				<span class="icon"><i class="fa fa-bar-chart"></i></span>
				<div>
					<strong class="big-number"><?php echo $data['average'] ?> <?php echo display_change($data['average_diff']) ?></small></strong>
					<span class="label uppercase">Posts / Day</span>
				</div>
			</li>
		</ul>
	</div>

	<?php else : ?>
	No posts were published during selected period.
	<?php endif; ?>


</div>

<?php if (isset($data['chart'])) : ?>
<script>
jQuery(document).on('ready', function($) {

	new Chartist.Line('#posts_chart', <?php echo json_encode($data['chart']) ?>, {
		height: 220,
		low: 0,
		showArea: true,
		fullWidth: true,
		chartPadding: 5,
		showPoint: false,
		showLabel: true,
		showLine: false,
		lineSmooth: Chartist.Interpolation.simple({
    		divisor: 2
  		}),
		axisX: {
			showGrid: <?php echo ($date_range->days > 7) ? 'false' : 'true' ?>,
			labelOffset: {
		      x: -25,
		      y: 10,
    		},
    		<?php
    			if ($date_range->days > 7): 
    				echo 'labelInterpolationFnc: function(value, index) {return index % 3 === 0 ? value : null;}';
    			endif;
    		?>
		},
		axisY: {
			offset: 20,
		    scaleMinSpace: 40,
		    labelInterpolationFnc: function(value, index) {return value % 1 === 0 ? value : null;},
		}
	});
});
</script>
<?php endif; ?>