<div id="wordpress-authors-widget" class="adp_widget">		

<?php if (isset($data['users']) && is_array($data['users'])) : ?>
	<table>
	<?php foreach ($data['users'] as $user): ?>	
		<tr>
			<td class="avatar"><?php echo get_avatar($user->user_email, 40); ?></td>
			<td>
				<a href="<?php echo $user->url ?>" target="new" class="name"><?php echo $user->display_name ?></a><br>
			</td>
			<td class="count">
				<span class="label uppercase"><?php echo $user->posts_count ?> posts</span>
			</td>
			<td class="line">
	
				<div class="bar">
					<span style="width: <?php echo round(100 * $user->posts_count / $data['users'][0]->posts_count)?>%;"></span>
				</div>				
			
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php else : ?>
No posts were published during selected period.
<?php endif ?>
</div>