<div id="twitter-tweets-widget" class="adp_widget">
	<?php if (!isset($data)) : ?>

		Please provide Twitter username in <a href="options-general.php?page=analytics-dashboard">plugin settings page</a>.

	<?php else: ?>

		<?php if (isset($data['chart'])) : ?>
		<div id="tweets_chart" class="ct-chart <?php echo ($date_range->days > 7) ? 'monthly' : 'weekly'?>"></div>
		<?php else: ?>
		No tweets data for selected period. Stats will be available after minimum 2 days.
		<?php endif; ?>

	<?php endif; ?>
</div>

<?php if (isset($data['chart'])) : ?>
<script>
jQuery(document).on('ready', function($) {

	new Chartist.Bar('#tweets_chart', <?php echo json_encode($data['chart']) ?>, {
		height: '296px',
		fullWidth: true,
		chartPadding: 0,
		axisY: {
			labelInterpolationFnc: function(value, index) {return value % 1 === 0 ? value : null;},
		},
		axisX: {
			showGrid: false,
			labelOffset: {
		      x: 0,
		      y: 10,
    		},
    		<?php echo ($date_range->days > 7) ? 'labelInterpolationFnc: function(value, index) {return index % 3 === 0 ? value : null;}' : ''; ?>
		},
	});

});
</script>
<?php endif; ?>