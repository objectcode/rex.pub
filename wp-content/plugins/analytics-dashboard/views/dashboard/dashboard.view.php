<?php echo $date_range->start->format('jS F Y'); ?> -  <?php echo $date_range->end->format('jS F Y'); ?>
    
<form name="adp_form" method="post">
    <input type="hidden" name="action" value="analytics-dashboard">
    <?php
	    wp_nonce_field('analytics-dashboard-nonce');     
	    wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false);
	    wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false);
    ?> 

    <div id="dashboard-widgets-wrap">
    	<div id="dashboard-widgets" class="metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>">
        	<div id="postbox-container-1" class="postbox-container">			
                <?php do_meta_boxes('','normal', null); ?>
        	</div>

            <div id="postbox-container-3" class="postbox-container">
                <?php do_meta_boxes('','side', null); ?>
            </div>
    	</div>
    </div>
</form>


<?php

/**
 * Helper function for formatting change (green/orange/red)
 */
function display_change($number)
{
    $color = 'orange';
    $icon = 'right';

    if ($number > 0)
    {
        $icon = 'up';
        $color = 'green';
    }
    else if ($number < 0)
    {
        $icon = 'down';
        $color = 'red';
    }

    return '<small class="'.$color.'"><i class="fa fa-caret-'.$icon.'"></i> '.abs($number).'</small>';
}
?>