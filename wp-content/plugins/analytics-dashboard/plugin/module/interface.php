<?php
/**
 * 
 * Interface for each module within the plugin
 * 
 */
interface Adp_Module_Interface
{

	/**
	 * Each module should be initializable
	 */
	public function init();
}