<?php
/**
 * 
 * Main class of the plugin responsible for initializing all modules
 * 
 */
class Analytics_Dashboard
{
	/**
	 * Path to plugin directory
	 */
	private $plugin_path;

	/**
	 * Plugin URL
	 */	
	private $plugin_url;

	/**
	 *  Save values from WP to further use as we don't want to use global functions
	 */
	public function __construct($plugin_path, $plugin_url)
	{
		$this->plugin_path = $plugin_path;
		$this->plugin_url = $plugin_url;
	}

	/**
	*  Called upon plugin activation in Wordpress
	*/
	public static function activate()
	{
		// Schedule Twitter stats updates (wp-cron)
		$timestamp = wp_next_scheduled('adp_update_twitter_stats');		
		if ($timestamp == false)
		{
			wp_schedule_event(time(), 'twicedaily', 'adp_update_twitter_stats');
		}

		// Schedule Facebook stats updates (wp-cron)
		$timestamp = wp_next_scheduled('adp_update_facebook_stats');
		if ($timestamp == false)
		{
			wp_schedule_event(time(), 'twicedaily', 'adp_update_facebook_stats');
		}

	}

	/**
	*  Called upon plugin deactivation in Wordpress
	*/
	public static function deactivate()
	{
		// Remove scheduled entries from wp-cron
		wp_clear_scheduled_hook('adp_update_twitter_stats');
		wp_clear_scheduled_hook('adp_update_facebook_stats');
	}

	/**
	*  Initializes the plugin
	*/
	public function init()
	{		
		// Load required files
		$this->load_dependencies();
		
		// Prepare module loader
		$loader = new Adp_Module_Loader();

		// Register module Dashboard
		$loader->register_module($this->plugin_path, $this->plugin_url, 'dashboard');

		// Register module Settings
		$loader->register_module($this->plugin_path, $this->plugin_url, 'settings');

		// Initialize registered modules
		$loader->init_modules();
	}

	/**
	*  Load required files
	*/
	private function load_dependencies()
	{
		include($this->plugin_path . 'plugin/module/loader.php');
		include($this->plugin_path . 'plugin/module/interface.php');
		include($this->plugin_path . 'plugin/module/admin.php');
	}

}