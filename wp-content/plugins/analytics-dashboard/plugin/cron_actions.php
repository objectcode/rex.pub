<?php
/**
 * 
 * Definitions of all wp-cron actions
 * 
 */


// Update Twitter statistics
add_action('adp_update_twitter_stats', 'adp_update_twitter_stats');

function adp_update_twitter_stats()
{
	$twitter = new Adp_Twitter();
	$twitter->update_today_stats();
}


// Update Facebook statistics
add_action('adp_update_facebook_stats', 'adp_update_facebook_stats');

function adp_update_facebook_stats()
{
	$facebook = new Adp_Facebook();
	$facebook->update_today_stats();
}