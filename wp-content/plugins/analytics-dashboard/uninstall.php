<?php
// If uninstall is not called from WordPress, exit
if (!defined('WP_UNINSTALL_PLUGIN'))
{
    exit;
}
 
// Delete WP options
$options = array(
	'adp_settings',
	'adp_twitter_stats',
	'adp_facebook_stats'
);

array_walk($options, 'delete_option');


// Delete WP user meta
$meta = array(
	'meta-box-order_dashboard_page_analytics',
	'screen_layout_dashboard_page_analytics',
	'closedpostboxes_dashboard_page_analytics',
	'metaboxhidden_dashboard_page_analytics',
	'adp_date_range'
);

foreach ($meta as $key)
{
	delete_metadata('user', 0, $key, '', true);
}