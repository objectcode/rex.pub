=== Analytics Dashboard ===
Contributors: Woj
Tags: analysis, analytics, chart, dashboard, facebook, metrics, report, social media, statistics, stats, tracking, twitter, widgets
Requires at least: 4.1
Tested up to: 4.1
Stable tag: 1.0.1

Analytics Dashboard displays in one place all essential metrics and statistics of your blog, Twitter Profile and Facebook Fan Page.

== Description ==
Analytics Dashboard displays in one place all essential metrics and statistics of your blog, Twitter Profile and Facebook Fan Page. It allows you to monitor posts & comments statistics, authors activity and social media popularity.

== Installation ==
1. Upload Analytics Dashboard plugin to your Wordpress installation
2. Activate the plugin using "Plugins" screen in wp-admin

== Changelog ==
= 1.0.1 =
* Better compatibility with different PHP configurations
* Fixed Fan Page statistics - it's required now to add Facebook App ID and App Secret in Settings screen
* Fixed colors of donut charts
* Fixed uninstall option, which removes now entire plugin data

= 1.0.0 =
* First public version of the plugin