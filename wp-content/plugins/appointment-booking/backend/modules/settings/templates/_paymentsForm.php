<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<form method="post" action="<?php echo esc_url( add_query_arg( 'type', '_payments' ) ) ?>" class="ab-settings-form">
    <table class="form-horizontal">
        <tr>
            <td style="width: 170px;">
                <label for="ab_paypal_currency"><?php _e( 'Currency', 'bookly' ) ?></label>
            </td>
            <td>
                <select id="ab_paypal_currency" class="form-control" name="ab_paypal_currency">
                    <?php foreach ( AB_PayPal::getCurrencyCodes() as $code ): ?>
                        <option value="<?php echo $code ?>" <?php selected( get_option( 'ab_paypal_currency' ), $code ) ?> ><?php echo $code ?></option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 170px;">
                <label for="ab_settings_coupons"><?php _e( 'Coupons', 'bookly' ) ?></label>
            </td>
            <td>
                <?php AB_Utils::optionToggle( 'ab_settings_coupons' ) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title"><?php _e( 'Service paid locally', 'bookly' ) ?></div></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php AB_Utils::optionToggle( 'ab_settings_pay_locally' ) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title">PayPal</div></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php AB_Utils::optionToggle( 'ab_paypal_type', array( 'f' => array( 'disabled', __( 'Disabled', 'bookly' ) ), 't' => array( 'ec', 'PayPal Express Checkout' ) ) ) ?>
            </td>
        </tr>
        <tr class="paypal_ec">
            <td><label for="ab_paypal_api_username"><?php _e( 'API Username', 'bookly' ) ?></label></td>
            <td><input id="ab_paypal_api_username" class="form-control" type="text" size="33" name="ab_paypal_api_username" value="<?php echo get_option( 'ab_paypal_api_username' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><label for="ab_paypal_api_password"><?php _e( 'API Password', 'bookly' ) ?></label></td>
            <td><input id="ab_paypal_api_password" class="form-control" type="text" size="33" name="ab_paypal_api_password" value="<?php echo get_option( 'ab_paypal_api_password' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><label for="ab_paypal_api_signature"><?php _e( 'API Signature', 'bookly' ) ?></label></td>
            <td><input id="ab_paypal_api_signature" class="form-control" type="text" size="33" name="ab_paypal_api_signature" value="<?php echo get_option( 'ab_paypal_api_signature' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><label for="ab_paypal_ec_mode"><?php _e( 'Sandbox Mode', 'bookly' ) ?></label></td>
            <td>
                <?php AB_Utils::optionToggle( 'ab_paypal_ec_mode', array( 't' => array( '.sandbox', __( 'Yes', 'bookly' ) ), 'f' => array( '', __( 'No', 'bookly' ) ) ) ) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title">Authorize.Net</div></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php AB_Utils::optionToggle( 'ab_authorizenet_type', array( 'f' => array( 'disabled', __( 'Disabled', 'bookly' ) ), 't' => array( 'aim', 'Authorize.Net AIM' ) ) ) ?>
            </td>
        </tr>
        <tr class="authorizenet">
            <td><label for="ab_authorizenet_api_login_id"><?php _e( 'API Login ID', 'bookly' ) ?></label></td>
            <td><input id="ab_authorizenet_api_login_id" class="form-control" type="text" size="33" name="ab_authorizenet_api_login_id" value="<?php echo get_option( 'ab_authorizenet_api_login_id' ) ?>"/></td>
        </tr>
        <tr class="authorizenet">
            <td><label for="ab_authorizenet_transaction_key"><?php _e( 'API Transaction Key', 'bookly' ) ?></label></td>
            <td><input id="ab_authorizenet_transaction_key" class="form-control" type="text" size="33" name="ab_authorizenet_transaction_key" value="<?php echo get_option( 'ab_authorizenet_transaction_key' ) ?>"/></td>
        </tr>
        <tr class="authorizenet">
            <td><label for="ab_authorizenet_sandbox"><?php _e( 'Sandbox Mode', 'bookly' ) ?></label></td>
            <td>
                <?php AB_Utils::optionToggle( 'ab_authorizenet_sandbox', array( 't' => array( 1, __( 'Yes', 'bookly' ) ), 'f' => array( 0, __( 'No', 'bookly' ) ) ) ) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title">Stripe</div></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php AB_Utils::optionToggle( 'ab_stripe' ) ?>
            </td>
        </tr>
        <tr class="ab-stripe">
            <td><label for="ab_stripe_secret_key"><?php _e( 'Secret Key', 'bookly' ) ?></label></td>
            <td><input id="ab_stripe_secret_key" class="form-control" type="text" size="33" name="ab_stripe_secret_key" value="<?php echo get_option( 'ab_stripe_secret_key' ) ?>"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php AB_Utils::submitButton() ?>
                <?php AB_Utils::resetButton( 'ab-payments-reset' ) ?>
            </td>
            <td></td>
        </tr>
    </table>
</form>