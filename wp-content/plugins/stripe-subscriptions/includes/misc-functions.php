<?php


/**
 * Check if the [stripe_subscription] shortcode exists on this page
 * 
 * @since 1.0.0
 */
function sc_sub_has_shortcode() {
	global $post;
	
	// Currently ( 5/8/2014 ) the has_shortcode() function will not find a 
	// nested shortcode. This seems to do the trick currently, will switch if 
	// has_shortcode() gets updated. -NY
	if ( strpos( $post->post_content, '[stripe_subscription' ) !== false ) {
		return true;
	}
	
	return false;
}

/**
 * Helper function to grab the subscription by ID and return the subscription object
 * 
 * @since 1.0.0
 */
function sc_sub_get_subscription_by_id( $id ) {
	
	global $sc_options;
	
	// Check if in test mode or live mode
	if( ! empty( $sc_options['enable_live_key'] ) && $sc_options['enable_live_key'] == 1 ) {
		$data_key = ( ! empty( $sc_options['live_secret_key'] ) ? $sc_options['live_secret_key'] : '' );
	} else {
		$data_key = ( ! empty( $sc_options['test_secret_key'] ) ? $sc_options['test_secret_key'] : '' );
	}
	
	if( empty( $data_key ) ) {
		
		if( current_user_can( 'manage_options' ) ) {
			return '<h6>' . __( 'You must enter your API keys before the Stripe button will show up here.', 'sc_sub' ) . '</h6>';
		}
		
		return '';
	}
	
	Stripe::setApiKey( $data_key );
	
	try {
		$return = Stripe_Plan::retrieve( trim( $id ) );
	} catch(Stripe_CardError $e) {

		$body = $e->getJsonBody();

		$return = sc_print_errors( $body['error'] );

	} catch (Stripe_AuthenticationError $e) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)

		$body = $e->getJsonBody();

		$return = sc_print_errors( $body['error'] );

	} catch (Stripe_ApiConnectionError $e) {
		// Network communication with Stripe failed

		$body = $e->getJsonBody();

		$return = sc_print_errors( $body['error'] );

	} catch (Stripe_Error $e) {
		
		$body = $e->getJsonBody();

		$return = sc_print_errors( $body['error'] );

	} catch (Exception $e) {
		// Something else happened, completely unrelated to Stripe
		$body = $e->getJsonBody();

		$return = sc_print_errors( $body['error'] );
	}
	
	return $return;
}


/**
 * Add the license key settings 
 * 
 * @since 1.0.0
 */
function sc_sub_license_settings( $settings ) {
	
	$settings['licenses']['note'] = array(
			'id'   => 'note',
			'name' => '',
			'desc' => '<p class="description">' . __( 'These license keys are used for access to automatic upgrades and support.', 'sc_sub' ) . '</p>',
			'type' => 'section'
	);

	$settings['licenses']['sc_sub_license_key'] = array(
			'id'   => 'sc_sub_license_key',
			'name' => __( 'Subscriptions License Key', 'sc_sub' ),
			'desc' => '',
			'type' => 'license',
			'size' => 'regular-text',
			'product' => 'Stripe Subscriptions'
	);

	return $settings;
}
add_filter( 'sc_settings', 'sc_sub_license_settings' );


function sc_sub_inactive_license_check( $inactive ) {
	global $sc_options;
	
	$licenses = get_option( 'sc_licenses' );


	if( empty( $sc_options['sc_sub_license_key'] ) || $licenses['Stripe Subscriptions'] != 'valid' ) {
		return true;
	}
	
	// Return the original if we are not returning true
	return $inactive;
	
}
add_action( 'sc_inactive_license', 'sc_sub_inactive_license_check' );


function sc_sub_add_payment_details( $html, $details ) {
	
	if( ! isset( $details->invoice ) ) {
		return $html;
	}
	
	$invoice = Stripe_Invoice::retrieve( $details->invoice );
	
	//return '<pre>' . print_r( $invoice, true ) . '</pre>';
	
	$interval = $invoice->lines->data[0]->plan->interval;
	$interval_count = $invoice->lines->data[0]->plan->interval_count;
	
	$html = '<div class="sc-payment-details-wrap">';
		
	$html .= '<p>' . __( 'Congratulations. Your payment went through!', 'sc' ) . '</p>' . "\n";

	if( ! empty( $details->description ) ) {
		$html .= '<p>' . __( "Here's what you bought:", 'sc' ) . '</p>';
		$html .= $details->description . '<br>' . "\n";
	}

	if ( isset( $_GET['store_name'] ) && ! empty( $_GET['store_name'] ) ) {
		$html .= 'From: ' . esc_html( $_GET['store_name'] ) . '<br/>' . "\n";
	}

	$html .= '<br><strong>' . __( 'Total Paid: ', 'sc' ) . sc_stripe_to_formatted_amount( $details->amount, $details->currency ) . ' ' . 
			strtoupper( $details->currency ) . '</strong>' . "\n";

	$html .= '<br>';
	$html .= __( 'You will be charged ', 'sc_sub' );
	$html .= sc_stripe_to_formatted_amount( $details->amount, $details->currency ) . ' ' . strtoupper( $details->currency );

	// For interval count of 1, use $1.00/month format.
	// For a count > 1, use $1.00 every 3 months format.
	if ( $interval_count == 1 ) {
		$html .= '/' . $interval;
	} else {
		$html .= ' ' . __( 'every', 'sc_sub' ) . ' ' . $interval_count . ' ' . $interval . 's';
	}
	
	$html .= '<p>' . sprintf( __( 'Your transaction ID is: %s', 'sc' ), $details->id ) . '</p>';

	$html .= '</div>';
	
	
	

	return $html;
	
	//return '<pre>' . print_r( $details, true ) . '</pre>';
}
add_filter( 'sc_payment_details', 'sc_sub_add_payment_details', 10, 2 );
