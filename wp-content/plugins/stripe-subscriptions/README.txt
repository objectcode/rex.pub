=== Stripe Subscriptions Add-On ===
Contributors: pderksen, nickyoung87
Requires at least: 3.7.4
Tested up to: 4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Subscriptions add-On for Stripe Checkout Pro.

== Changelog ==

= 1.0.6 =

* Adjustment for charge details change in Stripe Checkout Pro 2.0.7.

= 1.0.5 =

* Fixed plan labels and payment success message to show custom interval counts (i.e. "$50.00 every 3 months").

= 1.0.4 =

* Adjustment for payment button class change in Stripe Checkout Pro 2.0.5.

= 1.0.3 =

* Enabled subscription integration with user entered amounts.
* Enabled subscription integration with custom fields.
* Enabled subscription integration with Stripe coupons.
* Improved invalid Stripe API key handling.
* Better admin-only notices for invalid shortcode combinations.
* Now allows plans of different currencies.

= 1.0.2 =

* Fixed a bug where live keys were throwing errors.

= 1.0.1 =

* Fixed bug with amounts when multiple Stripe checkout forms exist on a single page.

= 1.0.0 =

* Initial add-on release.
