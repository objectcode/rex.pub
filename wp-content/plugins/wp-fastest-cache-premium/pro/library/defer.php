<?php
	class WpFastestCacheDefer{
		private $html = "";
		private $except = "";
		private $tags = array();
		private $header_start_index = 0;

		public function __construct($html){
			$this->html = $html;
			$this->set_header_start_index();
			$this->set_tags();
			$this->tags_reorder();
		}

		public function set_header_start_index(){
			$head_tag = $this->find_tags("<head", ">");
			$this->header_start_index = isset($head_tag[0]) && isset($head_tag[0]["start"]) && $head_tag[0]["start"] ? $head_tag[0]["start"] : 0;
		}

		public function tags_reorder(){
		    $sorter = array();
		    $ret = array();

		    foreach ($this->tags as $ii => $va) {
		        $sorter[$ii] = $va['start'];
		    }

		    asort($sorter);

		    foreach ($sorter as $ii => $va) {
		        $ret[$ii] = $this->tags[$ii];
		    }

		    $this->tags = $ret;
		}

		public function set_except($tags){
			foreach ($tags as $key => $value) {
				$this->except = $value["text"].$this->except;
			}
		}

		public function set_tags(){
			$this->set_comments();
			$this->set_css();
			$this->set_js();
		}

		public function set_css(){
			$style_tags = $this->find_tags("<style", "</style>");
			$this->tags = array_merge($this->tags, $style_tags);
			
			$link_tags = $this->find_tags("<link", ">");

			foreach ($link_tags as $key => $value) {
				if(preg_match("/href\s*\=/i", $value["text"])){
					if(preg_match("/rel\s*\=\s*[\'\"]\s*stylesheet\s*[\'\"]/i", $value["text"])){
						array_push($this->tags, $value);
					}
				}
			}
		}

		public function set_js(){
			$script_tag = $this->find_tags("<script", "</script>");

			foreach ($script_tag as $key => $value) {
				if(preg_match("/google_ad_client/", $value["text"])){
					continue;
				}

				if(preg_match("/googlesyndication\.com/", $value["text"])){
					continue;
				}
				if(preg_match("/srv\.sayyac\.net/", $value["text"])){
					continue;
				}

				array_push($this->tags, $value);
			}
		}

		public function set_comments(){
			$comment_tags = $this->find_tags("<!--", "-->");

			$this->set_except($comment_tags);

			foreach ($comment_tags as $key => $value) {
				if(preg_match("/\<\!--\s*\[if/i", $value["text"])){
					array_push($this->tags, $value);
				}
			}
		}

		public function find_tags($start_string, $end_string){
			$data = $this->html;

			$list = array();
			$start_index = false;
			$end_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
			    if(substr($data, $i, strlen($start_string)) == $start_string){
			    	$start_index = $i;
				}

				if($start_index && $i > $start_index){
					if(substr($data, $i, strlen($end_string)) == $end_string){
						$end_index = $i + strlen($end_string)-1;
						$text = substr($data, $start_index, ($end_index-$start_index + 1));
						
						if($start_index > $this->header_start_index){
							if($this->except){
								if(strpos($this->except, $text) === false){
									array_push($list, array("start" => $start_index, "end" => $end_index, "text" => $text));
								}
							}else{
								array_push($list, array("start" => $start_index, "end" => $end_index, "text" => $text));
							}
						}

						$start_index = false;
						$end_index = false;
					}
				}
			}

			return $list;
		}

		public function action(){
			$text = "";

			foreach (array_reverse($this->tags) as $key => $value) {
				$this->html = substr_replace($this->html, "", $value["start"], ($value["end"] - $value["start"] + 1));
			}

			foreach ($this->tags as $key => $value) {
				$this->html = str_replace("</body>", $value["text"]."\n"."</body>", $this->html);
			}

			return preg_replace("/^\s+/m", "", $this->html);
		}
	}
?>