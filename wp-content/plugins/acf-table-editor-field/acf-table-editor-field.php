<?php
/**
 * Plugin Name: Advanced Custom Fields: Table Editor Field
 * Plugin URI: http://www.polevaultweb.com/
 * Description: Adds a new ACF field that is an Excel like grid table editor
 * Version: 1.2.3
 * Author: polevaultweb
 * Author URI: http://www.polevaultweb.com/
 *
 * Copyright 2013  polevaultweb  (email : info@polevaultweb.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

class acf_table_editor_field_plugin {

	private $version = '1.2.3';

	function __construct() {

		// create remote update
		if ( is_admin() ) {
			require_once( 'includes/wp-updates-plugin.php' );
			new WPUpdatesPluginUpdater_284( 'http://wp-updates.com/api/2/plugin', plugin_basename( __FILE__ ) );
		}

		$this->setup_constants();

		// actions
		add_action( 'acf/register_fields', array( $this, 'register_fields' ) );
		add_action( 'acf/include_field_types', array( $this, 'include_fields' ) );

	}

	private function setup_constants() {
		// Plugin version
		if ( ! defined( 'ACF_TABLE_EDITOR_VERSION' ) ) {
			define( 'ACF_TABLE_EDITOR_VERSION', $this->version );
		}

		// Plugin Folder Path
		if ( ! defined( 'ACF_TABLE_EDITOR_PLUGIN_DIR' ) ) {
			define( 'ACF_TABLE_EDITOR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'ACF_TABLE_EDITOR_PLUGIN_URL' ) ) {
			define( 'ACF_TABLE_EDITOR_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'ACF_TABLE_EDITOR_PLUGIN_FILE' ) ) {
			define( 'ACF_TABLE_EDITOR_PLUGIN_FILE', __FILE__ );
		}
	}

	/**
	 *  register_fields - used for ACF v4
	 *
	 * @description:
	 * @since      : 1.0
	 * @created    : 29/11/13
	 */
	function register_fields() {
		include_once( 'includes/table-editor-field-v4.php' );
	}

	/**
	 *  include_fields - used for ACF v5
	 *
	 * @description:
	 * @since      : 1.2
	 * @created    : 29/11/13
	 */
	function include_fields() {
		include_once( 'includes/table-editor-field-v5.php' );
	}


}

new acf_table_editor_field_plugin();