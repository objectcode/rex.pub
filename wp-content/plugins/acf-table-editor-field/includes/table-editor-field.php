<?php

class field_table_editor {

	function defaults() {
		return array(
			'number_columns' => '3',
			'min_rows'       => '0',
			'custom_headers' => 'simple',
			'header_names'   => '',
			'row_headers'    => '',
			'edit_columns'   => '',
			'drag_down'      => 'on',
			'table_width'    => 'on',
			'table_class'    => '',
		);
	}
	function input_admin_enqueue_scripts() {

		// register acf scripts
		wp_register_script( 'acf-input-table-editor', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/js/input.js', array( 'acf-input' ), ACF_TABLE_EDITOR_VERSION );
		wp_register_style( 'acf-input-table-editor', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/css/input.css', array( 'acf-input' ), ACF_TABLE_EDITOR_VERSION );

		wp_register_script( 'acf-handsontable', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/js/jquery.handsontable.full.js', array( 'acf-input-table-editor' ), ACF_TABLE_EDITOR_VERSION );
		wp_register_style( 'acf-handsontable', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/css/jquery.handsontable.full.css', array( 'acf-input-table-editor' ), ACF_TABLE_EDITOR_VERSION );

		// scripts
		wp_enqueue_script(
			array(
				'acf-input-table-editor',
				'acf-handsontable',
			)
		);

		// styles
		wp_enqueue_style(
			array(
				'acf-input-table-editor',
				'acf-handsontable',
			)
		);
	}

	function field_group_admin_enqueue_scripts() {
		wp_register_script( 'acf-field-group-table-editor', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/js/field-group.js', array( 'acf-field-group' ), ACF_TABLE_EDITOR_VERSION );
		wp_register_style( 'acf-field-group-table-editor', ACF_TABLE_EDITOR_PLUGIN_URL . 'assets/css/field-group.css', array( 'acf-field-group' ), ACF_TABLE_EDITOR_VERSION );

		// scripts
		wp_enqueue_script(
			array(
				'acf-field-group-table-editor',
			)
		);

		// styles
		wp_enqueue_style(
			array(
				'acf-field-group-table-editor',
			)
		);
	}

	function render_field( $field ) {

		$settings = array(
			'number_columns' => $field['number_columns'],
			'row_headers'    => $field['row_headers'],
			'edit_columns'   => $field['edit_columns'],
			'drag_down'      => $field['drag_down'],
			'min_rows'       => ( ( is_numeric( $field['min_rows'] ) ) ? $field['min_rows'] : '0' ),
			'table_width'    => $field['table_width'],
		);
		$settings = htmlspecialchars( json_encode( $settings ) );

		$table_headers = '';
		if ( $field['custom_headers'] && $field['custom_headers'] == 'custom'
			 && $field['header_names'] && $field['header_names'] != ''
		) {
			$headers       = preg_split( '/\r\n|\r|\n/', $field['header_names'] );
			$table_headers = htmlspecialchars( json_encode( $headers ) );
			$table_headers = str_replace( "\r", "", $table_headers );
		} else {
			if ( $field['custom_headers'] && $field['custom_headers'] == 'none' ) {
				$table_headers = 'none';
			}
		}
		$table_data = ( isset( $field['value'] ) ) ? $field['value'] : '';
		$table_data = $this->clean_table_data( $table_data, $field['number_columns'] );
		?>
		<div class="handsontable_editor_wrapper" data-table="<?php echo $field['key']; ?>">
			<div class="handsontable_editor"></div>
			<input type="hidden" class="table-settings" value="<?php echo $settings; ?>">
			<input type="hidden" class="table-values" name="<?php echo $field['name']; ?>" value="<?php echo $table_data; ?>">
			<input type="hidden" class="table-headers" value="<?php echo $table_headers; ?>">
			<input type="hidden" class="table-generated" value="false">
		</div>
	<?php
	}

	function format_value( $value, $post_id, $field, $template = false ) {
		// validate
		if ( ! $value ) {
			return false;
		}

		if ( ! $template ) {
			return $value;
		}

		$html = '<table class="acf-table-editor ' . $field['table_class'] . '">';

		$headers = false;
		if ( $field['custom_headers'] == 'simple' ) {
			for ( $alpha = 65; $alpha < ( 65 + $field['number_columns'] ); $alpha ++ ) {
				$headers[] = chr( $alpha );
			}
		} else {
			if ( $field['custom_headers'] == 'custom' && $field['header_names'] != '' ) {
				$headers = preg_split( '/\r\n|\r|\n/', $field['header_names'] );
			}
		}

		if ( $headers && is_array( $headers ) ) {
			$headers = $this->clean_headers( $headers, $field['number_columns'] );
			$html .= '<thead><tr>';
			foreach ( $headers as $th ) {
				$html .= '<th>' . $th . '</th>';
			}
			$html .= '</tr></thead>';
		}
		$html .= '<tbody>';
		$table_data = $value;
		$table_data = str_replace( '\"', '"', $table_data );
		$table_data = str_replace( "\'", "'", $table_data );
		$table_data = json_decode( $table_data );
		if ( $table_data ) {
			$table_data = $this->clean_columns( $table_data, $field['number_columns'] );
			$row_count  = 0;
			foreach ( $table_data as $tr ) {
				$row_count ++;
				if ( $field['min_rows'] > 0 && $row_count > ( count( $table_data ) - $field['min_rows'] ) ) {
					continue;
				}
				$html .= '<tr>';
				foreach ( $tr as $td_key => $td ) {
					$value = str_replace( '&quot;', '"', $td );
					$value = do_shortcode( stripslashes( $value ) );
					$html .= '<td>' . $value . '</td>';
				}
				$html .= '</tr>';
			}
		}
		$html .= '</tbody>';
		$html .= '</table>';

		return $html;
	}

	private function clean_headers( $headers, $columns ) {
		$data    = array( $headers );
		$headers = $this->clean_columns( $data, $columns, true );

		return $headers[0];
	}

	private function clean_columns( $data, $columns, $header = false ) {
		if ( ! is_array( $data ) ) {
			//$data = htmlspecialchars(json_encode( $data ), ENT_QUOTES, 'UTF-8');
			$data = json_decode( $data );
		}
		if ( $data ) {
			foreach ( $data as $row_key => $row ) {
				$count = count( $row );
				if ( $count == $columns ) {
					continue;
				}
				if ( $count < $columns ) {
					$alpha = 65 + $count;
					for ( $r = 0; $r <= ( $columns - $count - 1 ); $r ++ ) {
						$alpha            = $alpha + $r;
						$td               = ( $header ) ? chr( $alpha ) : "";
						$data[$row_key][] = $td;
					}
				}
				if ( $count > $columns ) {
					for ( $r = ( $count - 1 ); $r >= ( $columns ); $r -- ) {
						unset( $data[$row_key][$r] );
					}
				}
			}
		}

		return $data;
	}

	private function clean_table_data( $data, $columns ) {
		$data   = $this->clean_columns( $data, $columns );
		$string = json_encode( $data );
		$string = str_replace( '&quot;', '\u0022', $string );
		$string = str_replace( '\&quot;', '&quot;', htmlspecialchars( $string ) );

		return str_replace( "\'", '\u0027', $string );
	}

}