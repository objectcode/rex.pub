<?php
require_once ACF_TABLE_EDITOR_PLUGIN_DIR . 'includes/table-editor-field.php';

class acf_field_table_editor extends acf_field {

	private $table_editor;

	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	1.0
	*  @date	29/11/13
	*/

	function __construct() {
		$this->table_editor = new field_table_editor();
		// vars
		$this->name     = 'table_editor';
		$this->label    = __( "Table Editor", 'acf-table-editor' );
		$this->category = __( "Content", 'acf-table-editor' );
		$this->defaults = $this->table_editor->defaults();

		// do not delete!
		parent::__construct();
	}

	function input_admin_enqueue_scripts() {
		$this->table_editor->input_admin_enqueue_scripts();
	}

	function field_group_admin_enqueue_scripts() {
		$this->table_editor->field_group_admin_enqueue_scripts();
	}

	function format_value_for_api( $value, $post_id, $field ) {
		return $this->table_editor->format_value( $value, $post_id, $field, true );
	}

	function create_field( $field ) {
		$this->table_editor->render_field( $field );
	}

	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	*
	*  @type	action
	*  @since	1.0
	*  @date	29/11/13
	*
	*  @param	$field	- an array holding all the field's data
	*/

	function create_options( $field ) {
		// vars
		$key = $field['name'];
		?>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( "Number of columns", 'acf-table-editor' ); ?></label>
				<p><?php _e( "Specify the number of columns for the table", 'acf-table-editor' ) ?></p>
			</td>
			<td>
				<?php
				$number_columns = ( isset( $field['number_columns'] ) ) ? $field['number_columns'] : '';
				do_action(
					'acf/create_field', array(
						'type'  => 'number',
						'name'  => 'fields[' . $key . '][number_columns]',
						'value' => $number_columns,
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?> column-headers">
			<td class="label">
				<label><?php _e( 'Column Headers', 'acf-table-editor' ); ?></label>
				<p><?php _e( 'Configure the column headers for the table', 'acf-table-editor' ) ?></p>
			</td>
			<td>
				<?php
				$custom_headers = ( isset( $field['custom_headers'] ) ) ? $field['custom_headers'] : 'simple';
				do_action(
					'acf/create_field', array(
						'type'    => 'radio',
						'name'    => 'fields[' . $key . '][custom_headers]',
						'value'   => $custom_headers,
						'layout'  => 'horizontal',
						'choices' => array(
							'simple' => __( "Simple", 'acf-table-editor' ),
							'custom' => __( "Custom", 'acf-table-editor' ),
							'none'   => __( "None", 'acf-table-editor' ),
						)
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?> column-header-names">
			<td class="label">
				<label><?php _e( 'Custom Header Names', 'acf-table-editor' ); ?></label>
				<p><?php _e( 'Enter a column header on a new line', 'acf-table-editor' ) ?></p>
			</td>
			<td>
				<?php
				do_action(
					'acf/create_field', array(
						'type'  => 'textarea',
						'name'  => 'fields[' . $key . '][header_names]',
						'value' => $field['header_names'],
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Minimum Rows', 'acf' ); ?></label>
				<p><?php _e( 'Specify the number of minimum spare rows for the table', 'acf-table-editor' ) ?></p>
			</td>
			<td>
				<?php
				$min_rows = ( isset( $field['min_rows'] ) ) ? $field['min_rows'] : '0';
				do_action(
					'acf/create_field', array(
						'type'  => 'number',
						'name'  => 'fields[' . $key . '][min_rows]',
						'value' => $min_rows,
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Row Numbers', 'acf-table-editor' ); ?></label>
			</td>
			<td>
				<?php
				do_action(
					'acf/create_field', array(
						'type'    => 'checkbox',
						'name'    => 'fields[' . $key . '][row_headers]',
						'value'   => $field['row_headers'],
						'layout'  => 'horizontal',
						'choices' => array(
							'on' => __( 'Show row numbers on the table', 'acf-table-editor' ),
						)
					)
				);
				?>
			</td>
		</tr>
		<?php /*
<tr class="field_option field_option_<?php echo $this->name; ?>">
    <td class="label">
        <label><?php _e("Editable Columns",'acf'); ?></label>
    </td>
    <td>
        <?php
        do_action('acf/create_field', array(
            'type'	    =>	'checkbox',
            'name'		=>	'fields['.$key.'][edit_columns]',
            'value'		=>	$field['edit_columns'],
            'layout'	=>	'horizontal',
            'choices'	=>	array(
                'on'	=>	__("Allow users to add or remove columns",'acf'),
            )
        ));
        ?>
    </td>
</tr>
 */
		?>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Full Width Table', 'acf-table-editor' ); ?></label>
			</td>
			<td>
				<?php
				do_action(
					'acf/create_field', array(
						'type'    => 'checkbox',
						'name'    => 'fields[' . $key . '][table_width]',
						'value'   => $field['table_width'],
						'layout'  => 'horizontal',
						'choices' => array(
							'on' => __( 'Makes the table width 100%', 'acf-table-editor' ),
						)
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Drag Down', 'acf-table-editor' ); ?></label>
			</td>
			<td>
				<?php
				do_action(
					'acf/create_field', array(
						'type'    => 'checkbox',
						'name'    => 'fields[' . $key . '][drag_down]',
						'value'   => $field['drag_down'],
						'layout'  => 'horizontal',
						'choices' => array(
							'on' => __( 'Allow users to drag down cells to repeat values', 'acf-table-editor' ),
						)
					)
				);
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Table Class', 'acf-table-editor' ); ?></label>

				<p><?php _e( 'CSS class for the table HTML output', 'acf-table-editor' ) ?></p>
			</td>
			<td>
				<?php
				do_action(
					'acf/create_field', array(
						'type'  => 'text',
						'name'  => 'fields[' . $key . '][table_class]',
						'value' => $field['table_class']
					)
				);
				?>
			</td>
		</tr>
	<?php

	}

}

new acf_field_table_editor();

?>
