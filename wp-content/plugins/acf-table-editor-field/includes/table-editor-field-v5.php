<?php
require_once ACF_TABLE_EDITOR_PLUGIN_DIR . 'includes/table-editor-field.php';

class acf_field_table_editor extends acf_field {

	private $table_editor;

	function __construct() {
		$this->table_editor = new field_table_editor();

		$this->name     = 'table_editor';
		$this->label    = __( 'Table Editor', 'acf-table-editor' );
		$this->category = 'content';
		$this->defaults = $this->table_editor->defaults();

		// do not delete!
		parent::__construct();

	}

	function input_admin_enqueue_scripts() {
		$this->table_editor->input_admin_enqueue_scripts();
	}

	function field_group_admin_enqueue_scripts() {
		$this->table_editor->field_group_admin_enqueue_scripts();
	}

	function format_value( $value, $post_id, $field, $template = true ) {
		return $this->table_editor->format_value( $value, $post_id, $field, $template );
	}

	function render_field( $field ) {
		$this->table_editor->render_field( $field );
	}

	function render_field_settings( $field ) {

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Number of columns', 'acf-table-editor' ),
				'instructions' => __( 'Specify the number of columns for the table', 'acf-table-editor' ),
				'type'         => 'number',
				'name'         => 'number_columns'
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Column Headers', 'acf-table-editor' ),
				'instructions' => __( 'Configure the column headers for the table', 'acf-table-editor' ),
				'type'         => 'radio',
				'name'         => 'custom_headers',
				'layout'       => 'horizontal',
				'choices'      => array(
					'simple' => __( 'Simple', 'acf-table-editor' ),
					'custom' => __( 'Custom', 'acf-table-editor' ),
					'none'   => __( 'None', 'acf-table-editor' ),
				)
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Custom Header Names', 'acf-table-editor' ),
				'instructions' => __( 'Enter a column header on a new line', 'acf-table-editor' ),
				'type'         => 'textarea',
				'name'         => 'header_names'
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Minimum Rows', 'acf-table-editor' ),
				'instructions' => __( 'Specify the number of minimum spare rows for the table', 'acf-table-editor' ),
				'type'         => 'number',
				'name'         => 'min_rows'
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Row Numbers', 'acf-table-editor' ),
				'instructions' => '',
				'type'         => 'checkbox',
				'name'         => 'row_headers',
				'layout'  => 'horizontal',
				'choices' => array(
					'on' => __( 'Show row numbers on the table', 'acf-table-editor' ),
				)
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Full Width Table', 'acf-table-editor' ),
				'instructions' => '',
				'type'         => 'checkbox',
				'name'         => 'table_width',
				'layout'  => 'horizontal',
				'choices' => array(
					'on' => __( 'Makes the table width 100%', 'acf-table-editor' ),
				)
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Drag Down', 'acf-table-editor' ),
				'instructions' => '',
				'type'         => 'checkbox',
				'name'         => 'drag_down',
				'layout'  => 'horizontal',
				'choices' => array(
					'on' => __( 'Allow users to drag down cells to repeat values', 'acf-table-editor' ),
				)
			)
		);

		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Table Class', 'acf-table-editor' ),
				'instructions' => __( 'CSS class for the table HTML output', 'acf-table-editor' ),
				'type'         => 'text',
				'name'         => 'table_class'
			)
		);

	}

}
new acf_field_table_editor();