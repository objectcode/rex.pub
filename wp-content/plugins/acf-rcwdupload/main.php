<?php

// UNCOMMENT THIS BLOCK ONLY FOR DEBUG

/* 
ini_set('display_errors', '1');
ini_set('error_reporting', E_ALL);
*/

if (!class_exists('acf_rcwd_clean_exit')){
	
	class acf_rcwd_clean_exit extends RuntimeException{}
	
}

class acf_rcwdup_m{

	function __construct(){

		define( 'ACF_RCWDUPLOAD_UP_TEMP_DIR', WP_CONTENT_DIR.'/acfrcwduploads_temp' );
		define( 'ACF_RCWDUPLOAD_UP_DIR', WP_CONTENT_DIR.'/acfrcwduploads' );
		define( 'ACF_RCWDUPLOAD_UP_TEMP_URL', get_site_url().'/wp-content/acfrcwduploads_temp' );
		define( 'ACF_RCWDUPLOAD_UP_URL', get_site_url().'/wp-content/acfrcwduploads' );	
		define( 'ACF_RCWDUPLOAD_ITEM_ID', '5078456' ); // DEFINE ENVATO ITEM ID

		add_action( 'admin_menu', array( $this, 'admin_menu' ), 11 );
		add_action( 'generate_rewrite_rules', array( $this, 'generate_rewrite_rules' ) );	
		add_action( 'admin_init', array( $this, 'flush_rules' ) );	
		add_action( 'init', array( $this, 'clean_temp' ) );	
		
		if(!$this->serial_is_valid()){
			
			add_action( 'admin_notices', array( $this, 'error_not_authenticated' ) );
			
			return false;
			
		}
		
		if(!wp_mkdir_p(ACF_RCWDUPLOAD_UP_DIR)){
			
			add_action( 'admin_notices', array( $this, 'error_folder_permissions' ) );
			
			return false;
			
		}else{
			
			wp_mkdir_p(ACF_RCWDUPLOAD_UP_TEMP_DIR);
			
		}

		add_action( 'init', array( $this, 'handle_upload_request' ) );				
		add_action( 'acf/input/form_data', array( $this, 'acf_input_form_data' ), 10, 1 );				
				
	}

	function admin_menu(){
		
		add_submenu_page( 'edit.php?post_type=acf', __( 'Rcwd Upload settings', 'acf-rcwdupload' ), __( 'Rcwd Upload settings', 'acf-rcwdupload' ), 'manage_options', self::get_var('domain'), array( $this, 'settings' ) );		
		add_submenu_page( 'edit.php?post_type=acf-field-group', __( 'Rcwd Upload settings', 'acf-rcwdupload' ), __( 'Rcwd Upload settings', 'acf-rcwdupload' ), 'manage_options', self::get_var('domain'), array( $this, 'settings' ) );		
	}
	
	function settings(){
		
		include_once('envato/check.php');
		
	}
	
	static function get_var($var){
		
		switch($var){
			case 'version'	:
				return '1.1.6';
				break;
			case 'name' 	: 
				return 'rcwdupload';
				break;	
			case 'label' 	: 
				return __( 'Rcwd Upload', 'acf-rcwdupload' );
				break;
			case 'category' : 
				return __( 'Content', 'acf-rcwdupload' );
				break;
			case 'domain' 	: 
				return 'acf-rcwdupload';
			case 'defaults':
				return array(
			
					'post_id'					=> 0,
					'clientpreview'				=> 'N',
					'clientpreview_max_width'	=> 150,
					'clientpreview_max_height'	=> 150,					
					'clientpreview_crop'    	=> 'N',
					'collection'				=> 'N',
					'collection_key'			=> '',
					'filters'    				=> 'Y',
					'filter_images'				=> 'jpg,gif,png',
					'filter_docs'   			=> 'pdf,doc,docx,xml,txt,xls,xlsx',
					'filter_compr'				=> 'zip,rar,ace',
					'filter_others'				=> '',
					'resize'					=> 'N',
					'max_width'					=> '0',
					'max_height'				=> '0',
					'max_quality'				=> '90',
					'csresize'					=> 'N',
					'autoupload'				=> 'N',
					'sizelimit'					=>  '',
					'sizelimit_type'			=>  'kb'					
									
				);
														
		}
		return false;
				
	}

    static function get_dir($file){
		
        $dir 	= trailingslashit(dirname($file));
        $count 	= 0;
        
        // sanitize for Win32 installs
        $dir = str_replace('\\' ,'/', $dir); 
        
        // if file is in plugins folder
        $wp_plugin_dir 	= str_replace('\\' ,'/', WP_PLUGIN_DIR); 
        $dir 			= str_replace($wp_plugin_dir, plugins_url(), $dir, $count);
        
        if( $count < 1 ){
	        // if file is in wp-content folder
	        $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
	        $dir 			= str_replace($wp_content_dir, content_url(), $dir, $count);
        }
        
        if( $count < 1 ){
	        // if file is in ??? folder
	        $wp_dir = str_replace('\\' ,'/', ABSPATH); 
	        $dir 	= str_replace($wp_dir, site_url('/'), $dir);
        }

        return $dir;
		
    }
	
	static function admin_head($v){

		if(self::serial_is_valid()){
			
			global $pagenow;	
	
			$pst = 0;
			
			if(is_admin()){
				
				if( isset($_GET['page']) and substr( $_GET['page'], 0, 11 ) == 'acf-options' ){
					
					$pst 		= 'options';
					$foldercode = 'options_'.substr( $_GET['page'], 12 );

					$opfolder = substr( $_GET['page'], 12 );

					if(empty($opfolder))
						$foldercode = 'options_rt';
												
				}
		
				if( $pagenow == 'edit-tags.php' and isset($_GET['taxonomy']) ){
					
					$foldercode = 'tax_'.$_GET['taxonomy'];
		
					if( isset($_GET['action']) and $_GET['action'] == "edit" ){
						
						$_GET['tag_ID']	= filter_var($_GET['tag_ID'], FILTER_SANITIZE_NUMBER_INT);
						$foldercode 	.= '_'.$_GET['tag_ID'];
						
					}
								
				}elseif(in_array( $pagenow, array( 'profile.php', 'user-new.php', 'user-edit.php' ) )){
					
					global $user_id;
					
					$foldercode = 'user_'.$user_id;
					
				}elseif( in_array( $pagenow, array( 'post.php', 'post-new.php', 'media.php' ) ) ){
					
					global $post;
					
					$pst 		= 'post';
					$foldercode = 'post_'.$post->ID;	
						
				}
				
			
			}else{
				
				if( is_page() or is_single() ){
					
					global $post, $rcwd_custom_post_id;

					$pst = 'post';

					if( !empty($rcwd_custom_post_id) and (int)$rcwd_custom_post_id > 0)
						$foldercode = 'post_'.(int)$rcwd_custom_post_id;
					else
						$foldercode = 'post_'.$post->ID;	
																						
				}
				
			}
?>
			<script type="text/javascript">
                acf_rcwdupload_url 					= '<?php echo admin_url('/?acfrcwdupload&post='.$pst.'&foldercode='.$foldercode) ?>';
                acf_rcwdupload_flash_swf_url 		= '<?php echo self::get_dir(__FILE__) ?>js/jquery/plupload/Moxie.swf';
                acf_rcwdupload_silverlight_xap_url 	= '<?php echo self::get_dir(__FILE__) ?>js/jquery/plupload/Moxie.xap';
							
				jQuery(function($){
					
					//$(".rcwdacflupload-tabs").tabs();
				
				});				
            </script>
<?php		

		}
	
	}

	function acf_input_form_data($args){

		global $acf_rcwdupload_5_post_id;

		$acf_rcwdupload_5_post_id = $args['post_id'];
		
	}
		
	static function group_admin_enqueue_scripts(){
		
		if(self::serial_is_valid()){
			
			wp_enqueue_style( 'acf-'.self::get_var('name').'-style', self::get_dir(__FILE__).'css/style-group.css');

		}
		
	}
	
	static function admin_enqueue_scripts($v){

		if(self::serial_is_valid()){
			
			wp_deregister_script('plupload');
			wp_enqueue_script( 'plupload', self::get_dir(__FILE__).'js/jquery/plupload/plupload.full.min.js', array('jquery'), '2.1.1' );
			
			wp_enqueue_script( 'rcwd-impromptu', self::get_dir(__FILE__).'js/jquery/impromptu/jquery-impromptu.min.js', array('jquery'), '5.2.5' );
			wp_enqueue_style( 'rcwd-impromptu', self::get_dir(__FILE__).'js/jquery/impromptu/jquery-impromptu.min.css');
			
			wp_enqueue_script( 'acf-'.self::get_var('name').'-scripts', self::get_dir(__FILE__).'js/scripts.js');
			
			//wp_enqueue_script('jquery-ui-dialog'); 
			//wp_enqueue_style('wp-jquery-ui-dialog');		
			//wp_enqueue_style( 'acf-'.self::get_var('name').'-jqueryui', self::get_dir(__FILE__).'css/jquery-ui-custom.min.css');
			
			wp_enqueue_style( 'acf-'.self::get_var('name').'-style', self::get_dir(__FILE__).'css/style.css');
			wp_enqueue_style( 'acf-'.self::get_var('name').'-jquerytabs', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');

			$wpversion = get_bloginfo('version');
			
			if(version_compare( $wpversion, '3.8', '>='))
				wp_enqueue_style( 'acf-'.self::get_var('name').'-style-2', self::get_dir(__FILE__).'css/style-2.css');
			
			$i18n = array(	'err' => 'We have a problem...',
							'err600' => sprintf( __( 'Sorry, the file size must not be greater than %s kb. Please try another file.', 'acf-rcwdupload' ), round( (int)wp_max_upload_size() / 1024 ) ),
							'err601' => __( 'Sorry, the file extension is not permitted. Please try another file.', 'acf-rcwdupload' ),
			);
			wp_localize_script( 'acf-'.self::get_var('name').'-scripts', 'acfrcwdi18n', $i18n );
			
			if($v == 3){
				
				self::group_admin_enqueue_scripts();
				
			}

		}		
				
	}
	
	static function create_options($args){
		
		if(self::serial_is_valid()){
			
			$v			= $args['v'];
			$field		= $args['field'];
			$key 		= $field['name'];
			$key_		= str_replace( '][', '___', $key ); //replace required to work with field inside a repeater
			$defaults 	= self::get_var('defaults');

			$field = array_merge( $defaults, $field );	
										
			if(file_exists(ACF_RCWDUPLOAD_UP_DIR)){
?>
                <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                    <td class="label">
                        <label><?php _e( 'Show image preview before upload', 'acf-rcwdupload' ); ?></label>
                    </td>
                    <td>
<?php
						$f_arr = array(	'type' 		=> 'select',
										'name'    	=> 'fields['.$key.'][clientpreview]',
										'value'  	=> $field['clientpreview'],
										'layout'  	=> 'horizontal',
                                        'class'		=>  self::get_var('name').'_group_clientpreview',
										'choices' 	=> array(	'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
																'N'	=> __( 'No', 'acf-rcwdupload' ) ) );
																
                        if($v == 4)
                            do_action( 'acf/create_field', $f_arr );
                        else
                            $this->parent->create_field($f_arr);
?>
					</td>
				</tr>
                <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                    <td class="label">
                        <script type='text/javascript'>
                            jQuery(document).ready(function($){
                
                                // Show/hide filter options ______________________________________________________
                                
                                    if($('.<?php echo self::get_var('name') ?>_group_filters_<?php echo $key_ ?>').val() == 'Y'){
                                        $('#<?php echo self::get_var('name') ?>_group_foptions_<?php echo $key_ ?>').fadeTo( 300, 1 );
                                    }
                
                                    $('.<?php echo self::get_var('name') ?>_group_filters_<?php echo $key_ ?>').on("change", function(event){
                                     if($(this).val() == 'Y'){
                                            $('#<?php echo self::get_var('name') ?>_group_foptions_<?php echo $key_ ?>').fadeTo( 300, 1 );
                                        }else{
                                            $('#<?php echo self::get_var('name') ?>_group_foptions_<?php echo $key_ ?>').fadeTo( 300, 0, function(){
                                                $(this).css( 'display', 'none' );
                                            });
                                        }
                                    });	
                                                                        
                                // Show/hide resize options ______________________________________________________
                                
                                    if($('.<?php echo self::get_var('name') ?>_group_resize_<?php echo $key_ ?>').val() == 'Y'){
                                        $('#<?php echo self::get_var('name') ?>_group_maxdims_<?php echo $key_ ?>').fadeTo( 300, 1 );
                                    }
                    
                                    $('.<?php echo self::get_var('name') ?>_group_resize_<?php echo $key_ ?>').on("change", function(event){
                                        if($(this).val() == 'Y'){
                                            $('#<?php echo self::get_var('name') ?>_group_maxdims_<?php echo $key_ ?>').fadeTo( 300, 1 );
                                        }else{
                                            $('#<?php echo self::get_var('name') ?>_group_maxdims_<?php echo $key_ ?>').fadeTo( 300, 0, function(){
                                                $(this).css( 'display', 'none' );
                                            });
                                        }
                                    });	
                                                    
                            });		
                        </script>                
                        <label><?php _e( 'Filters', 'acf-rcwdupload' ); ?></label>
                    </td>
                    <td>
                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Filters the files', 'acf-rcwdupload' ); ?></label>
<?php
						$f_arr = array(	'type' 		=> 'select',
										'name'    	=> 'fields['.$key.'][filters]',
										'value'  	=> $field['filters'],
										'layout'  	=> 'horizontal',
										'class'   	=> self::get_var('name')."_group_filters ".self::get_var('name')."_group_filters_{$key_}",
										'choices' 	=> array(	'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
																'N'	=> __( 'No', 'acf-rcwdupload' ) ) );
																	
                        if($v == 4)
                            do_action( 'acf/create_field', $f_arr );
                        else
                            $this->parent->create_field($f_arr);
?>
                        <table class="<?php echo self::get_var('name') ?>_group_foptions" id="<?php echo self::get_var('name') ?>_group_foptions_<?php echo $key_ ?>" style="width:100%">
                            <tbody>
                                <tr>
                                    <td style="padding-left:0;">
                                        <p class="<?php echo self::get_var('name') ?>_group_description"><?php _e( 'Enter the file extensions, separated by commas, to enable file uploading.', 'acf-rcwdupload' ); ?></a></p>
                                    </td>
                                </tr>
                                <tr>                        
                                    <td style="padding-left:0;">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Images', 'acf-rcwdupload' ); ?></label>
<?php 
										$f_arr = array(	'type'	=>	'text',
														'name'	=>	'fields['.$key.'][filter_images]',
														'value'	=>	strtolower($field['filter_images']) );
														                      
                                        if($v == 4)
                                            do_action( 'acf/create_field', $f_arr );
                                        else
                                            $this->parent->create_field($f_arr);
?>	
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:0;">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Documents','acf-rcwdupload' ); ?></label>
<?php 
										$f_arr = array(	'type'	=>	'text',
														'name'	=>	'fields['.$key.'][filter_docs]',
														'value'	=>	strtolower($field['filter_docs']) );
															    
                                        if($v == 4)
                                            do_action( 'acf/create_field', $f_arr );
                                        else
                                            $this->parent->create_field($f_arr);
?>	
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="padding-left:0;">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Compressed files', 'acf-rcwdupload' ); ?></label>
<?php 
										$f_arr = array(	'type'	=>	'text',
														'name'	=>	'fields['.$key.'][filter_compr]',
														'value'	=>	strtolower($field['filter_compr']) );
															    
                                        if($v == 4)
                                            do_action( 'acf/create_field', $f_arr );
                                        else
                                            $this->parent->create_field($f_arr);
?>	
                                    </td>
                                </tr> 
                                <tr>
                                    <td style="padding-left:0;">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Other files', 'acf-rcwdupload' ); ?></label>
<?php 
										$f_arr = array(	'type'	=>	'text',
														'name'	=>	'fields['.$key.'][filter_others]',
														'value'	=>	strtolower($field['filter_others']) );
															   
                                        if($v == 4)
                                            do_action( 'acf/create_field', $f_arr );
                                        else
                                            $this->parent->create_field($f_arr);
?>	
                                    </td>
                                </tr>                                                                   
                            </tbody>
                        </table> 
                    </td>
                </tr>
                <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                    <td class="label">
                        <label><?php _e( 'Resize images', 'acf-rcwdupload' ); ?></label>
                    </td>
                    <td>
                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Enable resizing','acf-rcwdupload' ); ?></label>
<?php
                        if($v == 4){
                            $f_arr = array(	'type' 		=> 'select',
                                            'name'    	=> 'fields['.$key.'][resize]',
                                            'value'  	=> $field['resize'],
                                            'layout'  	=> 'horizontal',
                                            'class'   	=> self::get_var('name')."_group_resize ".self::get_var('name')."_group_resize_{$key_}",
                                            'choices' 	=> array(	'N'	=> __( 'No', 'acf-rcwdupload' ),
                                                                    'Y'	=> __( 'Yes', 'acf-rcwdupload' ) )
                                          );
                            do_action( 'acf/create_field', $f_arr );
                        }else{
                            $this->parent->create_field($f_arr);
                        }
?>            
                        <table class="<?php echo self::get_var('name') ?>_group_maxdims" id="<?php echo self::get_var('name') ?>_group_maxdims_<?php echo $key_ ?>">
                            <tbody>
                                <tr>            
                                    <td style="padding-left:0;">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Width', 'acf-rcwdupload' ); ?></label>
<?php 
                                        if($v == 4){
                                            $f_arr = array(	'type'	=>	'text',
                                                            'name'	=>	'fields['.$key.'][max_width]',
                                                            'value'	=>	(int)$field['max_width'],
                                                            'class'	=>  self::get_var('name').'_group_maxdim'
                                                          );
                                            do_action( 'acf/create_field', $f_arr );
                                        }else{
                                            $this->parent->create_field($f_arr);
                                        }
?>					
                                    </td>
                                    <td>
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Height', 'acf-rcwdupload' ); ?></label>
<?php 
                                        if($v == 4){
                                            $f_arr = array(	'type'	=>	'text',
                                                            'name'	=>	'fields['.$key.'][max_height]',
                                                            'value'	=>	(int)$field['max_height'],
                                                            'class'	=>  self::get_var('name').'_group_maxdim'
                                                          );
                                            do_action( 'acf/create_field', $f_arr );
                                        }else{
                                            $this->parent->create_field($f_arr);
                                        }
?>
                                    </td>
                                    <td>
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Quality', 'acf-rcwdupload' ); ?></label>
<?php 
                                        if($field['max_quality'] == '')
                                            $field['max_quality'] = 90;
    
                                        if($v == 4){
                                            $f_arr = array(	'type'	=>	'text',
                                                            'name'	=>	'fields['.$key.'][max_quality]',
                                                            'value'	=>	(int)$field['max_quality'],
                                                            'class'	=>  self::get_var('name').'_group_maxdim'
                                                          );
                                            do_action( 'acf/create_field', $f_arr );
                                        }else{
                                            $this->parent->create_field($f_arr);
                                        }
?>
                                    </td>                            
                                </tr>
                                <tr>
                                    <td colspan="3" scope="row"><p class="<?php echo self::get_var('name') ?>_group_description"><?php _e( '0 = proportional resizing.','acf-rcwdupload' ); ?></a></p></td>
                                </tr> 
                                <tr>
                                    <td colspan="3" scope="row">
                                        <label class="<?php echo self::get_var('name') ?>_group_label"><?php _e( 'Resize the images to clientside', 'acf-rcwdupload' ); ?></label>
<?php
                                            if($v == 4){
                                                $f_arr = array(	'type' 		=> 'select',
                                                                'name'    	=> 'fields['.$key.'][csresize]',
                                                                'value'  	=> $field['csresize'],
                                                                'layout'  	=> 'horizontal',
                                                                'class'		=>  self::get_var('name').'_group_csresize',
                                                                'choices' 	=> array(	'N'	=> __( 'No', 'acf-rcwdupload' ),
                                                                                        'Y'	=> __( 'Yes', 'acf-rcwdupload' ) )
                                                              );
                                                do_action( 'acf/create_field', $f_arr );
                                            }else{
                                                $this->parent->create_field($f_arr);
                                            }
?>
                                    </td>                        
                                </tr>
                                <tr>
                                    <td colspan="3" scope="row"><p class="<?php echo self::get_var('name') ?>_group_description"><?php _e( 'Images will be automatically resized before the upload step in order to reduce the weight.', 'acf-rcwdupload' ); ?></a></p></td>
                                </tr>                                                 
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                    <td class="label">
                        <label><?php _e( 'Upload the file after selection', 'acf-rcwdupload' ); ?></label>
                    </td>
                    <td>
<?php
						$f_arr = array(	'type' 		=> 'select',
										'name'    	=> 'fields['.$key.'][autoupload]',
										'value'  	=> $field['autoupload'],
										'layout'  	=> 'horizontal',
                                        'class'		=>  self::get_var('name').'_group_autoupload',
										'choices' 	=> array(	'N'	=> __( 'No', 'acf-rcwdupload' ),
																'Y'	=> __( 'Yes', 'acf-rcwdupload' ) ) );
																
                        if($v == 4)
                            do_action( 'acf/create_field', $f_arr );
                        else
                            $this->parent->create_field($f_arr);
?>
                        
					</td>
				</tr>                   
<?php
            }else{
?>
                <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                    <td class="label">
                    </td>
                    <td>
<?php			
                        self::error_folder_permissions(false);	
?>            
                    </td>
                </tr>
<?php			
            }
				
		}else{

?>
            <tr class="field_option field_option_<?php echo self::get_var('name'); ?>">
                <td class="label">
                </td>
                <td>
<?php			
                   self::error_not_authenticated(false);	
?>            
                </td>
            </tr>
<?php
		}
		
	}
	
	static function create_field($args){		

		if(self::serial_is_valid()){
		
			global $pagenow;	
		
			$v				= $args['v'];
			$field			= $args['field'];
			$value			= '';
			$defaults		= self::get_var('defaults');
			$field 			= array_merge( $defaults, $field );

			if( is_array($args['field']) and is_array($args['field']['value']) )
				$value = $field['value']['file'];
	
			$maxfiles 					= 9999;
			$max_file_size				= round( (int)wp_max_upload_size() / 1024 ).'kb'; // This string can be in the following formats 100b, 10kb, 10mb, 1gb
			$multiselection				= true;
			$overwrite					= false;
			$clientpreview 				= $field['clientpreview'];
			$clientpreview_max_width	= $field['clientpreview_max_width'];
			$clientpreview_max_height 	= $field['clientpreview_max_height'];
			$clientpreview_crop 		= $field['clientpreview_crop'];
			$autoupload 				= $field['autoupload'];		
			$collection 				= $field['collection'];		
			$collection_key 			= $field['collection_key'];		
			$is_hidden 					= !defined('RCWD_ACFUPLOAD_DEBUG') ? 'hidden' : 'text';	
			$fieldlinked 				= false;
				
			if($maxfiles == 1){
				
				$multiselection	= false;
				$overwrite		= true;
				
			}
	
			// resize settings ___________________________________________
			
				$doresize 	= $field['resize'];
				$resize		= '';
				$csresize	= '';
				
				if($doresize == 'Y'){
		
					// serverside resize settings
					
						$resize = array('quality' => (int)$field['max_quality']);
						if((int)$field['max_width'] > 0)
							$resize['width'] = $field['max_width'];
						if((int)$field['max_height'] > 0)
							$resize['height'] = $field['max_height'];	
						$resize = json_encode($resize);				
				
					// clientside resize settings
				
						$do_csresize	= $field['csresize'];
						$csresize		= '';
						if($do_csresize == 'Y'){
							$csresize = array('quality' => 100);
							if((int)$field['max_width'] > 0)
								$csresize['width'] = $field['max_width'];
							if((int)$field['max_height'] > 0)
								$csresize['height'] = $field['max_height'];		
							$csresize = json_encode($csresize);		
						}
		
				}
			
			$dofilters = $field['filters'];
			
			if($dofilters == 'Y'){
				
				$upfilters 		= array();
				$filter_images 	= preg_replace('/\s+/', '', strtolower($field['filter_images']));
				$filter_docs 	= preg_replace('/\s+/', '', strtolower($field['filter_docs']));
				$filter_compr 	= preg_replace('/\s+/', '', strtolower($field['filter_compr']));
				$filter_others 	= preg_replace('/\s+/', '', strtolower($field['filter_others']));
				
				if( $filter_images != '')
					$upfilters[] = array( 'title' => __( 'Image files', '' ), 'extensions' => $filter_images );
					
				if( $filter_docs != '')
					$upfilters[] = array( 'title' => __( 'Document files', '' ), 'extensions' => $filter_docs );
					
				if( $filter_compr != '')
					$upfilters[] = array( 'title' => __( 'Compressed files', '' ), 'extensions' => $filter_compr );
					
				if( $filter_others != '')
					$upfilters[] = array( 'title' => __( 'Various files', '' ), 'extensions' => $filter_others );
						
				if(count($upfilters) > 0)
					$upfilters = json_encode($upfilters);
					
			}
			
			wp_mkdir_p(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$field['key']);
			wp_mkdir_p(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key']);
			
			if(is_admin()){
				
				if($pagenow == 'admin-ajax.php'){
					
					if($_POST['page_type'] == 'taxonomy'){
						
						$foldercode = 'tax_'.$_POST['option_name'];
				
					}elseif($_POST['page_type'] == 'user'){
				
						$foldercode = $_POST['option_name'];
						
					}
					
				}else{
					
					if( isset($_GET['page']) and substr( $_GET['page'], 0, 11 ) == 'acf-options' ){
												
						$foldercode = 'options_'.substr( $_GET['page'], 12 );

						$opfolder = substr( $_GET['page'], 12 );
	
						if(empty($opfolder))
							$foldercode = 'options_rt';
													
					}

					if( $pagenow == 'edit-tags.php' and isset($_GET['taxonomy']) ){
						
						$foldercode = 'tax_'.$_GET['taxonomy'];
			
						if( isset($_GET['action']) and $_GET['action'] == "edit" ){
							
							$_GET['tag_ID']	= filter_var( $_GET['tag_ID'], FILTER_SANITIZE_NUMBER_INT );
							$foldercode 	.= '_'.$_GET['tag_ID'];
							
						}
									
					}
								
					if( in_array( $pagenow, array( 'post.php', 'post-new.php', 'media.php' ) ) ){
						
						global $post;
						
						if(class_exists('SitePress')){
							
							$lang = rtrim( strip_tags( isset($_GET[ 'lang' ]) ? $_GET[ 'lang' ] : '' ), '/' );
							
							if(!empty($lang)){
								
								global $iclTranslationManagement;
								
								if (isset($iclTranslationManagement)){
								
									$fname = $field['_name'];
									
									if( !empty($fname) and isset($iclTranslationManagement->settings['custom_fields_translation'][$fname]) ){
										
										$trid 			= SitePress::get_element_trid( $post->ID, 'post_'.get_post_type($post->ID) );
										$cft			= (int)$iclTranslationManagement->settings['custom_fields_translation'][$fname];
										$translations	= TranslationManagement::get_element_translations( $trid, 'post_'.get_post_type($post->ID) );
										$post_lng		= SitePress::get_language_for_element($post->ID, 'post_'.get_post_type($post->ID));
										$trid_lng		= SitePress::get_language_for_element($trid, 'post_'.get_post_type($post->ID));
							
										if( $cft == 1 and $post_lng != $trid_lng){
											
											$field 			= get_field_object( $fname, $trid );
											$fieldlinked	= true;
											
										}
									
									}

								}
								
							}

							$foldercode = 'post_'.$trid;	
						
						}else
							$foldercode = 'post_'.$post->ID;	
							
					}
					
				}

				$foldercode_temp = $foldercode;
			
			}else{
				
				if( is_page() or is_single() ){
					
					global $post, $rcwd_custom_post_id, $acf_rcwdupload_5_post_id;
					
					$pst = 'post';

					if( function_exists('acf_get_setting') and version_compare( acf_get_setting('version'), "5", ">=" ) ){

						if( !empty($acf_rcwdupload_5_post_id) and (int)$acf_rcwdupload_5_post_id > 0)
							$foldercode = 'post_'.(int)$acf_rcwdupload_5_post_id;
						else
							$foldercode = 'post_'.$post->ID;

						$foldercode_temp = 'post_'.$post->ID;
												
					}else{

						if( !empty($rcwd_custom_post_id) and (int)$rcwd_custom_post_id > 0)
							$foldercode = 'post_'.(int)$rcwd_custom_post_id;
						else
							$foldercode = 'post_'.$post->ID;

						$foldercode_temp = 'post_'.$post->ID;
												
					}
					
				}
				
			}

			if($value != '')
				$nf_txt = __( 'To change with:', 'acf-rcwdupload' );
			else
				$nf_txt = __( 'File:', 'acf-rcwdupload' );
?>
			<div id="<?php echo $field['id']?>-acf_rcwdupload" class="acf_rcwdupload">
			 
				<div id="<?php echo $field['id']?>-container" class="rcwdplupload-container" data-fid="<?php echo $field['id']?>" data-fkey="<?php echo $field['key']?>" data-maxf="<?php echo $maxfiles ?>" data-clientpreview='<?php echo $clientpreview ?>' data-clientpreview_max_width='<?php echo $clientpreview_max_width ?>' data-clientpreview_max_height='<?php echo $clientpreview_max_height ?>' data-clientpreview_crop='<?php echo $clientpreview_crop ?>' data-upflrs='<?php echo $upfilters ?>' data-maxfsize="<?php echo $max_file_size ?>" data-overw="<?php echo $overwrite ?>" data-resize='<?php echo $resize ?>' data-csresize='<?php echo $csresize ?>' data-msel='<?php echo $multiselection ?>' data-nftxt="<?php echo $nf_txt ?>" data-autoupload='<?php echo $autoupload ?>' data-collection='<?php echo $collection ?>'  data-folder='<?php echo $foldercode_temp ?>'>
<?php
					if(!$fieldlinked){
?>                
						<span id="<?php echo $field['id']?>-ddbox" class="rcwdplupload-ddbox"><span class="rcwdplupload-ddbox-inner"><?php _e( 'Drag your file here', 'acf-rcwdupload' ) ?></span></span>
<?php
					}else{
						
						echo '<p>'.__( 'NOTE: You can change this file only in the original post.', 'acf-rcwdupload' ).'</p>';	
						
					}
							
					if( ( $value != '' and file_exists(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode.DIRECTORY_SEPARATOR.$value) ) or $field['collection'] == 'Y' ){
						
						if( ( $value != '' and file_exists(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode.DIRECTORY_SEPARATOR.$value) ) ){
							
							$file 	= ACF_RCWDUPLOAD_UP_URL.'/'.$field['key'].'/'.$foldercode.'/'.$value;
							$fsize	= size_format(filesize(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode.DIRECTORY_SEPARATOR.$value));
						
						}else{
							
							$file 	= ACF_RCWDUPLOAD_UP_URL.'/'.$field['key'].'/';
							$fsize	= '';							
							$value	= '';							
							
						}
?>
						<div id="<?php echo $field['id']?>-current" class="rcwdplupload-current" <?php if(empty($value)) echo 'style="display:none"'?> >
<?php
							if(!$fieldlinked){
?>                        
                            <a id="<?php echo $field['id']?>-removecf" class="rcwdplupload-removecf <?php echo( ($v == 5) ? 'acf-icon small' : 'acf-button-delete' ) ?> ir" href="#">
                            <?php echo( ($v == 5) ? '<i class="acf-sprite-remove"></i>' : __( 'Remove', 'acf-rcwdupload' ) ) ?>
                            </a>              
<?php
							}
?>                                          
							<div id="<?php echo $field['id']?>-current-file" class="rcwdplupload-current-file"><strong><?php _e( 'Current file:', 'acf-rcwdupload' ) ?></strong> <a href="<?php echo $file ?>" target="_blank"><?php echo $value; ?></a></div>
							<div id="<?php echo $field['id']?>-current-size" class="rcwdplupload-current-size"><strong><?php _e( 'Size:', 'acf-rcwdupload' ) ?></strong> <span><?php echo $fsize ?></span></div>
						</div>
<?php
					}
					
					if($fieldlinked){
						
						echo '<br>';
							
						return;
						
					}
?>  
					<div id="<?php echo $field['id']?>-temp" class="rcwdplupload-temp">
						<a id="<?php echo $field['id']?>-removetf" class="rcwdplupload-removetf <?php echo( ($v == 5) ? 'acf-icon small' : 'acf-button-delete' ) ?> ir" href="#">
                        <?php echo( ($v == 5) ? '<i class="acf-sprite-remove"></i>' : __( 'Remove', 'acf-rcwdupload' ) ) ?>
                        </a>
						<div id="<?php echo $field['id']?>-temp-file" class="rcwdplupload-temp-file"><strong><span class="rcwdplupload-temp-file-txt"></span> <a href="<?php echo ACF_RCWDUPLOAD_UP_TEMP_URL.'/'.$field['key'].'/' ?>" target="_blank"></a></strong></div>
						<div id="<?php echo $field['id']?>-temp-size" class="rcwdplupload-temp-size"><strong><?php _e( 'Size:', 'acf-rcwdupload' ) ?></strong> <span></span></div>
					</div>
<?php
					if($field['collection'] == 'Y'){
						
						$thefolder = isset($field['value']['folder']) ? $field['value']['folder'] : $foldercode;
?>                    
                        <div id="rcwdacflupload-<?php echo $field['id']?>-tabs" class="rcwdacflupload-tabs">
                            <ul>
                                <li><a href="#rcwdacflupload-<?php echo $field['id']?>-tab-1"><?php _e( 'Upload a file', 'acf-rcwdupload' ) ?></a></li>
                                <li><a href="#rcwdacflupload-<?php echo $field['id']?>-tab-2"><?php _e( 'Uploaded files', 'acf-rcwdupload' ) ?></a></li>
                            </ul>   
    
							<div id="rcwdacflupload-<?php echo $field['id']?>-tab-1">
								<input class="acf-file-folder" type="<?php echo $is_hidden ?>" id="<?php echo $field['id']; ?>-folder" name="<?php echo $field['key']?>-folder[]" value="<?php echo $thefolder; ?>" autocomplete=off />
<?php
					}
?>     
					<input class="acf-file-value" type="<?php echo $is_hidden ?>" id="<?php echo $field['id']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo $value; ?>" autocomplete=off />
					<!--<input class="rcwdplupload-cf" type="<?php echo $is_hidden ?>"" name="<?php echo $field['key']?>-cf[]" value="<?php echo $value; ?>" />-->                      
					
					<div id="<?php echo $field['id']?>-filelist" class="rcwdplupload-filelist">
						<div class="rcwdplupload-filewrapper">
							<span class="rcwdplupload-filename">...</span><span id="<?php echo $field['id']?>-remove" class="rcwdplupload-remove"></span>
						</div>
					</div>                
					<a id="<?php echo $field['id']?>-pickfiles" class="button rcwdplupload-pickfiles" href="#"><?php _e( 'Browse', 'acf-rcwdupload' ) ?></a>
					<a id="<?php echo $field['id']?>-uploadfiles" class="button button-primary rcwdplupload-uploadfiles" href="#"><?php _e( 'Upload', 'acf-rcwdupload' ) ?></a>
					<div class="rcwdplupload-clear"></div>
                    <span id="clientpreview-<?php echo $field['id'] ?>" class="rcwdplupload-clientpreview"></span>
					<span id="<?php echo $field['id']?>-filesize" class="rcwdplupload-filesize"><strong><?php _e( 'Size:', 'acf-rcwdupload' ) ?></strong><span></span></span>
<?php
					if($field['collection'] == 'Y'){
?>
                            </div>
                            <div id="rcwdacflupload-<?php echo $field['id']?>-tab-2">
                                <div id="rcwdacflupload-<?php echo $field['id']?>-folder-files-wrapper" class="rcwdacflupload-folder-files-wrapper">
<?php
						$fieldfolder 		= ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR;
						$fieldsubfolders	= glob( $fieldfolder.'/*', GLOB_ONLYDIR );
						$fimages			= array();
						$fothers			= array();
						
						if(count($fieldsubfolders) > 0){
							
							foreach($fieldsubfolders as $fieldsubfolder){
								
								$folder_images = glob( $fieldsubfolder.DIRECTORY_SEPARATOR.'*.{jpg,png,gif}', GLOB_BRACE );
								
								foreach($folder_images as $filename){
									
									$arrk			= sanitize_title(basename(basename($filename)));
									$selected		= ( $value != '' and $value == basename($filename) and basename($fieldsubfolder) == $thefolder) ? 'rcwdacflupload-files-selected' : '';
									$fsize			= size_format(filesize($filename));
									$fimages[$arrk]	= array( basename($filename), basename($fieldsubfolder), ACF_RCWDUPLOAD_UP_URL.'/'.$field['key'].'/'.basename($fieldsubfolder).'/'.basename($filename), $selected, $fsize );
									
								}
						
								$folder_others = glob( $fieldsubfolder.DIRECTORY_SEPARATOR.'*' );
								
								foreach($folder_others as $filename){
									
									if(!in_array( rcwd_get_file_extension($filename), array( 'jpg', 'png', 'gif' ) )){
							
										$arrk			= sanitize_title(basename(basename($filename)));
										$selected		= ( $value != '' and $value == basename($filename) and basename($fieldsubfolder) == $thefolder) ? 'rcwdacflupload-files-selected' : '';
										$fsize			= size_format(filesize($filename));
										$fothers[$arrk]	= array( basename($filename), basename($fieldsubfolder), ACF_RCWDUPLOAD_UP_URL.'/'.$field['key'].'/'.basename($fieldsubfolder).'/'.basename($filename), $selected, $fsize );
									
									}
									
								}
										
							}
							
							if(count($fimages))
								 foreach($fimages as $filename)	
									echo '<div class="rcwdacflupload-folder-files rcwdacflupload-folder-images '.$filename[3].'" data-filefolder="'.$filename[1].'" data-filename="'.$filename[0].'" data-size="'.$filename[4].'"><div><img src="'.$filename[2].'" alt=""  /></div><span>x</span></div>';

							if(count($fothers))
								 foreach($fothers as $filename)	
									echo '<div class="rcwdacflupload-folder-files rcwdacflupload-folder-others '.$filename[3].'" data-filefolder="'.$filename[1].'" data-filename="'.$filename[0].'" data-size="'.$filename[4].'"><div>'.$filename[0].'</div><span>x</span></div>';
									
						}
?>  
                                    <div class="rcwdplupload-clear"></div>
                                </div>
                            
                            </div>				   
                        </div>
<?php
					}
?>                          				   
				</div>  
			</div>
<?php
		
		}
		
	}

	static function update_value($args){

		if(self::serial_is_valid()){

			global $pagenow;	

			$value 			= trim($args['value']);
			$post_id 		= $args['post_id'];
			$field 			= $args['field'];
			$rptfieldkey 	= '';
			$ftodelete		= false;
			$defaults 		= self::get_var('defaults');
			$field 			= array_merge( $defaults, $field );	
					
			if( isset($_POST[$field['key'].'_rptfieldkey']) and $_POST[$field['key'].'_rptfieldkey'] != '' )
				$rptfieldkey = $_POST[$field['key'].'_rptfieldkey'];		
	
			if(is_admin()){

				if( isset($_GET['page']) and substr( $_GET['page'], 0, 11 ) == 'acf-options' ){
					
					$foldercode = $foldercode_temp = 'options_'.substr( $_GET['page'], 12 );
					$opfolder 	= substr( $_GET['page'], 12 );
	
					if(empty($opfolder))
						$foldercode = $foldercode_temp = $foldercode = 'options_rt';
												
				}
		
				if( $pagenow == 'edit-tags.php' ){
					
					$foldercode = $foldercode_temp = 'tax_'.$_POST['taxonomy'].'_'.$_POST['tag_ID'];
								
				}elseif(in_array( $pagenow, array( 'profile.php', 'user-new.php', 'user-edit.php' ) )){
					
					global $user_id;
					
					$foldercode = $foldercode_temp = 'user_'.$_POST['user_id'];
					
				}elseif( in_array( $pagenow, array( 'post.php', 'post-new.php', 'media.php' ) ) ){
					
					global $post;
					
					$pst 		= 'post';
					$foldercode = $foldercode_temp = 'post_'.$post->ID;	
						
				}
			
			}else{
				
				if( is_page() or is_single() ){

					global $post, $rcwd_custom_post_id, $acf_rcwdupload_5_post_id;

					$pst = 'post';

					if( !empty($rcwd_custom_post_id) and (int)$rcwd_custom_post_id > 0)
						$post_id = (int)$rcwd_custom_post_id;
					else
						$post_id = $post->ID;

					if( function_exists('acf_get_setting') and version_compare( acf_get_setting('version'), "5", ">=" ) ){

						$foldercode = 'post_'.$args['post_id'];
						$post_id	= $args['post_id'];
						
						if( !empty($acf_rcwdupload_5_post_id) and (int)$acf_rcwdupload_5_post_id > 0){
							
							$foldercode_temp = 'post_'.$acf_rcwdupload_5_post_id;
						
						}else{

							$foldercode_temp = 'post_'.$post->ID;
														
						}
						
					}else{
				
						if( isset($_POST) and isset($_POST['post_id']) and $_POST['post_id'] == 'new'){ // COMPATIBILITY WITH ACF FRONTEND FORM
	
							$foldercode 		= 'post_'.$post_id;
							$foldercode_temp 	= 'post_'.$post_id;
							
						}else
							$foldercode_temp = 'post_'.$post_id;
						
					}

				}
				
			}

			if(!empty($value)){

				$fnt	= ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode_temp.DIRECTORY_SEPARATOR.$value;
				$f		= ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode;
				$fn		= $f.DIRECTORY_SEPARATOR.$value;
				$ok		= false;
				
				if(file_exists($fnt)){
					
					$ok = true;
					
					wp_mkdir_p($f);
					
					rename( $fnt, $fn ); // move file from temp folder to field folder
	
					if($rptfieldkey != '')
						$ftodelete = self::ftd_from_repeater( $rptfieldkey, $field );
					else{
						
						if(is_numeric($post_id)){

							$ftodelete = get_post_meta( $post_id, $field['name'], true );

							if( $ftodelete and $ftodelete == $value )
								$ftodelete = false;

						}elseif( strpos($post_id, 'user_') !== false ){
							
							$post_id 	= str_replace('user_', '', $post_id);
							$ftodelete 	= get_user_meta( $post_id, $field['name'], true );
							
							if( $ftodelete and $ftodelete == $value )
								$ftodelete = false;
									
						}else{
							$ftodelete = get_option($post_id.'_'.$field['name']);
							
							if( $ftodelete and $ftodelete == $value )
								$ftodelete = false;

						}					
					}
					
				}elseif(!file_exists($fn))				
					$value = '';
				
				if( $ftodelete and $field['collection'] == 'Y' )
					$ftodelete = false;
								
			}else{
				
				if($rptfieldkey != '')
					$ftodelete = self::ftd_from_repeater( $rptfieldkey, $field );
				else{
					
					if( is_numeric($post_id) )
						$ftodelete = get_post_meta( $post_id, $field['name'], true );
					
					elseif( strpos($post_id, 'user_') !== false ){
						
						$post_id 	= str_replace('user_', '', $post_id);
						$ftodelete 	= get_user_meta( $post_id, $field['name'], true );	
								
					}else
						$ftodelete = get_option($post_id.'_'.$field['name']);

				}
			}

			if( is_array($ftodelete) and isset($ftodelete['file']) )
				$ftodelete = $ftodelete['file'];

			if( $ftodelete and ( ( $ok === true and $ftodelete != $value ) or $ok === false ) )
				if(file_exists(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode.DIRECTORY_SEPARATOR.$ftodelete))
					unlink(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$foldercode.DIRECTORY_SEPARATOR.$ftodelete);
	
			if($value != '')
				$value = array( 'file' => $value, 'folder' => $foldercode );
					
		}
		
		return $value;
		
	}

	static function ftd_from_repeater( $rptfieldkey, $field ){
		
		$fid_rep		= str_replace( 'acf-field-', '', $field['id'] ); 
		$fname_split	= explode( '_rcwd_bg_image', $field['name'] ); 
		$fname_split	= explode( '_', $fname_split[0] ); 
		$forder			= end($fname_split);
		$fields			= $_POST['fields'][$rptfieldkey];	
		$count 			= 0;
		$ftodelete		= false;
		$cf 			= $_POST[$field['key'].'-cf'];
		
		foreach( $fields as $key => $value_ ){
			if($count == $forder){
				if($value_[$field['key']] != $cf[$count]){
					$ftodelete = $cf[$count];
					foreach( $fields as $key => $value_ ){
						if($value_[$field['key']] == $cf[$count]){
							$ftodelete = false;	
							break;
						}
					}
					break;
				}
			}
			$count++;
		}	
		return $ftodelete;	
			
	}

	static function format_value_for_api($args){
		
		if(self::serial_is_valid()){
		
			$post_id 		= $args['post_id'];
			$field 			= $args['field'];
			$value 			= $args['value'];
			
			if(is_array($value)){
				$file 			= $value['file'];	
				$folder			= $value['folder'];							
				$pfolder 		= '';
				
				if(substr( $folder, 0, 8 ) == 'options_'){

					$opfolder = substr( $folder, 8 );

					if(empty($opfolder))
						$folder = 'options_rt';
										
					$pfolder = 'o/'.substr( $folder, 8 );
					
				}elseif(substr( $folder, 0, 4 ) == 'tax_'){
					
					$pfolder = 't/'.substr( $folder, 4 );
					
				}elseif(substr( $folder, 0, 5 ) == 'user_'){
					
					$pfolder = 'u/'.substr( $folder, 5 );
					
				}elseif(substr( $folder, 0, 5 ) == 'post_'){
					
					$pfolder = 'p/'.substr( $folder, 5 );
					
				}
	
				$fk			= str_replace( 'field_', rand(10009, 99999).'/', $field['key'] );
				$blogurl	= get_bloginfo('url');				
				
				if(class_exists('SitePress')){
					
					$pu 		= parse_url($blogurl);
					$blogurl	= $pu['scheme'].'//'.$pu['host'];
					
				}
				
				if(substr( $blogurl, -1 ) != '/')
					$blogurl .= '/';
										
				$inline	= $blogurl.'acfrcwdup/0/'.$fk.'/'.$pfolder.'/'.$file;
				$attach	= $blogurl.'acfrcwdup/1/'.$fk.'/'.$pfolder.'/'.$file;
				$value 	= array( 	'file'	=> $file,
									'path'	=> ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$file,
									'url' 	=> ACF_RCWDUPLOAD_UP_URL.'/'.$field['key'].'/'.$folder.'/'.$file,
									'size' 	=> size_format(filesize(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$file)),
									'purl' 	=> array( 	'inline' => $inline,
														'attach' => $attach ) );	
	
				$filext = strtolower(substr( strrchr( $file, "." ), 1 ));
				
				switch($filext){
					
					case 'gif'	:
					case 'png'	:
					case 'jpeg'	:
					case 'jpg'	:
					
						list($width, $height, $type, $attr) = getimagesize(ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field['key'].DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$file);
						$value['width'] 					= $width;
						$value['height'] 					= $height;
						
				}
																	
				return $value;
			}

		}
			
		return false;
					
	}

	function generate_rewrite_rules($wp_rewrite){	// SAME FUNCTION IN ACF-RCWDUPLOAD.PHP FOR PLUGIN USE
		
		$new_non_wp_rules 			= array('acfrcwdup/([^/\.]*)/[^/\.]+/([^/\.]+)/([^/]+)/([^/]+)/([^/]+)(\.[^.]*$|$)?$' => '/?mode=$1&acfrcwduplddwnlod=$2&type=$3&value=$4&file=$5' );
		$wp_rewrite->non_wp_rules 	= $new_non_wp_rules + $wp_rewrite->non_wp_rules;
		
	}

	function flush_rules(){ // SAME FUNCTION IN ACF-RCWDUPLOAD.PHP FOR PLUGIN USE
		
		global $wp_rewrite;

		$rulexists = get_option('acf_rcwdupload_rulexists');
		if(!$rulexists){
			
			$wp_rewrite->flush_rules();
			$rulexists = update_option( 'acf_rcwdupload_rulexists', 1 );	
				
		}		
	}	
	
	function handle_upload_request(){
		
		if (isset($_GET['acfrcwdupload'])){
			
			require_once(implode( DIRECTORY_SEPARATOR, array( trailingslashit(dirname(__FILE__)), 'upload.php' ) ));
			
			die();
			//throw new acf_rcwd_clean_exit();
			
		}
		
		if (isset($_GET['acfrcwduplddwnlod'])){
			
			require_once(implode( DIRECTORY_SEPARATOR, array( trailingslashit(dirname(__FILE__)), 'download.php' ) ));
			
			die();
			//throw new acf_rcwd_clean_exit();
			
		}
		
	}
		
	static function error_folder_permissions( $isnotice = true, $return = false ){
		
		$msg = sprintf( __( 'Sorry, %1s for ACF cannot create the following folder under wp-content: %2s. Please check your folder permissions or the add-on will not save your files. Thanks...', 'acf-rcwdupload' ), self::get_var('label'), '<strong>acfrcwduploads</strong>' );
		if( is_string($isnotice) and $isnotice == '') 
			$isnotice = true;
		if($isnotice){
			$msg = '<div id="message" class="error"><p>'.$msg.'</p></div>';
		}
		if(!$return)
			echo $msg;
		else
			return $msg;
			
	}

	static function error_not_authenticated( $isnotice = true, $return = false ){

		if( !isset($_GET['page']) or ( isset($_GET['page']) and $_GET['page'] != 'acf-rcwdupload' ) ){
	
			if( function_exists('acf_get_setting') and version_compare( acf_get_setting('version'), "5", ">=" ) )
				$page = 'edit.php?post_type=acf-field-group&page=acf-rcwdupload';
			else
				$page = 'edit.php?post_type=acf&page=acf-rcwdupload';

			$msg = sprintf( __( 'Please fill the correct data in <a href="%1s">this page</a>  before to use %2s for ACF. Thanks...', 'acf-rcwdupload' ), $page, self::get_var('label') );
			if( is_string($isnotice) and $isnotice == '') 
				$isnotice = true;
			if($isnotice){
				$msg = '<div id="message" class="error"><p>'.$msg.'</p></div>';
			}
			if(!$return)
				echo $msg;
			else
				return $msg;
			
		}
			
	}	

	static function serial_is_valid(){
		
		if ( substr($_SERVER['HTTP_HOST'], -6) == '.local' )
			return true;
			
		$check 				= get_option( 'acf_rcwdupload_verifypurchase' );
		$check['item_id'] 	= ACF_RCWDUPLOAD_ITEM_ID;
		$docheck 			= true;
		
		if( isset($check['username']) and isset($check['purchased']) and $check['purchased'] === true and isset($check['lastcheck']) ){
		
			//$docheck = false;
			
			//if(time() - (int)$check['lastcheck'] > 1800){
			
				$docheck = false;
				
				//unset($check['purchased']);
				//unset($check['lastcheck']);
				
			//}
			
		}

		if($docheck){
			
/*			if ($_SERVER['SERVER_NAME'] == 'localhost' or substr($_SERVER['HTTP_HOST'], -6) == '.local')
				$api_url = 'http://cantarano.local/';
			else*/
			$api_url = 'http://www.cantarano.com';
				
			$remote_post = wp_remote_post( $api_url.'/envato/api/', array( 'body' => $check ));

			if ( ( !is_wp_error($remote_post) and $remote_post['response']['code'] == 200 ) and $remote_post !== false ){
				
				$remote_post =  unserialize($remote_post['body']);
		
			}else{
				
				if( !isset($check['purchased']) or !$check['purchased']  ){		
				
					update_option( 'acf_rcwdupload_verifypurchase', $check );
					
					return false;
					
				}
					
			}
	
			if( ( $remote_post !== false or $remote_post == '' ) and ( !isset($remote_post['verify-purchase']) or !isset($remote_post['verify-purchase']['item_id']) or $remote_post['verify-purchase']['item_id'] != $check['item_id'] or !isset($remote_post['verify-purchase']['buyer']) or $remote_post['verify-purchase']['buyer'] != $check['username'] ) ){

				unset($check['purchased']);
				unset($check['lastcheck']);
				
				update_option( 'acf_rcwdupload_verifypurchase', $check );
				
				return false;
				
			}

			$check['purchased'] = true;
			$check['lastcheck'] = time();
				
			update_option( 'acf_rcwdupload_verifypurchase', $check );
		
		}
			
		return true;	
			
	}
	
	static function clean_temp(){
		
		$clean = get_option('acf_rcwdupload_clean_temp');
		
		if( !$clean or time() - $clean > 86400 ){
			
			if(is_dir(ACF_RCWDUPLOAD_UP_TEMP_DIR)){
				
				$fold_1 = scandir(ACF_RCWDUPLOAD_UP_TEMP_DIR);

				foreach($fold_1 as $fold_2){
					
					if ( $fold_2 != '.' and $fold_2 != '..' and $fold_2 != 'index.html' ){
						
						$fold_3 = scandir(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fold_2);
						
						foreach($fold_3 as $fold_4){

							if ( $fold_4 != '.' and $fold_4 != '..' and $fold_4 != 'index.html' ){
		
								$fold_5 = scandir(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fold_2.DIRECTORY_SEPARATOR.$fold_4);
								
								foreach($fold_5 as $file){
				
									if ( $file != '.' and $file != '..' and $file != 'index.html' ){

										$filemtime = filemtime(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fold_2.DIRECTORY_SEPARATOR.$fold_4.DIRECTORY_SEPARATOR.$file);
										
										if(time() - $filemtime > ACF_RCWDUPLOAD_UP_TEMP_DIR)
											if(file_exists(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fold_2.DIRECTORY_SEPARATOR.$fold_4.DIRECTORY_SEPARATOR.$file))
												unlink(ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fold_2.DIRECTORY_SEPARATOR.$fold_4.DIRECTORY_SEPARATOR.$file);
																									
									}
									
								}
								
							}
							
						}
						
					}
					
				}
											
			}
			
			$clean = update_option( 'acf_rcwdupload_clean_temp', time() );	
			
		}
		
	}
			
}

$acf_rcwdup_m = new acf_rcwdup_m();
?>