<?php
/*
Plugin Name: Rcwd Upload for Advanced Custom Fields
Plugin URI: 
Description: the Rcwd Upload field let you upload with no use of wp media upload.
Version: 1.1.7.8
Author: Roberto Cantarano
Author URI: http://www.cantarano.com
License: You should have purchased a license from http://codecanyon.net/ plugin download page
Copyright: 2013 Roberto Cantarano (email : roberto@cantarano.com) 
*/

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('Nice try :)'); }

class acf_rcwdupload_plugin{

	/*
	*  Constructor
	*/
	
	function __construct(){
		
		$settings = array(
		
			'version' 	=> '1.1.7.8',
			'basename' 	=> plugin_basename(__FILE__)
			
		);
		
		register_activation_hook( $settings['basename'], array( $this, 'activate' ) );

		add_action( 'generate_rewrite_rules', array( $this, 'generate_rewrite_rules' ) );

		// version 5+ 
		add_action('acf/include_field_types', array($this, 'include_field_types' ));
		
		// version 4+ 
		add_action('acf/register_fields', array($this, 'register_fields'));

		// version 3-
		add_action( 'init', array( $this, 'init' )); 
								
	}

	function generate_rewrite_rules($wp_rewrite){ // SAME RULE IN MAIN.PHP FOR ADD-ON MANUAL INSTALLATION
		
		$new_non_wp_rules 			= array('acfrcwdup/([0-1]*)/[0-9]+/([^/\.]+)/([^/]+)/([^/]+)/([^/]+)(\.[^.]*$|$)?$' => '/?mode=$1&acfrcwduplddwnlod=$2&type=$3&value=$4&file=$5' );
		$wp_rewrite->non_wp_rules 	= $new_non_wp_rules+$wp_rewrite->non_wp_rules;
		
	}
		
	function activate(){ 
		
		$this->flush_rules();	
		
	}

	function flush_rules(){ // SAME RULE IN MAIN.PHP FOR ADD-ON MANUAL INSTALLATION
	
		global $wp_rewrite;

		$rulexists = get_option('acf_rcwdupload_rulexists');
		if(!$rulexists){
			
			$wp_rewrite->flush_rules();
			$rulexists = update_option( 'acf_rcwdupload_rulexists', 1 );	
				
		}		
	}
	
	function init(){
		
		if(function_exists('register_field')){ 
		
			register_field('acf_field_location', dirname(__File__) . '/rcwdupload-v3.php');
			
		}	
			
	}
	/*
	*  register_fields
	*
	*/
	
	function register_fields(){
		
		include_once('rcwdupload-v4.php');
		
	}

	function include_field_types(){
		
		include_once('rcwdupload-v5.php');
		
	}
	
}

new acf_rcwdupload_plugin();
?>