<?php
/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

// HTTP headers for no cache etc
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

global $wpdb;

$pst		= @$_GET['post'];
$fkey 		= @$_GET['fkey'];
$overw 		= @(bool)$_GET['overw'];
$pst		= @$_GET['pst'];
$foldercode	= @$_GET['foldercode'];
$targetDir 	= ACF_RCWDUPLOAD_UP_TEMP_DIR.DIRECTORY_SEPARATOR.$fkey;

if($foldercode != ''){
	
	$targetDir .= DIRECTORY_SEPARATOR.$foldercode;
	
	wp_mkdir_p($targetDir);
	
}

$renamed			= 0;
$cleanupTargetDir 	= true; // Remove old files
$maxFileAge 		= 5 * 3600; // Temp file age in seconds

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Get parameters
$chunk 			= isset($_REQUEST["chunk"]) 	? intval($_REQUEST["chunk"]) 	: 0;
$chunks 		= isset($_REQUEST["chunks"]) 	? intval($_REQUEST["chunks"]) 	: 0;
$fileName 		= isset($_REQUEST["name"]) 		? $_REQUEST["name"] 			: '';

// Clean the fileName for security reasons
$fileName 		= preg_replace('/[^\w\._]+/', '_', $fileName);
$base_name 		= basename($fileName);
$file_pathinfo 	= pathinfo($fileName);	
$file_ext		= strtolower($file_pathinfo['extension']);	

// Make sure the fileName is unique but only if chunking is disabled
if ( !$overw and $chunks < 2 and file_exists($targetDir.DIRECTORY_SEPARATOR.$fileName)) {
	$ext 		= strrpos($fileName, '.');
	$fileName_a = substr($fileName, 0, $ext);
	$fileName_b = substr($fileName, $ext);

	$count = 1;
	
	while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
		$count++;

	$fileName 	= $fileName_a . '_' . $count . $fileName_b;
	$renamed	= 1;
}

$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

// Create target dir
if (!file_exists($targetDir))
	@mkdir($targetDir);

// Remove old temp files	
if ($cleanupTargetDir){
	
	if (is_dir($targetDir) && ($dir = opendir($targetDir))){
		
		while (($file = readdir($dir)) !== false){
			
			$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

			// Remove temp file if it is older than the max age and is not the current file
			if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part"))
				@unlink($tmpfilePath);

		}
		
		closedir($dir);
		
	}else
		die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');

}	

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

if (isset($_SERVER["CONTENT_TYPE"]))
	$contentType = $_SERVER["CONTENT_TYPE"];

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false){
	
	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		// Open temp file
		$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
		
		if ($out) {
			// Read binary input stream and append it to temp file
			$in = @fopen($_FILES['file']['tmp_name'], "rb");

			if($in)
				while($buff = fread($in, 4096))
					fwrite($out, $buff);
			else
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				
			@fclose($in);
			@fclose($out);
			@unlink($_FILES['file']['tmp_name']);
			
		}else
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
	} else
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		
}else{
	// Open temp file
	$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
	
	if ($out){
		// Read binary input stream and append it to temp file
		$in = @fopen("php://input", "rb");

		if ($in)
			while ($buff = fread($in, 4096))
				fwrite($out, $buff);
		else
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

		@fclose($in);
		@fclose($out);
		
	}else
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}

// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
	// Strip the temp .part suffix off 
	rename("{$filePath}.part", $filePath);
	
	if(in_array( $file_ext, array( 'gif', 'jpg', 'jpeg', 'png' ) )){
		
		$quality 	= isset($_GET['quality']) 	? (int)$_GET['quality'] : 100;
		$maxwidth 	= isset($_GET['width']) 	? (int)$_GET['width'] 	: 0;
		$maxheight 	= isset($_GET['height']) 	? (int)$_GET['height'] 	: 0;
		$crop		= true;
		
		if( $maxwidth > 0 or $maxheight > 0 ){
			
			if( $maxwidth == 0 or $maxheight == 0 )
				$crop = false;
				
			switch($file_ext){
				case 'gif':
					$fullsize = imagecreatefromgif($targetDir.DIRECTORY_SEPARATOR.$base_name);
					break;
				case 'jpg':
				case 'jpeg':
					$fullsize = imagecreatefromjpeg($targetDir.DIRECTORY_SEPARATOR.$base_name);
					break;
				case 'png':
					$fullsize = imagecreatefrompng($targetDir.DIRECTORY_SEPARATOR.$base_name);
			}

			$fullsize_width		= imagesx($fullsize);
			$fullsize_height	= imagesy($fullsize);		

			if( ( $maxwidth < $fullsize_width and $maxwidth > 0 ) or ( $maxheight < $fullsize_height and $maxheight > 0 ) ){	
		
				if($crop === true){
					
					if($maxwidth > 0) 
						$crop_width		= $maxwidth;
																									 
					if($maxheight > 0)
						$crop_height	= $maxheight;		
											
				}
				if( $maxwidth == 0 or $maxwidth == '' or !is_numeric($maxwidth) ){
					
					$maxwidth	= floor( $fullsize_width / ( $fullsize_height / $maxheight ) );
					$crop		= false;
					
				}elseif( $maxwidth > $fullsize_width ){
					
					$maxwidth	= $fullsize_width;
					
				}
				if( $maxheight == 0 or $maxheight == '' or !is_numeric($maxheight) ){
					
					$maxheight	= floor( $fullsize_height / ( $fullsize_width / $maxwidth ) );
					$crop		= false;
					
				}elseif($maxheight > $fullsize_width){
					
					$maxheight	= $fullsize_height;
					
				}
	
				$ratio = $fullsize_width/$fullsize_height;
	
				// compute the unspecified dimension
				if ($maxwidth < 1)
					$maxwidth = $maxheight * $ratio;
				elseif($maxheight < 1)
					$maxheight = $maxwidth/$ratio;
				
				$new_ratio 		= $maxwidth/$maxheight;
				$widthRatio		= $maxwidth/$fullsize_width;
				$heightRatio 	= $maxheight/$fullsize_height;
				
				if(!$crop){
					
					// keep original ratio
					if ( $new_ratio != $ratio and abs($new_ratio - $ratio) > 1 ){
						
						if ($widthRatio > $heightRatio)
							$maxwidth = $maxheight*$ratio;
						else
							$maxheight = $maxwidth/$ratio; 
						
					}
					$src_x	= 0;
					$src_y	= 0;
					
				}else{
					// cropping needs 2 dimensions
					$rratio = $ratio / $new_ratio;
					
					if ($rratio > 1){
						
						// horizontal crop
						$centerx   		= ($fullsize_width / 2);
						$destwidth 		= $maxwidth / $heightRatio;
						$rangex 		= $centerx-$destwidth / 2;
						// crop shift
						// normalize crop between -1 and +1
						// 0-> - 1 SX
						// 50->  0 Center
						// 100-> 1 DX
						$crop			= ($crop_width-50)/50;
						$src_x			= $centerx-($destwidth/2)+($rangex*$crop);
						$src_y 			= 0;
						$fullsize_width = $fullsize_width/$rratio;
						
					}else{
						// vertical crop
						$centery   			= ($fullsize_height/2);
						$destheight 		= $maxheight/$widthRatio;
						$rangey 			= $centery-$destheight/2;
						// crop shift
						$crop				= ($crop_height-50)/50;
						$src_y				= $centery-($destheight/2)+($rangey*$crop);
						$src_x				= 0;
						$fullsize_height 	= $fullsize_height*$rratio;
					}					
				}
				$newimage = imagecreatetruecolor( $maxwidth, $maxheight );
				imagealphablending($newimage, false);
				imagesavealpha($newimage, true); 
				imagecopyresampled( $newimage, $fullsize, 0, 0, $src_x, $src_y, $maxwidth, $maxheight, $fullsize_width, $fullsize_height );
				switch($file_ext){
					case 'gif':
						imagegif( $newimage, $targetDir.DIRECTORY_SEPARATOR.$base_name, $quality );
						break;
					case 'jpg':
					case 'jpeg':
						imagejpeg( $newimage, $targetDir.DIRECTORY_SEPARATOR.$base_name, $quality );
						break;
					case 'png':
						imagepng( $newimage, $targetDir.DIRECTORY_SEPARATOR.$base_name, round(abs(( $thumbs_quality - 100) / 11.111111 )) );
						break;
				}
				
			}

		}
								
	}
	
}

die('{"jsonrpc" : "2.0", "renamed" : '.$renamed.', "filename" : "'.$fileName.'"}');