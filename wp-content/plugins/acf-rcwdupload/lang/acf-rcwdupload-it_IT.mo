��    4      �  G   \      x     y     �     �     �     �     �     �     �  	   �     �       I   !     k     }     �     �     �     �     �  Z   �  T     _   g  $   �     �     �            a        |     �     �     �  8   �     �     �           "     C  �   I  "   �  D   	  M   U	  6   �	     �	     �	     �	     
     
  @   &
     g
     m
  �  q
       $   &     K     S     b     j  	   y  	   �  	   �     �     �  ]   �     $     8     >     E     S     [     d  b   m  Z   �  j   +  +   �     �     �     �  
   �  m   �     ]     p     y     �  8   �     �     �  $   �  &        =  �   C  #      E   $  O   j  +   �     �     �  &   �  
   %     0  S   9  	   �     �                           (   3   
                   /   .            ,   &      0       2   1       %      $   '          -             !   	      4          #       "                                             +                *                 )       &rarr; Purchase verification 0 = proportional resizing. Browse Compressed files Content Current file: Docs Document files Documents Drag your file here Enable resizing Enter the file extensions, separated by commas, to enable file uploading. File not found :( File: Filters Filters the files Height Image files Images Images will be automatically resized before the upload step in order to reduce the weight. Insert the ID for this item <a href="%1s" target="_blank">(click here for help)</a>. Insert the Purchase code for this item <a href="%1s" target="_blank">(click here for help)</a>. Insert your Envato account username. Item ID: Marketplace Username: No Other files Please fill the correct data in <a href="%1s">this page</a>  before to use %2s for ACF. Thanks... Purchase code: Quality Rcwd Upload Rcwd Upload settings Rcwd Upload: an add-on for Advanced Custom Fields plugin Remove Resize images Resize the images to clientside Show image preview before upload Size: Sorry, %1s for ACF cannot create the following folder under wp-content: %2s. Please check your folder permissions or the add-on will not save your files. Thanks... Sorry, file extension not allowed. Sorry, the file extension is not permitted. Please try another file. Sorry, the file size must not be greater than %s kb. Please try another file. Sorry, your data is invalid. Please insert valid data. To change with: Upload Upload the file after selection Various files Verify Well Done, all data is correct! The item is properly registered. Width Yes Project-Id-Version: Rcwd Upload for Advanced Custom Fields v1.0.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-11 17:01+0100
PO-Revision-Date: 2014-10-11 17:02+0100
Last-Translator: RCWD <roberto@cantarano.com>
Language-Team: Roberto Cantarano
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.6.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 &rarr; Verifica acquisto 0 = ridimensionamento proporzionale. Sfoglia File compressi Content File corrente: Documenti Documenti Documenti Trascina il file qui Abilita ridimensionamento Inserisci le estensioni dei file, separate da virgola, per abilitare il caricamento dei file. File non trovato :( File: Filtri Filtra i file Altezza Immagini Immagini Le immagini saranno automaticamente ridimensionate prima della fase di upload per ridurne il peso. Inserisci l'ID per questo plugin <a href="%1s" target="_blank">(clicca qui per aiuto)</a>. Inserisci il codice d'acquisto per questo plugin <a href="%1s" target="_blank">(clicca qui per aiuto)</a>. Inserisci l'username del tuo account Envato Item ID: Marketplace Username: No Altri file Inserisci le informazioni richieste in <a href="%1s">questa pagina</a>  prima di usare %2s for ACF. Grazie... Codice d'acquisto: Qualità Rcwd Upload Rcwd Upload settings Rcwd Upload: un add-on per Advanced Custom Fields plugin Rimuovi Ridimensiona immagini Ridimensiona le immagini lato client Mostra anteprima prima del caricamento Peso: Spiacenti, %1s per ACF non può creare la seguente cartella sotto wp-content: %2s. Controlla i permessi di scrittura delle cartelle altrimenti i file non potranno essere salvati. Grazie... Spiacente, estensione non permessa. Spiacenti, estensione del file non permessa. Prova con un altro file. Spiacenti, il peso del file non deve superare i %s kb. Prova con un altro file. Spiacente, le informazioni non sono valide. Da cambiare con: Carica Carica il file dopo averlo selezionato Altri file Verifica Ottimo, tutte le informazioni sono corrette. Il plugin è correttamente registrato. Larghezza Si 