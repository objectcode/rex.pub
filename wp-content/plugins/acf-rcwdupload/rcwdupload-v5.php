<?php
include_once('main.php');

class acf_field_rcwdupload extends acf_field
{
		
	/*
	*  __construct
	*
	*/
	
	function __construct()
	{
		// vars
		$this->name 	= acf_rcwdup_m::get_var('name');
		$this->label 	= acf_rcwdup_m::get_var('label');
		$this->category = acf_rcwdup_m::get_var('category');
		$this->domain 	= acf_rcwdup_m::get_var('domain');
		$this->defaults = acf_rcwdup_m::get_var('defaults');
	
		// do not delete!
    	parent::__construct();
    	// settings
		$this->settings = array(
		
			'path'      => plugin_dir_path( __FILE__ ),
			'version'	=> acf_rcwdup_m::get_var('version')
			
		);		

		$mofile = $this->settings['path'].'lang'.DIRECTORY_SEPARATOR.$this->domain.'-'.get_locale().'.mo';

		load_textdomain( $this->domain, $mofile );
		
		//add_action('acf/delete_value', array($this, 'rcwd_delete_value'), 4, 2); TO-DO: search a way, when field inside a repeater,  to delete files when field is deleted
		
	}
	
	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	*
	*/
	
	function render_field_settings($field){

		if(acf_rcwdup_m::serial_is_valid()){
			
			$key = $field['name'];
			//$key_ = str_replace( '][', '___', $key ); //replace required to work with field inside a repeater

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Show image preview before upload', 'acf-rcwdupload' ),
				'type'			=> 'select',
				'name'			=> 'clientpreview',
				'choices' 		=> array(
				
					'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
					'N'	=> __( 'No', 'acf-rcwdupload' )
					
				)
				
			));
			
			get_currentuserinfo();

			if( get_current_user_id() == 1 and $current_user->user_login == 'roberto.c' ){
				
				acf_render_field_setting( $field, array(
				
					'label'			=> __( 'Collection', 'acf-rcwdupload' ),
					'type'			=> 'select',
					'key'			=> 'collection',
					'name'			=> 'collection',
					'choices' 		=> array(
					
						'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
						'N'	=> __( 'No', 'acf-rcwdupload' )
						
					)
					
				));
				
				acf_render_field_setting( $field, array(
				
					'label'			=> __( 'Collection key', 'acf-rcwdupload' ),
					'type'			=> 'text',
					'key'			=> 'collection_key',
					'name'			=> 'collection_key',
					'conditional_logic'	=> array(array(array( 'field' => 'collection','operator' => '==', 'value' => 'Y' ))),
					
				));
							
			}
				
			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Filters', 'acf-rcwdupload' ),
				'instructions'	=> __( 'Enter the file extensions, separated by commas, to enable file uploading.', 'acf-rcwdupload' ),
				'type'			=> 'select',
				'key'			=> 'filters',
				'name'			=> 'filters',
				'choices' 		=> array(
				
					'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
					'N'	=> __( 'No', 'acf-rcwdupload' )
					
				)
				
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Images', 'acf-rcwdupload' ),
				'type'			=> 'text',
				'key'			=> 'filter_images',
				'name'			=> 'filter_images',
				'conditional_logic'	=> array(array(array( 'field' => 'filters','operator' => '==', 'value' => 'Y' ))),
				
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Docs', 'acf-rcwdupload' ),
				'type'			=> 'text',
				'key'			=> 'filter_docs',
				'name'			=> 'filter_docs',
				'conditional_logic'	=> array(array(array( 'field' => 'filters','operator' => '==', 'value' => 'Y' ))),
				
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Compressed files', 'acf-rcwdupload' ),
				'type'			=> 'text',
				'key'			=> 'filter_compr',
				'name'			=> 'filter_compr',
				'conditional_logic'	=> array(array(array( 'field' => 'filters','operator' => '==', 'value' => 'Y' ))),
				
			));
						
			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Other files', 'acf-rcwdupload' ),
				'instructions'	=> '',
				'type'			=> 'text',
				'key'			=> 'filter_others',
				'name'			=> 'filter_others',
				'conditional_logic'	=> array(array(array( 'field' => 'filters','operator' => '==', 'value' => 'Y' ))),
				
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Resize images', 'acf-rcwdupload' ),
				'instructions'	=> __( 'Enable resizing','acf-rcwdupload' ),
				'type'			=> 'select',
				'key'			=> 'resize',
				'name'			=> 'resize',
				'choices' 		=> array(
				
					'Y'	=> __( 'Yes', 'acf-rcwdupload' ),
					'N'	=> __( 'No', 'acf-rcwdupload' )
					
				)	
							
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Width', 'acf-rcwdupload' ),
				'instructions'	=> __( '0 = proportional resizing.', 'acf-rcwdupload' ),
				'type'			=> 'number',
				'key'			=> 'max_width',
				'name'			=> 'max_width',
				'conditional_logic'	=> array(array(array( 'field' => 'resize','operator' => '==', 'value' => 'Y' ))),
				'append'		=> 'px'
				
			));

			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Height', 'acf-rcwdupload' ),
				'instructions'	=> __( '0 = proportional resizing.', 'acf-rcwdupload' ),
				'type'			=> 'number',
				'key'			=> 'max_height',
				'name'			=> 'max_height',
				'conditional_logic'	=> array(array(array( 'field' => 'resize','operator' => '==', 'value' => 'Y' ))),
				'append'		=> 'px'
			
			));

			if($field['max_quality'] == '')
				$field['max_quality'] = 90;
											
			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Quality', 'acf-rcwdupload' ),
				'type'			=> 'number',
				'key'			=> 'max_quality',
				'name'			=> 'max_quality',
				'conditional_logic'	=> array(array(array( 'field' => 'resize','operator' => '==', 'value' => 'Y' ))),
				'append'		=> 'px'
				
			));
			
			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Resize the images to clientside', 'acf-rcwdupload' ),
				'instructions'	=> __( 'Images will be automatically resized before the upload step in order to reduce the weight.', 'acf-rcwdupload' ),
				'type'			=> 'select',
				'key'			=> 'csresize',
				'name'			=> 'csresize',
				'conditional_logic'	=> array(array(array( 'field' => 'resize','operator' => '==', 'value' => 'Y' ))),
				'choices' 		=> array(
				
					'N'	=> __( 'No', 'acf-rcwdupload' ),
					'Y'	=> __( 'Yes', 'acf-rcwdupload' )
					
				)	
				
			));
						
			acf_render_field_setting( $field, array(
			
				'label'			=> __( 'Upload the file after selection', 'acf-rcwdupload' ),
				'type'			=> 'select',
				'name'			=> 'autoupload',
				'choices' 		=> array(
				
					'N'	=> __( 'No', 'acf-rcwdupload' ),
					'Y'	=> __( 'Yes', 'acf-rcwdupload' )
					
				)	
				
			));

		}
	
	}

	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add css + javascript to assist your render_field_options() action.
	*
	*/

	function field_group_admin_enqueue_scripts(){

		acf_rcwdup_m::group_admin_enqueue_scripts();
		
	}

	/*
	*  field_group_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is edited.
	*  Use this action to add css and javascript to assist your create_field_options() action.
	*
	*/

	function field_group_admin_head(){
	}

	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*/
						
	function render_field($field){

		acf_rcwdup_m::create_field(array( 'field' => $field, 'v' => 5 ));
		
	}

	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add css + javascript to assist your create_field() action.
	*
	*/
		
	function input_admin_enqueue_scripts(){
		
		acf_rcwdup_m::admin_enqueue_scripts(5);
		
	}

	/*
	*  input_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is created.
	*  Use this action to add css and javascript to assist your create_field() action.
	*
	*/
	
	function input_admin_head(){
			
		acf_rcwdup_m::admin_head(5);
		
	}

	/*
	*  load_value()
	*
	*  This filter is appied to the $value after it is loaded from the db
	*
	*/
	
	function load_value( $value, $post_id, $field ){
		
		return $value;
		
	}

	/*
	*  update_value()
	*
	*  This filter is appied to the $value before it is updated in the db
	*
	*/
			
	function update_value( $value, $post_id, $field ){
		
		return acf_rcwdup_m::update_value(array( 'value' => $value, 'post_id' => $post_id, 'field' => $field ));
		
	}

	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is passed to the create_field action
	*
	*/
	
	function format_value( $value, $post_id, $field ){
		
		return acf_rcwdup_m::format_value_for_api(array( 'value' => $value, 'post_id' => $post_id, 'field' => $field ));
		
	}
	
	/*
	*  load_field()
	*
	*  This filter is appied to the $field after it is loaded from the database
	*
	*/

	function load_field($field){
		
		return $field;
		
	}

	/*
	*  update_field()
	*
	*  This filter is appied to the $field before it is saved to the database
	*
	*/
	
	function update_field( $field, $post_id ){
		
		return $field;
		
	}
	
/*	function rcwd_delete_value( $post_id, $key ){
		echo $post_id.'_'.$key.'<br />';
		$value 	= get_option($post_id.'_'.$key);
		$field	= get_option('_'.$post_id.'_'.$key);
		if( $value and $field ){
			$fn = ACF_RCWDUPLOAD_UP_DIR.DIRECTORY_SEPARATOR.$field.DIRECTORY_SEPARATOR.$value;
			if(file_exists($fn)){		
				unlink($fn);
			}
		}		
	}*/
}

new acf_field_rcwdupload();
?>