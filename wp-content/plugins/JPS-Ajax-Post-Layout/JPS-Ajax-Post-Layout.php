<?php
/*
Plugin Name: JPS Ajax Post Layout
Plugin URI: http://beeteam368.com/ajaxpostlayout/
Description: Json & Ajax Post Layout, Slider Multi Posts Parallax (Ajax), Carousel Ajax, Masonry Flat, Grid Box, Grid Normal, Grid Masonry, List Classic ... Best Choice For Building Your Website.
Author: BeeTeam368
Author URI: http://beeteam368.com/
Version: 1.3.8
License: Commercial
*/

if (!defined('JPS_BETE_VER')){
	define('JPS_BETE_VER','1.3.8');
};

if (!defined('JPS_BETE_PLUGIN_URL')){
    define('JPS_BETE_PLUGIN_URL', plugin_dir_url( __FILE__ ));
};

if (!defined('JPS_BETE_PLUGIN_OBFUSCATOR')){
    define('JPS_BETE_PLUGIN_OBFUSCATOR', 'minify'); //normal, minify, OBFUSCATOR
};

include_once('includes/plugin-settings/plugin-settings.php');
include_once('includes/customs-css.php');

include_once('includes/functions.php');

if(!class_exists('jps_bete_slider')){
	class jps_bete_slider {
		public function __construct() {				
			if(is_admin()){
				add_action('init', array(&$this, 'jps_bete_backend_scripts'));
			};		
			add_shortcode('jps_bete_sc', array(&$this, 'jps_bete_shortcode'));
		}
		
		public function jps_bete_backend_scripts(){
			if ( get_user_option('rich_editing') == 'true' ) {
				add_filter('mce_external_plugins', 	array(&$this, 'regplugins'));
				add_filter('mce_buttons_3', 		array(&$this, 'regbtns'));
				remove_filter('mce_buttons_3', 		array(&$this, 'remobtns'));
			};
			wp_enqueue_style('jps_bete_admin_css', 		JPS_BETE_PLUGIN_URL.'library/admin-css/style.css', array(), JPS_BETE_VER);
			wp_enqueue_script( 'jps_bete_admin_js', 	JPS_BETE_PLUGIN_URL.'library/admin-css/javascript.js', array(), JPS_BETE_VER, true);
		}
		
		public function regplugins($plgs){
			//$plgs[''] = JPS_BETE_PLUGIN_URL.'';
			return $plgs;
		}
		
		public function remobtns($buttons){
			//array_push($buttons, '');
			return $buttons;
		}
	
		public function regbtns($buttons){			
			//array_push($buttons, '');
			return $buttons;
		}
		
		public function jps_bete_shortcode($params){
			extract(shortcode_atts(array(
				'style'                 				=> '', //select		: 'default' or 'bt-2row'
				'filter_options'						=> '',
				'taxonomies'            				=> '', //idNumber	: '' or '1,2,3,4...'
				'ids'									=> '', //idNumber	: '' or '1,2,3,4...' 
				'condition'								=> '', //select		: 'Latest' or 'Sticky posts, Most commented, Title, Random' 				
				'sortby'                  				=> '', //select		: 'Descending' or 'Ascending' 
				'post_metadata'							=> '', //select		: 'yes' or 'no'
				'total_posts'							=> '', //number		: '30' or '100, 200...'
				'items_per_page'						=> '', //number		: '10' or '2, 15, 20...'
				'height'								=> '', //number		: 'full' or '700, 800, 900 ...' / min-height:650px;
				'background_opt'						=> '', //select		: 'no' or 'yes'
				'background_img'						=> '', //string		: '' or '...'
				'parallax'								=> '', //select		: 'yes' or 'no'
				'animate'								=> '', //select		: 'default' or 'random, random sync, ... 41 effect animate'
				'post_read_popup'						=> '', //select		: 'yes' or 'no'
				'more_button'							=> '', //select		: 'yes' or 'no'
				'autoplay'								=> '', //select		: 'yes' or 'no'
				'autoplayspeed'							=> '', //number		: '5000' or '4000, 2000...'					
				'extra_class'							=> '',
				'infinite_ajax'							=> '',
				'gutter'								=> '',
				'show_filter'							=> '',
				'combo_grid_list'						=> '',
				'priority_grid_list'					=> '',
				'show_taxonomy'							=> '',
				'show_author'							=> '',
				'show_datetime'							=> '',
				'show_viewscount'						=> '',
				'show_commentscount'					=> '',
				'show_excerpt'							=> '',
				'post_type'								=> '',
				'delay_spinner'							=> '',
				'show_start_date'						=> '',
				'show_end_date'							=> '',
				'open_popup_in'							=> '',
				'main_font'								=> '',
				'heading_font'							=> '',
				's_heading_font'						=> '',
				'link_color'							=> '',
				'link_hover_color'						=> '',	
				'light_link_color'						=> '',
				'light_link_hover_color'				=> '',
				'title_color'							=> '',
				'title_hover_color'						=> '',	
				'light_title_color'						=> '',
				'light_title_hover_color'				=> '',
				'metadata_color'						=> '',
				'light_metadata_color'					=> '',
				'text_color'							=> '',
				'thumb_overlay'							=> '',
				'text_transform'						=> '',
				
				'social_facebook'						=> '',
				'social_twitter'						=> '',
				'social_google_plus'					=> '',
				'social_pinterest'						=> '',
				'social_tumblr'							=> '',
				'social_linkedin'						=> '',
				'social_email'							=> '',
				
				'under_post_on_grid'					=> '',
				'grid_style_box'						=> '',
				'grid_style_title_on_img'				=> '',
				'hidden_grid_content'					=> '',
				
				'custom_margin'							=> '',
				'custom_margin_bottom'					=> '',
				'link_target'							=> '',
				'creative_custom_style'					=> '',
				
			), $params));
			
			$style     				= isset($params['style']) 			&& $params['style'] != '' 																? $params['style'] 						: 'default';
			$filter_options			= isset($params['filter_options']) 	&& $params['filter_options'] != '' 														? $params['filter_options'] 			: 'all_posts';
			$taxonomies				= isset($params['taxonomies']) 		&& $params['taxonomies'] != '' 															? $params['taxonomies'] 				: '';
			$ids					= isset($params['ids']) 			&& $params['ids'] != '' 																? $params['ids'] 						: '';
			$condition     			= isset($params['condition']) 		&& $params['condition'] != '' 															? $params['condition'] 					: 'lastest';			
			$sortby					= isset($params['sortby']) 			&& $params['sortby'] != '' 																? $params['sortby'] 					: 'DESC';
			$post_metadata			= isset($params['post_metadata']) 	&& $params['post_metadata'] != ''														? $params['post_metadata'] 				: 'yes';
			$total_posts     		= isset($params['total_posts']) 	&& $params['total_posts'] != '' 	&& is_numeric(trim($params['total_posts'])) 		? trim($params['total_posts']) 			: 100;
			$items_per_page			= isset($params['items_per_page']) 	&& $params['items_per_page'] != '' 	&& is_numeric(trim($params['items_per_page'])) 		? trim($params['items_per_page']) 		: '10';			
			$height     			= isset($params['height']) 			&& $params['height'] != '' 			&& is_numeric(trim($params['height'])) 				? trim($params['height']) 				: 'full';
			$background_opt			= isset($params['background_opt']) 	&& $params['background_opt'] != '' 														? $params['background_opt'] 			: 'no';
			$background_img			= isset($params['background_img']) 	&& $params['background_img'] != '' 														? $params['background_img'] 			: '';
			$parallax				= isset($params['parallax'])		&& $params['parallax'] != '' 															? $params['parallax'] 					: 'yes';
			$animate				= isset($params['animate']) 		&& $params['animate'] != '' 															? $params['animate'] 					: 'default';
			$post_read_popup		= isset($params['post_read_popup']) && $params['post_read_popup'] != '' 													? $params['post_read_popup'] 			: 'yes';
			$more_button     		= isset($params['more_button']) 	&& $params['more_button'] != '' 														? $params['more_button'] 				: 'yes';
			$autoplay     			= isset($params['autoplay']) 		&& $params['autoplay'] != '' 															? $params['autoplay'] 					: 'no';
			$autoplayspeed			= isset($params['autoplayspeed']) 	&& $params['autoplayspeed'] != '' 	&& is_numeric(trim($params['autoplayspeed'])) 		? trim($params['autoplayspeed']) 		: '10000';	
			$extra_class			= isset($params['extra_class']) 	&& $params['extra_class'] != '' 														? $params['extra_class'] 				: '';
			$infinite_ajax			= isset($params['infinite_ajax']) 	&& $params['infinite_ajax'] != ''														? $params['infinite_ajax'] 				: 'no';
			$gutter					= isset($params['gutter']) 			&& $params['gutter'] != ''																? $params['gutter'] 					: 'no';	
			$show_filter			= isset($params['show_filter']) 	&& $params['show_filter'] != ''															? $params['show_filter'] 				: 'yes';
			$combo_grid_list		= isset($params['combo_grid_list']) && $params['combo_grid_list'] != ''														? $params['combo_grid_list'] 			: 'no';
			$priority_grid_list		= isset($params['priority_grid_list']) 	&& $params['priority_grid_list'] != '' && is_numeric(trim($params['priority_grid_list']))		? $params['priority_grid_list'] : '0';
			
			$show_taxonomy			= isset($params['show_taxonomy']) 	&& $params['show_taxonomy'] != ''														? $params['show_taxonomy'] 				: 'yes';
			$show_author			= isset($params['show_author']) 	&& $params['show_author'] != ''															? $params['show_author'] 				: 'yes';
			$show_datetime			= isset($params['show_datetime']) 	&& $params['show_datetime'] != ''														? $params['show_datetime'] 				: 'yes';
			
			$show_viewscount		= isset($params['show_viewscount']) && $params['show_viewscount'] != ''														? $params['show_viewscount'] 			: 'no';
			$show_commentscount		= isset($params['show_commentscount']) 	&& $params['show_commentscount'] != ''												? $params['show_commentscount'] 		: 'no';
			
			$show_excerpt			= isset($params['show_excerpt']) 	&& $params['show_excerpt'] != ''														? $params['show_excerpt'] 				: 'yes';
			$post_type				= isset($params['post_type']) 		&& $params['post_type'] != '' 															? $params['post_type'] 					: 'post';
			
			$delay_spinner     		= isset($params['delay_spinner']) 	&& $params['delay_spinner'] != '' 	&& is_numeric(trim($params['delay_spinner'])) 		? trim($params['delay_spinner']) 		: '';
			
			$show_start_date		= isset($params['show_start_date']) && $params['show_start_date'] != ''														? $params['show_start_date'] 			: 'no';
			$show_end_date			= isset($params['show_end_date']) 	&& $params['show_end_date'] != ''														? $params['show_end_date'] 				: 'no';
			$open_popup_in			= isset($params['open_popup_in']) 	&& $params['open_popup_in'] != ''														? $params['open_popup_in'] 				: 'no';	
			
			$main_font				= isset($params['main_font']) 		&& $params['main_font'] != ''  		&& trim($params['main_font']) != '' 				? $params['main_font'] 					: '';
			$heading_font			= isset($params['heading_font']) 	&& $params['heading_font'] != ''  	&& trim($params['heading_font']) != '' 				? $params['heading_font'] 				: '';
			$s_heading_font			= isset($params['s_heading_font']) 	&& $params['s_heading_font'] != ''  && trim($params['s_heading_font']) != '' 			? $params['s_heading_font'] 			: '';
			
			$link_color				= isset($params['link_color']) 				&& $params['link_color'] != ''														? $params['link_color'] 				: '';
			$link_hover_color		= isset($params['link_hover_color']) 		&& $params['link_hover_color'] != ''												? $params['link_hover_color'] 			: '';
			$light_link_color		= isset($params['light_link_color']) 		&& $params['light_link_color'] != ''												? $params['light_link_color'] 			: '';
			$light_link_hover_color	= isset($params['light_link_hover_color']) 	&& $params['light_link_hover_color'] != ''											? $params['light_link_hover_color'] 	: '';
			$title_color			= isset($params['title_color']) 			&& $params['title_color'] != ''														? $params['title_color'] 				: '';
			$title_hover_color		= isset($params['title_hover_color']) 		&& $params['title_hover_color'] != ''												? $params['title_hover_color'] 			: '';
			$light_title_color		= isset($params['light_title_color']) 		&& $params['light_title_color'] != ''												? $params['light_title_color'] 			: '';
			$light_title_hover_color= isset($params['light_title_hover_color']) && $params['light_title_hover_color'] != ''											? $params['light_title_hover_color'] 	: '';
			$metadata_color			= isset($params['metadata_color']) 			&& $params['metadata_color'] != ''													? $params['metadata_color'] 			: '';
			$light_metadata_color	= isset($params['light_metadata_color']) 	&& $params['light_metadata_color'] != ''											? $params['light_metadata_color'] 		: '';
			$text_color				= isset($params['text_color']) 				&& $params['text_color'] != ''														? $params['text_color'] 				: '';
			$thumb_overlay			= isset($params['thumb_overlay']) 			&& $params['thumb_overlay'] != ''													? $params['thumb_overlay'] 				: '';
			
			$text_transform			= isset($params['text_transform']) 			&& $params['text_transform'] != ''													? $params['text_transform'] 			: 'uppercase';	
			
			$social_facebook		= isset($params['social_facebook']) 	&& $params['social_facebook'] != ''												? $params['social_facebook'] 				: 'yes';
			$social_twitter			= isset($params['social_twitter']) 		&& $params['social_twitter'] != ''												? $params['social_twitter'] 				: 'yes';
			$social_google_plus		= isset($params['social_google_plus']) 	&& $params['social_google_plus'] != ''											? $params['social_google_plus'] 			: 'yes';
			$social_pinterest		= isset($params['social_pinterest']) 	&& $params['social_pinterest'] != ''											? $params['social_pinterest'] 				: 'yes';
			$social_tumblr			= isset($params['social_tumblr']) 		&& $params['social_tumblr'] != ''												? $params['social_tumblr'] 					: 'yes';
			$social_linkedin		= isset($params['social_linkedin']) 	&& $params['social_linkedin'] != ''												? $params['social_linkedin'] 				: 'yes';			
			$social_email			= isset($params['social_email']) 		&& $params['social_email'] != ''												? $params['social_email'] 					: 'yes';
			
			$under_post_on_grid		= isset($params['under_post_on_grid']) 	&& $params['under_post_on_grid'] != ''												? $params['under_post_on_grid'] 			: 'no';
			$grid_style_box			= isset($params['grid_style_box']) 		&& $params['grid_style_box'] != ''													? $params['grid_style_box'] 				: 'no';
			$grid_style_title_on_img	= isset($params['grid_style_title_on_img']) && $params['grid_style_title_on_img'] != ''									? $params['grid_style_title_on_img'] 		: 'no';
			$hidden_grid_content	= isset($params['hidden_grid_content']) && $params['hidden_grid_content'] != ''									? $params['hidden_grid_content'] 		: 'no';
			
			$custom_margin     		= isset($params['custom_margin']) 	&& $params['custom_margin'] != '' 	&& is_numeric(trim($params['custom_margin'])) 		? trim($params['custom_margin']) 		: '';
			$custom_margin_bottom   = isset($params['custom_margin_bottom']) && $params['custom_margin_bottom'] != '' && is_numeric(trim($params['custom_margin_bottom'])) 	? trim($params['custom_margin_bottom']) : '';
			$link_target			= isset($params['link_target']) && $params['link_target'] != ''									? $params['link_target'] 		: 'no';
			
			$creative_custom_style	= isset($params['creative_custom_style']) && $params['creative_custom_style'] != ''				? $params['creative_custom_style'] 		: 'bt_black_white';
			
			return jps_bete_html_data($style, $filter_options, $taxonomies, $ids, $condition, $sortby, $post_metadata, $total_posts, $items_per_page, $height, $background_opt, $background_img, $parallax, $animate, $post_read_popup, $more_button, $autoplay, $autoplayspeed, $extra_class, $infinite_ajax, $gutter, $show_filter, $combo_grid_list, $priority_grid_list, $show_taxonomy, $show_author, $show_datetime, $show_viewscount, $show_commentscount, $show_excerpt, $post_type, $delay_spinner, $show_start_date, $show_end_date, $open_popup_in, $main_font, $heading_font, $s_heading_font, $link_color, $link_hover_color, $light_link_color, $light_link_hover_color, $title_color, $title_hover_color, $light_title_color, $light_title_hover_color, $metadata_color, $light_metadata_color, $text_color, $thumb_overlay, $text_transform, $social_facebook, $social_twitter, $social_google_plus, $social_pinterest, $social_tumblr, $social_linkedin, $social_email, $under_post_on_grid, $grid_style_box, $grid_style_title_on_img, $hidden_grid_content, $custom_margin, $custom_margin_bottom, $link_target, $creative_custom_style, '1', false, false, false, 0);
		}
		
	}
};

$jps_bete_slider = new jps_bete_slider();

if(!function_exists('reg_JPS_BETE_VC')) {
	function reg_JPS_BETE_VC(){
		if(function_exists('wpb_map')){
			
			/*taxonomies*/
			$taxonomies_for_filter = array();		
			/*taxonomies*/
			
			/*Post types*/
			$bt_post_types = get_post_types(array());
			$bt_post_types_list = array();		
			/*Post types*/
			
			$autocompleteTaxonomies = array(
											'type' 				=> 		'autocomplete',
											'heading' 			=> 		__('INCLUDE from filter list', JPS_BETE_SETTING_TEXT_DOMAIN),			/*translate*/
											'param_name' 		=> 		'taxonomies',
											'description' 		=> 		__('Enter categories, tags won\'t be shown in the filters list.', JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/
											'settings' 			=> 		array(
																			'multiple' 					=> true,																					
																			'min_length' 				=> 1,
																			'groups' 					=> true,
																			'unique_values' 			=> true,
																			'display_inline' 			=> false,
																			'delay' 					=> 500,
																			'auto_focus' 				=> true,
																		),
											'dependency' 		=> 		array(
																				'element'			 	=> 'filter_options',																						
																				'value_not_equal_to' 	=> array('all_posts', 'list_of_ids'),
																				'callback' 				=> 'vcGridFilterExcludeCallBack_bt_custom',
																			),												 
									);	
									
			$autocompleteListOfIDs = array(
											'type' 				=> 		'autocomplete',
											'heading' 			=> 		__('List of IDs', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
											'param_name' 		=> 		'ids',
											'description' 		=> 		__('Add posts by title - Works in conjunction with selected "Post types".', JPS_BETE_SETTING_TEXT_DOMAIN),   /*translate*/
											'settings' 			=> 		array(
																			'multiple' 					=> true,																					
																			'min_length' 				=> 2,
																			'groups' 					=> true,
																			'unique_values' 			=> true,
																			'display_inline' 			=> false,
																			'delay' 					=> 500,
																			'auto_focus' 				=> true,
																		),														
											'dependency' 		=> 		array(
																				'element' 	=> 'filter_options',
																				'value' 	=> array( 'list_of_ids'),																					
																			),								 
									);	
			$conditionCheckVC 	= array(
											'type' 				=> 		'dropdown',
											'heading' 			=> 		__('Conditions', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
											'param_name'		=> 		'condition',
											'value' 			=> 		array(
																			__('Latest', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> 'latest', 		/*translate*/
																			__('Most commented', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'mostcommented', /*translate*/
																			__('Title', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> 'title',			/*translate*/
																			__('Views Count', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'viewscount',	/*translate*/
																			__('Random', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> 'random',															
																		),
											'description' 		=> 		__('Condition to query items.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
											'dependency' 		=> 		array(
																				'element' => 'filter_options',
																				'value_not_equal_to' => array('list_of_ids'),																																														
																			),					
									);	
			$showFilterCheckVC	= array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Filter', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_filter',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__('Show Taxonomies Filter', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'filter_options',
																							'value_not_equal_to' => array('all_posts', 'list_of_ids'),
																						),					
													);																
			if(!function_exists('vc_autocomplete_taxonomies_field_render') || !function_exists('vc_autocomplete_taxonomies_field_search')) {
				$autocompleteTaxonomies = array(
												'type' 				=> 		'textfield',
												'heading' 			=> 		__('INCLUDE Taxonomy ID', JPS_BETE_SETTING_TEXT_DOMAIN),			/*translate*/
												'param_name' 		=> 		'taxonomies',
												'description' 		=> 		__(	'List of taxonomy (ID) to query items from, separated by a comma.
																				Works in conjunction with selected "Post types" & taxonomy by "Filter Options".', JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/
										);	
										
				$autocompleteListOfIDs = array(
												'type' 				=> 		'textfield',
												'heading' 			=> 		__('List of IDs', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
												'param_name' 		=> 		'ids',
												'description' 		=> 		__(	'List of post IDs to query, separated by a comma. If this value is not empty, "taxonomies filter" & "Conditions" are omitted. 
																				Works in conjunction with selected "Post types".', JPS_BETE_SETTING_TEXT_DOMAIN),   /*translate*/																										 
										);	
				$conditionCheckVC 	= array(
											'type' 				=> 		'dropdown',
											'heading' 			=> 		__('Conditions', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
											'param_name'		=> 		'condition',
											'value' 			=> 		array(
																			__('Latest', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> 'latest', 		/*translate*/
																			__('Most commented', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'mostcommented', /*translate*/
																			__('Title', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> 'title',			/*translate*/
																			__('Views Count', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'viewscount',	/*translate*/															
																		),
											'description' 		=> 		__('Condition to query items. Do not work with "List of IDs".', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/															
									);
				$showFilterCheckVC	= array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Filter', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_filter',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__('Show Taxonomies Filter', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/																		
													);											
				
				
				/*taxonomies*/
				$taxonomies_for_filter[__('All Posts (in post types select)', JPS_BETE_SETTING_TEXT_DOMAIN)] = 'all_posts'; /*translate*/	
				$vc_taxonomies_types = get_taxonomies( array( 'public' => true ), 'objects' );
				if (is_array($vc_taxonomies_types) && ! empty( $vc_taxonomies_types )) {
					foreach ( $vc_taxonomies_types as $t => $data ) {
						if ($t!=='post_format' && is_object($data)) {
							$taxonomies_for_filter['['.$t.'] - '.($data->labels->name)] = $t;
						}
					}
				};
				$taxonomies_for_filter[__('List of IDs', JPS_BETE_SETTING_TEXT_DOMAIN)] = 'list_of_ids'; /*translate*/	
				/*taxonomies*/
				
				/*Post types*/
				if ( is_array( $bt_post_types ) && ! empty( $bt_post_types ) ) {
					foreach ( $bt_post_types as $bt_post_type ) {				
						if ( $bt_post_type !== 'revision' && $bt_post_type !== 'nav_menu_item' && $bt_post_type !== 'attachment' && $bt_post_type !== 'page' ) {
							$bt_label = ucfirst( $bt_post_type );
							$bt_post_types_list['['.$bt_post_type.'] - '.($bt_label)] = $bt_post_type;
						}
					}
				}	
				/*Post types*/								
			}else{
				/*taxonomies*/
				$taxonomies_for_filter[] = array('all_posts', __('All Posts (in post types select)', JPS_BETE_SETTING_TEXT_DOMAIN)); /*translate*/	
				if ( 'vc_edit_form' === vc_post_param( 'action' ) ) {
					$vc_taxonomies_types = vc_taxonomies_types();
					$i_next = 0;
					if ( is_array( $vc_taxonomies_types ) && ! empty( $vc_taxonomies_types ) ) {					
						foreach ( $vc_taxonomies_types as $t => $data ) {
							if ( $t !== 'post_format' && is_object( $data ) ) {
								$taxonomies_for_filter['['.$t.'] - '.($data->labels->name) ] = $t;
							}
							$i_next++;
						}
					}
				}else{
					$taxonomies_for_filter[] = array('category', __('Categories', JPS_BETE_SETTING_TEXT_DOMAIN)); 	/*translate*/
					$taxonomies_for_filter[] = array('post_tag', __('Tags', JPS_BETE_SETTING_TEXT_DOMAIN));			/*translate*/	
				}
				$taxonomies_for_filter[] = array('list_of_ids', __('List of IDs', JPS_BETE_SETTING_TEXT_DOMAIN)); 		/*translate*/
				/*taxonomies*/
				
				/*Post types*/
				if ( is_array( $bt_post_types ) && ! empty( $bt_post_types ) ) {
					foreach ( $bt_post_types as $bt_post_type ) {				
						if ( $bt_post_type !== 'revision' && $bt_post_type !== 'nav_menu_item' && $bt_post_type !== 'attachment' && $bt_post_type !== 'page' ) {
							$bt_label = ucfirst( $bt_post_type );
							$bt_post_types_list[] = array( $bt_post_type, __('['.$bt_post_type.'] - '.($bt_label), JPS_BETE_SETTING_TEXT_DOMAIN ));
						}
					}
				}
				/*Post types*/
			};			
			
			/*theme EventBuilder*/
			$checkShowEventBuilderStarts = array(
													'type' 				=> 		'dropdown',
													'heading' 			=> 		__('Event Start Date', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
													'param_name'		=> 		'show_start_date',
													'value' 			=> 		array(
																					__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																					__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/															
																				),
													'description' 		=> 		__('Show Event Start Date - (support theme: EventBuilder)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
													'group' 			=> 		__('EventBuilder', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/						
											);
			
			$checkShowEventBuilderEnds = array(
												'type' 				=> 		'dropdown',
												'heading' 			=> 		__('Event End Date', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
												'param_name'		=> 		'show_end_date',
												'value' 			=> 		array(
																				__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																				__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/															
																			),
												'description' 		=> 		__('Show Event End Date - (support theme: EventBuilder)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
												'group' 			=> 		__('EventBuilder', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/						
										);
			/*theme EventBuilder*/
			
			wpb_map(array(
				"name"      			=> 		__("JPS Ajax Post Layout - BeeTeam368", JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
				"base"      			=>		"jps_bete_sc",
				"icon"      			=> 		JPS_BETE_PLUGIN_URL.'library/images/json-posts-slider.png',
				"category" 				=>		__('BeeTeam368', JPS_BETE_SETTING_TEXT_DOMAIN), 						/*translate*/
				"params"    			=> 		array(
													array(
															'type' 				=> 			'dropdown',
															'heading' 			=> 			__("Styles", JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 			'style',
															'value' 			=> 		array(
																							__("Classic [Slider] - Full Width", 		JPS_BETE_SETTING_TEXT_DOMAIN)	=> 'default',  			/*translate*/
																							__("Classic [Slider] - Full Container", 	JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-2row', 			/*translate*/
																							__("Classic [Slider] - Medium", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-medium',			/*translate*/
																							__("Classic [Slider] - Small", 				JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-small',			/*translate*/
																							
																							__("Creative Layout", 						JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-creative-1',		/*translate*/
																							
																							__("Masonry Flat Layout - Full Width", 		JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-metro-fw', 		/*translate*/
																							__("Masonry Flat Layout - Full Container", 	JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-metro-fc', 		/*translate*/
																							__("Masonry Flat Layout - Medium", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-metro-m', 		/*translate*/
																							
																							__("Grid Box Layout - 1 Column", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-grid-1',			/*translate*/
																							__("Grid Box Layout - 2 Columns", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-grid-2',			/*translate*/
																							__("Grid Box Layout - 3 Columns", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-grid-3',			/*translate*/
																							__("Grid Box Layout - 4 Columns", 			JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-grid-4',			/*translate*/																		
																							
																							__("Grid Layout - 1 Columns", 				JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-1',	/*translate*/	
																							__("Grid Layout - 2 Columns", 				JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-2',	/*translate*/	
																							__("Grid Layout - 3 Columns", 				JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-3',	/*translate*/	
																							__("Grid Layout - 4 Columns", 				JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-4',	/*translate*/
																							
																							__("Grid Masonry Layout - 1 Columns", 		JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-m-1',	/*translate*/
																							__("Grid Masonry Layout - 2 Columns", 		JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-m-2',	/*translate*/	
																							__("Grid Masonry Layout - 3 Columns", 		JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-m-3',	/*translate*/
																							__("Grid Masonry Layout - 4 Columns", 		JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-post-grid-m-4',	/*translate*/
																							
																							__("Carousel - Style 1", 					JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-carousel-1',		/*translate*/
																							__("Carousel - Style 2", 					JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-carousel-2',		/*translate*/
																							__("Carousel - Style 3", 					JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-carousel-3',		/*translate*/
																							__("Carousel - Style 4", 					JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'bt-carousel-4',		/*translate*/
																						),	
															'description' 		=> 		__(	'Choose layout (Full Width: best choice for full width 
																							- Full Container: best choice for full container in bootstrap).', 	JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/				
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Creative Styles', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'creative_custom_style',
															'description' 		=> 		__('Styles: Black & White, Black, White', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value' 			=> 		array(
																							__('Black & White', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'bt_black_white', 		/*translate*/
																							__('Black', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'bt_black', 		/*translate*/	
																							__('White', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'bt_white', 		/*translate*/															
																						),
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-creative-1'),
																						),													 
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Custom Margin For Column (margin right)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'custom_margin',
															'description' 		=> 		__('Ext: 5, 10, 15 ... Max = 200 (unit pixel) - Default = empty', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-metro-fw', 'bt-metro-fc', 'bt-metro-m', 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),													 
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Custom Margin For Column (margin bottom)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'custom_margin_bottom',
															'description' 		=> 		__('Ext: 5, 10, 15 ... Max = 200 (unit pixel) - Default = empty', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-metro-fw', 'bt-metro-fc', 'bt-metro-m', 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),													 
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Including: list & grid', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'combo_grid_list',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', 		/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', 		/*translate*/															
																						),
															'description' 		=> 		__('Enables Including list & grid.', JPS_BETE_SETTING_TEXT_DOMAIN),/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),				
														),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Priority list & grid', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'priority_grid_list',
															'value' 			=> 		array(
																							__('Grid ahead', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> '0', /*translate*/
																							__('List ahead', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> '1', /*translate*/
																							__('Only include list', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> '2', /*translate*/															
																						),
															'description' 		=> 		__('Priority list or grid.', JPS_BETE_SETTING_TEXT_DOMAIN), 	/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'combo_grid_list',
																							'value' => array( 'yes'),
																						),				
														),	
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Style Box For Grid Normal & Masonry', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'grid_style_box',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', 		/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', 		/*translate*/															
																						),
															'description' 		=> 		__('Style Box For Grid Normal & Masonry.', JPS_BETE_SETTING_TEXT_DOMAIN),/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),				
														),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('"Post metadata" Under The Photo', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'under_post_on_grid',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', 		/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', 		/*translate*/															
																						),
															'description' 		=> 		__('"Post metadata" Under The Photo.', JPS_BETE_SETTING_TEXT_DOMAIN),/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),				
														),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('"Article Title" Located On The Image', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'grid_style_title_on_img',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', 		/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', 		/*translate*/															
																						),
															'description' 		=> 		__('"Article Title" Located On The Image.', JPS_BETE_SETTING_TEXT_DOMAIN),/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'bt-post-grid-1', 'bt-post-grid-2', 'bt-post-grid-3', 'bt-post-grid-4', 'bt-post-grid-m-1', 'bt-post-grid-m-2', 'bt-post-grid-m-3', 'bt-post-grid-m-4'),
																						),				
														),	
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Hidden Content For Post Grid', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'param_name'		=> 		'hidden_grid_content',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', 		/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', 		/*translate*/															
																						),
															'description' 		=> 		__('"Hidden Content For Post Grid.', JPS_BETE_SETTING_TEXT_DOMAIN),/*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'grid_style_title_on_img',
																							'value' => array( 'yes'),
																						),				
														),							
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Gutter', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'gutter',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'no',		/*translate*/
																							__('Yes',JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/																																			
																						),
															'description' 		=> 		__('Padding Item Yes (or) No', JPS_BETE_SETTING_TEXT_DOMAIN),  /*translate*/
															'dependency' 		=> 		array(
																								'element'	 	=> 'style',
																								'value' 		=> array(
																													'bt-grid-1', 
																													'bt-grid-2', 
																													'bt-grid-3', 
																													'bt-grid-4', 																																																					
																													)
																							),								
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Data Source', JPS_BETE_SETTING_TEXT_DOMAIN),			/*translate*/
															'param_name' 		=> 		'post_type',
															'value' 			=> 		$bt_post_types_list,
															'description' 		=> 		__('Select "Post type".', JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Filter Options', JPS_BETE_SETTING_TEXT_DOMAIN),		/*translate*/
															'param_name' 		=> 		'filter_options',
															'value' 			=> 		$taxonomies_for_filter,
															'description' 		=> 		__(	'Filter Options (Categories, Tags, Taxonomies, IDs Query, ...) 
																							- Works in conjunction with selected "Post types".', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
													),
													$autocompleteTaxonomies,												
													$autocompleteListOfIDs,																							
													$conditionCheckVC,																						
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Sort By', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'sortby',
															'value' 			=> 		array(
																							__('Descending', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'DESC',	/*translate*/
																							__('Ascending', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> 'ASC',	/*translate*/															
																						),
															'description' 		=> 		__('Designates the ascending or descending order.', JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/				
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Total Posts', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'total_posts',
															'description' 		=> 		__('Number of items to query. Ext: 50, 200 ... (max: 1000, default: 100). 
																							If this value is empty, default = All items in Query.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',												 
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Items Per Page', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'items_per_page',
															'description' 		=> 		__('Number of items per page. Ext: 5, 10 ... If this value is empty, default = 10 items.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',												 
													),
													/*infinite_ajax*/
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Pagination', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'infinite_ajax',
															'value' 			=> 		array(
																							__('Button [Load More]', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'no', /*translate*/
																							__('Infinite AJAX Scroll', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes', /*translate*/																																					
																						),
															'description' 		=> 		__('Button [Load More] (or) Infinite AJAX Scroll.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__( 'Pagination', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element'	 	=> 'style',
																							'value' 		=> array( 
																												'bt-metro-fw', 
																												'bt-metro-fc', 
																												'bt-metro-m', 
																												'bt-grid-1', 
																												'bt-grid-2', 
																												'bt-grid-3', 
																												'bt-grid-4',
																												'bt-post-grid-1',																											
																												'bt-post-grid-2', 
																												'bt-post-grid-3', 
																												'bt-post-grid-4', 
																												'bt-post-grid-m-1',
																												'bt-post-grid-m-2', 
																												'bt-post-grid-m-3', 
																												'bt-post-grid-m-4',
																												'bt-creative-1',
																												)
																						),								
													),			
													/*infinite_ajax*/
													
													/*Slider Settings*/
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Height Slider', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'height',
															'description' 		=> 		__('The height of the slider. Ext: " full " (full width) or number: 700, 800... [Min Height = 650], 
																							Unit = pixel. If this value is empty, default = " full "', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'default', 'bt-2row', 'bt-medium', 'bt-small')
																						),		
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ),	/*translate*/																 
													),	
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Background Image Option', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'background_opt',
															'value' 			=> 		array(
																							__('Automatically Taken From The First Article', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																							__('Retrieved From The Selection List', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> 'yes', /*translate*/	 														
																						),
															'description' 		=> 		__('Mode selection of wallpaper.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'default', 'bt-2row', 'bt-medium', 'bt-small')
																						),	
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ),	/*translate*/											
													),
													array(
															'type' 				=> 		'attach_images',
															'heading' 			=> 		__('Background Image Listing [Standard Size: full-HD]', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'background_img',
															'dependency' 		=> 		array(
																							'element' => 'background_opt',
																							'value' => array( 'yes' )
																						),	
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/			
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Parallax Background', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'parallax',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__('Parallax background on/off.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'default', 'bt-2row', 'bt-medium', 'bt-small')
																						),	
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ),	/*translate*/					
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Scroll Down More Button', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'more_button',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__('Scroll Down More Button on/off.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'default', 'bt-2row', 'bt-medium', 'bt-small')
																						),
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/						
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Autoplay', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'autoplay',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',/*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes',/*translate*/															
																						),
															'description' 		=> 		__('Enables Autoplay.', JPS_BETE_SETTING_TEXT_DOMAIN),	/*translate*/
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => array( 'default', 'bt-2row', 'bt-medium', 'bt-small', 'bt-carousel-1', 'bt-carousel-2', 'bt-carousel-3', 'bt-carousel-4')
																						),
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ),	/*translate*/				
													),	
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Autoplay Speed', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'autoplayspeed',
															'description' 		=> 		__('Autoplay Speed in milliseconds. If this value is empty, 
																							default = " 10000 " (=10s -> best choice). Ext: 3000 (=3s), 5000 (=5s)...', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'value'				=> 		'',
															'dependency' 		=> 		array(
																							'element' => 'autoplay',
																							'value' => array( 'yes' ),
																						),	
															'group' 			=> 		__( 'Slider', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/														 
													),
													/*Slider Settings*/
													
													/*Design Options*/
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Animation [42 Animation Effects]', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'animate',
															'value' 			=> 		array(
																							__('Default', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> 'default', /*translate*/
																							__('Random', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> 'rand', /*translate*/
																							__('Random Asynchronous', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'randsync', /*translate*/	 
																							
																							__('bounce', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '0', /*translate*/
																							__('flash', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '1', /*translate*/
																							__('pulse', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '2', /*translate*/
																							__('rubberBand', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '3', /*translate*/
																							__('shake', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '4', /*translate*/
																							__('swing', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '5', /*translate*/
																							__('tada', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '6', /*translate*/
																							__('wobble', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '7', /*translate*/
																							__('jello', JPS_BETE_SETTING_TEXT_DOMAIN) 					=> '8', /*translate*/
																							__('bounceIn', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '9', /*translate*/
																							__('bounceInDown', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '10', /*translate*/
																							__('bounceInLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '11', /*translate*/
																							__('bounceInRight', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '12', /*translate*/
																							__('bounceInUp', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '13', /*translate*/
																							__('fadeIn', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '14', /*translate*/
																							__('fadeInDown', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '15', /*translate*/
																							__('fadeInDownBig', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '16', /*translate*/
																							__('fadeInLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '17', /*translate*/
																							__('fadeInLeftBig', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '18', /*translate*/
																							__('fadeInRight', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '19', /*translate*/
																							__('fadeInRightBig', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> '20', /*translate*/
																							__('fadeInUp', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '21', /*translate*/
																							__('fadeInUpBig', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '22', /*translate*/
																							__('flipInX', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '23', /*translate*/
																							__('flipInY', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '24', /*translate*/
																							__('lightSpeedIn', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '25', /*translate*/
																							__('rotateIn', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '26', /*translate*/
																							__('rotateInDownLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> '27', /*translate*/
																							__('rotateInDownRight', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> '28', /*translate*/
																							__('rotateInUpLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> '29', /*translate*/
																							__('rotateInUpRight', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> '30', /*translate*/
																							__('rollIn', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '31', /*translate*/
																							__('zoomIn', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '32', /*translate*/
																							__('zoomInDown', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '33', /*translate*/
																							__('zoomInLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '34', /*translate*/
																							__('zoomInRight', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '35', /*translate*/
																							__('zoomInUp', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '36', /*translate*/
																							__('slideInDown', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '37', /*translate*/
																							__('slideInLeft', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '38', /*translate*/
																							__('slideInRight', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> '39', /*translate*/
																							__('slideInUp', JPS_BETE_SETTING_TEXT_DOMAIN) 				=> '40',  /*translate*/														
																						),
															'description' 		=> 		__('The list posts appear, there will be more animated (not support Carousel Style 1,2 & 3)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'style',
																							'value' => 	array( 
																											'bt-metro-fw', 
																											'bt-metro-fc', 
																											'bt-metro-m', 
																											'bt-grid-1', 
																											'bt-grid-2',
																											'bt-grid-3', 
																											'bt-grid-4', 
																											'bt-post-grid-1',
																											'bt-post-grid-2', 
																											'bt-post-grid-3', 
																											'bt-post-grid-4', 
																											'bt-post-grid-m-1',
																											'bt-post-grid-m-2', 
																											'bt-post-grid-m-3', 
																											'bt-post-grid-m-4', 
																											'default', 
																											'bt-2row', 
																											'bt-medium', 
																											'bt-small', 
																											'bt-creative-1',
																										),
																							),				
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Post Metadata', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'post_metadata',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__('Show post metadata (published date, taxonomy, views count, comments count, author)', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ),	 /*translate*/				
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Taxonomies', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_taxonomy', /*translate*/
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__( 'Append Taxonomies to layout', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),													
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Author', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_author',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/														
																						),
															'description' 		=> 		__( 'Append Author to layout (only for: Slider Classic, carousel, grid normal & grid masonry style)', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),
														array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Datetime', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_datetime',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__( 'Append Author to layout (only for: Slider Classic, carousel, grid normal & grid masonry style)', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Views Count', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_viewscount',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/															
																						),
															'description' 		=> 		__( 'Append Author to layout (only for: Slider Classic, carousel, grid normal & grid masonry style)', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Comments Count', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_commentscount',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/															
																						),
															'description' 		=> 		__( 'Append Author to layout (only for: Slider Classic, carousel, grid normal & grid masonry style)', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Excerpt', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'show_excerpt',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/															
																						),
															'description' 		=> 		__( 'Append Excerpt to layout (only for: carousel style 1, grid normal & grid masonry)', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'dependency' 		=> 		array(
																							'element' => 'post_metadata',
																							'value' => 	array('yes',),
																							),					
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Show Post Read Popup', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'post_read_popup',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/														
																						),
															'description' 		=> 		__('Show Post Read Popup on/off.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/																	
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Open Post By', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'open_popup_in',
															'value' 			=> 		array(
																							__('Basic button', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																							__('All Links on Layout', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/														
																						),
															'description' 		=> 		__('Open by basic button (or) All links on layout.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/	
															'dependency' 		=> 		array(
																							'element' => 'post_read_popup',
																							'value' => 	array('yes',),
																							),																
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Text Transform', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'text_transform',
															'value' 			=> 		array(
																							__('Uppercase', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> 'uppercase', /*translate*/
																							__('Capitalize', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'capitalize', /*translate*/	
																							__('Normal', JPS_BETE_SETTING_TEXT_DOMAIN) 			=> 'normal', /*translate*/														
																						),
															'description' 		=> 		__('Text Transform For Title.', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
													),	
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Open Link In New Tab', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name'		=> 		'link_target',
															'value' 			=> 		array(
																							__('No', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no', /*translate*/
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'yes', /*translate*/															
																						),
															'description' 		=> 		__( '', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/																				
													),	
													$showFilterCheckVC,
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Latency For Spinner (only for "Carousel Content Box" [all styles])', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'delay_spinner',														
															'value'				=> 		'',
															'description' 		=> 		__('Latency when loading pages appear icon. If this value is empty, 
																							default = blank (best choice). Ext: 500 = 0.5s, 3000 (=3s), 5000 (=5s)...', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/													 
													),	
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Custom Main Font', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'main_font',														
															'value'				=> 		'',
															'description' 		=> 		__('If this value is empty, use the default font of theme. Only Support Google fonts. Ext: Open Sans', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/													 
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Custom Heading Font', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'heading_font',														
															'value'				=> 		'',
															'description' 		=> 		__('If this value is empty, use the default font of theme. Only Support Google fonts. Ext: Oswald', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/													 
													),
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Custom Special Font', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		's_heading_font',														
															'value'				=> 		'',
															'description' 		=> 		__('If this value is empty, use the default font of theme. Only Support Google fonts & Primary Post of Classic Slider. Ext: Lobster', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/													 
													),																			
													array(
															'type' 				=> 		'textfield',
															'heading' 			=> 		__('Extra Class Name', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/
															'param_name' 		=> 		'extra_class',														
															'value'				=> 		'',
															'group' 			=> 		__('Options', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/													 
													),
													/*Design Options*/
													
													/*EventBuilder*/
													$checkShowEventBuilderStarts,
													$checkShowEventBuilderEnds,
													/*EventBuilder*/
													
													/*Custom Color*/
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Dark] Link Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'link_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Dark] Link Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'link_hover_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Light] Link Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'light_link_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Light] Link Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'light_link_hover_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Dark] Title Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'title_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Dark] Title Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'title_hover_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Light] Title Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'light_title_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Light] Title Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'light_title_hover_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Dark] Metadata Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'metadata_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('[Light] Metadata Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'light_metadata_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('Text Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'text_color',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													array(
															'type'				=> 'colorpicker',
															'heading' 			=> __('Thumb Overlay Color', JPS_BETE_SETTING_TEXT_DOMAIN), /*translate*/	
															'param_name' 		=> 'thumb_overlay',
															'value' 			=> '',
															'group' 			=> 	__('Color', JPS_BETE_SETTING_TEXT_DOMAIN ), /*translate*/		
													),
													/*Custom Color*/
													
													/*Social*/
														
													/*Social*/
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Facebook', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_facebook',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Facebook Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/	
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																				
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Twitter', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_twitter',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Twitter Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/	
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																		
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Google Plus', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_google_plus',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Google Plus Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/	
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																		
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Pinterest', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_pinterest',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Pinterest Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																			
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Tumblr', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_tumblr',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Tumblr Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																			
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Linkedin', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_linkedin',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Linkedin Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																			
													),
													array(
															'type' 				=> 		'dropdown',
															'heading' 			=> 		__('Email', JPS_BETE_SETTING_TEXT_DOMAIN), 					/*translate*/
															'param_name' 		=> 		'social_email',
															'value' 			=> 		array(
																							__('Yes', JPS_BETE_SETTING_TEXT_DOMAIN) 	=> 'yes',		/*translate*/
																							__('No',JPS_BETE_SETTING_TEXT_DOMAIN) 		=> 'no',		/*translate*/																																			
																						),
															'description' 		=> 		__('Show Email Button', JPS_BETE_SETTING_TEXT_DOMAIN), 		/*translate*/
															'group' 			=> 		__('Social', JPS_BETE_SETTING_TEXT_DOMAIN ), 					/*translate*/																			
													),
												),
			));		
		};
		if(class_exists('WPBakeryShortCode')){
			class WPBakeryShortCode_jps_bete_sc extends WPBakeryShortCode{}
		};
	};
};

add_action('init', 'reg_JPS_BETE_VC', 9998);

add_filter('vc_autocomplete_jps_bete_sc_taxonomies_callback', 'vc_autocomplete_taxonomies_field_search', 10, 1 );
add_filter('vc_autocomplete_jps_bete_sc_taxonomies_render', 'vc_autocomplete_taxonomies_field_render', 10, 1 );

add_filter('vc_autocomplete_jps_bete_sc_ids_callback', 'vc_include_field_search', 10, 1 );
add_filter('vc_autocomplete_jps_bete_sc_ids_render', 'vc_include_field_render', 10, 1 );

if(!function_exists('jps_bete_language_load_init')) {
	function jps_bete_language_load_init() {
		load_plugin_textdomain( JPS_BETE_SETTING_TEXT_DOMAIN, false, JPS_BETE_PLUGIN_URL.'/languages');
	}
};
add_action('init', 'jps_bete_language_load_init', 9999);

if(!function_exists('jps_bete_frontend_scripts')){
	function jps_bete_frontend_scripts(){
		//CSS
		/*	
		//All plugin jquery css
								
		wp_enqueue_style('jps_bete_slick_css', 		JPS_BETE_PLUGIN_URL.'library/slick/slick.css', array(), JPS_BETE_VER );
		wp_enqueue_style('jps_bete_malihu_css', 	JPS_BETE_PLUGIN_URL.'library/malihu/jquery.mCustomScrollbar.min.css', array(), JPS_BETE_VER);
		wp_enqueue_style('jps_bete_animate_css', 	JPS_BETE_PLUGIN_URL.'library/animate/animate.min.css', array(), JPS_BETE_VER);
		*/
		wp_enqueue_style('jps_bete_core_init_css', 	JPS_BETE_PLUGIN_URL.'library/0-init/core-init.css', array(), JPS_BETE_VER);			
	
		switch(JPS_BETE_PLUGIN_OBFUSCATOR) {
			case 'normal':
				wp_enqueue_style('jps_bete_css', 			JPS_BETE_PLUGIN_URL.'library/core.css', array(), JPS_BETE_VER);
				break;
			case 'minify':
				wp_enqueue_style('jps_bete_css', 			JPS_BETE_PLUGIN_URL.'library/1-minify/core-min.css', array(), JPS_BETE_VER);
				break;
			case 'OBFUSCATOR':
				wp_enqueue_style('jps_bete_css', 			JPS_BETE_PLUGIN_URL.'library/1-minify/core-min.css', array(), JPS_BETE_VER);
				break;		
		};
		
		//Javascript
		wp_enqueue_script('jquery');
		/*
		//All plugin jquery
		
		wp_enqueue_script('jps_bete_slick_js', 		JPS_BETE_PLUGIN_URL.'library/slick/slick.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_malihu_js', 	JPS_BETE_PLUGIN_URL.'library/malihu/jquery.mCustomScrollbar.concat.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_masonry', 		JPS_BETE_PLUGIN_URL.'library/masonry/masonry.pkgd.min.js', array(), JPS_BETE_VER, true);			
		wp_enqueue_script('jps_bete_imagesloaded', 	JPS_BETE_PLUGIN_URL.'library/imagesloaded/imagesloaded.pkgd.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_transit', 		JPS_BETE_PLUGIN_URL.'library/jquery-transit/jquery.transit.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_smoothscroll', 	JPS_BETE_PLUGIN_URL.'library/smoothscroll/SmoothScroll-min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_tweenmax', 		JPS_BETE_PLUGIN_URL.'library/TweenMax/TweenMax.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_panr', 			JPS_BETE_PLUGIN_URL.'library/panr/jquery.panr.min.js', array(), JPS_BETE_VER, true);
		wp_enqueue_script('jps_bete_cookie', 		JPS_BETE_PLUGIN_URL.'library/jquery-cookie/jquery.cookie.min.js', array(), JPS_BETE_VER, true);			
		*/	
		wp_enqueue_script('jps_bete_core_init_js', 	JPS_BETE_PLUGIN_URL.'library/0-init/core-init.js', array(), JPS_BETE_VER, true);
		
		switch(JPS_BETE_PLUGIN_OBFUSCATOR) {
			case 'normal':
				wp_enqueue_script('jps_bete_js', 			JPS_BETE_PLUGIN_URL.'library/core.js', array(), JPS_BETE_VER, true);
				break;
			case 'minify':
				wp_enqueue_script('jps_bete_js', 			JPS_BETE_PLUGIN_URL.'library/1-minify/core-min.js', array(), JPS_BETE_VER, true);
				break;
			case 'OBFUSCATOR':
				wp_enqueue_script('jps_bete_js', 			JPS_BETE_PLUGIN_URL.'library/1-minify/Obfuscator.js', array(), JPS_BETE_VER, true);
				break;		
		};				
	}
};

add_action('wp_enqueue_scripts', 'jps_bete_frontend_scripts', 9999);

/*Viewcount*/
if(!function_exists('betePlug_set_post_views')){
	function betePlug_set_post_views($postID) {
		if (class_exists('MipTheme_Post_Views')) { 
			$count_key = 'mip_post_views_count';
		}else{
			$count_key = 'beteplug_post_views_count';
		}
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		};
	};
};
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

if(!function_exists('betePlug_track_post_views')){
	function betePlug_track_post_views($post_id) {
		if (!is_single()) return;
		if (empty($post_id)){
			global $post;
			$post_id = $post->ID;   
		};
		betePlug_set_post_views($post_id);
	};
};
add_action('wp_head', 'betePlug_track_post_views');

if(!function_exists('betePlug_get_post_views')){	
	function betePlug_get_post_views($postID){		
		if (class_exists('MipTheme_Post_Views')) { 
			$count = MipTheme_Post_Views::get_post_views($postID, $counter_view = 'mip_post_views_count');	
			if($count==''){
				return 0;
			}
		}
		elseif(function_exists('yt_simple_post_views_tracker_display')){
			return number_format( yt_simple_post_views_tracker_display( $postID, false ) );	
		}else{		
			$count_key = 'beteplug_post_views_count';
			$count = get_post_meta($postID, $count_key, true);
			if($count==''){
				delete_post_meta($postID, $count_key);
				add_post_meta($postID, $count_key, '0');
				return '0';
			};
		};
		return $count;
	};
};
if (class_exists('MipTheme_Post_Views')) {
	add_action('wp_footer', 'miptheme_ajax_post_views_call', 100);
}
/*Viewcount*/

/*svg icon*/
if(!function_exists('echoActionSvgIcon_1')){
	function echoActionSvgIcon_1(){
		echo 	'<svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
					<symbol id="bete-awesome-svg-chat-bubble-two" viewBox="0 0 512 512">
						<path d="m201 110c-29 0-56 5-82 15c-25 9-45 23-60 40c-15 17-22 35-22 54c0 16 5 31 15 46c10 14 24 27 42 37l28 16l-10 24c6-4 12-7 18-11l12-9l15 3c15 3 30 4 44 4c29 0 57-5 82-15c25-10 45-23 60-40c15-17 23-35 23-55c0-19-8-37-23-54c-15-17-35-31-60-40c-25-10-53-15-82-15z m0-37c37 0 70 7 101 20c31 13 56 31 73 53c18 22 27 47 27 73c0 27-9 51-27 74c-17 22-42 40-73 53c-31 13-64 20-101 20c-16 0-33-2-50-5c-24 17-50 29-80 37c-6 1-15 3-24 4l-1 0c-2 0-4 0-6-2c-2-2-3-4-3-6c0-1 0-1 0-2c0-1 0-1 0-2c0 0 0-1 0-1l1-2c0 0 0 0 1-1c1-1 1-2 1-2c0 0 1 0 1-1c1-1 2-2 2-2c1-1 3-3 6-7c4-3 6-6 8-8c1-2 3-5 6-8c3-4 5-8 7-11c2-4 4-8 6-13c-24-14-42-31-56-51c-13-20-20-41-20-64c0-26 9-51 27-73c18-22 42-40 73-53c31-13 65-20 101-20z m235 334c2 5 4 9 6 13c2 4 4 7 7 11c3 3 5 6 6 8c2 2 4 5 8 8c3 4 5 6 6 8c1 0 1 0 2 1c0 1 1 1 1 1c0 1 1 1 1 2c0 0 1 1 1 1l1 2c0 0 0 0 0 1c1 2 1 2 0 2c0 0 0 1 0 2c0 3-2 5-4 6c-1 2-4 3-6 2c-9-1-18-2-24-4c-30-8-56-20-80-37c-17 3-34 5-50 5c-52 0-97-13-135-38c11 1 19 1 25 1c31 0 60-4 88-13c29-8 54-20 76-36c24-18 42-38 55-61c12-23 19-47 19-73c0-14-2-29-7-43c25 14 44 30 59 51c14 20 21 42 21 66c0 22-7 44-20 64c-14 20-32 36-56 50z"/>
					</symbol>
					<symbol id="bete-awesome-svg-eye" viewBox="0 0 512 512">
						<path d="m475 274c-29-45-65-78-108-101c11 20 17 42 17 65c0 35-13 65-38 90c-25 25-55 38-90 38c-35 0-65-13-90-38c-25-25-38-55-38-90c0-23 6-45 17-65c-43 23-79 56-108 101c25 39 57 70 95 94c38 23 79 34 124 34c45 0 86-11 124-34c38-24 70-55 95-94z m-205-109c0-4-2-7-4-10c-3-3-6-4-10-4c-24 0-44 8-61 25c-17 17-26 38-26 62c0 4 1 7 4 9c3 3 6 4 10 4c4 0 7-1 10-4c2-2 4-5 4-9c0-17 5-31 17-42c12-12 26-18 42-18c4 0 7-1 10-4c2-2 4-6 4-9z m242 109c0 7-2 13-6 20c-26 44-62 79-107 105c-45 27-93 40-143 40c-50 0-98-13-143-40c-45-26-81-61-107-105c-4-7-6-13-6-20c0-6 2-13 6-19c26-44 62-79 107-106c45-26 93-39 143-39c50 0 98 13 143 39c45 27 81 62 107 106c4 6 6 13 6 19z"/>
					</symbol>
					<symbol id="bete-awesome-svg-clock" viewBox="0 0 512 512">
						<path d="m293 155l0 128c0 3-1 5-3 7c-2 2-4 3-7 3l-91 0c-3 0-5-1-7-3c-1-2-2-4-2-7l0-18c0-3 1-5 2-6c2-2 4-3 7-3l64 0l0-101c0-2 1-4 3-6c1-2 3-3 6-3l18 0c3 0 5 1 7 3c2 2 3 4 3 6z m118 101c0-28-7-54-20-78c-14-24-33-43-57-57c-24-13-50-20-78-20c-28 0-54 7-78 20c-24 14-43 33-57 57c-13 24-20 50-20 78c0 28 7 54 20 78c14 24 33 43 57 57c24 13 50 20 78 20c28 0 54-7 78-20c24-14 43-33 57-57c13-24 20-50 20-78z m64 0c0 40-9 77-29 110c-20 34-46 60-80 80c-33 20-70 29-110 29c-40 0-77-9-110-29c-34-20-60-46-80-80c-20-33-29-70-29-110c0-40 9-77 29-110c20-34 46-60 80-80c33-20 70-29 110-29c40 0 77 9 110 29c34 20 60 46 80 80c20 33 29 70 29 110z"/>
					</symbol>
					<symbol id="bete-awesome-svg-hospital-square" viewBox="0 0 512 512">
						<path d="m402 274l0-36c0-5-2-10-5-13c-4-4-8-6-13-6l-91 0l0-91c0-5-2-9-6-13c-3-3-8-5-13-5l-36 0c-5 0-10 2-13 5c-4 4-6 8-6 13l0 91l-91 0c-5 0-9 2-13 6c-3 3-5 8-5 13l0 36c0 5 2 10 5 13c4 4 8 6 13 6l91 0l0 91c0 5 2 9 6 13c3 3 8 5 13 5l36 0c5 0 10-2 13-5c4-4 6-8 6-13l0-91l91 0c5 0 9-2 13-6c3-3 5-8 5-13z m73-155l0 274c0 23-8 42-24 58c-16 16-35 24-58 24l-274 0c-23 0-42-8-58-24c-16-16-24-35-24-58l0-274c0-23 8-42 24-58c16-16 35-24 58-24l274 0c23 0 42 8 58 24c16 16 24 35 24 58z"/>
					</symbol>
					<symbol id="bete-awesome-svg-user" viewBox="0 0 512 512">
						<path d="m457 401c0 23-7 41-21 55c-14 13-32 19-55 19l-250 0c-23 0-41-6-55-19c-14-14-21-32-21-55c0-10 0-20 1-29c1-10 2-20 4-31c2-11 4-22 7-31c3-10 8-19 13-28c5-9 11-17 17-23c7-7 15-12 25-16c9-3 20-5 32-5c1 0 5 2 12 6c6 4 13 9 21 14c8 5 18 9 31 13c13 4 25 6 38 6c13 0 25-2 38-6c13-4 23-8 31-13c8-5 15-10 21-14c7-4 11-6 12-6c12 0 23 2 32 5c10 4 18 9 25 16c6 6 12 14 17 23c5 9 10 18 13 28c3 9 5 20 7 31c2 11 3 21 4 31c1 9 1 19 1 29z m-91-255c0 31-11 56-32 78c-22 21-48 32-78 32c-30 0-56-11-78-32c-21-22-32-47-32-78c0-30 11-56 32-77c22-22 48-32 78-32c30 0 56 10 78 32c21 21 32 47 32 77z"/>
					</symbol>
					<symbol id="bete-awesome-svg-present-gift" viewBox="0 0 512 512">
						<path d="m302 387l0-204l-92 0l0 204c0 5 2 9 5 11c4 3 8 4 14 4l54 0c6 0 10-1 14-4c3-2 5-6 5-11z m-131-241l56 0l-36-46c-5-6-11-9-20-9c-7 0-14 3-19 8c-5 6-8 12-8 20c0 7 3 14 8 19c5 6 12 8 19 8z m197-27c0-8-3-14-8-20c-5-5-12-8-19-8c-9 0-15 3-20 9l-36 46l56 0c7 0 14-2 19-8c5-5 8-12 8-19z m107 73l0 91c0 3 0 5-2 7c-2 2-4 3-7 3l-27 0l0 118c0 8-3 15-8 20c-5 5-12 8-20 8l-310 0c-8 0-15-3-20-8c-5-5-8-12-8-20l0-118l-27 0c-3 0-5-1-7-3c-2-2-2-4-2-7l0-91c0-3 0-5 2-7c2-1 4-2 7-2l125 0c-17 0-32-6-45-19c-12-12-19-27-19-45c0-18 7-33 19-45c13-13 28-19 45-19c21 0 37 7 48 22l37 47l37-47c11-15 27-22 48-22c17 0 32 6 45 19c12 12 19 27 19 45c0 18-7 33-19 45c-13 13-28 19-45 19l125 0c3 0 5 1 7 2c2 2 2 4 2 7z"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-facebook" viewBox="0 0 28 28">
						<path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z" class="cls-2" fill-rule="evenodd" />
					</symbol>
					
					<symbol id="bete-awesome-svg-twitter" viewBox="0 0 28 28">
						<path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62c-3.122.162-6.22-.646-8.86-2.32 2.702.18 5.375-.648 7.507-2.32-2.072-.248-3.818-1.662-4.49-3.64.802.13 1.62.077 2.4-.154-2.482-.466-4.312-2.586-4.412-5.11.688.276 1.426.408 2.168.387-2.135-1.65-2.73-4.62-1.394-6.965C5.574 7.816 9.54 9.84 13.802 10.07c-.842-2.738.694-5.64 3.434-6.48 2.018-.624 4.212.043 5.546 1.682 1.186-.213 2.318-.662 3.33-1.317-.386 1.256-1.248 2.312-2.4 2.942 1.048-.106 2.07-.394 3.02-.85-.458 1.182-1.343 2.15-2.48 2.71z"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-google-plus" viewBox="0 0 28 28">
						<path d="M14.703 15.854l-1.22-.948c-.37-.308-.88-.715-.88-1.46 0-.747.51-1.222.95-1.662 1.42-1.12 2.84-2.31 2.84-4.817 0-2.58-1.62-3.937-2.4-4.58h2.098l2.203-1.384h-6.67c-1.83 0-4.467.433-6.398 2.027C3.768 4.287 3.06 6.018 3.06 7.576c0 2.634 2.02 5.328 5.603 5.328.34 0 .71-.033 1.083-.068-.167.408-.336.748-.336 1.324 0 1.04.55 1.685 1.01 2.297-1.523.104-4.37.273-6.466 1.562-1.998 1.187-2.605 2.915-2.605 4.136 0 2.512 2.357 4.84 7.288 4.84 5.822 0 8.904-3.223 8.904-6.41.008-2.327-1.36-3.49-2.83-4.73h-.01zM10.27 11.95c-2.913 0-4.232-3.764-4.232-6.036 0-.884.168-1.797.744-2.51.543-.68 1.49-1.12 2.372-1.12 2.807 0 4.256 3.797 4.256 6.24 0 .613-.067 1.695-.845 2.48-.537.55-1.438.947-2.295.95v-.003zm.032 13.66c-3.62 0-5.957-1.733-5.957-4.143 0-2.408 2.165-3.223 2.91-3.492 1.422-.48 3.25-.545 3.556-.545.34 0 .52 0 .767.034 2.574 1.838 3.706 2.757 3.706 4.48-.002 2.072-1.736 3.664-4.982 3.648l.002.017zM23.254 11.89V8.52H21.57v3.37H18.2v1.714h3.367v3.4h1.684v-3.4h3.4V11.89"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-pinterest" viewBox="0 0 28 28">
						<path d="M14.02 1.57c-7.06 0-12.784 5.723-12.784 12.785S6.96 27.14 14.02 27.14c7.062 0 12.786-5.725 12.786-12.785 0-7.06-5.724-12.785-12.785-12.785zm1.24 17.085c-1.16-.09-1.648-.666-2.558-1.22-.5 2.627-1.113 5.146-2.925 6.46-.56-3.972.822-6.952 1.462-10.117-1.094-1.84.13-5.545 2.437-4.632 2.837 1.123-2.458 6.842 1.1 7.557 3.71.744 5.226-6.44 2.924-8.775-3.324-3.374-9.677-.077-8.896 4.754.19 1.178 1.408 1.538.49 3.168-2.13-.472-2.764-2.15-2.683-4.388.132-3.662 3.292-6.227 6.46-6.582 4.008-.448 7.772 1.474 8.29 5.24.58 4.254-1.815 8.864-6.1 8.532v.003z"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-tumblr" viewBox="0 0 510 510">
						<path d="M459,0H51C22.95,0,0,22.95,0,51v408c0,28.05,22.95,51,51,51h408c28.05,0,51-22.95,51-51V51C510,22.95,487.05,0,459,0zM357,229.5h-76.5c0,0,0,96.9,0,99.45c0,17.85,2.55,28.05,28.05,28.05c22.95,0,48.45,0,48.45,0v76.5c0,0-25.5,2.55-53.55,2.55c-66.3,0-99.45-40.8-99.45-86.7c0-30.6,0-119.85,0-119.85h-51v-71.4c61.2-5.1,66.3-51,71.4-81.6h56.1V153H357V229.5z"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-linkedin" viewBox="0 0 28 28">
						<path d="M25.424 15.887v8.447h-4.896v-7.882c0-1.98-.71-3.33-2.48-3.33-1.354 0-2.158.91-2.514 1.802-.13.315-.162.753-.162 1.194v8.216h-4.9s.067-13.35 0-14.73h4.9v2.087c-.01.017-.023.033-.033.05h.032v-.05c.65-1.002 1.812-2.435 4.414-2.435 3.222 0 5.638 2.106 5.638 6.632zM5.348 2.5c-1.676 0-2.772 1.093-2.772 2.54 0 1.42 1.066 2.538 2.717 2.546h.032c1.71 0 2.77-1.132 2.77-2.546C8.056 3.593 7.02 2.5 5.344 2.5h.005zm-2.48 21.834h4.896V9.604H2.867v14.73z"/>
					</symbol>
					
					<symbol id="bete-awesome-svg-email" viewBox="0 0 28 28">
						<path d="M20.11 26.147c-2.335 1.05-4.36 1.4-7.124 1.4C6.524 27.548.84 22.916.84 15.284.84 7.343 6.602.45 15.4.45c6.854 0 11.8 4.7 11.8 11.252 0 5.684-3.193 9.265-7.398 9.3-1.83 0-3.153-.934-3.347-2.997h-.077c-1.208 1.986-2.96 2.997-5.023 2.997-2.532 0-4.36-1.868-4.36-5.062 0-4.75 3.503-9.07 9.11-9.07 1.713 0 3.7.4 4.6.972l-1.17 7.203c-.387 2.298-.115 3.3 1 3.4 1.674 0 3.774-2.102 3.774-6.58 0-5.06-3.27-8.994-9.304-8.994C9.05 2.87 3.83 7.545 3.83 14.97c0 6.5 4.2 10.2 10 10.202 1.987 0 4.09-.43 5.647-1.245l.634 2.22zM16.647 10.1c-.31-.078-.7-.155-1.207-.155-2.572 0-4.596 2.53-4.596 5.53 0 1.5.7 2.4 1.9 2.4 1.44 0 2.96-1.83 3.31-4.088l.592-3.72z"/>
					</symbol>
					
				</svg>';
	};
};
add_action('wp_head', 'echoActionSvgIcon_1');
/*svg icon*/