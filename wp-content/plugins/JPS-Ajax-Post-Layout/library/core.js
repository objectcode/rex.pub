/*
Plugin Name: JPS Ajax Post Layout
Plugin URI: http://beeteam368.com/
Description: Json & Ajax Post Layout, Slider Multi Posts Parallax (Ajax), Carousel Ajax, Masonry Flat, Grid Box, Grid Normal, Grid Masonry ... Best Choice For Building Your Website.
Author: BeeTeam368
Author URI: http://beeteam368.com/
Version: 1.3.8
License: Commercial
*/
;(function($){
	
	/*requestAnimationFrame*/
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}; 
	if (!window.requestAnimationFrame){
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}; 
	if (!window.cancelAnimationFrame){
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	};	
	/*requestAnimationFrame*/
	
	/*check IE & Safari*/
	function checkIeSafari(){
		if(window.navigator.userAgent.indexOf("MSIE ") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) || navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
			return true;					
		}else{
			return false;			
		};
	};
	/*check IE & Safari*/
	
	if($.browser.msie){
		if($.browser.version!=9 && $.browser.version!=10){
			_bt_smoothScroll();
		};
	}else{
		_bt_smoothScroll();
		console.log('smooth');
	}
	
	/*check number*/
	function isNumber(n) {return !isNaN(parseFloat(n)) && isFinite(n);};
	/*check number*/
	
	$.fn.parallaxBackground = function (options){
		var $itemParallax = $(this);
		var param_speed = options.speed;
		
		function setUpdate() {
			
			$itemParallax.each(function(index, element) {				
				var $this=$(this);
				var speed = param_speed;
				var elmsPos = $(window).scrollTop();				
				
				var elmsTop, elmsHeight;
				if(checkIeSafari()){ 
					elmsTop = $this.offset().top;
					elmsHeight = $this.outerHeight();
				}else{
					elmsTop = $this.parents('.bt-background-slider-row').offset().top;
					elmsHeight = $this.parents('.bt-background-slider-row').outerHeight();
				};
				
				var scrollPos = Math.round((elmsTop-elmsPos)*speed);
											
				if(elmsPos >= (elmsTop-window.innerHeight) && elmsPos<(elmsTop+elmsHeight)){
					if(checkIeSafari()) {
						$this.css({"background-position":"50% "+ scrollPos +"px"});
					}else{						
						$this.transition({ 'y':scrollPos+"px"},0);
					};
				};	
			});			
		};
		
		window.addEventListener('scroll', function(){ 
			requestAnimationFrame(setUpdate); 
		}, false);
		$(window).on('resize.parallaxBackground', function(){
			setUpdate();
		});
		
		setTimeout(setUpdate,100);
		
	};
	
	function setParallaxPicItem(elms){
		elms.each(function(index, element) {
			var $this = $(this);
			var optionsParallaxImg = {
				moveTarget: $this,
				sensitivity: 20,
				scale: false,
				scaleOnHover: true,
				scaleTo: 1.1,
				scaleDuration: 0.5,
				panY: true,
				panX: true,
				panDuration: 1.5,
				resetPanOnMouseLeave: false,
			};
			$('.bt-img-parallax', $this).panr(optionsParallaxImg);
			$this.addClass('bt-ready-parallax');
		});
	};	
	
	$.fn.jps_bete_action = function (options){
		var myJsonSlider = [];
		var myJsonSliderChange = [];
		var myMasonryUpdate = [];
		var ajaxLoadMore = [];
		
		$(this).each(function(index, element) {
			var $callBackThis 			= $(this);
			var $this 					= $(this).find('.bt-json-slider');
			
			var $ajaxGetpost 			= $callBackThis.attr('data-server-request');
			var $strStyle 				= $callBackThis.attr('data-style');
			var $paged_calculator 		= $callBackThis.attr('data-max-pages'); if(!isNumber($paged_calculator)){$paged_calculator=0}else{$paged_calculator=parseInt($paged_calculator)};
			var $isEffectInSlider 		= $callBackThis.attr('data-effect-slider');
			var $serverParams 			= $callBackThis.attr('data-server-params');			
			var $textError				= $callBackThis.attr('data-information-err');
			var $lastPostsCount			= $callBackThis.attr('data-last-posts-count');
			var $autoplay				= $callBackThis.attr('data-autoplay');
			var $autoplaySpeed			= $callBackThis.attr('data-autoplay_speed');
			
			function getNewParams(){
				$ajaxGetpost 			= $callBackThis.attr('data-server-request');
				$strStyle 				= $callBackThis.attr('data-style');
				$paged_calculator 		= $callBackThis.attr('data-max-pages'); if(!isNumber($paged_calculator)){$paged_calculator=0}else{$paged_calculator=parseInt($paged_calculator)};
				$isEffectInSlider 		= $callBackThis.attr('data-effect-slider');
				$serverParams 			= $callBackThis.attr('data-server-params');			
				$textError				= $callBackThis.attr('data-information-err');
				$lastPostsCount			= $callBackThis.attr('data-last-posts-count');
				$autoplay				= $callBackThis.attr('data-autoplay');
				$autoplaySpeed			= $callBackThis.attr('data-autoplay_speed');
			};	
			
			var $thisMenuOpen 			= $('.bt-open-category-filter', $callBackThis);
			var $canvasmenu 			= $('.bt-canvas-menu', $callBackThis);
			var $contentTransform 		= $('.bt368-nav-ptt-content, .bt-background-slider-row', $callBackThis);
			var $contentOverlay 		= $('.bt-content-overlay', $callBackThis);
			var $closeCanvasMenu 		= $('.bt-close-menu', $callBackThis);	
			
			var elmsCheckPar = '.bt-is-parallax';			
			if(checkIeSafari()) {
				elmsCheckPar = '.bt-is-parallax.bt368-nav-ptt-content';
			}else{
				elmsCheckPar = '.bt-is-parallax.bt-background-slider';
			};		
						
			function hiddenButton(){
				if($callBackThis.find('.bt-slider-item').length>1){
					$callBackThis.removeClass('bt-no-change');
				}else{
					$callBackThis.addClass('bt-no-change');
				};
			};
			
			hiddenButton();
			
			$callBackThis.attr('data-bete-s-id',index);
			
			ajaxLoadMore[index] = 0;
			
			function checkCarousel(){
				if($strStyle=='bt-carousel-1' || $strStyle=='bt-carousel-2' || $strStyle=='bt-carousel-3' || $strStyle=='bt-carousel-4'){
					return true;
				}else{
					return false;
				};
			};
						
			function checkSlider(){
				if($strStyle=='' || $strStyle=='bt-2row' || $strStyle=='bt-medium' || $strStyle=='bt-small' || $strStyle=='default'){
					return true;
				}else{
					return false;
				};
			}
			
			var optionsSlider = {
				dots: true,
				arrows:true,
				infinite: true,
				speed: 0,
				fade: true,
  				cssEase: 'ease-in-out',
				adaptiveHeight: false,
				autoplay: $autoplay=='yes'?true:false,
				autoplaySpeed: isNumber($autoplaySpeed)?$autoplaySpeed:10000,
				touchThreshold:50,
				accessibility:false,
			};
			if(checkCarousel()){
				optionsSlider = {
					dots: false,
					arrows:true,
					infinite: false,
					speed: 600,
					fade: false,
					cssEase: 'ease-in-out',
					adaptiveHeight: true,
					autoplay: $autoplay=='yes'?true:false,
					autoplaySpeed: isNumber($autoplaySpeed)?$autoplaySpeed:10000,
					touchThreshold:50,
					slidesToShow: 1,					
					accessibility:false,
				};
			};
			
			myJsonSliderChange[index] = 0;
			myJsonSlider[index] = $this;
			
			function setEventSlideAction(nextSlide){
				var maxPages = $paged_calculator;
				if(nextSlide==0) {
					$('.bt-slider-prev span', $callBackThis).text(maxPages+'/'+maxPages);
					if(checkCarousel()){
						$('.bt-slider-prev', $callBackThis).addClass('remove-click');
					};
				}else{
					$('.bt-slider-prev span', $callBackThis).text((nextSlide)+'/'+maxPages);
					if(checkCarousel()){
						$('.bt-slider-prev', $callBackThis).removeClass('remove-click');
					};
				};	
				
				if((nextSlide+1)==parseInt(maxPages)) {
					$('.bt-slider-next span', $callBackThis).text('1/'+maxPages);
					if(checkCarousel()){
						$('.bt-slider-next', $callBackThis).addClass('remove-click');
					};
				}else{
					$('.bt-slider-next span', $callBackThis).text((nextSlide+2)+'/'+maxPages);
					if(checkCarousel()){
						$('.bt-slider-next', $callBackThis).removeClass('remove-click');
					};
				};	
			};		
			
			if(checkCarousel()){
				myJsonSlider[index].on('init', function(event, slick){
					$('.bt-slider-item.slick-slide .bt-slider-item-content', $callBackThis).height($('.bt-slider-item.slick-slide.slick-active', $callBackThis).height());
					setEventSlideAction(0);
				});
			};
			
			myJsonSlider[index].slick(optionsSlider);
			
			/*delay loading*/
			var $delayloading = $callBackThis.attr('data-delay-spinner');
			if(isNumber($delayloading) && checkCarousel()){
				$('<style>.bt368-nav-ptt[id^=jps_bete_][data-bete-s-id="'+index+'"] .bt-loader { opacity:0; transition:opacity 0.2s '+$delayloading+'ms; -webkit-transition:opacity 0.2s '+$delayloading+'ms;}.bt368-nav-ptt[id^=jps_bete_][data-bete-s-id="'+index+'"] .slick-active .bt-loader { opacity:1;}</style>').appendTo('head');				
			};
			/*delay loading*/
			
			function checkEffectSlider(intIndex) {
				
				//var $thisItemCheck = $('.slick-slide:not(.slick-cloned):eq('+intIndex+') .bt-slider-item-content', $callBackThis);				
				//$('.slick-slide .bt-slider-item-content', $callBackThis).removeClass('bt-set-active');
				
				var $thisItemCheck = $('.bt-slider-item:eq('+intIndex+') .bt-slider-item-content', $callBackThis);				
				$('.bt-slider-item .bt-slider-item-content', $callBackThis).removeClass('bt-set-active');
							
				var classRemove = 		'animated '+
										'bounce '+
										'flash '+
										'pulse '+
										'rubberBand '+
										'shake '+
										'swing '+
										'tada '+
										'wobble '+
										'jello '+
										'bounceIn '+
										'bounceInDown '+
										'bounceInLeft '+
										'bounceInRight '+
										'bounceInUp '+
										'bounceOut '+
										'bounceOutDown '+
										'bounceOutLeft '+
										'bounceOutRight '+
										'bounceOutUp '+
										'fadeIn '+
										'fadeInDown '+
										'fadeInDownBig '+
										'fadeInLeft '+
										'fadeInLeftBig '+
										'fadeInRight '+
										'fadeInRightBig '+
										'fadeInUp '+
										'fadeInUpBig '+
										'fadeOut '+
										'fadeOutDown '+
										'fadeOutDownBig '+
										'fadeOutLeft '+
										'fadeOutLeftBig '+
										'fadeOutRight '+
										'fadeOutRightBig '+
										'fadeOutUp '+
										'fadeOutUpBig '+
										'flipInX '+
										'flipInY '+
										'flipOutX '+
										'flipOutY '+
										'lightSpeedIn '+
										'lightSpeedOut '+
										'rotateIn '+
										'rotateInDownLeft '+
										'rotateInDownRight '+
										'rotateInUpLeft '+
										'rotateInUpRight '+
										'rotateOut '+
										'rotateOutDownLeft '+
										'rotateOutDownRight '+
										'rotateOutUpLeft '+
										'rotateOutUpRight '+
										'hinge '+
										'rollIn '+
										'rollOut '+
										'zoomIn '+
										'zoomInDown '+
										'zoomInLeft '+
										'zoomInRight '+
										'zoomInUp '+
										'zoomOut '+
										'zoomOutDown '+
										'zoomOutLeft '+
										'zoomOutRight '+
										'zoomOutUp '+
										'slideInDown '+
										'slideInLeft '+
										'slideInRight '+
										'slideInUp '+
										'slideOutDown '+
										'slideOutLeft '+
										'slideOutRight '+
										'slideOutUp';
							
				var arrayAnimate = 	[	'bounce', 
										'flash', 
										'pulse', 
										'rubberBand', 
										'shake', 
										'swing', 
										'tada', 
										'wobble', 
										'jello', 
										'bounceIn', 
										'bounceInDown', 
										'bounceInLeft', 
										'bounceInRight', 
										'bounceInUp', 
										'fadeIn', 
										'fadeInDown', 
										'fadeInDownBig', 
										'fadeInLeft', 
										'fadeInLeftBig', 
										'fadeInRight', 
										'fadeInRightBig', 
										'fadeInUp',
										'fadeInUpBig',
										'flipInX',
										'flipInY',
										'lightSpeedIn',
										'rotateIn',
										'rotateInDownLeft',
										'rotateInDownRight',
										'rotateInUpLeft',
										'rotateInUpRight',
										'rollIn',
										'zoomIn',
										'zoomInDown',
										'zoomInLeft',
										'zoomInRight',
										'zoomInUp',
										'slideInDown',
										'slideInLeft',
										'slideInRight',
										'slideInUp'
									];
				
				$('.bt-primary-content, .bt-sub-content', $thisItemCheck).removeClass(classRemove);
								
				var randAnimate1 			= arrayAnimate[Math.floor(Math.random() * arrayAnimate.length)];
				var randAnimate2		 	= arrayAnimate[Math.floor(Math.random() * arrayAnimate.length)];				
				var thisBackground 			= $thisItemCheck.attr('data-img-bg');
				var thisBackgroundWidth		= $thisItemCheck.attr('data-width');
				var thisBackgroundHeight	= $thisItemCheck.attr('data-height');
				
				var $primaryContent 		= $('.bt-primary-content:not(.bt-no-effect)', $thisItemCheck);
				var $subContent				= $('.bt-sub-content:not(.bt-no-effect)', $thisItemCheck);
				var $postItem				= $('.bt-post-item:not(.bt-ready-load):not(.bt-no-effect)', $thisItemCheck);
				var $postItemOdd			= $('.bt-post-item:not(.bt-ready-load):not(.bt-no-effect):nth-child(odd)', $thisItemCheck);
				var $postItemEven			= $('.bt-post-item:not(.bt-ready-load):not(.bt-no-effect):nth-child(even)', $thisItemCheck);
				
				function setEffect(){
					if($isEffectInSlider!='' && $isEffectInSlider!=null && typeof($isEffectInSlider)!='undefined' && $isEffectInSlider=='rand') { //nosync					
						$primaryContent.addClass('animated '+randAnimate1);
						$subContent.addClass('animated '+randAnimate2);						
						$postItemOdd.addClass('bt-ready-load animated '+randAnimate1);
						$postItemEven.addClass('bt-ready-load animated '+randAnimate2);
						
						setTimeout(function(){
							$primaryContent.removeClass('animated '+randAnimate1);
							$subContent.removeClass('animated '+randAnimate2);
							$postItemOdd.removeClass('animated '+randAnimate1);
							$postItemEven.removeClass('animated '+randAnimate2);
						},2000);
					};
					
					if($isEffectInSlider!='' && $isEffectInSlider!=null && typeof($isEffectInSlider)!='undefined' && $isEffectInSlider=='randsync') { //sync					
						$primaryContent.addClass('animated '+randAnimate1);	
						$subContent.addClass('animated '+randAnimate1);						
						$postItem.addClass('bt-ready-load animated '+randAnimate1);
						
						setTimeout(function(){
							$primaryContent.removeClass('animated '+randAnimate1);
							$subContent.removeClass('animated '+randAnimate1);
							$postItem.removeClass('animated '+randAnimate1);
						},2000);						
					};
					
					if($isEffectInSlider!='' && $isEffectInSlider!=null && typeof($isEffectInSlider)!='undefined' && isNumber($isEffectInSlider)) {
						if(parseInt($isEffectInSlider)<arrayAnimate.length) {
							
							$primaryContent.addClass('animated '+arrayAnimate[$isEffectInSlider]);
							$subContent.addClass('animated '+arrayAnimate[$isEffectInSlider]);
							$postItem.addClass('bt-ready-load animated '+arrayAnimate[$isEffectInSlider]);
														
							setTimeout(function(){
								$primaryContent.removeClass('animated '+arrayAnimate[$isEffectInSlider]);
								$subContent.removeClass('animated '+arrayAnimate[$isEffectInSlider]);
								$postItem.removeClass('animated '+arrayAnimate[$isEffectInSlider]);
							},2000);
						};
					};
				};
				
				if(thisBackground!='' && thisBackground!=null && typeof(thisBackground)!='undefined'){
										
					var $id='bt-video-control-'+index;
					var $fileType = $thisItemCheck.attr('data-type-background');
					
					if($fileType=='mp4' || $fileType=='ogv' || $fileType=='webm'){
						
						$('video#'+$id).remove();
						
						if(checkIeSafari() || !$('.bt-background-slider', $callBackThis).hasClass('bt-is-parallax')){
							$('.bt368-nav-ptt-content', $callBackThis).css({'background-image':''});
						}else{
							$('.bt-background-slider', $callBackThis).css({'background-image':''});
						};	
						
						var stringHTMLVideo  = 	'<video id="'+$id+'" preload="auto" autoplay loop>';
							stringHTMLVideo += 		'<source src="'+thisBackground+'" type="video/mp4">';
							stringHTMLVideo += 		'<source src="'+thisBackground+'" type="video/webm">';
							stringHTMLVideo += 		'<source src="'+thisBackground+'" type="video/ogg">';
							stringHTMLVideo += 	'</video>';
												
						$('.bt-background-slider', $callBackThis).prepend(stringHTMLVideo);
						
						var myVideoPlay = document.getElementById($id);						
						myVideoPlay.play();	
											
						$thisItemCheck.addClass('bt-set-active');
						setEffect();
					}else{
						$('video#'+$id).remove();
						
						if(checkIeSafari() || !$('.bt-background-slider', $callBackThis).hasClass('bt-is-parallax')){
							$('.bt368-nav-ptt-content', $callBackThis).css({'background-image':'url(' + thisBackground + ')'});
						}else{																
							$('.bt-background-slider', $callBackThis).css({'background-image':'url(' + thisBackground + ')'}).attr({'data-width':thisBackgroundWidth, 'data-height':thisBackgroundHeight});							
						};
											
						$('<img src="'+thisBackground+'">').load(function(){														
							$thisItemCheck.addClass('bt-set-active');												
							setEffect();
						});
					};
					
				}else{
					$thisItemCheck.addClass('bt-set-active');					
					setEffect();
				};
			};				
			checkEffectSlider(0);
			
			/*Masonry*/
			var elmsMasonry = $('.bt-is-masonry', $callBackThis);
			
			function setMasonry(elms){	
				if(elms.hasClass('bt-is-isotope')) {
									
				}else{
					if(!elmsMasonry.parents('.bt368-nav-ptt').hasClass('bt-combo-list-ahead')) {
						myMasonryUpdate[index]=elms.masonry({
							itemSelector: '.bt-post-item',
							transitionDuration: 0,
						});
					};
				};
			};
			/*Masonry*/
			
			function getSliderItemAjax(ajax_paged, category, tag, itemsLastPage){
				
				if($('[data-slick-index="'+ajax_paged+'"] .bt-slider-item-content', $callBackThis).attr('data-html')=='wait'){					
					
					if(ajax_paged < ($('.slick-track .slick-slide', $callBackThis).length-1)) {
						itemsLastPage='';						
					};					
					
					getNewParams();
									
					var $paramsRequest = {'p_default':$serverParams, 'paged':(ajax_paged+1), 'category':category, 'tag':tag, 'itemsLastPage':itemsLastPage, action:'jpsbeteajaxaction'};
					
					$.ajax({
						url:		$ajaxGetpost,						
						type: 		'POST',
						data:		$paramsRequest,
						dataType: 	'html',
						cache:		false,
						success: 	function(data){
							if(data=='0' || data==''){
								$('[data-slick-index="'+ajax_paged+'"] .bt-slider-item-content', $callBackThis).html('<div class="bt-information-err bt-h3">'+$textError+'</div>');
								checkEffectSlider(ajax_paged);
							}else{								
								
								if(checkCarousel()){
									$('[data-slick-index="'+ajax_paged+'"]', $callBackThis).html(data);	
									//imagesLoaded( $('[data-slick-index="'+ajax_paged+'"]', $callBackThis), function( instance ) {
										$('.slick-list', $callBackThis).height($('.bt-slider-item.slick-slide.slick-active', $callBackThis).height());
										$('.bt-slider-item.slick-slide .bt-slider-item-content', $callBackThis).height($('.bt-slider-item.slick-slide.slick-active', $callBackThis).height());
										setTimeout(function(){																				
											checkEffectSlider(ajax_paged);									
										},368);
									//});
								}else{
									$('[data-slick-index="'+ajax_paged+'"]', $callBackThis).html(data);								
									setTimeout(function(){
										$('[data-slick-index="'+ajax_paged+'"] .bt-scroll-bar', $callBackThis).mCustomScrollbar({theme:'rounded',});									
										checkEffectSlider(ajax_paged);									
									},368);
								};
																
							};
						},
						error:		function(){
							$('[data-slick-index="'+ajax_paged+'"] .bt-slider-item-content', $callBackThis).html('<div class="bt-information-err bt-h3">'+$textError+'</div>');
							checkEffectSlider(ajax_paged);
						},
					});
					
				}else{
					checkEffectSlider(ajax_paged);
				};
			};
			
			function getPostsAjax(ajax_paged, category, tag, itemsLastPage){
				
				getNewParams();
				
				ajax_paged = parseInt(ajax_paged);
				var $containerHTML = $('.bt-ajax-listing', $callBackThis);
				if($paged_calculator >= ajax_paged){					
					
					if(ajax_paged < $paged_calculator) {
						itemsLastPage='';						
					};
									
					var $paramsRequest = {'p_default':$serverParams, 'paged':(ajax_paged), 'category':category, 'tag':tag, 'itemsLastPage':itemsLastPage, action:'jpsbeteajaxaction'};
					
					$.ajax({
						url:		$ajaxGetpost,						
						type: 		'POST',
						data:		$paramsRequest,
						dataType: 	'html',
						cache:		false,
						success: 	function(data){
							if(data=='0' || data==''){
								$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();	
								ajaxLoadMore[index] = 1;							
							}else{										
								var $containerImgLoad = $('.bt-ajax-imagesload', $callBackThis);						
								if($containerHTML.hasClass('bt-is-masonry')) {									
									function loaderFinish(){
										function setCompleteTransition(){
											console.log('Load Complete');											
											$containerImgLoad.html('');											
											$('.bt-post-item:not(.bt-ready-parallax).bt-opacity-loading, .bt-check-post-normal', $callBackThis).removeClass('bt-opacity-loading');
											checkEffectSlider(0);
											setParallaxPicItem($('.bt-post-item:not(.bt-ready-parallax)', $callBackThis));											
											ajaxLoadMore[index] = 0;
											
											if(ajax_paged==$paged_calculator) {									
												$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();									
											}else{
												$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active');									
											};
										};
										if(elmsMasonry.hasClass('bt-is-isotope')) {
										}else{	
											if(!$callBackThis.hasClass('bt-combo-list-ahead')) {										
												myMasonryUpdate[index]
												.append($containerImgLoad.html())
												.masonry('appended', $containerImgLoad.html())
												.masonry('reloadItems')
												.masonry('layout');
											}else{
												$(data).appendTo($containerHTML);
											};
											
											setCompleteTransition();	
										};
									};
																		
									$containerImgLoad
									.html(data)
									.imagesLoaded()
									.done( function( instance ) {										
										loaderFinish();
									})
									.fail( function() {
										loaderFinish();
									});
																		
								}else{
									function loaderNotMasonryFinish(){
										$(data).appendTo($containerHTML);
										$containerImgLoad.html('');
										checkEffectSlider(0);
										setParallaxPicItem($('.bt-post-item:not(.bt-ready-parallax)', $callBackThis));
										ajaxLoadMore[index] = 0;
										
										if(ajax_paged==$paged_calculator) {									
											$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();									
										}else{
											$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active');									
										};
									};
									$containerImgLoad
									.html(data)
									.imagesLoaded()
									.done( function( instance ) {
										loaderNotMasonryFinish();
									})
									.fail( function() {
										loaderNotMasonryFinish();
									});								
								};								
								
								$('.bt-load-more, .bt-infinite-data', $callBackThis).attr('data-paged', ajax_paged);
																							
							};
						},
						error:		function(){
							$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active');
							ajaxLoadMore[index] = 0;	
						},
					});
					
				}else{
					$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();
					ajaxLoadMore[index] = 1;
				};
			};
			
			function setPageNextPrev() {			
				$('.bt-slider-prev span', $callBackThis).text($paged_calculator+'/'+$paged_calculator);
				$('.bt-slider-next span', $callBackThis).text(2+'/'+$paged_calculator);
			};			
			setPageNextPrev();
			
			$('.bt-load-more', $callBackThis).live('click', function(){	
				if(ajaxLoadMore[index]==0) {			
					var $this = $(this);
					var intCurrentPage = $(this).attr('data-paged');
					if(!isNumber(intCurrentPage)) { return false;}
					$this.addClass('bt-set-active');
					ajaxLoadMore[index] = 1;
					getPostsAjax((parseInt(intCurrentPage)+1), '', '', $lastPostsCount);
				};
			});
			
			function loadmoreScrollActionImprove() {
				if(ajaxLoadMore[index]==0) {															
					if($('.bt-load-more-control', $callBackThis).hasClass('bt-infinite-ajax')){
						var $this = $('.bt-load-more-control.bt-infinite-ajax', $callBackThis);
						
						var ajaxVisible = $this.offset().top;
						var ajaxScrollTop = $(window).scrollTop()+$(window).height();
						
						if(ajaxVisible <= (ajaxScrollTop) && (ajaxVisible + $(window).height()) > ajaxScrollTop){
							ajaxLoadMore[index] = 1;
							$this.find('.bt-infinite-data').addClass('bt-set-active');
							var intCurrentPage = $this.find('.bt-infinite-data').attr('data-paged');
							if(!isNumber(intCurrentPage)) { return false;}
							
							getPostsAjax((parseInt(intCurrentPage)+1), '', '', $lastPostsCount);
						};						
						
					};
				};
			};
			
			$(window).load(function(){
				setMasonry(elmsMasonry);
				loadmoreScrollActionImprove();
				$(window).bind('scroll', function(){
					loadmoreScrollActionImprove();
				});
			});
			
			myJsonSlider[index].on('beforeChange', function(event, slick, currentSlide, nextSlide){	
			
				if(myJsonSliderChange[index]==0) {			
					getSliderItemAjax(nextSlide, '', '', $lastPostsCount);	
				};
				
				setEventSlideAction(nextSlide);
			});
			
			$('.bt-slider-prev', $callBackThis)
			.click(function(){				
				myJsonSlider[index].slick('slickPrev');						
			})
			.hover(
				function(){
					if($autoplay=='yes'){
						myJsonSlider[index].slick('slickPause');
					};
				},
				function(){
					if(!$('.bt-open-category-filter', $callBackThis).hasClass('bt-set-active')){
						if($autoplay=='yes'){
							myJsonSlider[index].slick('slickPlay');
						};
					};
				}//
			);	
			
			$('.bt-slider-next', $callBackThis)
			.click(function(){				
				myJsonSlider[index].slick('slickNext');						
			})
			.hover(
				function(){
					if($autoplay=='yes'){
						myJsonSlider[index].slick('slickPause');
					};
				},
				function(){
					if(!$('.bt-open-category-filter', $callBackThis).hasClass('bt-set-active')){
						if($autoplay=='yes'){
							myJsonSlider[index].slick('slickPlay');
						};
					};
				}//
			);
			
			function jsonGetDataFilter(filterCat, filterTag){	
			
				myJsonSliderChange[index]==1;	
						
				$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).addClass('bt-loading-now');
				
				if($autoplay=='yes'){
					myJsonSlider[index].slick('slickPause');
				};				
							
				$('.slick-slide.slick-active', $callBackThis).addClass('setNewJson').removeClass('bt-set-active');
				myJsonSlider[index].slick('unslick');
				
				$('.bt-slider-item:not(.setNewJson)', $callBackThis).remove();
				$('.bt-slider-item.setNewJson .bt-slider-item-content > *:not(.bt-animate-thumb-overlay)', $callBackThis).remove();	
				$('.bt-slider-item.setNewJson .bt-slider-item-content', $callBackThis).attr('data-img-bg','').removeClass('bt-set-active');
				$('.bt368-nav-ptt-content', $callBackThis).attr('style','');
				$('.bt-slider-item', $callBackThis).removeClass('setNewJson');			
				
				getNewParams();							
				var $paramsRequest = {'p_default':$serverParams, 'paged':1, 'category':filterCat, 'tag':filterTag, 'isJson':'yes', action:'jpsbeteajaxaction'};
				
				function resetContru() {
					$thisMenuOpen.removeClass('bt-set-active');
					$canvasmenu.removeClass('bt-set-active');
					$contentTransform.removeClass('bt-trans-close').removeAttr('style');
					$contentOverlay.removeClass('bt-set-active');
					$callBackThis.removeAttr('style');	
				};
				
				$.ajax({
					url:		$ajaxGetpost,						
					type: 		'POST',
					data:		$paramsRequest,
					dataType: 	'json',
					cache:		false,
					success: 	function(data){
						if(data=='0' || data=='' || data.success=='0'){
							myJsonSlider[index] = $this.slick(optionsSlider);																
							$('[data-slick-index="0"] .bt-slider-item-content', $callBackThis).html('<div class="bt-information-err bt-h3">'+$textError+'</div>');
							
							$('.bt-slider-prev span', $callBackThis).text('');
							$('.bt-slider-next span', $callBackThis).text('');							
							
							resetContru();
																
							$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
							myJsonSliderChange[index]==0;	
														
						}else{
							
							$callBackThis.css({'max-height':$callBackThis.outerHeight(), 'overflow':'hidden'});
							$('.bt368-nav-ptt-content', $callBackThis).css({'max-height':$callBackThis.height(), 'overflow':'hidden'});
														
							var appendSlideHTML ='';
							for(var loopSlides=2;loopSlides<=parseInt(data.data_max_pages);loopSlides++){								
								appendSlideHTML+='<div class="bt-slider-item">';
								if(isNumber(data.data_server_height)){
									appendSlideHTML+='<div class="bt-slider-item-content" style="height:'+data.data_server_height+'px;" data-html="wait">';
								}else{
									appendSlideHTML+='<div class="bt-slider-item-content" data-html="wait">';
								}
								appendSlideHTML+='<div class="bt-animate-thumb-overlay '+(checkCarousel()?'bt-white-div':'')+'"><div class="bt-loader"></div></div></div></div>';																
							};
							$(appendSlideHTML).appendTo($('.bt-json-slider', $callBackThis));
																	
							$callBackThis.attr('data-server-params', data.data_server_params);
							$callBackThis.attr('data-max-pages', data.data_max_pages);
							$callBackThis.attr('data-last-posts-count', data.data_last_posts_count);
							
							var $newParamsRequest = {'p_default':data.data_server_params, 'paged':1, 'category':'', 'tag':'', 'itemsLastPage':'', 'isJsonFirst':'yes', action:'jpsbeteajaxaction'};
							
							$.ajax({
								url:		$ajaxGetpost,						
								type: 		'POST',
								data:		$newParamsRequest,
								dataType: 	'html',
								cache:		false,
								success: 	function(data){
									if(data=='0' || data==''){
										
									}else{
											
										myJsonSlider[index] = $this.slick(optionsSlider);
										
										function setDataNewJson(){
											
											getNewParams();
											setPageNextPrev();
											
											resetContru();											
																				
											$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
											if(checkCarousel()){												
												$('.bt-filter-item-sub-list .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
												$('.bt-json-carousel', $callBackThis).removeClass('bt-set-active');
												setEventSlideAction(0);
												setTimeout(function(){
													$('.bt-filter-item-default', $callBackThis).removeClass('bt-set-active');													
												},368);
											};
											
											hiddenButton();											
											
											checkEffectSlider(0);
											myJsonSliderChange[index]==0;
											
											if($autoplay=='yes'){
												myJsonSlider[index].slick('slickPlay');
											};
										};
										
										$('[data-slick-index="0"]', $callBackThis).html(data);
										var ccThisBackground 			= $('[data-slick-index="0"] .bt-slider-item-content', $callBackThis).attr('data-img-bg');
										if(ccThisBackground!='' && ccThisBackground!=null && typeof(ccThisBackground)!='undefined'){
											$('<img src="'+ccThisBackground+'">').load(function(){														
												setDataNewJson();
												$('[data-slick-index="0"] .bt-scroll-bar', $callBackThis).mCustomScrollbar({theme:'rounded',});
											});
										}else{
											setDataNewJson();
											$('[data-slick-index="0"] .bt-scroll-bar', $callBackThis).mCustomScrollbar({theme:'rounded',});
										};
																				
									};
								},
								error:		function(){
									
								},
							});
							
						};
					},
					error:		function(){
						
						myJsonSlider[index] = $this.slick(optionsSlider);																
						$('[data-slick-index="0"] .bt-slider-item-content', $callBackThis).html('<div class="bt-information-err bt-h3">'+$textError+'</div>');
						
						$('.bt-slider-prev span', $callBackThis).text('');
						$('.bt-slider-next span', $callBackThis).text('');
						
						resetContru();
															
						$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
						myJsonSliderChange[index]==0;
					},
				});
				
			};
			
			function jsonGetDataFilterPosts(filterCat, filterTag){
						
				$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).addClass('bt-loading-now');
				//$('.bt-slider-item .bt-slider-item-content', $callBackThis).removeClass('bt-set-active');
				
				getNewParams();							
				var $paramsRequest = {'p_default':$serverParams, 'paged':1, 'category':filterCat, 'tag':filterTag, 'isJson':'yes', action:'jpsbeteajaxaction'};
				
				function resetContru() {
					$thisMenuOpen.removeClass('bt-set-active');
					$canvasmenu.removeClass('bt-set-active');
					$contentTransform.removeClass('bt-trans-close').removeAttr('style');
					$contentOverlay.removeClass('bt-set-active');
					$callBackThis.removeAttr('style');
					$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active').removeAttr('style');
					ajaxLoadMore[index] = 0;	
				};
				
				$.ajax({
					url:		$ajaxGetpost,						
					type: 		'POST',
					data:		$paramsRequest,
					dataType: 	'json',
					cache:		false,
					success: 	function(data){
						if(data=='0' || data=='' || data.success=='0'){
							resetContru();																
							$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
						}else{
							
							$callBackThis.css({'max-height':$callBackThis.outerHeight(), 'overflow':'hidden'});
							$('.bt368-nav-ptt-content', $callBackThis).css({'max-height':$callBackThis.height(), 'overflow':'hidden'});
																	
							$callBackThis.attr('data-server-params', data.data_server_params);
							$callBackThis.attr('data-max-pages', data.data_max_pages);
							$callBackThis.attr('data-last-posts-count', data.data_last_posts_count);
							
							var $containerHTML = $('.bt-ajax-listing', $callBackThis);
							
							var $newParamsRequest = {'p_default':data.data_server_params, 'paged':1, 'category':'', 'tag':'', 'itemsLastPage':'', 'isJsonFirst':'yes', action:'jpsbeteajaxaction'};
							
							$.ajax({
								url:		$ajaxGetpost,						
								type: 		'POST',
								data:		$newParamsRequest,
								dataType: 	'html',
								cache:		false,
								success: 	function(data){
									if(data=='0' || data==''){
										resetContru();															
										$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
									}else{
										
										function setDataNewJson(){											
											getNewParams();																						
											resetContru();					
											$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
											checkEffectSlider(0);											
										};
										
										var $containerImgLoad = $('.bt-ajax-imagesload', $callBackThis);
										if($containerHTML.hasClass('bt-is-masonry')) {	
											function loaderFinish(){
												$containerHTML.html(data);
												$containerImgLoad.html('');
												function setCompleteTransition(){
													console.log('Load Complete Json');											
													$('.bt-post-item:not(.bt-ready-parallax).bt-opacity-loading, .bt-check-post-normal', $callBackThis).removeClass('bt-opacity-loading');
													setDataNewJson();
													setParallaxPicItem($('.bt-post-item:not(.bt-ready-parallax)', $callBackThis));											
													
													if(1==$paged_calculator) {
														$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();
													}else{
														$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active').removeAttr('style');
													};	
												};	
												if(elmsMasonry.hasClass('bt-is-isotope')) {
												}else{
													if(!$callBackThis.hasClass('bt-combo-list-ahead')) {
														myMasonryUpdate[index]
														.masonry('reloadItems')
														.masonry('layout');
													};
													setCompleteTransition();	
												};				
											};
																				
											$containerImgLoad
											.html(data)
											.imagesLoaded()
											.done( function( instance ) {
												loaderFinish();
											})
											.fail( function() {
												loaderFinish();
											});	
										}else{
											function loaderNotMasonryFinish(){
												$containerHTML.html(data);
												$containerImgLoad.html('');
												setDataNewJson();
												setParallaxPicItem($('.bt-post-item:not(.bt-ready-parallax)', $callBackThis));	
												
												if(1==$paged_calculator) {
													$('.bt-load-more, .bt-infinite-data', $callBackThis).hide();
												}else{
													$('.bt-load-more, .bt-infinite-data', $callBackThis).removeClass('bt-set-active').removeAttr('style');
												};		
											};
											
											$containerImgLoad
											.html(data)
											.imagesLoaded()
											.done( function( instance ) {
												loaderNotMasonryFinish();
											})
											.fail( function() {
												loaderNotMasonryFinish();
											});	
										};
										
										$('.bt-load-more, .bt-infinite-data', $callBackThis).attr('data-paged', 1);										
									};
								},
								error:		function(){
									resetContru();															
									$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');
								},
							});
							
						};
					},
					error:		function(){	
						resetContru();															
						$('.bt-canvas-menu .bt-animate-thumb-overlay', $callBackThis).removeClass('bt-loading-now');						
					},
				});
				
			};
			
			$('.bt-filter-item[data-filter-pos="category"]', $callBackThis).click(function(){
				var $this = $(this);
				$('.bt-filter-item[data-filter-pos="category"]', $callBackThis).removeClass('bt-set-active');
				$this.addClass('bt-set-active');
				if(checkCarousel()){
					$this.parents('.bt-filter-style-top').find('.bt-filter-item-default').text($this.text());
					$('.bt-filter-item-sub-list .bt-animate-thumb-overlay', $callBackThis).addClass('bt-loading-now');
					$('.bt-json-carousel', $callBackThis).addClass('bt-set-active');
					$callBackThis.addClass('bt-no-change');
				};
				var $strTopLoading = Math.round($this.offset().top - $callBackThis.offset().top + 30);
				$('.bt-canvas-menu .bt-animate-thumb-overlay .bt-loader', $callBackThis).css({'margin-top':'0px', 'top':$strTopLoading+'px'});
				if(!checkSlider() && !checkCarousel()){					
					jsonGetDataFilterPosts($this.attr('data-filter-id'), '');				
				}else{
					jsonGetDataFilter($this.attr('data-filter-id'), '');
				};
			});
			
			$(elmsCheckPar, $callBackThis).parallaxBackground({speed:0.32});
			
			$('.bt-scroll-bar', $callBackThis).mCustomScrollbar({theme:'rounded',});
			$('.bt-filter-all', $callBackThis).mCustomScrollbar({theme:'minimal',});
			
			$('.bt-scroll-down-more', $callBackThis).click(function(){
				var $this = $(this);
				$('html, body').animate({scrollTop:($callBackThis.offset().top + $callBackThis.outerHeight())}, 1000);			
			});	
			
			$('.bt-open-category-filter', $callBackThis).click(function(){
				var $this = $(this);
				
				if($this.hasClass('bt-set-active')){
					$this.removeClass('bt-set-active');
					$canvasmenu.removeClass('bt-set-active');
					$contentTransform.removeClass('bt-trans-close');
					$contentOverlay.removeClass('bt-set-active');
					if($autoplay=='yes') {
						myJsonSlider[index].slick('slickPlay');
					};
					
					ajaxLoadMore[index] = 0;
				}else{
					$this.addClass('bt-set-active');
					$canvasmenu.addClass('bt-set-active');
					$contentTransform.addClass('bt-trans-close');
					$contentOverlay.addClass('bt-set-active');
					if($autoplay=='yes') {
						myJsonSlider[index].slick('slickPause');
					};
					
					ajaxLoadMore[index] = 1;
				};
				
				$closeCanvasMenu.on('click.setCloseClass', function(){
					$this.removeClass('bt-set-active');
					$canvasmenu.removeClass('bt-set-active');
					$contentTransform.removeClass('bt-trans-close');
					$contentOverlay.removeClass('bt-set-active');
					$closeCanvasMenu.off('.setCloseClass');				
					if($autoplay=='yes') {				
						myJsonSlider[index].slick('slickPlay');
					};
					
					ajaxLoadMore[index] = 0;
				});
			});	
			
			$('.bt-filter-item-default', $callBackThis).click(function(){
				var $this = $(this);
				if($this.hasClass('bt-set-active')) {
					$this.removeClass('bt-set-active');
					if($autoplay=='yes') {
						myJsonSlider[index].slick('slickPlay');
					};
				}else{
					$this.addClass('bt-set-active');
					if($autoplay=='yes') {
						myJsonSlider[index].slick('slickPause');
					};
				};
			});	
			
			var $social_facebook = $callBackThis.attr('data-social-facebook');
			var $social_twitter = $callBackThis.attr('data-social-twitter');
			var $social_google_plus = $callBackThis.attr('data-social-google-plus');
			var $social_social_pinterest = $callBackThis.attr('data-social-pinterest');
			var $social_social_tumblr = $callBackThis.attr('data-social-tumblr');
			var $social_social_linkedin = $callBackThis.attr('data-social-linkedin');
			var $social_social_email = $callBackThis.attr('data-social-email');	
			
			$('.open-full-post', $callBackThis).live('click', function(){
				var $this = $(this);
				if($('#bt-lightbox-post').length==0) {
					$('<div id="bt-lightbox-post"><div class="bt-loader"></div><div id="bt-lightbox-close"></div></div><div id="bt-post-ajax-content" class="bt-white-div"><div id="jps_bete_style" class="bt-entry-content '+$this.parents('.bt368-nav-ptt').attr('data-custom-css')+' bt368-nav-ptt"></div></div>').appendTo('body');				
				};
				$('#bt-lightbox-post').addClass('bt-set-active');
				
				function removeElms(){
					$('#bt-lightbox-post, #bt-lightbox-close, #bt-post-ajax-content').removeClass('bt-set-active').off('.btCloseLightBox');			
					setTimeout(function(){
						$('#bt-lightbox-post, #bt-post-ajax-content').remove();
					}, 400);
				};
				
				$('#bt-lightbox-post, #bt-lightbox-close').on('click.btCloseLightBox',function(){
					removeElms();
				});
				
				$.ajax({
					url:		$ajaxGetpost,						
					type: 		'POST',
					data:		{'post_id':$this.attr('data-id'), 'post_type':$this.attr('data-post-type'), 'taxonomy':$this.attr('data-taxonomy'), 'tc_eventbuild':$this.attr('data-tc-eventbuild'), 'tc_eventbuild_starts':$this.attr('data-tc-eventbuild-starts'), 'tc_eventbuild_ends':$this.attr('data-tc-eventbuild-ends'), 'social_facebook':$social_facebook, 'social_twitter':$social_twitter, 'social_google_plus':$social_google_plus, 'social_pinterest':$social_social_pinterest, 'social_tumblr':$social_social_tumblr, 'social_linkedin':$social_social_linkedin, 'social_email':$social_social_email, action:'jpsbetepostaction'},
					dataType: 	'html',
					cache:		false,
					success: 	function(data){
						if(data=='0' || data==''){
							removeElms();
						}else{	
							$('#bt-post-ajax-content .bt-entry-content').html(data);
							$('#bt-post-ajax-content').mCustomScrollbar({theme:'minimal-dark',});						
							$('#bt-post-ajax-content').addClass('bt-set-active');
						};
					},
					error:		function(){
						removeElms();
					},
				});
				
				return false;
			});	
			
			if($strStyle=='' || $strStyle=='bt-2row' || $strStyle==null || typeof($strStyle)=='undefined' || $strStyle=='default'){
				$('.slick-dots li button', $callBackThis).live({
					mouseenter: function () {
						var $this = $(this);
						var $this_parent = $this.parents('li');
						var $paged_text = $this.text();
						
						if($('.bt-thumb-slider-item', $this_parent).length==0){
							$this_parent.append('<div class="bt-thumb-slider-item"><div class="bt-thumb-slider-item-content"><div class="bt-loader"></div></div></div>');
							
							getNewParams();							
							var $paramsRequest = {'p_default':$serverParams, 'paged':$paged_text, 'isJsonThumb':'yes', action:'jpsbeteajaxaction'};
								
							$.ajax({
								url:		$ajaxGetpost,						
								type: 		'POST',
								data:		$paramsRequest,
								dataType: 	'json',
								cache:		false,
								success: 	function(data){	
									var d = new Date();
									$('<img src="'+data.data_thumb_img+'?'+d.getTime()+'">').load(function(){														
										$('.bt-thumb-slider-item .bt-thumb-slider-item-content', $this_parent).append('<img src="'+data.data_thumb_img+'?'+d.getTime()+'">');
										//console.log(data.data_thumb_img+'?'+d.getTime());
									});
								},
							});
						}else{
							$('.bt-thumb-slider-item .bt-thumb-slider-item-content', $this_parent).find('.bt-loader').remove();
							$('.bt-thumb-slider-item .bt-thumb-slider-item-content', $this_parent).prepend('<div class="bt-loader"></div>');
						};
						
						$('.slick-dots li .bt-thumb-slider-item', $callBackThis).removeClass('bt-set-active');
						$('.slick-dots li button', $callBackThis).removeClass('bt-set-active');
						$('.bt-thumb-slider-item', $this_parent).addClass('bt-set-active');
						$this.addClass('bt-set-active');
					},
					mouseleave: function () {
						var $this = $(this);
						var $this_parent = $this.parents('li');
						$('.bt-thumb-slider-item', $this_parent).removeClass('bt-set-active');
						$this.removeClass('bt-set-active');
					}
				});
			};
			
			$('.bt-switch-grid, .bt-switch-list', $callBackThis).live('click', function(){
				var $this = $(this);
				if($this.hasClass('.bt-set-active')) { return false;};
				$('.bt-switch-grid, .bt-switch-list', $callBackThis).removeClass('bt-set-active');
				$this.addClass('bt-set-active');
				var $setStyle = $this.attr('data-set-style');
				if($this.attr('data-set-style')=='bt-combo-list-ahead') {
					$callBackThis.addClass('bt-combo-list-ahead');
					$.cookie('btSettingListAhead', '1', { path: '/' });
					if($('.bt-ajax-listing', $callBackThis).length>0) {
						if($('.bt-ajax-listing', $callBackThis).hasClass('bt-is-masonry')) {
							myMasonryUpdate[index].masonry('destroy');							
						};
					};
				}else{
					$callBackThis.removeClass('bt-combo-list-ahead');	
					$.cookie('btSettingListAhead', '0', { path: '/' });				
					if($('.bt-ajax-listing', $callBackThis).length>0) {
						if($('.bt-ajax-listing', $callBackThis).hasClass('bt-is-masonry')) {
							setMasonry(elmsMasonry);
						};
					};					
				};				
				
			});
				
			
		});
	};
	
	$(document).ready(function(e) {
		if(typeof _gsScope == "undefined" || typeof _gsQueue=="undefined") {
			__bt_run_tweenMax();
			console.log('run');
		};
        $('.bt368-nav-ptt').jps_bete_action({});
		setParallaxPicItem($('.bt-post-item:not(.bt-ready-parallax)'));		
    });	
}(jQuery));