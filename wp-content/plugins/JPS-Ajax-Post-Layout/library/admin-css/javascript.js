var vcGridFilterExcludeCallBack_bt_custom = function () {
	var $ = jQuery, $filterBy, $exclude, autocomplete, defaultValue;
	$filterBy = $( '.wpb_vc_param_value[name="filter_options"]', this.$content );
	defaultValue = $filterBy.val();
	$exclude = $( '.wpb_vc_param_value[name="taxonomies"]', this.$content );
	autocomplete = $exclude.data( 'object' );
	$filterBy.change( function () {
		var $this = $( this );
		defaultValue !== $this.val() && autocomplete.clearValue();
		autocomplete.source_data = function () {
			return { vc_filter_by: $this.val() };
		};
	} ).trigger('change');
};