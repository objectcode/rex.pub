<?php
$fileAttach = '';
$sub_img_creative = '';
 if(has_post_thumbnail()){
	if($creative_custom_style=='bt_black' || $creative_custom_style=='bt_white'){
		$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');	
	}else{
		$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_1A');
	}
	$fileAttach = $image_attributes[0];	
	$imgWidth = $image_attributes[1];
	$imgHeight = $image_attributes[2];	
	
	$sub_img_creative_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_1A');
	$sub_img_creative = $sub_img_creative_attributes[0];																		
};
if($intLoopNumber==1 && $paged==1 && $isJsonFirst==false){?>                                                                                         	
    <div class="bt-ajax-listing <?php echo $creative_custom_style;?>">
<?php };?>
        <div class="bt-post-item <?php if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect';};?>">
        	<div class="bt-img-background-absolute" style="background-image:url(<?php echo esc_attr($fileAttach);?>)"></div>
            
            <div class="bt-picture-item">                
                <div class="bt-img-background">
                	<?php if($fileAttach!='') {?>
                		<img src="<?php echo esc_attr($sub_img_creative);?>" alt="<?php echo strip_tags(get_the_title());?>">
                    <?php };?>
                </div>
                <?php if($post_read_popup=='yes') {?>
                    <div href="javascript:;" class="open-full-post bt-oval-open" <?php echo $data_open_post_details;?>><span></span><span></span></div>
                <?php };?> 
                         
            </div>
            
            <div class="bt-post-content <?php if((($intLoopMasonry%2)!=0 && $creative_custom_style!='bt_black') || $creative_custom_style=='bt_white'){?>bt-white-div<?php };?>">
				<?php if($post_metadata=='yes' && $show_taxonomy=='yes') {?>
                    <div class="bt-posted-on bt-check-categories bt-font-main-1"> 
                        
                        <?php                            												
                        if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
                            foreach($taxonomies_listing as $taxonomy_item) {
                        ?>                           	
                        <a 
                        href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?>
                        title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                        <?php 
                            };
                        };
                        ?>
                        
                    </div>
                <?php };?>
            
                
                <div class="bt-h3 bt-title-config"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a></div>                    
                
                <?php if($post_metadata=='yes') {?>
                    <div class="bt-posted-on bt-font-main-1">
                        <?php if($show_author=='yes') {?>
                            <div class="bt-author-text"><svg class="bete-awesome-svg-user"><use xlink:href="#bete-awesome-svg-user"></use></svg><?php echo get_the_author();?></div>
                        <?php };?>
                        <?php if($show_commentscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-chat-bubble-two"><use xlink:href="#bete-awesome-svg-chat-bubble-two"></use></svg><?php echo get_comments_number(get_the_ID());?></div><?php };?>
                        <?php if($show_viewscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-eye"><use xlink:href="#bete-awesome-svg-eye"></use></svg><?php echo betePlug_get_post_views(get_the_ID());?></div><?php };?>
                        <?php if($show_datetime=='yes') {?><div><svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-clock"></use></svg><?php echo date_i18n(get_option('date_format'), get_the_time('U'));?></div><?php };?>
                        <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                            <div class="event-time-for-builder">
                            <svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                            </div>
                        <?php };?>                                        
                    </div>
                <?php };?> 
                
                <?php if(get_the_excerpt()!='' && $show_excerpt=='yes'){?>                    
                    <div class="bt-get-excerpt bt-font-main-1">
                        <?php echo get_the_excerpt();?>
                    </div>
                <?php };?>
                
                <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-style-button bt-read-more bt-font-main-1 <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>>
                    <span>
                        <?php echo __('Read More', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?>
                    </span>
                </a>    
            	
            </div>
        </div>
<?php
if($intLoopNumber>=1 && $intLoopNumber==($the_query->post_count) && $paged==1 && $isJsonFirst==false){?>    
    </div> 
    <!--<div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>-->
<?php
};