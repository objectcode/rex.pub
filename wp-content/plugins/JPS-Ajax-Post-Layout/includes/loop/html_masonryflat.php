<?php
$fileAttach = '';                                            
switch($html_style) {
	case 'bt-metro-fw':
		if(has_post_thumbnail()){
			if(((($intLoopMasonry-1)%7==0) && ($intLoopMasonry-1 >= 7)) || $intLoopMasonry==1) {
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_3');				
			}else{
				if(((($intLoopMasonry-6)%7==0) && ($intLoopMasonry-6 >= 7)) || $intLoopMasonry==6) {
					$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_2');
				}elseif(((($intLoopMasonry-2)%7==0) && ($intLoopMasonry-2 >= 7)) || $intLoopMasonry==2) {
					$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_1');
				}else{
					$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_1');
				}
			};
			$fileAttach = $image_attributes[0];	
			$imgWidth = $image_attributes[1];
			$imgHeight = $image_attributes[2];	
		};		
		break;
	case 'bt-metro-fc':
		if(has_post_thumbnail()){
			if(((($intLoopMasonry-4)%8==0) && ($intLoopMasonry-4 >= 8)) || $intLoopMasonry==4){
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_1');
			}elseif(((($intLoopMasonry-8)%8==0) && ($intLoopMasonry-8 >= 8)) || $intLoopMasonry==8){
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_2');
			}else{
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_1');
			};
			$fileAttach = $image_attributes[0];
			$imgWidth = $image_attributes[1];
			$imgHeight = $image_attributes[2];	
		};																						
		break;
	case 'bt-metro-m':
		if(has_post_thumbnail()){
			if(((($intLoopMasonry-3)%3==0) && ($intLoopMasonry-3 >= 3)) || $intLoopMasonry==3) {
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_mreto_size_2');	
			}else{
				$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_1');
			}
			$fileAttach = $image_attributes[0];
			$imgWidth = $image_attributes[1];
			$imgHeight = $image_attributes[2];																							
		};		
		break;		
};	

if($intLoopNumber==1 && $paged==1 && $isJsonFirst==false){?>                                                                                         	
	<div class="bt-is-masonry bt-ajax-listing"> <!--bt-is-isotope-->
<?php };?>
		<div class="bt-post-item 
		<?php 
		if($paged!=1 || $isJsonFirst==true) {echo 'bt-opacity-loading ';}
		if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect ';};
		
		if(($html_style=='bt-metro-fw' && (($intLoopMasonry-1)%7==0) && ($intLoopMasonry-1 >= 7)) || ($html_style=='bt-metro-fw' && $intLoopMasonry==1)) {echo 'bt-masonry-style-3';}
		if(($html_style=='bt-metro-fw' && (($intLoopMasonry-2)%7==0) && ($intLoopMasonry-2 >= 7)) || ($html_style=='bt-metro-fw' && $intLoopMasonry==2)) {echo 'bt-masonry-style-2';}
		if(($html_style=='bt-metro-fw' && (($intLoopMasonry-6)%7==0) && ($intLoopMasonry-6 >= 7)) || ($html_style=='bt-metro-fw' && $intLoopMasonry==6)) {echo 'bt-masonry-style-1';}
			
		if(($html_style=='bt-metro-fc' && (($intLoopMasonry-4)%8==0) && ($intLoopMasonry-4 >= 8)) || ($html_style=='bt-metro-fc' && $intLoopMasonry==4)) {echo 'bt-masonry-style-2';}
		if(($html_style=='bt-metro-fc' && (($intLoopMasonry-8)%8==0) && ($intLoopMasonry-8 >= 8)) || ($html_style=='bt-metro-fc' && $intLoopMasonry==8)) {echo 'bt-masonry-style-1';}
		
		
		if(($html_style=='bt-metro-m' && (($intLoopMasonry-3)%3==0) && ($intLoopMasonry-3 >= 3)) || ($html_style=='bt-metro-m' && $intLoopMasonry==3)) {echo 'bt-masonry-style-1';}
		?>    
        " data-id-next="<?php echo $intLoopMasonry;?>">
            <div class="bt-picture-item">
                <?php if($fileAttach!='') {?>
                <div class="bete-update-padding">
                    <div class="bt-img-parallax">
                        <img src="<?php echo esc_attr($fileAttach);?>" alt="<?php echo strip_tags(get_the_title());?>" width="<?php echo esc_attr($imgWidth);?>" height="<?php echo esc_attr($imgHeight);?>" data-depth="1.15" class="layer">
                    </div>
                </div>
                <?php };?>
                <div class="bt-hover-grid-overlay"></div>
                
                <?php if($post_read_popup=='yes') {?>
                <div href="javascript:;" class="open-full-post bt-oval-open" <?php echo $data_open_post_details;?>><span></span><span></span></div>
                <?php };?>
                
                <div class="bt-hover-grid-content">
                
                    <div class="bt-table-grid-content">
                        <div class="bt-cell-grid-content">
                        
                            <div class="bt-h5"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a></div>
                            
                            <?php if($post_metadata=='yes' && $show_taxonomy=='yes') {?>
                                <div class="bt-posted-on bt-font-main-1">                                     
                                    <?php	
									if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
										foreach($taxonomies_listing as $taxonomy_item) {		
                                    ?>                           	
                                    <a 
                                    href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?> 
                                    title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                                    <?php 
										};
									};
                                    ?>
                                    
                                    <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                                        <div class="event-time-for-builder">
                                        <svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                                        </div>
                                    <?php };?>
                                                                        
                                </div>
                            <?php };?>
                            
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
<?php
if($intLoopNumber>=1 && $intLoopNumber==($the_query->post_count) && $paged==1 && $isJsonFirst==false){?>   
	</div> 
	<!--<div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>-->
<?php
};