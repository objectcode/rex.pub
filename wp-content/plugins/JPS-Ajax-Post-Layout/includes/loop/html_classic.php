<?php
if($intLoopNumber==1){
                                                
	$fileAttach = '';
	$typeAttach = array('ext'=>'jpg');
	$imgWidth = 0;
	$imgHeight = 0;
	if(isset($background_opt) && $background_opt=='yes' && isset($background_img) && $background_img!='') {	
		if($html_background_img_count>=(int)$paged) {	
			$arrayIDattach = $html_background_img[(int)$paged-1];
																							
			if(isset($arrayIDattach)) {																								
				if(wp_attachment_is_image($arrayIDattach)) {
					$image_attributes=array(''=>'', ''=>'0', ''=>'0');
					$image_attributes = wp_get_attachment_image_src($arrayIDattach, 'full');
					$fileAttach = $image_attributes[0];
					$typeAttach = wp_check_filetype($fileAttach);
					$imgWidth = $image_attributes[1];
					$imgHeight = $image_attributes[2];
				}else{
					$fileAttach = wp_get_attachment_url( $arrayIDattach );
					$typeAttach = wp_check_filetype($fileAttach);
				};
			};
			
		};
	}else{
		$image_attributes=array(''=>'', ''=>'0', ''=>'0');;
		if(has_post_thumbnail()){
			$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');	
			$fileAttach = $image_attributes[0];
			$typeAttach = wp_check_filetype($fileAttach);
			$imgWidth = $image_attributes[1];
			$imgHeight = $image_attributes[2];											
		};											
	};
?>                                            
    <div class="bt-slider-item-content" data-img-bg="<?php echo esc_attr($fileAttach);?>" data-type-background="<?php echo esc_attr($typeAttach['ext']);?>" data-width="<?php echo esc_attr($imgWidth)?>" data-height="<?php echo esc_attr($imgHeight)?>"
    <?php if(is_numeric($html_height)) echo 'style="height:'.esc_attr($html_height).'px;"';?>
    >                                          
                                        
        <div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>
    
        <div class="bt-primary-content <?php if($countPosts==1) {echo esc_attr('style-one-post-count');};?> <?php //if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect';};?>">
        	
            <?php if($countPosts==1) {?>
            	<div class="bt-one-post-table">
                	<div class="bt-one-post-cell">
        	<?php };?> 
            
				<?php if($post_metadata=='yes' || $post_read_popup=='yes') {?>
                <div class="bt-posted-on">
                
                    <?php if($post_read_popup=='yes') {?>
                    <div class="bt-cat-list open-full-post" <?php echo $data_open_post_details;?>>&bull;&bull;&bull;</div>
                    <?php };?> 
                    
                    <?php 
                    if($post_metadata=='yes' && $show_taxonomy=='yes') {												
                        if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
                            foreach($taxonomies_listing as $taxonomy_item) {		
                    ?>                           	
                    <a 
                    href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?> 
                    title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                    <?php 
                            };
                        };
                    };
                    ?>
                    
                    <?php if($post_metadata=='yes') {?>
                    	<?php if($show_author=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-user"><use xlink:href="#bete-awesome-svg-user"></use></svg><?php echo get_the_author();?></div><?php };?>
						<?php if($show_commentscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-chat-bubble-two"><use xlink:href="#bete-awesome-svg-chat-bubble-two"></use></svg><?php echo get_comments_number(get_the_ID());?></div><?php };?>
                        <?php if($show_viewscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-eye"><use xlink:href="#bete-awesome-svg-eye"></use></svg><?php echo betePlug_get_post_views(get_the_ID());?></div><?php };?>
                        <?php if($show_datetime=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-clock"></use></svg><?php echo esc_html(date_i18n(get_option('date_format'), get_the_time('U')));?></div><?php };?>
                        <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                            <div class="event-time-for-builder">
                            <svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                            </div>
                        <?php };?>
                    <?php };?>
                </div>
                <?php };?>
                
                <div class="bt-h1 bt-primary-title">
                    <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-2 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a>
                </div>
                
        	<?php if($countPosts==1) {?>
            		</div>
                </div>
        	<?php };?>         
        	   
            
        </div>	
<?php 
}else{
	if($intLoopNumber==2){?>
		<div class="bt-sub-content <?php //if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect';};?>">
                <div class="bt-sub-content-table">
                    <div class="bt-sub-content-cell">                                                
                        <div class="bt-scroll-bar">                                                
                            <div class="bt-scroll-bar-content">                                    
	<?php };?>
    							<div class="bt-list-item">                                                                                                               	
									<?php if($post_metadata=='yes' || $post_read_popup=='yes') {?>
                                    <div class="bt-posted-on"> 
                                    
                                    	<?php if($post_read_popup=='yes') {?>
                                        <div class="bt-cat-list open-full-post" <?php echo $data_open_post_details;?>>&bull;&bull;&bull;</div>
                                        <?php };?>
                                        
                                        <?php 
                                        if($post_metadata=='yes' && $show_taxonomy=='yes') {												
                                            if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
                                                foreach($taxonomies_listing as $taxonomy_item) {		
                                        ?>                           	
                                        <a 
                                        href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?> 
                                        title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                                        <?php 
                                                };
                                            };
                                        };
                                        ?>
                                        
                                        <?php if($post_metadata=='yes') {?>
                                        	<?php if($show_author=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-user"><use xlink:href="#bete-awesome-svg-user"></use></svg><?php echo get_the_author();?></div><?php };?>
											<?php if($show_commentscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-chat-bubble-two"><use xlink:href="#bete-awesome-svg-chat-bubble-two"></use></svg><?php echo get_comments_number(get_the_ID());?></div><?php };?>
                                            <?php if($show_viewscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-eye"><use xlink:href="#bete-awesome-svg-eye"></use></svg><?php echo betePlug_get_post_views(get_the_ID());?></div><?php };?>
                                            <?php if($show_datetime=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-clock"></use></svg><?php echo esc_attr(date_i18n(get_option('date_format'), get_the_time('U')));?></div><?php };?>
                                            <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                                                <div class="event-time-for-builder">
                                                <svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                                                </div>
                                            <?php };?>
                                        <?php };?>
                                    </div>
                                    <?php };?>   
                                    <div class="bt-h6 bt-sub-title">
                                        <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a>
                                    </div>
                                </div>
	<?php
	if($intLoopNumber>=2 && $intLoopNumber==($the_query->post_count)){?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php 
	};
};
if($intLoopNumber>=1 && $intLoopNumber==($the_query->post_count)){?>                                    
	</div>                                    
<?php
};