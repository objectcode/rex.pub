<?php
$fileAttach = '';
?>
<?php if($intLoopNumber==1){ 
	if(has_post_thumbnail()){
		$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_1');	
		$fileAttach = $image_attributes[0];	
		$imgWidth = $image_attributes[1];
		$imgHeight = $image_attributes[2];																					
	};
?>
    <div class="bt-carousel-content">
    	<div class="bt-first-post">
        	<div class="bt-post-item bt-ready-parallax <?php if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect';};?>">
                <div class="bt-post-item-content">
                    <?php if($fileAttach!='') {?>
                    <div class="bt-picture-item"> 
                                   
                        <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="<?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>>
                            <div class="bt-img-parallax">                            
                                <img src="<?php echo esc_attr($fileAttach);?>" alt="<?php echo strip_tags(get_the_title());?>" width="<?php echo esc_attr($imgWidth);?>" height="<?php echo esc_attr($imgHeight);?>">                            
                            </div>
                            <div class="bt-hover-grid-overlay"></div>
                        </a>             
                        <?php if($post_read_popup=='yes') {?>
                        <div href="javascript:;" class="open-full-post bt-oval-open" <?php echo $data_open_post_details;?>><span></span><span></span></div>
                        <?php };?>
                    </div>
                    <?php };?>
                    
                    <div class="bt-post-content bt-white-div">
                        <?php if($post_metadata=='yes' && $show_taxonomy=='yes') {?>
                            <div class="bt-posted-on bt-font-main-1"> 
                                
                                <?php                            												
                                if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
                                    foreach($taxonomies_listing as $taxonomy_item) {
                                ?>                           	
                                <a 
                                href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?>
                                title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                                <?php 
                                    };
                                };
                                ?>
                                
                            </div>
                        <?php };?>
                    
                        <div class="bt-h5 bt-title-config"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a></div>
                        
                        <?php if($post_metadata=='yes') {?>
                            <div class="bt-posted-on bt-font-main-1">                                        
                                <?php if($show_author=='yes') {?><div class="bt-author-text"><svg class="bete-awesome-svg-user"><use xlink:href="#bete-awesome-svg-user"></use></svg><?php echo get_the_author();?></div><?php };?>   
                                <?php if($show_commentscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-chat-bubble-two"><use xlink:href="#bete-awesome-svg-chat-bubble-two"></use></svg><?php echo get_comments_number(get_the_ID());?></div><?php };?>
                    			<?php if($show_viewscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-eye"><use xlink:href="#bete-awesome-svg-eye"></use></svg><?php echo betePlug_get_post_views(get_the_ID());?></div><?php };?>
                                <?php if($show_datetime=='yes') {?><div><svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-clock"></use></svg><?php echo date_i18n(get_option('date_format'), get_the_time('U'));?></div><?php };?>
                                <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                                	<div class="event-time-for-builder">
									<svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                                    </div>
                                <?php };?>                                
                            </div>                            
                        <?php };?>
                        
                        <?php if(get_the_excerpt()!='' && $show_excerpt=='yes'){?>                    
                            <div class="bt-get-excerpt bt-font-main-1">
                                <?php if(strlen(get_the_excerpt()) > 120) {echo substr(get_the_excerpt(),0,120).'...';}else{echo get_the_excerpt();};?>
                            </div>
                        <?php };?>    
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bt-next-post">
    <?php }else{ 
		if(has_post_thumbnail()){
			$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_3');	
			$fileAttach = $image_attributes[0];	
			$imgWidth = $image_attributes[1];
			$imgHeight = $image_attributes[2];																					
		};
	?>
    		<div class="bt-post-item bt-ready-parallax">
                <div class="bt-post-item-content">
                    <?php if($fileAttach!='') {?>
                    <div class="bt-picture-item"> 
                                   
                        <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="<?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>>
                            <div class="bt-img-parallax">                            
                                <img src="<?php echo esc_attr($fileAttach);?>" alt="<?php echo strip_tags(get_the_title());?>" width="<?php echo esc_attr($imgWidth);?>" height="<?php echo esc_attr($imgHeight);?>">
                                <div class="bt-hover-grid-overlay"></div>                            
                            </div>                            
                        </a>                 
                        <?php if($post_read_popup=='yes') {?>
                        <div href="javascript:;" class="open-full-post bt-oval-open bt-small-click" <?php echo $data_open_post_details;?>><span></span><span></span></div>
                        <?php };?>
                    </div>
                    <?php };?>
                    
                    <div class="bt-post-content bt-white-div">
                    
                        <div class="bt-h6 bt-title-config"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a></div>
                        
                        <?php if($post_metadata=='yes') {?>
                            <div class="bt-posted-on bt-font-main-1">
                            	<?php if($show_commentscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-chat-bubble-two"><use xlink:href="#bete-awesome-svg-chat-bubble-two"></use></svg><?php echo get_comments_number(get_the_ID());?></div><?php };?>                            	
                    			<?php if($show_viewscount=='yes') {?><div class="bt-font-main-1"><svg class="bete-awesome-svg-eye"><use xlink:href="#bete-awesome-svg-eye"></use></svg><?php echo betePlug_get_post_views(get_the_ID());?></div><?php };?>
                                <?php if($show_datetime=='yes') {?><div><svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-clock"></use></svg><?php echo date_i18n(get_option('date_format'), get_the_time('U'));?></div><?php };?>
                                <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                                	<div class="event-time-for-builder">
									<svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                                    </div>
                                <?php };?>
                            </div>                            
                        <?php };?>
                          
                    </div>
                </div>
            </div>
    <?php }; 
	if($intLoopNumber>=1 && $intLoopNumber==($the_query->post_count)){
	?>    	
    	</div>
    </div>
<?php };