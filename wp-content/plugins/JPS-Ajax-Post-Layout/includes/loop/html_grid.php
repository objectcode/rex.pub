<?php
$fileAttach = '';
 if(has_post_thumbnail()){
	$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_1');	
	$fileAttach = $image_attributes[0];	
	$imgWidth = $image_attributes[1];
	$imgHeight = $image_attributes[2];																					
};
if($intLoopNumber==1 && $paged==1 && $isJsonFirst==false){?>                                                                                         	
    <div class="bt-ajax-listing">
<?php };?>
        <div class="bt-post-item <?php if($paged==1 && $isJsonFirst==false){echo 'bt-no-effect';};?>">
            <div class="bt-picture-item">
                <?php if($fileAttach!='') {?>
                <div class="bt-img-parallax">
                    <img src="<?php echo esc_attr($fileAttach);?>" alt="<?php echo strip_tags(get_the_title());?>" width="<?php echo esc_attr($imgWidth);?>" height="<?php echo esc_attr($imgHeight);?>" data-depth="1.15" class="layer">
                </div>
                <?php };?>
                <div class="bt-hover-grid-overlay"></div>
                
                <?php if($post_read_popup=='yes') {?>
                <div href="javascript:;" class="open-full-post bt-oval-open" <?php echo $data_open_post_details;?>><span></span><span></span></div>
                <?php };?>
                
                <div class="bt-hover-grid-content">
                
                    <div class="bt-table-grid-content">
                        <div class="bt-cell-grid-content">
                        
                            <div class="bt-h5"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" class="bt-font-heading-1 bt-color-title <?php if($post_read_popup=='yes'&&$open_popup_in=='yes'){?>open-full-post bacmnsic<?php };?>" <?php echo $data_open_post_details;?> <?php if($link_target=='yes'){?>target="_blank"<?php };?>><?php the_title(); ?></a></div>
                            
                            <?php if($post_metadata=='yes' && $show_taxonomy=='yes') {?>
                                <div class="bt-posted-on bt-font-main-1">
                                    <?php											
									if(!is_wp_error($taxonomies_listing)&&!empty($taxonomies_listing)) {
										foreach($taxonomies_listing as $taxonomy_item) {		
                                    ?>                           	
                                    <a 
                                    href="<?php echo esc_url(get_term_link($taxonomy_item))?>" <?php if($link_target=='yes'){?>target="_blank"<?php };?> 
                                    title="<?php echo esc_attr($taxonomy_item->name)?>" class="bt-cat-list bt-font-main-1"><svg class="bete-awesome-svg-hospital-square"><use xlink:href="#bete-awesome-svg-hospital-square"></use></svg><?php echo esc_html($taxonomy_item->name)?></a>
                                    <?php 
										};
									};
                                    ?>
                                    
                                    <?php if(($show_start_date=='yes' || $show_end_date=='yes') && $themeActive!==false) {?>
                                        <div class="event-time-for-builder">
                                        <svg class="bete-awesome-svg-clock"><use xlink:href="#bete-awesome-svg-present-gift"></use></svg><?php if($show_start_date=='yes') {?><span><?php echo __('Starts: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_start_date.' '.$event_start_time;?></span><?php };?><?php if($show_end_date=='yes') {?><span><?php echo __('Ends: ', JPS_BETE_SETTING_TEXT_DOMAIN ).$event_end_date.' '.$event_end_time;?></span><?php };?>
                                        </div>
                                    <?php };?>
                                    
                                </div>
                            <?php };?>
                            
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
<?php
if($intLoopNumber>=1 && $intLoopNumber==($the_query->post_count) && $paged==1 && $isJsonFirst==false){?>    
    </div> 
    <!--<div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>-->
<?php
};   