<?php
if(!function_exists('jps_bete_query')){
	function jps_bete_query($jps_bete_options_query) {
		
		$param_post_type		= trim($jps_bete_options_query['post_type']);
		$param_filter_options	= trim($jps_bete_options_query['filter_options']);
		$param_taxonomies 		= trim($jps_bete_options_query['taxonomies']);
		$param_ids 				= trim($jps_bete_options_query['ids']);
		$param_condition 		= trim($jps_bete_options_query['condition']);		
		$param_sortby 			= trim($jps_bete_options_query['sortby']); 			//
		$param_total_posts 		= trim($jps_bete_options_query['total_posts']);		//
		$param_items_per_page 	= trim($jps_bete_options_query['items_per_page']); 	//
		$param_paged 			= trim($jps_bete_options_query['paged']); 			//	
		$param_showpost_cal		= trim($jps_bete_options_query['showpost_call']);	//
		$param_showCalPosts		= trim($jps_bete_options_query['showCalPosts']);
		
		$argsColumnQuery	=	array(
									'post_type' 			=> $param_post_type,
									'order'					=> $param_sortby!='DESC'?'ASC':$param_sortby,
									'post_status' 			=> 'publish',
									'ignore_sticky_posts'	=> 1,											
								);	
		
		if(isset($param_condition) && $param_filter_options!='list_of_ids'){
			switch($param_condition){
				case 'latest':				
					break;					
				case 'mostcommented':
					$argsColumnQuery['orderby'] 			= 'comment_count';
					break;
				case 'title':
					$argsColumnQuery['orderby'] 			= 'title';
					break;
				case 'viewscount':
					if(function_exists('yt_simple_post_views_counter')){
						$argsColumnQuery['meta_key'] 			= '_post_views';
					}else{
						$argsColumnQuery['meta_key'] 			= 'beteplug_post_views_count';						
					};
						$argsColumnQuery['orderby'] 			= 'meta_value_num';
					break;	
				case 'random':
					$argsColumnQuery['orderby'] 			= 'rand';
					break;			
				default:	
			};
		};	
		
		$taxonomies_to_array=array();
		if(isset($param_taxonomies) && trim($param_taxonomies)!='' && $param_filter_options!='all_posts' && $param_filter_options!='list_of_ids') {
			$param_taxonomies = explode(',', $param_taxonomies);
			
			foreach ($param_taxonomies as $taxonomies_item_arr) {						
				array_push($taxonomies_to_array, trim(str_replace(' ', '-', trim($taxonomies_item_arr))) );
			};
			
			if(is_array($taxonomies_to_array)){
				if(is_numeric($param_taxonomies[0])){							
					$argsColumnQuery['tax_query'] = array(
														array(
															'taxonomy' => $param_filter_options,                
															'field' => 'id',                    
															'terms' => $taxonomies_to_array,
												  		),
													);	
				}else{
					$argsColumnQuery['tax_query'] = array(
														array(
															'taxonomy' => $param_filter_options,                
															'field' => 'slug',                    
															'terms' => $taxonomies_to_array,
												  		),
													);
				};
			};
		};
		
		$ids_to_array=array();
		if(isset($param_ids) && trim($param_ids)!='' && $param_filter_options=='list_of_ids') {
			$param_ids = explode(',', $param_ids);
			
			foreach ($param_ids as $id_item_arr) {	
				if(is_numeric(trim($id_item_arr))) {			
					array_push($ids_to_array, trim($id_item_arr));
				};
			};
			
			if(is_array($ids_to_array)){							
				$argsColumnQuery['post__in'] = $ids_to_array;
				//$argsColumnQuery['order'] = 'post__in';				
			};			
		};	
		
		if(isset($param_showpost_cal) && is_numeric($param_showpost_cal) && (int)$param_showpost_cal>0) {
			$argsColumnQuery['offset'] = ( ( ( (int) $param_paged ) - 1 ) * ((int)$param_items_per_page) );
			$argsColumnQuery['showposts'] = $param_showpost_cal;
		};
		
		if(isset($param_paged) && $param_paged!='' && is_numeric(trim($param_paged))){				
			$argsColumnQuery['paged'] = $param_paged;	
		}else{
			$argsColumnQuery['paged'] = 1;
		};
		
		if(isset($param_items_per_page) && is_numeric($param_items_per_page)) {
			if(isset($param_total_posts) && is_numeric($param_total_posts)) {
				if((int)$param_items_per_page > (int)$param_total_posts) {
					$argsColumnQuery['posts_per_page'] 			= $param_total_posts;
				}else{
					$argsColumnQuery['posts_per_page'] 			= $param_items_per_page;
				};
			}else{
				if((int)$param_items_per_page > 100) {
					$argsColumnQuery['posts_per_page'] 			= 100;
				}else{
					$argsColumnQuery['posts_per_page'] 			= $param_items_per_page;
				};
			};
			
		}else{
			$argsColumnQuery['posts_per_page'] 			= 10;
		};
		
		$newQuery = new WP_Query($argsColumnQuery);
		//echo $newQuery->request;
		
		$totalCountPosts = 	($newQuery->found_posts);
		if(isset($param_total_posts) && is_numeric($param_total_posts) && $param_total_posts!=-1) {
			if($totalCountPosts > (int)($param_total_posts)) {
				$totalCountPosts = $param_total_posts;
			}else{
				$totalCountPosts = ($newQuery->found_posts);
			};
		};			
		
		if(isset($param_showCalPosts) && $param_showCalPosts==0) {
			return $newQuery;
		}else{
			return $totalCountPosts;
		};
	};
};

if(!function_exists('bt_checkSliderStyle')){
	function bt_checkSliderStyle($html_style){
		if($html_style=='' || $html_style=='bt-2row' || $html_style=='bt-medium' || $html_style=='bt-small' || $html_style=='default') {
			return true;
		}else{
			return false;
		};
	};
};

if(!function_exists('bt_checkMasonryStyle')){
	function bt_checkMasonryStyle($html_style) {
		if($html_style=='bt-metro-fw' || $html_style=='bt-metro-fc' || $html_style=='bt-metro-m') {
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('bt_checkGridBoxStyle')){
	function bt_checkGridBoxStyle($html_style) {
		if($html_style=='bt-grid-1' || $html_style=='bt-grid-2' || $html_style=='bt-grid-3' || $html_style=='bt-grid-4' || $html_style=='bt-grid-5') {
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('bt_checkCreativeStyle')){
	function bt_checkCreativeStyle($html_style) {
		if($html_style=='bt-creative-1') {
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('bt_checkGridNormalStyle')){
	function bt_checkGridNormalStyle($html_style) {
		if($html_style=='bt-post-grid-1' || $html_style=='bt-post-grid-2' || $html_style=='bt-post-grid-3' || $html_style=='bt-post-grid-4') {
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('bt_checkGridMasonryStyle')){
	function bt_checkGridMasonryStyle($html_style) {
		if($html_style=='bt-post-grid-m-1' || $html_style=='bt-post-grid-m-2' || $html_style=='bt-post-grid-m-3' || $html_style=='bt-post-grid-m-4') {
			return true;
		}else{
			return false;
		}
	}
}

if(!function_exists('bt_checkCarouselStyle')){
	function bt_checkCarouselStyle($html_style) {
		if($html_style=='bt-carousel-1' || $html_style=='bt-carousel-2' || $html_style=='bt-carousel-3' || $html_style=='bt-carousel-4') {
			return true;
		}else{
			return false;
		}
	}
}


if(!function_exists('jps_bete_html_data')){
	function jps_bete_html_data($style, $filter_options, $taxonomies, $ids, $condition, $sortby, $post_metadata, $total_posts, $items_per_page, $height, $background_opt, $background_img, $parallax, $animate, $post_read_popup, $more_button, $autoplay, $autoplayspeed, $extra_class, $infinite_ajax, $gutter, $show_filter, $combo_grid_list, $priority_grid_list, $show_taxonomy, $show_author, $show_datetime, $show_viewscount, $show_commentscount, $show_excerpt, $post_type, $delay_spinner, $show_start_date, $show_end_date, $open_popup_in, $main_font, $heading_font, $s_heading_font, $link_color, $link_hover_color, $light_link_color, $light_link_hover_color, $title_color, $title_hover_color, $light_title_color, $light_title_hover_color, $metadata_color, $light_metadata_color, $text_color, $thumb_overlay, $text_transform, $social_facebook, $social_twitter, $social_google_plus, $social_pinterest, $social_tumblr, $social_linkedin, $social_email, $under_post_on_grid, $grid_style_box, $grid_style_title_on_img, $hidden_grid_content, $custom_margin, $custom_margin_bottom, $link_target, $creative_custom_style, $paged, $isJson, $isJsonFirst, $isJsonThumb, $showpost_cal){
		
		if(isset($filter_options) && $filter_options!='') {$filter_options=$filter_options;}else{$filter_options='all_posts';}
		if(isset($total_posts) && is_numeric($total_posts)) {
			$total_posts=(int)$total_posts;
			if($total_posts>1000) {$total_posts=1000;}
		}else{
			$total_posts=100;
		}
		
		$jps_bete_options_query = 	array(
												'post_type'			=>  $post_type,
												'filter_options'	=>  $filter_options,
												'taxonomies' 		=> 	$taxonomies,
												'ids'				=>	$ids,
												'condition'			=>	$condition,												
												'sortby'			=>	$sortby,
												'total_posts'		=>	$total_posts,
												'items_per_page'	=>	$items_per_page,
												'paged'				=>	$paged,
												'showpost_call'		=>  $showpost_cal,
												'showCalPosts'		=>  0,
											);			
		
		$the_query = jps_bete_query($jps_bete_options_query);		
		if ($the_query->have_posts()):	
		
			$jps_params = $style.'[|]'.$filter_options.'[|]'.$taxonomies.'[|]'.$ids.'[|]'.$condition.'[|]'.$sortby.'[|]'.$post_metadata.'[|]'.$total_posts.'[|]'.$items_per_page.'[|]'.$height.'[|]'.$background_opt.'[|]'.$background_img.'[|]'.$parallax.'[|]'.$animate.'[|]'.$post_read_popup.'[|]'.$more_button.'[|]'.$autoplay.'[|]'.$autoplayspeed.'[|]'.$extra_class.'[|]'.$infinite_ajax.'[|]'.$gutter.'[|]'.$show_filter.'[|]'.$combo_grid_list.'[|]'.$priority_grid_list.'[|]'.$show_taxonomy.'[|]'.$show_author.'[|]'.$show_datetime.'[|]'.$show_viewscount.'[|]'.$show_commentscount.'[|]'.$show_excerpt.'[|]'.$post_type.'[|]'.$delay_spinner.'[|]'.$show_start_date.'[|]'. $show_end_date.'[|]'.$open_popup_in.'[|]'.$main_font.'[|]'.$heading_font.'[|]'.$s_heading_font.'[|]'.$link_color.'[|]'.$link_hover_color.'[|]'.$light_link_color.'[|]'.$light_link_hover_color.'[|]'.$title_color.'[|]'.$title_hover_color.'[|]'.$light_title_color.'[|]'.$light_title_hover_color.'[|]'.$metadata_color.'[|]'.$light_metadata_color.'[|]'.$text_color.'[|]'.$thumb_overlay.'[|]'.$text_transform.'[|]'.$social_facebook.'[|]'.$social_twitter.'[|]'.$social_google_plus.'[|]'.$social_pinterest.'[|]'.$social_tumblr.'[|]'.$social_linkedin.'[|]'.$social_email.'[|]'.$under_post_on_grid.'[|]'.$grid_style_box.'[|]'.$grid_style_title_on_img.'[|]'.$hidden_grid_content.'[|]'.$custom_margin.'[|]'.$custom_margin_bottom.'[|]'.$link_target.'[|]'.$creative_custom_style.'[|]'.$paged.'[|]'.$isJson.'[|]'.$isJsonFirst.'[|]'.$isJsonThumb.'[|]'.$showpost_cal;			

							
			$html_animate = '';
			if($animate!='default') {
				$html_animate = $animate;
			};
			
			$html_style = '';
			if($style!='default'){
				$html_style = $style;
			};
			
			$html_parallax = '';
			if($parallax=='yes' && bt_checkSliderStyle($html_style)){
				$html_parallax = 'bt-is-parallax';
			};
			
			$html_height = '';
			if(bt_checkSliderStyle($html_style)) {
				if($height=='full'){
					$html_height = 'bt-full-height';
				}elseif($height!='' && $height!='full'){
					$html_height = $height;
				};
			};
			
			$html_background_img = array();
			$html_background_img_count = 1;
			if(isset($background_opt) && $background_opt=='yes' && isset($background_img) && $background_img!='') {
				$background_img = explode(',', $background_img);
				foreach ($background_img as $background_img_arr) {	
					if(is_numeric(trim($background_img_arr))) {			
						array_push($html_background_img, trim($background_img_arr));
					};
				};
				$html_background_img_count = count($html_background_img);
			}
						
		
			$totalCountPosts = 	($the_query->found_posts);
			if(is_numeric($total_posts) && $total_posts!=-1) {
				if($totalCountPosts > (int)($total_posts)) {
					$totalCountPosts = $total_posts;
				}else{
					$totalCountPosts = ($the_query->found_posts);
				};
			};
			
			$countPosts = $the_query->post_count;
			
			$allItems			= (int)$totalCountPosts;
			$allItemsPerPage	= (int)$items_per_page;
			$paged_calculator	= 1;
			$percentItems		= 0;
			
			if($allItems > $allItemsPerPage) {
				$percentItems = ($allItems % $allItemsPerPage);		
				if($percentItems!=0){
					$paged_calculator=( ($allItems-$percentItems) / $allItemsPerPage ) + 1;
				}else{
					$paged_calculator=($allItems / $allItemsPerPage );
				};	
			};
			
			$html_noimg = JPS_BETE_PLUGIN_URL.'library/images/no.jpg';
			
			$html_gutter = '';
			if(isset($gutter) && $gutter!='no') {
				$html_gutter = 'bt-padding-item';
			};
			
			$html_show_filter = '';
			if(isset($show_filter) && $show_filter=='yes') {
				$html_show_filter = 'yes';
			};	
			
			$cookie_grid_list_combo = '';
			if(isset($_COOKIE['btSettingListAhead'])){
				$cookie_grid_list_combo = $_COOKIE['btSettingListAhead'];
			};
			
			$rand_ID                    =  rand(1, 9999);
			$id                         = 'jps_bete_custom_css' . $rand_ID;
			$output_id                  = ' id= "' . $id . '" ';
			
			$json_data = array();
			if(!$isJson || $isJsonThumb) {
				if($isJsonThumb) {
					$intLoopNumber = 1;		
                    while ($the_query->have_posts()):$the_query->the_post();
						
						if($intLoopNumber==1) {
							
							$fileAttach = $html_noimg;
							
							if(isset($background_opt) && $background_opt=='yes' && isset($background_img) && $background_img!='') {	
							
								if($html_background_img_count>=(int)$paged) {	
									$arrayIDattach = $html_background_img[(int)$paged-1];
																													
									if(isset($arrayIDattach)) {																								
										if(wp_attachment_is_image($arrayIDattach)) {
											$image_attributes=array(''=>'', ''=>'0', ''=>'0');
											$image_attributes = wp_get_attachment_image_src($arrayIDattach, 'bt_thumb_size_3');	
											$fileAttach = $image_attributes[0];										
										};
									};
								};
								
							}else{
								$image_attributes=array(''=>'', ''=>'0', ''=>'0');
								if(has_post_thumbnail()){
									$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'bt_thumb_size_3');
									$fileAttach = $image_attributes[0];	
								};											
							};
														
							$json_data = array('success'=>'1', 'data_thumb_img' => $fileAttach,);
							echo json_encode($json_data);
						}
						$intLoopNumber++;
                    endwhile;
				}else{					
					ob_start();
										
					if($paged==1 && $isJsonFirst==false) {	
					
					//function custom_fonts_css(){
						if(trim($main_font)!=''){
							$new_main_font = trim($main_font);
							$new_main_font = str_replace(' ', '+', $new_main_font);
							echo 
							"
							<link href='http://fonts.googleapis.com/css?family=".$new_main_font."' rel='stylesheet' type='text/css'>
							<style type='text/css'>
								html body .".$id.".bt368-nav-ptt[id^=jps_bete_] .bt-font-main-1 {font-family:'".trim($main_font)."' !important;}
							</style>
							";
						}
						if(trim($heading_font)!=''){
							$new_heading_font = trim($heading_font);
							$new_heading_font = str_replace(' ', '+', $new_heading_font);
							echo 
							"
							<link href='http://fonts.googleapis.com/css?family=".$new_heading_font."' rel='stylesheet' type='text/css'>
							<style type='text/css'>
								html body .".$id.".bt368-nav-ptt[id^=jps_bete_] .bt-font-heading-1 {font-family:'".trim($heading_font)."' !important;}
							</style>
							";
						}
						if(trim($s_heading_font)!=''){
							$new_s_heading_font = trim($s_heading_font);
							$new_s_heading_font = str_replace(' ', '+', $new_s_heading_font);
							echo 
							"
							<link href='http://fonts.googleapis.com/css?family=".$new_s_heading_font."' rel='stylesheet' type='text/css'>
							<style type='text/css'>
								html body .".$id.".bt368-nav-ptt[id^=jps_bete_] .bt-font-heading-2 {font-family:'".trim($s_heading_font)."' !important;}
							</style>
							";
						}
						
						/*Link*/
						if(trim($link_color)!=''){
							echo 
							"							
							<style type='text/css'>
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover), 
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover):focus,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):not(:hover), 
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):not(:hover):focus,
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:not(:hover),
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-filter-item:not(:hover) {color:".trim($link_color).";}
							</style>
							";
						};
						
						if(trim($link_hover_color)!=''){
							echo 
							"							
							<style type='text/css'>
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):hover,
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-filter-item:hover {color:".trim($link_hover_color).";}
							</style>
							";
						};
						
						if(trim($light_link_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover), 
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover):focus,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover), 
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover):focus,
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:not(:hover),
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:not(:hover) {color:".trim($light_link_color).";}
							</style>
							";
						};
						
						if(trim($light_link_hover_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):hover,
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:hover {color:".trim($light_link_hover_color).";}
							</style>
							";
						};
						/*Link*/
						
						/*Title*/
						if(trim($title_color)!=''){
							echo 
							"							
							<style type='text/css'>
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover), 
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover):focus,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:not(:hover), 
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:not(:hover):focus {color:".trim($title_color).";}
							</style>
							";
						};
						
						if(trim($title_hover_color)!=''){
							echo 
							"							
							<style type='text/css'>
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:hover{color:".trim($title_hover_color).";}
							</style>
							";
						};
						
						if(trim($light_title_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover), 
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover):focus,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover), 
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover):focus {color:".trim($light_title_color).";}
							</style>
							";
						};
						
						if(trim($light_title_hover_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:hover,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:hover {color:".trim($light_title_hover_color).";}
							</style>
							";
						};
						/*Title*/
						
						if(trim($text_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content),
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content),
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div{color:".trim($text_color).";}
							</style>
							";
						};
						
						if(trim($thumb_overlay)!=''){
							echo 
							"							
							<style type='text/css'>
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-hover-grid-overlay,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-thumb-gradient,
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-thumb-gradient.bt-light-version 
								{background:".trim($thumb_overlay)." !important; background-color:".trim($thumb_overlay)." !important}
							</style>
							";
						};
						
						if(trim($metadata_color)!=''){
							echo 
							"							
							<style type='text/css'>
								*:not(.bt-white-div) .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-posted-on > *:not(a),
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-posted-on > *:not(a) {color:".trim($metadata_color).";}
							</style>
							";
						};
						
						if(trim($light_metadata_color)!=''){
							echo 
							"							
							<style type='text/css'>
								.bt-white-div .".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-posted-on > *:not(a),
								.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-posted-on > *:not(a) {color:".trim($light_metadata_color).";}
							</style>
							";
						};
						
						if($text_transform=='capitalize') {
							echo "<style type='text/css'>.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-color-title,.".$id.".bt368-nav-ptt[id^=jps_bete_] .bt-ajax-title {text-transform: capitalize !important;}</style>";
						}
						if($text_transform=='normal') {
							echo "<style type='text/css'>.".$id.".bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-color-title,.".$id.".bt368-nav-ptt[id^=jps_bete_] .bt-ajax-title {text-transform: none !important;}</style>";
						}
						
						if($custom_margin!=''&&(bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)||bt_checkMasonryStyle($html_style))){
							if((int)$custom_margin <= 200) {
								$bete_padding_left_right = (int)$custom_margin/2;
								if(bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)){
									echo 
									"<style type='text/css'>
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-post-grid-]:not(.bt-combo-list-ahead) .bt-ajax-listing {
											margin-left:-".$bete_padding_left_right."px !important; margin-right:-".$bete_padding_left_right."px !important;
										}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-post-grid-]:not(.bt-combo-list-ahead) .bt-ajax-listing > .bt-post-item { 
											padding-left:".$bete_padding_left_right."px !important; padding-right:".$bete_padding_left_right."px !important;
										}
									</style>";
								}elseif(bt_checkMasonryStyle($html_style)){
									echo 
									"
									<style type='text/css'>
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-ajax-listing {margin-left:-".$custom_margin."px !important}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-post-item .bt-picture-item .bete-update-padding {left:".$custom_margin."px !important}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-post-item .bt-picture-item .bt-hover-grid-overlay {left:".$custom_margin."px !important}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-hover-grid-content {left:".(30+((int)$custom_margin))."px !important}
									</style>";
								};
							}
						}
						
						if($custom_margin_bottom!=''&&(bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)||bt_checkMasonryStyle($html_style))){
							if((int)$custom_margin_bottom <= 200) {
								if(bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)){
									echo 
									"<style type='text/css'>
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-post-grid-]:not(.bt-combo-list-ahead) .bt-ajax-listing {
											margin-bottom:-".$custom_margin_bottom."px !important;
										}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-post-grid-]:not(.bt-combo-list-ahead) .bt-ajax-listing > .bt-post-item { 
											margin-bottom:".$custom_margin_bottom."px !important;
										}
									</style>";
								}elseif(bt_checkMasonryStyle($html_style)){
									echo 
									"
									<style type='text/css'>
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-post-item .bt-picture-item .bete-update-padding {bottom:".$custom_margin_bottom."px !important}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-post-item .bt-picture-item .bt-hover-grid-overlay {bottom:".$custom_margin_bottom."px !important}
										.".$id.".bt368-nav-ptt[id^=jps_bete_][data-style*=bt-metro-] .bt-hover-grid-content {bottom:".$custom_margin_bottom."px !important}
									</style>";
								};
							}
						}
						
					//};
					//custom_fonts_css();
	?>
	
                    <div class="bt368-nav-ptt 
                    			<?php if(bt_checkSliderStyle($html_style)) {echo 'bt-slider-nowrap';};?>
								<?php echo esc_attr($extra_class);?> 
								<?php echo esc_attr($html_gutter);?> 
                                <?php echo esc_attr($id);?>
								<?php if($paged_calculator==1 && (bt_checkCarouselStyle($html_style) || bt_checkSliderStyle($html_style))) {echo 'bt-no-change';}?> 
								<?php if($filter_options=='all_posts' || $filter_options=='list_of_ids' || $html_show_filter=='') {echo 'bt-no-change-cat';}?> 
								<?php if($combo_grid_list=='yes' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style))) {echo 'bt-combo-list-grid';};?> 
								<?php if(($cookie_grid_list_combo=='1' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style))) && $combo_grid_list=='yes') {
											echo 'bt-combo-list-ahead';
									  }else{
										  if($priority_grid_list=='1' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)) && $cookie_grid_list_combo!='0' && $combo_grid_list=='yes') {
											  echo 'bt-combo-list-ahead';
										  };
									  };
								?>
                                <?php if(($priority_grid_list=='2' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style))) && $combo_grid_list=='yes') {
									echo 'bt-combo-list-ahead';
								}
								?>
                                <?php if($grid_style_box=='yes' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style))) {echo 'grid-style-box-nav';};?>
                                " 
                                
								<?php echo $output_id;?> 
                                data-information-err="<?php echo __('no results were found. Please press F5 to reload!', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?>" 
                                data-effect-slider="<?php echo esc_attr($html_animate);?>" 
                                data-style="<?php echo esc_attr($html_style);?>" 
                                data-autoplay="<?php echo esc_attr($autoplay);?>" 
                                data-autoplay_speed="<?php echo esc_attr($autoplayspeed);?>" 
                                data-server-request="<?php echo admin_url('admin-ajax.php');?>" 
                                data-max-pages="<?php echo esc_attr($paged_calculator);?>" 
                                data-last-posts-count="<?php echo esc_attr($percentItems);?>"  
                                data-server-params="<?php echo esc_attr($jps_params);?>"
                                data-custom-post-type="<?php echo esc_attr($post_type);?>"
								data-delay-spinner="<?php if(bt_checkCarouselStyle($html_style)) {echo esc_attr($delay_spinner);};?>"
                                data-custom-css="<?php echo esc_attr($id);?>"
                                
                                data-social-facebook="<?php echo esc_attr($social_facebook);?>"
                                data-social-twitter="<?php echo esc_attr($social_twitter);?>"
                                data-social-google-plus="<?php echo esc_attr($social_google_plus);?>"
                                data-social-pinterest="<?php echo esc_attr($social_pinterest);?>"
                                data-social-tumblr="<?php echo esc_attr($social_tumblr);?>"
                                data-social-linkedin="<?php echo esc_attr($social_linkedin);?>"
                                data-social-email="<?php echo esc_attr($social_email);?>"
                       >                	
                       <?php if(bt_checkSliderStyle($html_style)){?> 
                       <div class="bt-background-slider-row"><div class="bt-background-slider <?php echo esc_attr($html_parallax);?>"></div></div>
                       <?php };?>
                        
                        <div class="bt368-nav-ptt-content <?php echo esc_attr($html_parallax);?> <?php if(($filter_options!='all_posts' && $filter_options!='list_of_ids' && (bt_checkGridNormalStyle($html_style) || bt_checkGridMasonryStyle($html_style)) && $html_show_filter=='yes') || ($combo_grid_list=='yes' && $priority_grid_list!='2' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style)))) {echo 'bt-fix-top-filter';};?>"> <!--bt368-nav-ptt-content--> 
                        	
                            <div class="bt-thumb-gradient"></div>
                            
                            <?php if(bt_checkCarouselStyle($html_style)) {?>
                                <div class="bt-animate-thumb-overlay bt-json-carousel bt-white-div"></div>
                            <?php };?>
                            
                            <!--data-->
                            <div class="bt-control-wrap <?php if(bt_checkSliderStyle($html_style) || bt_checkCarouselStyle($html_style)){echo esc_attr('bt-json-slider');};?> <?php echo esc_attr($html_height);?>">
                                                        
                                <!--item-->
                                <div class="bt-slider-item">  
                                                           	
                                <?php
                                if(!bt_checkSliderStyle($html_style) && !bt_checkCarouselStyle($html_style)){?>
                                    <div class="bt-slider-item-content" data-img-bg="" data-type-background="">
                                    <?php
                                };
                    };				
									$taxonomy_default = 'category';
									if($filter_options!='all_posts' && $filter_options!='list_of_ids') {
										$taxonomy_default = $filter_options;
									};									
                                    $intLoopNumber = 1;
									$intLoopMasonry = ($paged-1)*$items_per_page;
									$theme_name = wp_get_theme();
									$themeActive = strrpos($theme_name, 'EventBuilder');
									
									$themeCheckStandardPost = '0';
									if($themeActive!==false){
										$themeCheckStandardPost = '1';
									}									
                                    while ($the_query->have_posts()):$the_query->the_post();
                                    	$intLoopMasonry++;     
										$taxonomies_listing = wp_get_post_terms(get_the_ID(), $taxonomy_default, array('fields' => 'all'));   
										$data_open_post_details=' data-id="'.esc_attr(get_the_ID()).'" data-post-type="'.esc_attr($post_type).'" data-taxonomy="'.esc_attr($taxonomy_default).'" data-tc-eventbuild="'.esc_attr($themeCheckStandardPost).'" data-tc-eventbuild-starts="'.$show_start_date.'" data-tc-eventbuild-ends="'.$show_end_date.'"';
										
										/*theme EventBuilder*/
										$event_start_date='';
										$event_start_time='';
										$event_end_date='';
										$event_end_time='';    
										if($themeActive!==false){		      
											$event_start_date = get_post_meta(get_the_ID(), 'event_start_date', true); 
											$event_start_time = get_post_meta(get_the_ID(), 'event_start_time', true); 
	
											$event_end_date = get_post_meta(get_the_ID(), 'event_end_date', true); 
											$event_end_time = get_post_meta(get_the_ID(), 'event_end_time', true);
										}
										/*theme EventBuilder*/
													                                                          
                                        if(bt_checkMasonryStyle($html_style)){                                            
                                        	include('loop/html_masonryflat.php');
										 
                                        }elseif(bt_checkGridBoxStyle($html_style)){
											include('loop/html_grid.php');
											 											 
                                        }elseif(bt_checkCreativeStyle($html_style)){
											include('loop/html_creative.php');
											
										}elseif(bt_checkGridNormalStyle($html_style)){
											include('loop/html_grid_normal.php');
											
										}elseif(bt_checkGridMasonryStyle($html_style)){
											include('loop/html_grid_masonry.php');
											
										}elseif(bt_checkCarouselStyle($html_style)){
											switch($html_style) {
												case 'bt-carousel-1':
													include('loop/html_carousel_1.php');
													break;
												case 'bt-carousel-2':
													include('loop/html_carousel_2.php');
													break;
												case 'bt-carousel-3':
													include('loop/html_carousel_3.php');
													break;
												case 'bt-carousel-4':
													include('loop/html_carousel_4.php');
													break;			
											}
												
                                        }else{
											include('loop/html_classic.php');                               
                                        };										
                                        $intLoopNumber++;
                                    endwhile;
                    if($paged==1 && $isJsonFirst==false) {
						
								if(!bt_checkSliderStyle($html_style) && !bt_checkCarouselStyle($html_style)){
									if($paged_calculator>1)	{
								?>   
                                        <!--load more-->
                                        <div class="bt-load-more-control <?php if($infinite_ajax=='yes'){echo esc_attr('bt-infinite-ajax');}?>">
                                        	<div class="bt-ajax-imagesload"></div>
                                            
                                            <?php if($infinite_ajax=='yes'){?>
                                            	<div class="bt-infinite-data bt-load-more bt-style-button bt-font-main-1" data-paged="1">
                                                	<div class="bt-thumbloading bt-white-div">
                                                        <div class="bt-loader"></div>
                                                    </div>
                                                </div>
                                            <?php }else{?>
                                                <div class="bt-load-more bt-style-button bt-font-main-1" data-paged="1">
                                                    <span>
                                                        <?php echo __('Load More', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?>
                                                    </span>
                                                    <div class="bt-thumbloading">
                                                        <div class="bt-loader"></div>
                                                    </div>
                                                </div>
                                             <?php };?>
                                        </div>
                                        <!--load more-->
                                    <?php };?>
                                        
									</div>
								<?php };?>
                                                                    
                                </div>
                                <!--item-->
                                
                                <?php                    
                                //$totalPages = $the_query->max_num_pages;
                                $currentPaged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                
								if(bt_checkSliderStyle($html_style) || bt_checkCarouselStyle($html_style)){
									for($loopSlides=2;$loopSlides<=$paged_calculator;$loopSlides++){
									?>                    
									<div class="bt-slider-item">
										<div class="bt-slider-item-content" <?php if(is_numeric($html_height) && bt_checkSliderStyle($html_style)) echo 'style="height:'.esc_attr($html_height).'px;"';?> data-html="wait">
											<div class="bt-animate-thumb-overlay <?php if(bt_checkCarouselStyle($html_style)) {echo 'bt-white-div';}?>">
                                            	<div class="bt-loader"></div>
                                            </div>
										</div>                                	
									</div>
									<?php	
									};
								};?>
                            
                            </div>
                            <!--data-->
                            
							<?php
							$styleFilter = 'bt-border-radius'; 
							if(bt_checkSliderStyle($html_style) || bt_checkCarouselStyle($html_style)){
							$styleFilter = '';
							?>
                        
                                <!--Button-->
                                <div class="bt-slider-prev bt-font-main-1" data-change-paged="">
                                    <span></span>
                                </div>
                                <div class="bt-slider-next bt-font-main-1" data-change-paged="">
                                    <span></span>
                                </div>
                                <!--Button-->
                                
                                <?php if($more_button=='yes' && !bt_checkCarouselStyle($html_style) && !bt_checkGridNormalStyle($html_style) && !bt_checkGridMasonryStyle($html_style)){?>
                                <!--Scroll down more-->
                                <div class="bt-scroll-down-more"></div>
                                <!--Scroll down more-->
                                <?php };?>
                            
                        	<?php };
							if($filter_options!='all_posts' && $filter_options!='list_of_ids' && $html_show_filter=='yes' && !bt_checkCarouselStyle($html_style)) {?>                            
                                <!--Open Category Filter-->
                                <div class="bt-open-category-filter <?php echo $styleFilter;?>">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <div class="bt-white-div bt-font-main-1"><?php echo __('Filter', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?></div>
                                </div>
                                <!--Open Category Filter-->
                        	<?php };
							if($combo_grid_list=='yes' && $priority_grid_list!='2' && (bt_checkGridNormalStyle($html_style)||bt_checkGridMasonryStyle($html_style))) {	
							?>
                            	<!--Switch Grid List-->
                                <div class="bt-switch-grid-list">                                	
                                	<div class="bt-switch-grid 
												<?php 	if($cookie_grid_list_combo=='0') {
															echo 'bt-set-active';
														}else{
															if($priority_grid_list!='1' && $cookie_grid_list_combo!='1') {
																echo 'bt-set-active';
															};
														};
												?>" 
                                    data-set-style=""
                                    >
                                    	<span></span>
                                    	<span></span>
                                    </div>
                                    
                                    <div class="bt-switch-list 
												<?php 	if($cookie_grid_list_combo=='1') {
															echo 'bt-set-active';
														}else{
															if($priority_grid_list=='1' && $cookie_grid_list_combo!='0') {
																echo 'bt-set-active';
															};
														};
												?>" 
                                    data-set-style="bt-combo-list-ahead"
                                    >
                                    	<span></span>
                                    	<span></span>
                                    </div>                                    
                                </div>
                                <!--Switch Grid List-->
                            <?php
							};
							if($filter_options!='all_posts' && $filter_options!='list_of_ids' && $html_show_filter=='yes' && bt_checkCarouselStyle($html_style)) {
								$the_query_number=0;								
								$jps_bete_options_query['showCalPosts'] = 1;
								$the_query_number = jps_bete_query($jps_bete_options_query);
								?>
                                	
                                <div class="bt-filter-style-top bt-white-div">
                                    <div class="bt-filter-item-default"><?php echo __('show all', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?> (<?php echo $the_query_number;?>)</div>
                                    <div class="bt-filter-item-sub-list">
                                        <div class="bt-filter-item bt-set-active bt-font-main-1" data-filter-id="<?php echo esc_attr($taxonomies);?>" data-filter-pos="category">
                                            <?php echo __('show all', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?> (<?php echo $the_query_number;?>)
                                        </div>
                                        
                                        <?php
                                        if(isset($taxonomies) && trim($taxonomies)!='') {								
                                            $taxonomies = explode(',', $taxonomies);
                                            
                                            foreach ($taxonomies as $category_item_arr) {	
                                                if(is_numeric(trim($category_item_arr))) {
                                                    $catnameCheck = get_term(trim($category_item_arr), $taxonomy_default);
                                                    if(!is_wp_error($catnameCheck) && !empty($catnameCheck)) {															
                                                        $jps_bete_options_query['taxonomies'] = trim($category_item_arr);
                                                        $jps_bete_options_query['showCalPosts'] = 1;
                                                        $the_query_number = jps_bete_query($jps_bete_options_query);
                                                    ?>
                                                        <div class="bt-filter-item bt-font-main-1" data-filter-id="<?php echo esc_attr(trim($category_item_arr));?>" data-filter-pos="category">
                                                            <?php echo esc_html($catnameCheck->name);?> 
                                                            (<?php echo $the_query_number;?>)
                                                        </div>
                                                    <?php
                                                    };
                                                };
                                            };
                                            
                                        };	
                                        ?>
                                        <div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>
                                    </div>
                                </div>                                                      
                                
                        	<?php };?>        
                            
						</div><!--bt368-nav-ptt-content--> 
                        
                        <?php if($filter_options!='all_posts' && $filter_options!='list_of_ids' && $html_show_filter=='yes' && !bt_checkCarouselStyle($html_style)) {?>   
                        
                            <!--Category filter-->
                            <div class="bt-content-overlay"></div>
                            
                            <div class="bt-canvas-menu <?php if(bt_checkGridNormalStyle($html_style) || bt_checkGridMasonryStyle($html_style)){echo 'bt-white-div';}?>">
                                <div class="bt-close-menu"></div>
                                <div class="bt-filter-all">
                                    <div class="bt-filter-all-content">
                                    
                                    	<?php 	$the_query_number=0;												
												$jps_bete_options_query['showCalPosts'] = 1;
												$the_query_number = jps_bete_query($jps_bete_options_query);
										?>
                                        <div class="bt-h5 bt-filter-heading bt-font-heading-1"><?php echo __('Filter', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?></div>                                            
                                        <div 	class="bt-filter-item bt-set-active bt-font-main-1" 
                                        		data-filter-id="<?php echo esc_attr($taxonomies);?>" 
                                                data-filter-pos="category"><?php echo __('show all', JPS_BETE_SETTING_TEXT_DOMAIN); /*translate*/?> (<?php echo $the_query_number;?>)</div>
                                        
                                        <?php
                                        if(isset($taxonomies) && trim($taxonomies)!='') {								
                                            $taxonomies = explode(',', $taxonomies);
                                            
                                            foreach ($taxonomies as $category_item_arr) {	
                                                if(is_numeric(trim($category_item_arr))) {
                                                    $catnameCheck = get_term(trim($category_item_arr), $taxonomy_default);
                                                    if(!is_wp_error($catnameCheck) && !empty($catnameCheck)) {															
                                                        $jps_bete_options_query['taxonomies'] = trim($category_item_arr);
                                                        $jps_bete_options_query['showCalPosts'] = 1;
                                                        $the_query_number = jps_bete_query($jps_bete_options_query);
                                                    ?>
                                                        <div class="bt-filter-item bt-font-main-1" data-filter-id="<?php echo esc_attr(trim($category_item_arr));?>" data-filter-pos="category">
                                                            <?php echo esc_html($catnameCheck->name);?> 
                                                            (<?php echo $the_query_number;?>)
                                                        </div>
                                                    <?php
                                                    };
                                                };
                                            };
                                            
                                        };	
                                        ?>
                                        	    
                                    </div>
                                </div>
                                
                                <div class="bt-animate-thumb-overlay"><div class="bt-loader"></div></div>
                            </div>                    
                            <!--Category filter-->
                            
                    	<?php };?>        
                        
					</div>
		
					<?php 
					};
					$output_string = ob_get_contents();
					ob_end_clean();
					return $output_string;					
				};
			}else{
				$json_data = array('success'=>'1', 'data_max_pages' => $paged_calculator, 'data_last_posts_count' => $percentItems, 'data_server_params' => $jps_params, 'data_server_height' => $html_height);
				echo json_encode($json_data);
			};			
		else:
			if(!$isJson || $isJsonThumb) {
				if($isJsonThumb) {
					$json_data = array('success'=>'0', 'data_thumb_img' => $html_noimg,);
					echo json_encode($json_data);
				}else{
					return '';
				};								
			}else{
				$json_data = array('success'=>'0', 'data_information' => __('no results were found', JPS_BETE_SETTING_TEXT_DOMAIN)); /*translate*/	
				echo json_encode($json_data);
			};
		endif;
		
		wp_reset_postdata();
		exit;
	};
};

if(!function_exists('jps_bete_ajax')){
	function jps_bete_ajax(){
		if(isset($_POST['p_default'])){
			$explode_params = explode('[|]',$_POST['p_default']);
			
			$style     				= $explode_params[0];
			$filter_options     	= $explode_params[1];
			
			$taxonomies				= $explode_params[2];			
			if(isset($_POST['category']) && $_POST['category']!='') {
				$taxonomies=trim($_POST['category']);
			};
			
			$ids					= $explode_params[3];
			$condition     			= $explode_params[4];			
			$sortby					= $explode_params[5];
			$post_metadata			= $explode_params[6];
			$total_posts     		= $explode_params[7];
			
			$items_per_page			= $explode_params[8];
			$showpost_cal			= 0;
			if(isset($items_per_page) && is_numeric($items_per_page) && isset($_POST['itemsLastPage']) && is_numeric($_POST['itemsLastPage'])) {
				if( ((int)$_POST['itemsLastPage']) < ((int)$items_per_page) && ((int)$_POST['itemsLastPage']) > 0) {
					$showpost_cal = ((int)$_POST['itemsLastPage']);
				};
			};
			
			$height     			= $explode_params[9];
			$background_opt			= $explode_params[10];
			$background_img			= $explode_params[11];
			$parallax				= $explode_params[12];
			$animate				= $explode_params[13];
			$post_read_popup		= $explode_params[14];
			$more_button     		= $explode_params[15];
			$autoplay     			= $explode_params[16];
			$autoplayspeed     		= $explode_params[17];
			$extra_class			= $explode_params[18];
			$infinite_ajax			= $explode_params[19];
			$gutter					= $explode_params[20];
			$show_filter			= $explode_params[21];
			$combo_grid_list		= $explode_params[22];
			$priority_grid_list		= $explode_params[23];
			$show_taxonomy			= $explode_params[24];
			$show_author			= $explode_params[25];
			$show_datetime			= $explode_params[26];
			$show_viewscount		= $explode_params[27];
			$show_commentscount		= $explode_params[28];
			$show_excerpt			= $explode_params[29];
			$post_type				= $explode_params[30];
			$delay_spinner			= $explode_params[31];
			$show_start_date		= $explode_params[32];
			$show_end_date			= $explode_params[33];
			$open_popup_in			= $explode_params[34];
			$main_font				= $explode_params[35];
			$heading_font			= $explode_params[36];
			$s_heading_font			= $explode_params[37];
			$link_color				= $explode_params[38];
			$link_hover_color		= $explode_params[39];
			$light_link_color		= $explode_params[40];
			$light_link_hover_color = $explode_params[41];
			$title_color			= $explode_params[42];
			$title_hover_color		= $explode_params[43];
			$light_title_color		= $explode_params[44];
			$light_title_hover_color= $explode_params[45];
			$metadata_color			= $explode_params[46];
			$light_metadata_color	= $explode_params[47];
			$text_color				= $explode_params[48];
			$thumb_overlay			= $explode_params[49];
			$text_transform			= $explode_params[50];
			$social_facebook		= $explode_params[51];
			$social_twitter			= $explode_params[52];
			$social_google_plus		= $explode_params[53];
			$social_pinterest		= $explode_params[54];
			$social_tumblr			= $explode_params[55];
			$social_linkedin		= $explode_params[56];
			$social_email			= $explode_params[57];
			$under_post_on_grid		= $explode_params[58]; 
			$grid_style_box			= $explode_params[59];
			$grid_style_title_on_img= $explode_params[60];
			$hidden_grid_content	= $explode_params[61];
			$custom_margin			= $explode_params[62];
			$custom_margin_bottom	= $explode_params[63];
			$link_target			= $explode_params[64];
			$creative_custom_style	= $explode_params[65];
			
			$isJson = false;
			if(isset($_POST['isJson']) && $_POST['isJson']=='yes'){
				$isJson = true;
			};
			
			$isJsonFirst = false;
			if(isset($_POST['isJsonFirst']) && $_POST['isJsonFirst']=='yes'){
				$isJsonFirst = true;
			};
			
			$isJsonThumb = false;
			if(isset($_POST['isJsonThumb']) && $_POST['isJsonThumb']=='yes'){
				$isJsonThumb = true;
			};
					
			echo jps_bete_html_data($style, $filter_options, $taxonomies, $ids, $condition, $sortby, $post_metadata, $total_posts, $items_per_page, $height, $background_opt, $background_img, $parallax, $animate, $post_read_popup, $more_button, $autoplay, $autoplayspeed, $extra_class, $infinite_ajax, $gutter, $show_filter, $combo_grid_list, $priority_grid_list, $show_taxonomy, $show_author, $show_datetime, $show_viewscount, $show_commentscount, $show_excerpt, $post_type, $delay_spinner, $show_start_date, $show_end_date, $open_popup_in, $main_font, $heading_font, $s_heading_font, $link_color, $link_hover_color, $light_link_color, $light_link_hover_color, $title_color, $title_hover_color, $light_title_color, $light_title_hover_color, $metadata_color, $light_metadata_color, $text_color, $thumb_overlay, $text_transform,  $social_facebook, $social_twitter, $social_google_plus, $social_pinterest, $social_tumblr, $social_linkedin, $social_email, $under_post_on_grid, $grid_style_box, $grid_style_title_on_img, $hidden_grid_content, $custom_margin, $custom_margin_bottom, $link_target, $creative_custom_style, $_POST['paged'], $isJson, $isJsonFirst, $isJsonThumb, $showpost_cal);
		};
		exit;
	};
};

if(!function_exists('jps_bete_post_ajax')){
	function jps_bete_post_ajax(){		
		if(isset($_POST['post_id']) && is_numeric(trim($_POST['post_id']))){			
			$post_id = trim($_POST['post_id']);
			$default_post_type='post';
			if(isset($_POST['post_type']) && trim($_POST['post_type'])!='') {
				$default_post_type = trim($_POST['post_type']);
			}
			$taxonomy_default = 'category';
			if(isset($_POST['taxonomy']) && trim($_POST['taxonomy'])!='all_posts' && trim($_POST['taxonomy'])!='list_of_ids' && trim($_POST['taxonomy'])!='') {
				$taxonomy_default = trim($_POST['taxonomy']);
			};
			$themeCheckStandardPost = '0';
			if(isset($_POST['tc_eventbuild'])){
				$themeCheckStandardPost = trim($_POST['tc_eventbuild']);
			}
			
			$themeCheckStandardPostStarts = 'no';
			if(isset($_POST['tc_eventbuild_starts'])){
				$themeCheckStandardPostStarts=trim($_POST['tc_eventbuild_starts']);
			}
			$themeCheckStandardPostEnds = 'no';	
			if(isset($_POST['tc_eventbuild_ends'])){
				$themeCheckStandardPostEnds=trim($_POST['tc_eventbuild_ends']);
			}	
			
			$check_social_facebook		='';
			if(isset($_POST['social_facebook'])){
				$check_social_facebook 		= trim($_POST['social_facebook']);
			}
			
			$check_social_twitter		='';
			if(isset($_POST['social_twitter'])){
				$check_social_twitter 		= trim($_POST['social_twitter']);
			}
			
			$check_social_google_plus	='';
			if(isset($_POST['social_google_plus'])){
				$check_social_google_plus 	= trim($_POST['social_google_plus']);
			}
			
			$check_social_pinterest		='';
			if(isset($_POST['social_pinterest'])){
				$check_social_pinterest 	= trim($_POST['social_pinterest']);
			}
			
			$check_social_tumblr 		='';
			if(isset($_POST['social_tumblr'])){
				$check_social_tumblr 		= trim($_POST['social_tumblr']);
			}
			
			$check_social_linkedin		='';
			if(isset($_POST['social_linkedin'])){
				$check_social_linkedin 		= trim($_POST['social_linkedin']);
			}
			
			$check_social_email			='';
			if(isset($_POST['social_email'])){
				$check_social_email 		= trim($_POST['social_email']);
			}
			
			$argsColumnQuery	=	array(
										'p'						=> $post_id,
										'post_type' 			=> $default_post_type,
										'posts_per_page'		=> 1,
										'post_status' 			=> 'publish',	
																		
									);
			$the_query = new WP_Query($argsColumnQuery);
			if ($the_query->have_posts()):
				ob_start();
				
				while ($the_query->have_posts()):$the_query->the_post();	
					$taxonomies_listing = wp_get_post_terms(get_the_ID(), $taxonomy_default, array('fields' => 'all'));		
					/*theme EventBuilder*/
					$event_start_date='';
					$event_start_time='';
					$event_end_date='';
					$event_end_time='';    
					if($themeCheckStandardPost=='1'){		      
						$event_start_date = get_post_meta(get_the_ID(), 'event_start_date', true); 
						$event_start_time = get_post_meta(get_the_ID(), 'event_start_time', true); 

						$event_end_date = get_post_meta(get_the_ID(), 'event_end_date', true); 
						$event_end_time = get_post_meta(get_the_ID(), 'event_end_time', true);
					}
					/*theme EventBuilder*/		
					include('single/html_standard.php');					
                endwhile;
				
				$output_string = ob_get_contents();
				ob_end_clean();
				echo $output_string;	
			else:
				echo '0';
			endif;
			wp_reset_postdata();								
		}else{
			echo '0';
		};
		exit;
	};
};

add_action('wp_ajax_jpsbeteajaxaction', 'jps_bete_ajax');
add_action('wp_ajax_nopriv_jpsbeteajaxaction', 'jps_bete_ajax');

add_action('wp_ajax_jpsbetepostaction', 'jps_bete_post_ajax');
add_action('wp_ajax_nopriv_jpsbetepostaction', 'jps_bete_post_ajax');

if(!function_exists('jps_bete_plugin_setup')){
	function jps_bete_plugin_setup(){		
		add_image_size('bt_mreto_size_1', 960, 675, true);
		add_image_size('bt_mreto_size_1A', 960, 960, true);
		add_image_size('bt_mreto_size_2', 960, 340, true);	
		add_image_size('bt_mreto_size_3', 675, 960, true);
		add_image_size('bt_thumb_size_1', 500, 355, true);
		add_image_size('bt_thumb_size_2', 500, 700, false);		
		add_image_size('bt_thumb_size_3', 360, 240, true);	
					
	};
};	
add_action('after_setup_theme', 'jps_bete_plugin_setup');