<?php

if (!defined( 'ABSPATH')) exit;

if (!defined('JPS_BETE_SETTING_PREFIX')){
    define('JPS_BETE_SETTING_PREFIX', 'jps_pf_');
};

if (!defined('JPS_BETE_SETTING_STATIC')){
    define('JPS_BETE_SETTING_STATIC', '');
};

if (!defined('JPS_BETE_SETTING_TEXT_DOMAIN')){
    define('JPS_BETE_SETTING_TEXT_DOMAIN', 'js_composer');
};

if (!defined('JPS_BETE_SETTING_GROUP')){
    define('JPS_BETE_SETTING_GROUP', 'jsp_ajax_post_layout_settings_group');
};

if (!defined('JPS_BETE_SETTING_NAME')){
    define('JPS_BETE_SETTING_NAME', __( 'JPS Ajax Post Layout Settings', JPS_BETE_SETTING_TEXT_DOMAIN ));
};

if(!class_exists('jps_Ajax_post_layout_settings')) {
	class jps_Ajax_post_layout_settings {
		private $dir;
		private $file;
		private $assets_dir;
		private $assets_url;
		private $settings_base;
		private $settings;
	
		public function __construct( $file ) {
			$this->file = $file;
			$this->dir = dirname( $this->file );
			$this->assets_dir = trailingslashit( $this->dir ) . JPS_BETE_SETTING_STATIC;
			$this->assets_url = esc_url( trailingslashit( plugins_url( '/'.JPS_BETE_SETTING_STATIC.'/', $this->file ) ) );
			$this->settings_base = JPS_BETE_SETTING_PREFIX;
	
			// Initialise settings
			add_action( 'admin_init', array( $this, 'init' ) );
	
			// Register plugin settings
			add_action( 'admin_init' , array( $this, 'register_settings' ) );
	
			// Add settings page to menu
			add_action( 'admin_menu' , array( $this, 'add_menu_item' ) );
	
			// Add settings link to plugins page
			add_filter( 'plugin_action_links_' . plugin_basename( $this->file ) , array( $this, 'add_settings_link' ) );
		}
	
		/**
		 * Initialise settings
		 * @return void
		 */
		public function init() {
			$this->settings = $this->settings_fields();
		}
	
		/**
		 * Add settings page to admin menu
		 * @return void
		 */
		public function add_menu_item() {
			$page = add_menu_page(__(JPS_BETE_SETTING_NAME, JPS_BETE_SETTING_TEXT_DOMAIN), __(JPS_BETE_SETTING_NAME, JPS_BETE_SETTING_TEXT_DOMAIN), 'administrator', $this->file, array( $this, 'settings_page' ), '');
			add_action( 'admin_print_styles-' . $page, array( $this, 'settings_assets' ) );
		}
	
		/**
		 * Load settings JS & CSS
		 * @return void
		 */
		public function settings_assets() {
	
			// We're including the farbtastic script & styles here because they're needed for the colour picker
			// If you're not including a colour picker field then you can leave these calls out as well as the farbtastic dependency for the wpt-admin-js script below
			wp_enqueue_style( 'farbtastic' );
			wp_enqueue_script( 'farbtastic' );
	
			// We're including the WP media scripts here because they're needed for the image upload field
			// If you're not including an image upload then you can leave this function call out
			wp_enqueue_media();
	
			wp_register_script( 'wpt-admin-js', $this->assets_url . 'js/settings.js', array( 'farbtastic', 'jquery' ), '1.0.0' );
			wp_enqueue_script( 'wpt-admin-js' );
		}
	
		/**
		 * Add settings link to plugin list table
		 * @param  array $links Existing links
		 * @return array 		Modified links
		 */
		public function add_settings_link( $links ) {
			$settings_link = '<a href="options-general.php?page='.JPS_BETE_SETTING_GROUP.'">' . __( 'Settings', JPS_BETE_SETTING_TEXT_DOMAIN ) . '</a>';
			array_push( $links, $settings_link );
			return $links;
		}
	
		/**
		 * Build settings fields
		 * @return array Fields to be displayed on settings page
		 */
		private function settings_fields() {
	
			$settings['jps_color_dark'] = array(
				'title'					=> __( '[Dark] Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'description'			=> __( 'For those content to be placed above the image.', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'fields'				=> array(
					array(
						'id' 			=> 'jps_dark_link_color',
						'label'			=> __( '[Dark] Link Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #FFFFFF', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#FFFFFF'
					),
					array(
						'id' 			=> 'jps_dark_link_hover_color',
						'label'			=> __( '[Dark] Link Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #DCDCDC', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#DCDCDC'
					),
					array(
						'id' 			=> 'jps_dark_title_color',
						'label'			=> __( '[Dark] Title Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #FFFFFF', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#FFFFFF'
					),
					array(
						'id' 			=> 'jps_dark_title_hover_color',
						'label'			=> __( '[Dark] Title Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #DCDCDC', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#DCDCDC'
					),
					array(
						'id' 			=> 'jps_dark_metadata_color',
						'label'			=> __( '[Dark] Metadata Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #FFFFFF', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#FFFFFF'
					),	
					array(
						'id' 			=> 'jps_dark_text_color',
						'label'			=> __( 'Text Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #FFFFFF', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#FFFFFF'
					),			
				)
			);
			$settings['jps_color_light'] = array(
				'title'					=> __( '[Light] Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'description'			=> __( 'For the content placed on light background.', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'fields'				=> array(
					array(
						'id' 			=> 'jps_light_link_color',
						'label'			=> __( '[Light] Link Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #0F0F0F', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#0F0F0F'
					),
					array(
						'id' 			=> 'jps_light_link_hover_color',
						'label'			=> __( '[Light] Link Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #999999', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#999999'
					),
					array(
						'id' 			=> 'jps_light_title_color',
						'label'			=> __( '[Light] Title Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #0F0F0F', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#0F0F0F'
					),
					array(
						'id' 			=> 'jps_light_title_hover_color',
						'label'			=> __( '[Light] Title Hover Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #999999', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#999999'
					),
					array(
						'id' 			=> 'jps_light_metadata_color',
						'label'			=> __( '[Light] Metadata Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #878787', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#878787'
					),
					array(
						'id' 			=> 'jps_text_color',
						'label'			=> __( 'Text Color', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: #999999', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'color',
						'default'		=> '#999999'
					),
				)
			);
			$settings['jps_typography'] = array(
				'title'					=> __( 'Typography', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'description'			=> __( 'If you want to add your personal font to your site (from Google Webfonts) you can apply the font parameters in the below fields. Firstly include the font URL that is given in Google Webfonts site, then enter the name of that font (without font-family:) next to that field.', JPS_BETE_SETTING_TEXT_DOMAIN ),
				'fields'				=> array(
					array(
						'id' 			=> 'jps_main_font_url',
						'label'			=> __( 'Main Font URL' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Only Support Google Fonts', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( 'i.e. http://fonts.googleapis.com/css?family=Oswald', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_main_font_name',
						'label'			=> __( 'Main Font Name' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( '', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( "'Oswald', sans-serif", JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_heading_font_url',
						'label'			=> __( 'Heading Font URL' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Only Support Google Fonts', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( 'i.e. http://fonts.googleapis.com/css?family=Oswald', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_heading_font_name',
						'label'			=> __( 'Heading Font Name' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( '', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( "'Oswald', sans-serif", JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_special_font_url',
						'label'			=> __( 'Special Font URL' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Only Support Google Fonts', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( 'i.e. http://fonts.googleapis.com/css?family=Oswald', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_special_font_name',
						'label'			=> __( 'Special Font Name' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( '', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'text',
						'default'		=> '',
						'placeholder'	=> __( "'Oswald', sans-serif", JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_heading_1',
						'label'			=> __( '[H1] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 40px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '40', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_heading_2',
						'label'			=> __( '[H2] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 32px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '32', JPS_BETE_SETTING_TEXT_DOMAIN )
					),	
					array(
						'id' 			=> 'jps_heading_3',
						'label'			=> __( '[H3] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 24px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '24', JPS_BETE_SETTING_TEXT_DOMAIN )
					),	
					array(
						'id' 			=> 'jps_heading_4',
						'label'			=> __( '[H4] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 18px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '18', JPS_BETE_SETTING_TEXT_DOMAIN )
					),	
					array(
						'id' 			=> 'jps_heading_5',
						'label'			=> __( '[H5] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 16px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '16', JPS_BETE_SETTING_TEXT_DOMAIN )
					),	
					array(
						'id' 			=> 'jps_heading_6',
						'label'			=> __( '[H6] Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 14px', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '14', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					array(
						'id' 			=> 'jps_heading_font_weight',
						'label'			=> __( 'Heading Font Weight', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'bold, normal, 300, 400, 500, 600, 700, 800, 900.', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'select',
						'options'		=> array( 'bold' => 'Bold', 'normal' => 'Normal', '300' => '300', '400' => '400', '500' => '500', '600' => '600', '700' => '700', '800' => '800', '900' => '900' ),
						'default'		=> 'bold'
					),
					array(
						'id' 			=> 'jps_main_font_size',
						'label'			=> __( 'Main Font Size' , JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'Default: 14', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'number',
						'default'		=> '',
						'placeholder'	=> __( '14', JPS_BETE_SETTING_TEXT_DOMAIN )
					),
					/*array(
						'id' 			=> 'jps_rtl_setting',
						'label'			=> __( 'RTL Mode', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( 'On/Off RTL Mode.', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'select',
						'options'		=> array( '' => 'OFF', 'on' => 'ON' ),
						'default'		=> ''
					),
					array(
						'id' 			=> 'jps_unicode_bidi',
						'label'			=> __( 'Unicode-bidi', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'description'	=> __( '', JPS_BETE_SETTING_TEXT_DOMAIN ),
						'type'			=> 'select',
						'options'		=> array( 'normal' => 'Normal', 'embed' => 'Embed' ),
						'default'		=> ''
					),*/						
				)
			);
	
			$settings = apply_filters( JPS_BETE_SETTING_GROUP.'_fields', $settings );
	
			return $settings;
		}
	
		/**
		 * Register plugin settings
		 * @return void
		 */
		public function register_settings() {
			if( is_array( $this->settings ) ) {
				foreach( $this->settings as $section => $data ) {
	
					// Add section to page
					add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), JPS_BETE_SETTING_GROUP );
	
					foreach( $data['fields'] as $field ) {
	
						// Validation callback for field
						$validation = '';
						if( isset( $field['callback'] ) ) {
							$validation = $field['callback'];
						}
	
						// Register field
						$option_name = $this->settings_base . $field['id'];
						register_setting( JPS_BETE_SETTING_GROUP, $option_name, $validation );
	
						// Add field to page
						add_settings_field( $field['id'], $field['label'], array( $this, 'display_field' ), JPS_BETE_SETTING_GROUP, $section, array( 'field' => $field ) );
					}
				}
			}
		}
	
		public function settings_section( $section ) {
			$html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
			echo $html;
		}
	
		/**
		 * Generate HTML for displaying fields
		 * @param  array $args Field data
		 * @return void
		 */
		public function display_field( $args ) {
	
			$field = $args['field'];
	
			$html = '';
	
			$option_name = $this->settings_base . $field['id'];
			$option = get_option( $option_name );
	
			$data = '';
			if( isset( $field['default'] ) ) {
				$data = $field['default'];
				if( $option ) {
					$data = $option;
				}
			}
	
			switch( $field['type'] ) {
	
				case 'text':
				case 'password':
				case 'number':
					$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . $field['type'] . '" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value="' . $data . '"/>' . "\n";
				break;
	
				case 'text_secret':
					$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="text" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value=""/>' . "\n";
				break;
	
				case 'textarea':
					$html .= '<textarea id="' . esc_attr( $field['id'] ) . '" rows="5" cols="50" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '">' . $data . '</textarea><br/>'. "\n";
				break;
	
				case 'checkbox':
					$checked = '';
					if( $option && 'on' == $option ){
						$checked = 'checked="checked"';
					}
					$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . $field['type'] . '" name="' . esc_attr( $option_name ) . '" ' . $checked . '/>' . "\n";
				break;
	
				case 'checkbox_multi':
					foreach( $field['options'] as $k => $v ) {
						$checked = false;
						if( in_array( $k, $data ) ) {
							$checked = true;
						}
						$html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '"><input type="checkbox" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '[]" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
					}
				break;
	
				case 'radio':
					foreach( $field['options'] as $k => $v ) {
						$checked = false;
						if( $k == $data ) {
							$checked = true;
						}
						$html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '"><input type="radio" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
					}
				break;
	
				case 'select':
					$html .= '<select name="' . esc_attr( $option_name ) . '" id="' . esc_attr( $field['id'] ) . '">';
					foreach( $field['options'] as $k => $v ) {
						$selected = false;
						if( $k == $data ) {
							$selected = true;
						}
						$html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '">' . $v . '</option>';
					}
					$html .= '</select> ';
				break;
	
				case 'select_multi':
					$html .= '<select name="' . esc_attr( $option_name ) . '[]" id="' . esc_attr( $field['id'] ) . '" multiple="multiple">';
					foreach( $field['options'] as $k => $v ) {
						$selected = false;
						if( in_array( $k, $data ) ) {
							$selected = true;
						}
						$html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '" />' . $v . '</label> ';
					}
					$html .= '</select> ';
				break;
	
				case 'image':
					$image_thumb = '';
					if( $data ) {
						$image_thumb = wp_get_attachment_thumb_url( $data );
					}
					$html .= '<img id="' . $option_name . '_preview" class="image_preview" src="' . $image_thumb . '" /><br/>' . "\n";
					$html .= '<input id="' . $option_name . '_button" type="button" data-uploader_title="' . __( 'Upload an image' , JPS_BETE_SETTING_TEXT_DOMAIN ) . '" data-uploader_button_text="' . __( 'Use image' , JPS_BETE_SETTING_TEXT_DOMAIN ) . '" class="image_upload_button button" value="'. __( 'Upload new image' , JPS_BETE_SETTING_TEXT_DOMAIN ) . '" />' . "\n";
					$html .= '<input id="' . $option_name . '_delete" type="button" class="image_delete_button button" value="'. __( 'Remove image' , JPS_BETE_SETTING_TEXT_DOMAIN ) . '" />' . "\n";
					$html .= '<input id="' . $option_name . '" class="image_data_field" type="hidden" name="' . $option_name . '" value="' . $data . '"/><br/>' . "\n";
				break;
	
				case 'color':
					?><div class="color-picker" style="position:relative;">
						<input type="text" name="<?php esc_attr_e( $option_name ); ?>" class="color" value="<?php esc_attr_e( $data ); ?>" />
						<div style="position:absolute;background:#FFF;z-index:99;border-radius:100%;" class="colorpicker"></div>
					</div>
					<?php
				break;
	
			}
	
			switch( $field['type'] ) {
	
				case 'checkbox_multi':
				case 'radio':
				case 'select_multi':
					$html .= '<br/><span class="description">' . $field['description'] . '</span>';
				break;
	
				default:
					$html .= '<label for="' . esc_attr( $field['id'] ) . '"><span class="description">' . $field['description'] . '</span></label>' . "\n";
				break;
			}
	
			echo $html;
		}
	
		/**
		 * Validate individual settings field
		 * @param  string $data Inputted value
		 * @return string       Validated value
		 */
		public function validate_field( $data ) {
			if( $data && strlen( $data ) > 0 && $data != '' ) {
				$data = urlencode( strtolower( str_replace( ' ' , '-' , $data ) ) );
			}
			return $data;
		}
	
		/**
		 * Load settings page content
		 * @return void
		 */
		public function settings_page() {
	
			// Build page HTML
			$html = '<div class="wrap" id="'.JPS_BETE_SETTING_GROUP.'">' . "\n";
				$html .= '<h2>' . __( JPS_BETE_SETTING_NAME, JPS_BETE_SETTING_TEXT_DOMAIN ) . '</h2>' . "\n";
				$html .= '<form method="post" action="options.php" enctype="multipart/form-data">' . "\n";
	
					// Setup navigation
					$html .= '<ul id="settings-sections" class="subsubsub hide-if-no-js">' . "\n";
						$html .= '<li><a class="tab all current" href="#all">' . __( 'All' , JPS_BETE_SETTING_TEXT_DOMAIN ) . '</a></li>' . "\n";
	
						foreach( $this->settings as $section => $data ) {
							$html .= '<li>| <a class="tab" href="#' . $section . '">' . $data['title'] . '</a></li>' . "\n";
						}
	
					$html .= '</ul>' . "\n";
	
					$html .= '<div class="clear"></div>' . "\n";
	
					// Get settings fields
					ob_start();
					settings_fields( JPS_BETE_SETTING_GROUP );
					do_settings_sections( JPS_BETE_SETTING_GROUP );
					$html .= ob_get_clean();
	
					$html .= '<p class="submit">' . "\n";
						$html .= '<input name="Submit" type="submit" class="button-primary" value="' . esc_attr( __( 'Save Settings' , JPS_BETE_SETTING_TEXT_DOMAIN ) ) . '" />' . "\n";
					$html .= '</p>' . "\n";
				$html .= '</form>' . "\n";
			$html .= '</div>' . "\n";
			$html .= '<script>var '.JPS_BETE_SETTING_GROUP.' = "'.JPS_BETE_SETTING_GROUP.'";</script>' . "\n";
	
			echo $html;
		}
	
	}
}
$jsp_ajax_post_layout_settings = new jps_Ajax_post_layout_settings( __FILE__ );