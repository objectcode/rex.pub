<?php
if(!function_exists('jps_customs_css_footer')) {
	function jps_customs_css_footer(){
		$jps_main_font_url 				= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_main_font_url'));
		$jps_main_font_name 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_main_font_name'));
		
		$jps_heading_font_url 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_font_url'));
		$jps_heading_font_name 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_font_name'));
		
		$jps_special_font_url 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_special_font_url'));
		$jps_special_font_name 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_special_font_name'));
		
		$jps_main_font_size 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_main_font_size'));
		
		$jps_heading_1 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_1'));
		$jps_heading_2 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_2'));
		$jps_heading_3 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_3'));
		$jps_heading_4 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_4'));
		$jps_heading_5 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_5'));
		$jps_heading_6 					= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_6'));
		
		$jps_heading_font_weight		= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_heading_font_weight'));
		
		$jps_dark_link_color 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_link_color'));
		$jps_dark_link_hover_color 		= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_link_hover_color'));
		$jps_dark_title_color 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_title_color'));
		$jps_dark_title_hover_color 	= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_title_hover_color'));
		$jps_dark_metadata_color		= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_metadata_color'));
		$jps_dark_text_color 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_dark_text_color'));
		
		$jps_light_link_color 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_light_link_color'));
		$jps_light_link_hover_color	 	= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_light_link_hover_color'));
		$jps_light_title_color 			= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_light_title_color'));
		$jps_light_title_hover_color	= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_light_title_hover_color'));
		$jps_light_metadata_color 		= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_light_metadata_color'));
		$jps_text_color 				= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_text_color'));
		//$jps_rtl_setting 				= trim(get_option(JPS_BETE_SETTING_PREFIX.'jps_rtl_setting'));
		
	
		if(isset($jps_main_font_url)&&$jps_main_font_url!=''&&isset($jps_main_font_name)&&$jps_main_font_name!=''){
			echo "<link href='".$jps_main_font_url."' rel='stylesheet' type='text/css'>
								<style type='text/css'>
									html body .bt368-nav-ptt[id^=jps_bete_] .bt-font-main-1 {font-family:".$jps_main_font_name.";}
								</style>";
		}
			
		if(isset($jps_heading_font_url)&&$jps_heading_font_url!=''&&isset($jps_heading_font_name)&&$jps_heading_font_name!=''){
			echo "<link href='".$jps_heading_font_url."' rel='stylesheet' type='text/css'>
								<style type='text/css'>
									html body .bt368-nav-ptt[id^=jps_bete_] .bt-font-heading-1 {font-family:".$jps_heading_font_name.";}
								</style>";
		}
			
		if(isset($jps_special_font_url)&&$jps_special_font_url!=''&&isset($jps_special_font_name )&&$jps_special_font_name!=''){
			echo "<link href='".$jps_special_font_url."' rel='stylesheet' type='text/css'>
								<style type='text/css'>
									html body .bt368-nav-ptt[id^=jps_bete_] .bt-font-heading-2 {font-family:".$jps_special_font_name.";}
								</style>";
		}
		
		if(isset($jps_main_font_size)&&$jps_main_font_size!=''&&$jps_main_font_size!='40'&&is_numeric($jps_main_font_size)){
			echo 	'<style type="text/css">
						.bt368-nav-ptt[id^="jps_bete_"],
						.bt368-nav-ptt[id^="jps_bete_"] .bt368-nav-ptt-content .bt-control-wrap.bt-json-slider .slick-dots > *,
						.bt368-nav-ptt[id^="jps_bete_"] .bt368-nav-ptt-content .bt-control-wrap.bt-json-slider .slick-dots button,
						.bt368-nav-ptt[id^="jps_bete_"] .bt-style-button,
						.bt368-nav-ptt[id^="jps_bete_"][data-style*="bt-post-grid-"] .bt-ajax-listing > .bt-post-item,
						.bt368-nav-ptt[id^="jps_bete_"][data-style="bt-creative-1"] .bt-post-item > *						
						 {font-size:'.$jps_main_font_size.'px;}
					</style>';
		}
		
		if(isset($jps_heading_1)&&$jps_heading_1!=''&&$jps_heading_1!='40'&&is_numeric($jps_heading_1)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h1 {font-size:'.$jps_heading_1.'px;}</style>';
		}
		if(isset($jps_heading_2)&&$jps_heading_2!=''&&$jps_heading_2!='32'&&is_numeric($jps_heading_2)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h2 {font-size:'.$jps_heading_2.'px;}</style>';
		}
		if(isset($jps_heading_3)&&$jps_heading_3!=''&&$jps_heading_3!='24'&&is_numeric($jps_heading_3)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h3 {font-size:'.$jps_heading_3.'px;}</style>';
		}
		if(isset($jps_heading_4)&&$jps_heading_4!=''&&$jps_heading_4!='18'&&is_numeric($jps_heading_4)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h4 {font-size:'.$jps_heading_4.'px;}</style>';
		}
		if(isset($jps_heading_5)&&$jps_heading_5!=''&&$jps_heading_5!='16'&&is_numeric($jps_heading_5)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h5 {font-size:'.$jps_heading_5.'px;}</style>';
		}
		if(isset($jps_heading_6)&&$jps_heading_6!=''&&$jps_heading_6!='14'&&is_numeric($jps_heading_6)){
			echo '<style type="text/css">.bt368-nav-ptt[id^=jps_bete_] .bt-h6 {font-size:'.$jps_heading_6.'px;}</style>';
		}
		
		if(isset($jps_heading_font_weight)&&$jps_heading_font_weight!=''&&$jps_heading_font_weight!='bold'){
			echo 	'<style type="text/css">
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h1, 
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h2, 
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h3, 
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h4, 
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h5, 
						.bt368-nav-ptt[id^="jps_bete_"] .bt-h6,
						.bt368-nav-ptt[id^=jps_bete_] .bt-font-heading-1 {font-weight:'.$jps_heading_font_weight.';}
					</style>';
		}
		
		if(isset($jps_dark_link_color)&&$jps_dark_link_color!=''&&$jps_dark_link_color!='#FFFFFF'){
			echo 
			"							
			<style type='text/css'>
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover), 
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):not(:hover):focus,
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:not(:hover),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-filter-item:not(:hover) {color:".$jps_dark_link_color.";}
				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover), 
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover):focus,
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:not(:hover),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:not(:hover) {color:#0F0F0F;}
			</style>
			";
		};
		
		if(isset($jps_dark_link_hover_color)&&$jps_dark_link_hover_color!=''&&$jps_dark_link_hover_color!='#DCDCDC'){
			echo 
			"						
			<style type='text/css'>
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a:not(.bt-color-title):hover,
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-filter-item:hover {color:".$jps_dark_link_hover_color.";}
				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):hover,
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:hover {color:#999999;}
			</style>
			";
		};
		
		if(isset($jps_dark_title_color)&&$jps_dark_title_color!=''&&$jps_dark_title_color!='#FFFFFF'){
			echo 
			"							
			<style type='text/css'>
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover), 
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:not(:hover):focus {color:".$jps_dark_title_color.";}
				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover), 
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover):focus {color:#0F0F0F;}
			</style>
			";
		};
		
		if(isset($jps_dark_title_hover_color)&&$jps_dark_title_hover_color!=''&&$jps_dark_title_hover_color!='#DCDCDC'){
			echo 
			"							
			<style type='text/css'>
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) a.bt-color-title:hover{color:".$jps_dark_title_hover_color.";}
				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:hover {color:#999999;}
			</style>
			";
		};
		
		if(isset($jps_dark_metadata_color)&&$jps_dark_metadata_color!=''&&$jps_dark_metadata_color!='#FFFFFF'){
			echo 
			"							
			<style type='text/css'>
				*:not(.bt-white-div) .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-posted-on > *:not(a),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) *:not(.bt-white-div) .bt-posted-on > *:not(a) {color:".$jps_dark_metadata_color.";}
				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-posted-on > *:not(a),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-posted-on > *:not(a) {color:#878787;}
			</style>
			";
		};
		
		if(isset($jps_light_link_color)&&$jps_light_link_color!=''&&$jps_light_link_color!='#0F0F0F'){
			echo 
			"							
			<style type='text/css'>
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover), 
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):not(:hover):focus,
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:not(:hover),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:not(:hover) {color:".$jps_light_link_color.";}
			</style>
			";
		};
		
		if(isset($jps_light_link_hover_color)&&$jps_light_link_hover_color!=''&&$jps_light_link_hover_color!='#999999'){
			echo 
			"						
			<style type='text/css'>				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a:not(.bt-color-title):hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a:not(.bt-color-title):hover,
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-filter-item:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-filter-item:hover {color:".$jps_light_link_hover_color.";}
			</style>
			";
		};
		
		if(isset($jps_light_title_color)&&$jps_light_title_color!=''&&$jps_light_title_color!='#0F0F0F'){
			echo 
			"							
			<style type='text/css'>				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover), 
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:not(:hover):focus,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover), 
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:not(:hover):focus {color:".$jps_light_title_color.";}
			</style>
			";
		};
		
		if(isset($jps_light_title_hover_color)&&$jps_light_title_hover_color!=''&&$jps_light_title_hover_color!='#999999'){
			echo 
			"							
			<style type='text/css'>				
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) a.bt-color-title:hover,
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div a.bt-color-title:hover {color:".$jps_light_title_hover_color.";}
			</style>
			";
		};
		
		if(isset($jps_light_metadata_color)&&$jps_light_metadata_color!=''&&$jps_light_metadata_color!='#878787'){
			echo 
			"							
			<style type='text/css'>								
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-posted-on > *:not(a),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div .bt-posted-on > *:not(a) {color:".$jps_light_metadata_color.";}
			</style>
			";
		};
		
		if(isset($jps_text_color)&&$jps_text_color!=''&&$jps_text_color!='#999999'){
			echo 
			"							
			<style type='text/css'>								
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) {color:#FFFFFF}
				.bt-white-div .bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) .bt-white-div{color:".$jps_text_color.";}
			</style>
			";
		};
		
		if(isset($jps_dark_text_color)&&$jps_dark_text_color!=''&&$jps_dark_text_color!='#FFFFFF'){
			echo 
			"							
			<style type='text/css'>								
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content),
				.bt368-nav-ptt[id^=jps_bete_]:not(.bt-entry-content) {color:".$jps_dark_text_color.";}
			</style>
			";
		};
		
		/*if(isset($jps_rtl_setting)&&$jps_rtl_setting!=''&&$jps_rtl_setting=='on'){
			echo
			"<style type='text/css'>
				.bt368-nav-ptt {direction:rtl;}
				.bt368-nav-ptt[id^=jps_bete_][data-style*=bt-post-grid-].grid-style-box-nav:not(.bt-combo-list-ahead) .bt-ajax-listing .bt-title-config:after {left:auto; right:0;}
				.bt368-nav-ptt[id^=jps_bete_] .bt-posted-on > * { padding-left:6px; padding-right:26px;}
				.bt368-nav-ptt[id^=jps_bete_] .bt-posted-on > * > [class^=bete-awesome-svg-] { left:auto; right:6px;}
				.bt368-nav-ptt[id^=jps_bete_] .slick-list {direction:ltr;}
				.bt368-nav-ptt[id^=jps_bete_] .bt-slider-item {direction:rtl;}
				
				.bt368-nav-ptt[id^=jps_bete_] .bt368-nav-ptt-content .bt-control-wrap.bt-json-slider .slick-dots {
					left: auto;
					right: 140px;
				}
				@media (max-width: 991px){
					.bt368-nav-ptt[id^=jps_bete_] .bt368-nav-ptt-content .bt-control-wrap.bt-json-slider .slick-dots {
						left: auto;
						right: 30px;
					}
				}
			</style>
			";
		}*/
	}
}

add_action('wp_head', 'jps_customs_css_footer');