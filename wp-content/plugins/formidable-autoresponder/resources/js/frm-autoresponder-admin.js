jQuery( function($){
	$('#frm_notification_settings')
		.on( 'click', 'h3.frm_add_autoresponder_link', function(e){
			e.preventDefault();
			var me = $(this);
			me.fadeOut(function(){
				me.siblings('.frm_autoresponder_rows').fadeIn().find( '.frm-autoresponder-is-active' ).val('1');				
			});
		})
		.on( 'click', '.frm-autoresponder-send-after,.frm-autoresponder-send-after-limit', function(){
			if ( $(this).is( ':checked' ) ) {
				$(this).closest('label').next( '.frm-autoresponder-send-after-meta' ).stop().fadeIn();
			} else {
				$(this).closest('label').next( '.frm-autoresponder-send-after-meta' ).stop().fadeOut();
			}
		})
		.on( 'click', '.frm_remove_autoresponder', function(e){
			e.preventDefault();
			var wrapper = $(this).closest( '.frm_autoresponder_rows' );
			wrapper.find( '.frm-autoresponder-is-active' ).val('');
			wrapper.fadeOut(function(){
				wrapper.siblings('.frm_add_autoresponder_link').fadeIn();
			})
		})
		.on( 'mouseenter', '.frm-autoresponder-trigger-verbage,.frm-autoresponder-trigger-select', function(){
			$(this).closest( 'td' ).find( 'p' ).first().css('background','#ffccff');
		})
		.on( 'mouseleave', '.frm-autoresponder-trigger-verbage,.frm-autoresponder-trigger-select', function(){
			$(this).closest( 'td' ).find( 'p' ).first().css('background','none');
		})
		.on( 'change', '.frm-autoresponder-send-after-interval,.frm-autoresponder-send-after-interval-field', function(){
			if ( $(this).val() ) {
				$(this).prev('input').prop( 'checked', true );
			}
		})
		.on( 'change', '.frm-autoresponder-before-after', function(){
			var date_field = $(this).next( '.frm_autoresponder_date_field' ),
				options = date_field.find( 'option[value="create"],option[value="update"]' );
				
				
			if ( $(this).val() == 'before' ) {
				if ( date_field.val() == 'create' || date_field.val() == 'update' ) {
					date_field.val('');
				}
				options.prop('disabled',true);
			} else {
				options.removeAttr('disabled');
			}
		})
})