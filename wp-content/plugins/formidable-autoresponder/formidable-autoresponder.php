<?php
/*
Plugin Name: Formidable Auto Responder
Description: Allows you to set future notifications on form entries
Version: 1.0.2
Author URI: http://topquark.com
Author: Trevor Mills
*/

/*  Copyright 2015  Top Quark  (email : support@topquark.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
  
require_once __DIR__ . '/classes/controllers/FrmAutoResponderAppController.php'; 
?><?php //BEGIN::SELF_HOSTED_PLUGIN_MOD
					
	/**********************************************
	* The following was added by Self Hosted Plugin
	* to enable automatic updates
	* See http://wordpress.org/extend/plugins/self-hosted-plugins
	**********************************************/
	require "__plugin-updates/plugin-update-checker.class.php";
	$__UpdateChecker = new PluginUpdateChecker('http://topquark.com/extend/plugins/formidable-autoresponder/update', __FILE__,'formidable-autoresponder');			
	

	include_once("__plugin-updates/topquark.settings.php");
	add_action('init',create_function('$a','do_action("register_topquark_plugin","Formidable Autoresponder");'));
//END::SELF_HOSTED_PLUGIN_MOD ?>