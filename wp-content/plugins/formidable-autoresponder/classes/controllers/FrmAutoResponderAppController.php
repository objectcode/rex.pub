<?php

/**
 * FrmAutoResponderAppController
 *
 * This class is the main controller for the Formidable Auto Responder
 * plugin.  Its purpose is to hook into formidable to run the auto responder
 * functionality.
 */
class FrmAutoResponderAppController
{
    /**
     * Initialize everything
     *
     * @return void
     */
    public static function init()
    {
        // Easiest way to detect if Formidable Pro 2.
        if ( !function_exists( 'frm_forms_autoloader' )  ) {
            add_action( 'admin_notices', __CLASS__ . '::incompatible_version' );
            return;
        }

        spl_autoload_register( __CLASS__ . '::autoload' );
        add_action( 'frm_additional_action_settings', __CLASS__ . '::form_action_settings', 10, 2);
        add_action( 'admin_init', __CLASS__ . '::admin_js' );
        add_action( 'frm_after_create_entry', __CLASS__ . '::trigger_create_action', 5, 2 );
        add_action( 'frm_after_update_entry', __CLASS__ . '::trigger_update_action', 5, 2 );
        add_action( 'formidable_send_autoresponder', __CLASS__ . '::send_autoresponder', 10, 2 );

    }

    /**
     * Returns the complete path to this plugin's root directory, possibly to a subudirectory
     *
     * @param string $subpath - will be added to the end of the path
     *
     * @return string the path
     */
    public static function plugin_path( $subpath = '' )
    {
        return trailingslashit( realpath( __DIR__ . '/../../' ) ) . $subpath;
    }

    /**
     * Returns the complete url to this plugin's root directory, possibly to a subudirectory
     *
     * @param string $subpath - will be added to the end of the url
     *
     * @return string the url
     */
    public static function plugin_url( $subpath = '' )
    {
        return plugins_url( "formidable-autoresponder/$subpath" );
    }

    /**
     * Autoloader for Formidable Auto Responder classes
     *
     * @param string $class_name - the class name
     *
     * @return void
     */
    public static function autoload( $class_name )
    {
        // Only load FrmAutoResponder classes here
        if ( ! preg_match('/^FrmAutoResponder.+$/', $class_name) ) {
            return;
        }

        $filepath = self::plugin_path( 'classes' );

        if ( preg_match('/^.+Helper$/', $class_name) ) {
            $filepath .= '/helpers/';
        } else if ( preg_match('/^.+Controller$/', $class_name) ) {
            $filepath .= '/controllers/';
        }

        $filepath .= $class_name .'.php';

        if ( file_exists( $filepath ) ) {
            include_once "$filepath";
        }
    }

    /**
     * Setup and include the settings view
     *
     * @return void
     */
    public static function form_action_settings( $form_action, $atts )
    {
        extract($atts); // gives $form, $action_key

        $fields = $atts['values']['fields'];
        $input_name = $action_control->get_field_name( 'autoresponder' );
        $settings = $form_action->post_content['autoresponder'];
        $is_active = $settings['is_active'];
        include self::plugin_path( 'classes/views/settings.php' );
    }

    /**
     * Enqueue our Javascript
     *
     * @return void
     */
    public static function admin_js()
    {
        $page = FrmAppHelper::simple_get( 'page', 'sanitize_title' );

        if ( strpos( $page, 'formidable' ) === 0 ) {
            $path = 'resources/js/frm-autoresponder-admin.js';
            wp_enqueue_script( 'frm-autoresponder-admin', self::plugin_url( $path ), array( 'formidable_admin' ), filemtime( self::plugin_path( $path ) ), true );
        }
    }

    public static function trigger_create_action( $entry_id, $form_id )
    {
        self::trigger_action( 'create', $entry_id, $form_id );
    }

    public static function trigger_update_action( $entry_id, $form_id )
    {
        self::trigger_action( 'update', $entry_id, $form_id );
    }

    public static function trigger_action( $type, $entry_id, $form_id )
    {
        // Let's see if there are any Autoresponder notifications
        $actions = FrmFormActionsHelper::get_action_for_form( ( $form_id ), 'email' );

        if ( !empty( $actions ) ) {
            foreach ( $actions as $action_id => $action ) {
                wp_clear_scheduled_hook( 'formidable_send_autoresponder', array( $entry_id, $action_id ) );

                if ( isset( $action->post_content['autoresponder'] ) && ( $settings = $action->post_content['autoresponder'] ) && $settings['is_active'] ) {

                    if ( $type == 'create' || ( $type == 'update' && $settings['send_date'] == 'update' ) ) {
                        self::trigger_autoresponder( $entry_id, $action_id );

                        if ( $settings['do_default_trigger'] == 'no' ) {
                            // bit of a kloodge here, but the only ( easiest ) way that I can figure out how to prevent the default action
                            // is to define this constant.  At the beginning of FrmNotification::trigger_email, it returns if this constant is defined.
                            define( 'WP_IMPORTING', true );
                        }
                    }
                }
            }
        }
    }

    public static function trigger_autoresponder( $entry_id, $action_id )
    {
        $action = self::get_action( $action_id );

        if ( empty( $action ) ) {
            return;
        }

        $settings = $action->post_content['autoresponder'];

        if ( empty( $settings ) || !$settings['is_active'] ) {
            return;
        }

        $entry = FrmEntry::getOne( $entry_id, true );

        if ( is_numeric( $settings['send_date'] ) ) {
            // based on a field
            $reference_date = $entry->metas[ $settings['send_date'] ];
        } elseif ( $settings['send_date'] == 'update' ) {
            $reference_date = $entry->updated_at;
        } else {
            $reference_date = $entry->created_at;
        }

        if ( empty( $reference_date ) ) {
            // No date supplied, nothing to do
            return;
        }

        $reference_ts = strtotime( $reference_date );
        $one = ( $settings['send_before_after'] == 'before' ) ? -1 : 1;
        $unit_multiplier = ( $settings['send_unit'] == 'days' ) ? 60 * 60 * 24 : ( $settings['send_unit'] == 'hours' ) ? 60 * 60 : 60;
        $multiplier = $settings['send_interval'];

        $trigger_ts = $reference_ts + ( $one * $multiplier * $unit_multiplier );
        wp_schedule_single_event( $trigger_ts, 'formidable_send_autoresponder', array( $entry_id, $action_id ) );
    }

    public static function send_autoresponder( $entry_id, $action_id )
    {
        $entry = FrmEntry::getOne( $entry_id, true );

        if ( !$entry ) {
            // entry no longer exists, do not send the autoresponder
            return;
        }

        $action = self::get_action( $action_id );
        $settings = $action->post_content['autoresponder'];

        $stop = FrmFormActionsHelper::action_conditions_met($action, $entry);
        if ( !$stop ) {

            $sent_count = get_transient( "frm_autoresponder_sent_count_{$entry_id}" );
            if ( !$sent_count ) {
                $sent_count = 0;
            } else {
                $sent_count = intval( $sent_count );
            }

            if ( !$sent_count || ( $settings['send_after'] && ( !$settings['send_after_limit'] || $sent_count < $settings['send_after_count'] ) ) ) {
                // make sure hooks are loaded
                new FrmNotification();

                // It's the first one, or it's a later one but still allowed according to the send_after settings
                do_action( 'frm_trigger_email_action', $action, $entry, FrmForm::getOne( $entry->form_id ) );

                $sent_count++;
                set_transient( "frm_autoresponder_sent_count_{$entry_id}", $sent_count );

                // If necessary, setup the next event
                if ( $settings['send_after'] ) {
                    if ( !$settings['send_after_limit'] || $sent_count < $settings['send_after_count'] ) {
                        $reference_ts = time();
                        $unit_multiplier = ( $settings['send_after_unit'] == 'days' ) ? 60 * 60 * 24 : ( $settings['send_after_unit'] == 'hours' ) ? 60 * 60 : 60;
                        if ( $settings['send_after_interval_type'] == 'field' ) {
                            $multiplier = $entry->metas[ $settings['send_after_interval_field'] ];
                        } else {
                            $multiplier = $settings['send_after_interval'];
                        }

                        $trigger_ts = $reference_ts + ( $multiplier * $unit_multiplier );
                        wp_schedule_single_event( $trigger_ts, 'formidable_send_autoresponder', array( $entry_id, $action_id ) );
                    }
                }
            }
        }
    }

    public function get_action( $action_id )
    {
        $actions = FrmFormActionsHelper::get_action_for_form( ( $form_id ), 'email' );
        return $actions[ $action_id ];
    }

    public static function incompatible_version()
    {
        ?>
        <div class="updated">
            <p><?php _e( 'Formidable Autoresponder requires that Formidable Pro version 2.0 or greater be installed.  Until then, keep Formidable Autoresponder activated only to continue enjoying this insightful message.', 'formidable-autoresponder' ); ?></p>
        </div>
    <?php
    }
}

add_action( 'init', 'FrmAutoResponderAppController::init' );

