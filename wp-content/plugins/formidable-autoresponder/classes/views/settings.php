<h3 class="frm_add_autoresponder_link <?php echo $is_active ? ' frm_hidden' : ''; ?>" id="autoresponder_link_<?php echo $action_key ?>" ><a href="#" class="frm_add_form_autoresponder" data-emailkey="<?php echo $action_key ?>" id="email_autoresponder_<?php echo $action_key ?>" ><?php _e( 'Setup Autoresponder', 'formidable-autoresponder' ) ?></a></h3>
<div class="frm_autoresponder_rows" <?php echo $is_active ? '' : ' style="display:none"'; ?>>
	<input type="hidden" class="frm-autoresponder-is-active" name="<?php echo $input_name; ?>[is_active]" value="<?php echo $settings['is_active']; ?>" />
    <h3 style="display:inline-block;margin-right:0"><?php _e( 'Auto Responder', 'formidable-autoresponder' ) ?></h3>
	<a href="#" class="frm_icon_font frm_delete_icon frm_remove_autoresponder"> </a>
    <div id="frm_autoresponder_row_<?php echo $action_key ?>">
		<select class="frm-autoresponder-trigger-select" name="<?php echo $input_name; ?>[do_default_trigger]">
			<option value="no" <?php selected( 'no', $settings['do_default_trigger'] ); ?>><?php _e( 'Ignore', 'formidable-autoresponder' ); ?></option>
			<option value="yes" <?php selected( 'yes', $settings['do_default_trigger'] ); ?>><?php _e( 'Respect', 'formidable-autoresponder' ); ?></option>
		</select>	
		<span class="frm-autoresponder-trigger-verbage">
			<?php _e( sprintf( 'the "%s" setting above', '<em>' . __( 'Trigger this action after', 'formidable' ) . '</em>' ), 'formidable-autoresponder' ); ?>
		</span>	
		<br/>
		<?php _e( 'Send this notification', 'formidable-autoresponder' ); ?>
		<input type="number" name="<?php echo $input_name; ?>[send_interval]" style="width:50px;text-align:center" value="<?php echo empty( $settings['send_interval'] ) ? '0' : $settings['send_interval']; ?>" />
		<select name="<?php echo $input_name; ?>[send_unit]">
			<?php foreach ( array( 'days' => __( 'Days', 'formidable-autoresponder' ), 'hours' => __( 'Hours', 'formidable-autoresponder' ), 'minutes' => __( 'Minutes', 'formidable-autoresponder' ) ) as $unit => $label ) : ?>
				<option value="<?php echo $unit; ?>" <?php selected( $settings['send_unit'], $unit ); ?>><?php echo $label; ?></option>
			<?php endforeach; ?>	
		</select>	
		<select class="frm-autoresponder-before-after" name="<?php echo $input_name; ?>[send_before_after]">
			<?php foreach ( array( 'after' => __( 'After', 'formidable-autoresponder' ), 'before' => __( 'Before', 'formidable-autoresponder' ) ) as $unit => $label ) : ?>
				<option value="<?php echo $unit; ?>" <?php selected( $settings['send_before_after'], $unit ); ?>><?php echo $label; ?></option>
			<?php endforeach; ?>	
		</select>	
		<select class="frm_autoresponder_date_field" name="<?php echo $input_name; ?>[send_date]">
		    <option value=""><?php echo '&mdash; ' . __( 'Select Field', 'formidable-autoresponder' ) .  ' &mdash;' ?></option>
		    <option value="create" <?php selected( 'create', $settings['send_date'] ); ?>><?php _e( 'Create Date' ) ?></option>
		    <option value="update" <?php selected( 'update', $settings['send_date'] ); ?>><?php _e( 'Update Date' ) ?></option>
			<?php foreach ( $fields as $field ) : if ( $field['type'] != 'date' ) continue; ?>
				<option value="<?php echo $field['id']; ?>" <?php selected( $settings['send_date'], $field['id'] ); ?>><?php echo $field['name']; ?></option>
			<?php endforeach; ?>	
		</select><br/>
		<label style="padding:8px 0;display:inline-block">
			<input type="checkbox" class="frm-autoresponder-send-after" name="<?php echo $input_name; ?>[send_after]" value="1" <?php checked( 1, $settings['send_after'] ); ?> />
			<?php _e( '...and then every', 'formidable-autoresponder' ); ?> 
		</label>	
		<span class="frm-autoresponder-send-after-meta" style="<?php echo $settings['send_after'] ? '' : 'display:none'; ?>">
			<?php
			$has_number_field = false;
			foreach ( $fields as $field ){
				if ( $field['type'] == 'number' ) {
					$has_number_field = true;
					break;
				}
			}
			?>
			<?php if ( $has_number_field ) : ?>
				<input type="radio" name="<?php echo $input_name; ?>[send_after_interval_type]" value="number" <?php checked( true, empty( $settings['send_after_interval_type'] ) || ( 'number' == $settings['send_after_interval_type'] ) ); ?> />
				<input type="number" class="frm-autoresponder-send-after-interval" name="<?php echo $input_name; ?>[send_after_interval]" style="width:50px;text-align:center" value="<?php echo ( empty( $settings['send_after_interval'] ) || ( $settings['send_after_interval_type'] == 'field' ) ) ? '0' : $settings['send_after_interval']; ?>" />
				or
				<input type="radio" name="<?php echo $input_name; ?>[send_after_interval_type]" value="field" <?php checked( 'field', $settings['send_after_interval_type'] ); ?> />
				<select class="frm-autoresponder-send-after-interval-field" name="<?php echo $input_name; ?>[send_after_interval_field]">
				    <option value=""><?php echo '&mdash; ' . __( 'Select Field', 'formidable-autoresponder' ) .  ' &mdash;' ?></option>
					<?php foreach ( $fields as $field ) : if ( $field['type'] != 'number' ) continue; ?>
						<option value="<?php echo $field['id']; ?>" <?php selected( true, $settings['send_after_interval_type'] == 'field' && $settings['send_after_interval_field'] == $field['id'] ); ?>><?php echo $field['name']; ?></option>
					<?php endforeach; ?>	
				</select>
			<?php else : ?>	
				<input type="hidden" name="<?php echo $input_name; ?>[send_after_interval_type]" value="number" <?php checked( true, empty( $settings['send_after_interval_type'] ) || ( 'number' == $settings['send_after_interval_type'] ) ); ?> />
				<input type="number" class="frm-autoresponder-send-after-interval" name="<?php echo $input_name; ?>[send_after_interval]" style="width:50px;text-align:center" value="<?php echo empty( $settings['send_after_interval'] ) ? '0' : $settings['send_after_interval']; ?>" />
			<?php endif; ?>	
			<select name="<?php echo $input_name; ?>[send_after_unit]">
				<?php foreach ( array( 'days' => __( 'Days', 'formidable-autoresponder' ), 'hours' => __( 'Hours', 'formidable-autoresponder' ), 'minutes' => __( 'Minutes', 'formidable-autoresponder' ) ) as $unit => $label ) : ?>
					<option value="<?php echo $unit; ?>" <?php selected( $settings['send_after_unit'], $unit ); ?>><?php echo $label; ?></option>
				<?php endforeach; ?>	
			</select>	
			<?php _e( 'after that', 'formidable-autoresponder' ); ?><br/>
			<label style="padding:8px 0;display:inline-block">
				<input type="checkbox" class="frm-autoresponder-send-after-limit" name="<?php echo $input_name; ?>[send_after_limit]" value="1" <?php checked( 1, $settings['send_after_limit'] ); ?> />
				<?php _e( '...a maximum of', 'formidable-autoresponder' ); ?> 
			</label>	
			<span class="frm-autoresponder-send-after-meta" style="<?php echo $settings['send_after_limit'] ? '' : 'display:none'; ?>">
				<input type="number" name="<?php echo $input_name; ?>[send_after_count]" style="width:50px;text-align:center" value="<?php echo empty( $settings['send_after_count'] ) ? '1' : $settings['send_after_count']; ?>" />
				<?php _e( 'times', 'formidable-autoresponder' ); ?>
			</span>	
		</span>	
    </div>
</div>
