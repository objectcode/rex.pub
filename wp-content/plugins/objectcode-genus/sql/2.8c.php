DELETE FROM wp_usermeta WHERE user_id = 1;
DELETE FROM wp_usermeta WHERE user_id = 101;
DELETE FROM wp_usermeta WHERE user_id = 102;
DELETE FROM wp_usermeta WHERE user_id = 103;
DELETE FROM wp_usermeta WHERE user_id = 104;
DELETE FROM wp_usermeta WHERE user_id = 203;
DELETE FROM wp_usermeta WHERE user_id = 204;
DELETE FROM wp_usermeta WHERE user_id = 205;

REPLACE INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'rex', '$P$Byam0HM9M9lAXOgy9FUATNq5wgIdFS1', 'rex', 'cho@objectco.org', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0, 'Cho [systems admin]'),
(102, 'largetest', '$P$Byam0HM9M9lAXOgy9FUATNq5wgIdFS1', 'wstest', 'fromlargemarketing@gmail.com', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0, 'John Doe [admin]'),
(103, 'ken', '$yam0HM9M9lAXOgy9FUATNq5wgIdFS', 'kenhammons', 'ken1@objectco.org', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0,'Ken  [raptor]'),
(203, 'denise', '$P$Byam0HM9M9lAXOgy9FUATNq5wgIdFS1', 'denise', 'denise@objectco.org', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0,'Denise [deinonychus]'),
(204, 'lau1ra', '$P$Byam0HM9M9lAXOgy9FUATNq5wgIdFS11', 'lau1ra', 'laura1@objectco.org', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0,'Laura [gallimimus]'),
(205, 'zachary', '$P$Byam0HM9M9lAXOgy9FUATNq5wgIdFS1', 'zachary', 'zachary@objectco.org', 'http://www.large.marketing', '2015-01-30 13:17:00', '', 0,'zachary [megaladon]');

REPLACE INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
('', 1, 'first_name', 'Chris'),
('', 1, 'last_name', 'H.'),
('', 1, 'nickname', 'Chris H.'),
('', 1, 'description', 'Host Admin'),
('', 1, 'rich_editing', 'true'),
('', 1, 'comment_shortcuts', 'false'),
('', 1, 'admin_color', 'fresh'),
('', 1, 'use_ssl', '0'),
('', 1, 'show_admin_bar_front', 'false'),
('', 1, 'show_welcome_panel', '0'),
('', 1, 'custom_login_ignore_announcement', '1'),
('', 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
('', 1, 'tgmpa_dismissed_notice', '1'),
('', 1, 'wp_capabilities', 'a:12:{s:13:"administrator";b:1;s:14:"frm_view_forms";b:1;s:14:"frm_edit_forms";b:1;s:16:"frm_delete_forms";b:1;s:19:"frm_change_settings";b:1;s:16:"frm_view_entries";b:1;s:18:"frm_create_entries";b:1;s:16:"frm_edit_entries";b:1;s:18:"frm_delete_entries";b:1;s:16:"frm_view_reports";b:1;s:17:"frm_edit_displays";b:1;s:13:"bbp_keymaster";b:1;}'),

('',  102, 'nickname', 'largetest'),
('',  102, 'first_name', 'Test'),
('',  102, 'last_name', 'Account'),
('',  102, 'description', 'Test Account'),
('',  102, 'rich_editing', 'true'),
('',  102, 'use_ssl', '0'),
('',  102, 'wp_user_level', '7'),
('',  102, 'show_welcome_panel', '0'),
('',  102, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
('',  102, 'show_welcome_panel', '0'),
('',  102, 'custom_login_ignore_announcement', '1'),
('',  102, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
('',  102, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
('',  102, 'metaboxhidden_dashboard', 'a:0:{}'),
('',  102, 'tgmpa_dismissed_notice', '1'),
('',  102, 'wp_capabilities', 'a:1:{s:6:"editor";b:1;}'),

('', 103, 'first_name', 'Ken'),
('', 103, 'last_name', 'H.'),
('', 103, 'nickname', 'Ken H.'),
('', 103, 'comment_shortcuts', 'false'),
('', 103, 'admin_color', 'fresh'),
('', 103, 'use_ssl', '0'),
('', 103, 'show_admin_bar_front', 'false'),
('', 103, 'wp_user_level', '10'),
('', 103, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
('', 103, 'show_welcome_panel', '0'),
('', 103, 'custom_login_ignore_announcement', '1'),
('', 103, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:89:"gadash-widget,custom_help_widget,dashboard_activity,dashboard_right_now,dashboard_primary";s:4:"side";s:21:"dashboard_quick_press";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}
'),
('', 103, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
('', 103, 'metaboxhidden_dashboard', 'a:0:{}'),
('', 103, 'tgmpa_dismissed_notice', '1'),
('', 103, 'wp_capabilities', 'a:12:{s:13:"administrator";b:1;s:14:"frm_view_forms";b:1;s:14:"frm_edit_forms";b:1;s:16:"frm_delete_forms";b:1;s:19:"frm_change_settings";b:1;s:16:"frm_view_entries";b:1;s:18:"frm_create_entries";b:1;s:16:"frm_edit_entries";b:1;s:18:"frm_delete_entries";b:1;s:16:"frm_view_reports";b:1;s:17:"frm_edit_displays";b:1;s:13:"bbp_keymaster";b:1;}'),

('', 203, 'first_name', 'Denise'),
('', 203, 'last_name', 'E.'),
('', 203, 'nickname', 'Denise'),
('', 203, 'comment_shortcuts', 'false'),
('', 203, 'admin_color', 'fresh'),
('', 203, 'use_ssl', '0'),
('', 203, 'show_admin_bar_front', 'false'),
('', 203, 'wp_user_level', '10'),
('', 203, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
('', 203, 'show_welcome_panel', '0'),
('', 203, 'custom_login_ignore_announcement', '1'),
('', 203, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:89:"gadash-widget,custom_help_widget,dashboard_activity,dashboard_right_now,dashboard_primary";s:4:"side";s:21:"dashboard_quick_press";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}
'),
('', 203, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
('', 203, 'tgmpa_dismissed_notice', '1'),
('', 203, 'wp_capabilities', 'a:12:{s:13:"administrator";b:1;s:14:"frm_view_forms";b:1;s:14:"frm_edit_forms";b:1;s:16:"frm_delete_forms";b:1;s:19:"frm_change_settings";b:1;s:16:"frm_view_entries";b:1;s:18:"frm_create_entries";b:1;s:16:"frm_edit_entries";b:1;s:18:"frm_delete_entries";b:1;s:16:"frm_view_reports";b:1;s:17:"frm_edit_displays";b:1;s:13:"bbp_keymaster";b:1;}'),

('', 204, 'first_name', 'Laura'),
('', 204, 'last_name', 'S.'),
('', 204, 'nickname', 'Laura'),
('', 204, 'comment_shortcuts', 'false'),
('', 204, 'admin_color', 'fresh'),
('', 204, 'use_ssl', '0'),
('', 204, 'show_admin_bar_front', 'false'),
('', 204, 'wp_user_level', '10'),
('', 204, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
('', 204, 'show_welcome_panel', '0'),
('', 204, 'custom_login_ignore_announcement', '1'),
('', 204, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:89:"gadash-widget,custom_help_widget,dashboard_activity,dashboard_right_now,dashboard_primary";s:4:"side";s:21:"dashboard_quick_press";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}
'),
('', 204, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
('', 204, 'metaboxhidden_dashboard', 'a:0:{}'),
('', 204, 'tgmpa_dismissed_notice', '1'),
('', 204, 'wp_capabilities', 'a:12:{s:13:"administrator";b:1;s:14:"frm_view_forms";b:1;s:14:"frm_edit_forms";b:1;s:16:"frm_delete_forms";b:1;s:19:"frm_change_settings";b:1;s:16:"frm_view_entries";b:1;s:18:"frm_create_entries";b:1;s:16:"frm_edit_entries";b:1;s:18:"frm_delete_entries";b:1;s:16:"frm_view_reports";b:1;s:17:"frm_edit_displays";b:1;s:13:"bbp_keymaster";b:1;}'),

('', 205, 'first_name', 'Zachary'),
('', 205, 'last_name', 'B.'),
('', 205, 'nickname', 'Zachary'),
('', 205, 'comment_shortcuts', 'false'),
('', 205, 'admin_color', 'fresh'),
('', 205, 'use_ssl', '0'),
('', 205, 'show_admin_bar_front', 'false'),
('', 205, 'wp_user_level', '10'),
('', 205, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
('', 205, 'show_welcome_panel', '0'),
('', 205, 'custom_login_ignore_announcement', '1'),
('', 205, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:89:"gadash-widget,custom_help_widget,dashboard_activity,dashboard_right_now,dashboard_primary";s:4:"side";s:21:"dashboard_quick_press";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}
'),
('', 205, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
('', 205, 'tgmpa_dismissed_notice', '1'),
('', 205, 'wp_capabilities', 'a:12:{s:13:"administrator";b:1;s:14:"frm_view_forms";b:1;s:14:"frm_edit_forms";b:1;s:16:"frm_delete_forms";b:1;s:19:"frm_change_settings";b:1;s:16:"frm_view_entries";b:1;s:18:"frm_create_entries";b:1;s:16:"frm_edit_entries";b:1;s:18:"frm_delete_entries";b:1;s:16:"frm_view_reports";b:1;s:17:"frm_edit_displays";b:1;s:13:"bbp_keymaster";b:1;}');

