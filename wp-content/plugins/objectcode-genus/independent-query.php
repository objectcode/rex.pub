<?php
/******************************************************
 * Objectcode Independent Query Processor 
 * - Runs SQL across our entire network
 * - Designed to speed up mass permissions changes
 * - Designed to aid the installation of plugins
 * - Designed to rapidly deploy settings changes
 * 
 * @author: c. hogan for objectcode ltd
 ******************************************************/

/*
 * debug if defined 
 *
 */

  //define('OC_DEBUG_QUERY_VERBOSE', TRUE);
  //define('OC_NO_ACTIVATIONS', TRUE);

  require SITEHOME. "/wp-load.php";
  require SITEHOME. "/wp-admin/includes/admin.php";


global $wpdb;
$sql = "2.8c.php";

$w = getcwd();
$filename = $w."/sql/".$sql;

if (preg_match('/independent/',$_SERVER['REQUEST_URI'])) exit;  // security 

if (file_exists($filename)) {

    $templine = '';
    // Read in entire file
    $lines = file($filename);

    // Loop through each line
    foreach ($lines as $line)
    {
    // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;

    // Add this line to the current segment
    $templine .= $line;
    // If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';')
    {
        // Perform the query

        $s = $wpdb->query($templine);
        //mysql_query($templine) or print('Error performing query : ' . mysql_error() );
        // Reset temp variable to empty
        $templine = '';
    }
    }
    if (defined('OC_DEBUG_QUERY_VERBOSE'))
    echo "[OK] Independent Query ";

} else {

   if (defined('OC_DEBUG_QUERY_VERBOSE'))  
   echo "\n [!] Couldn't find the file: ".$filename;
}
