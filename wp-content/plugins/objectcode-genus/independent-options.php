<?php
/*******************************************
 *  Objectcode Independent
 *  - Options
 *  - Pull down and PREG
 *  - Useful for RB Launching
 *  @author: c.hogan for objectcode ltd
 *****************************************/


/*
 * debug if defined 
 *
 */

  //define('OC_DEBUG_SHELL', TRUE);
  //define('OC_DEBUG_VERBOSE', TRUE);

/*
 * respository settings
 *
 */

  // this is the repository database for 2.8C Objectcode
  define('SECOND_DB_NAME', 'rds_lar2_wp1');
  define('SECOND_DB_USER', 'rds_lar2');
  define('SECOND_DB_PASSWORD', '1d..2ja.asd-292sx.sa');
  define('SECOND_DB_HOST', 'wbspkdb-small.cdbkzbw6jymn.us-west-1.rds.amazonaws.com');
  define('HOME_DIR_BASE_PATH', '/home');
  define('TEST_WEBSITE_PATH', '/large.marketing/test.large.marketing');


// prevent arbitrary front-end execution of this script
if (isset($_SERVER['REQUEST_URI']) && preg_match('/independent/',$_SERVER['REQUEST_URI'])) exit; 

/* 
 *  [sites that are secure]
 *
 *  @desc: this is a list of secure sites that use our framework, since we mandate secure URLs
 */
$secure = array(
   'nwneuro.com',
   'smilemoredentalaz.com',
   'oveyecare.com',
);

/*
 *  [settings targets]
 *
 *  @desc: these are the specific settings in the wp_options table we target to move across the objectcode site network
 */
$settings = array(
   'ws_menu_editor_pro',
   'active_plugins',
   'ultimate_google_fonts',
   //'hefo',
   'clu_config',
   'login_page_url_ws',
   'hide_login_slug',
   'htaccess_rules',
   'smile_fonts',
   'ultimate_selected_google_fonts',
   'ultraadmin_',
   'ultraadmin_menuorder',
   'ultraadmin_submenuorder',
   'ultraadmin_menurename',
   'ultraadmin_submenurename',
   'ultraadmin_menudisable',
   'ultraadmin_submenudisable',
   'ultraadmin_plugin_access',
   'ultraadmin_plugin_page',
   'ultraadmin_plugin_userid',
   'ultraadmin_menumng_page',
   'ultraadmin_admin_menumng_page',
   'ultraadmin_admintheme_page',
   'ultraadmin_logintheme_page',
   'ultraadmin_master_theme',
   'ultra_demo',
   'admin_email',
   'smtp_auth',
   'smtp_host',
   'smtp_pass',
   'smtp_ssl',
   'smtp_user',
   'current_theme',
   'theme_switched',
   'wp_user_roles',
   'template',
   'stylesheet',
   'frmpro_options',       // this copies the formidable settings, except it should ignore the API Keys for reCaptcha
   //'theme_mods_lmg',     // note that this option determines the navigations so over-riding it on older upgrades causes havoc till they are replaced
   //'theme_mods_lmg3'
);

/*
 *  [staging]
 *
 *  Objectcode RTD Staging - Plugin Activation Detection
 *  @desc: if the base tweaks package detects the $staging (array)
 *  then it will automatically merge the two.  Thus the staging plugin will
 *  pull from the repository for certain values, while only doing it on one site.
 */
if (is_array($staging)) {
   $settings = array_merge($settings, $staging);
}

require SITEHOME. "/wp-load.php";
require SITEHOME. "/wp-admin/includes/admin.php";

/*
 * SITEHOME - Verifier
 *
 * check to see if SITEHOME is defined, as this is what is passed from the update.sh script.
 * it is also pre-defined from any site trying to update itself from the repository automatically
 * a plugin that does that sort of thing is for example: Objectcode RTD - Make it Staging.
 */

if (defined('SITEHOME')) {

  // connect to the repo and share the connection for speed
  $tdb = new wpdb(SECOND_DB_USER, SECOND_DB_PASSWORD, SECOND_DB_NAME, SECOND_DB_HOST);

  // get the repository options and pull them down one by one
  gather_repo_settings();

  // get the individual sites and pull their data and filter it
  get_site();

}


/*
 * FUNCTIONS
 */

  function get_repo_record($val) {
    global $tdb;
    $row = $tdb->get_row("SELECT * from wp_options where option_name = '$val' LIMIT 1");
    return $row->option_value; // this just returns the option value, like wordpress is supposed to stop being an asshat about not doing on other dbs
  }

  function get_repo_row($val, &$repo) {
    $repo[$val] = get_repo_record($val);
  }

  function gather_repo_settings() {
    global $repo, $settings;

    if (defined('OC_DEBUG_SHELL'))
    echo "SELECT option_id, option_name, option_value FROM wp_options WHERE ";

    foreach ($settings as $val)  {
      if (defined('OC_DEBUG_SHELL'))
      echo "option_name = '$val' OR ";

      get_repo_row($val, $repo);
    }
  }

  function update_site_setting($key, $val) {
    global $wpdb;
    $wpdb->query( $wpdb->prepare( 
      "UPDATE wp_options SET option_value = '%s', autoload = 'yes' WHERE option_name = '%s' LIMIT 1", 
         array(
           $val, $key
         ) 
    ) );
  }

  function get_site_setting($val) {
    global $wpdb;
    $row = $wpdb->get_row("SELECT * from wp_options where option_name = '$val' LIMIT 1");
    return $row->option_value;
  }

  function get_site() {
    global $wpdb, $repo, $secure, $domain_from_path;    

    // dev note:  SITEHOME comes from the Root/ Sudo update.sh shell script and is not defined explicitly 
    $d = defined('SITEHOME') ? SITEHOME : HOME_DIR_BASE_PATH.'/'. TEST_WEBSITE_PATH;
    $e = explode('/', $d);
    if (!is_array($e)) return;
    $domain = (isset($e[3]) && substr_count($e[3], '.') > 1) ? $e[3] : "www." . $e[2];
    $is_staging = (isset($e[3]) && substr_count($e[3], '.') > 1) ? true : false;
    $ssl = preg_match('/'.implode("|",$secure).'/', $domain) ? 'https://' : 'http://';

    // create the domain from the filesystem path while accounting for different home dirs 
    $domain_from_path = $ssl . $domain . '/';
    
    if (defined('OC_DEBUG_SHELL'))
    echo ' [URL] ' . $domain_from_path . ' [DEBUG ON] ';  

    // salient update with fixed urls 
    $salient = unserialize(get_site_setting( 'salient' ));
    $active  = get_site_setting( 'active_plugins' ); 
    $formid  = unserialize(get_site_setting( 'frmpro_options') ); 
      
 
    // salient: fix any URLs that do NOT point to the site contained in $domain_from_path
    array_walk( $salient, "change_url" );
    array_walk( $salient, "kill_webspeak_in_arrays" );

    update_option( 'salient', $salient);
    
    $menu = unserialize($repo['ws_menu_editor_pro']);
    $ultra = unserialize($repo['ultra_demo']); 

    // keep the recaptcha key from the site formidable settings but replace everything else
    $formidable = unserialize($repo['frmpro_options']);
    $formidable['privkey'] = $formid['privkey'];
    $formidable['pubkey'] = $formid['pubkey'];
 
    // fix web speak references
    array_walk( $menu, "kill_webspeak_in_arrays" );
    array_walk( $ultra, "kill_webspeak_in_arrays" );

    // change all urls to more fashionable ones if you so desire
    //array_walk( $menu, "change_url" );
    array_walk( $ultra, "change_url" );  // this really wasn't neccessary


    if (defined('OC_DEBUG_VERBOSE')) {
        print_r($salient); // theme debug for most common theme settings
        //print_r($ultra);
        //print_r($menu); // this one is incredibly fucking long so beware on debug
    }
     
     // update the ultra admin and menu editor, and formidable pro options
     update_option( 'ws_menu_editor_pro', $menu);
     update_option( 'ultra_demo', $ultra);
     update_option( 'frmpro_options', $formidable);

     // if any theme other than salient keep it
     $theme = get_site_setting( 'current_theme' );
     if ($theme != "Salient") {
        unset($repo['current_theme']);
        unset($repo['template']);
        unset($repo['stylesheet']);
     }
     // unset all the repo vaues that have already been modified
     unset($repo['salient']);
     //unset($repo['ws_menu_editor_pro']);
     //unset($repo['ultra_demo']);

     // loop through all the other repository settings and update them to this website
     foreach($repo as $k => $v)  {
         
        update_site_setting($k, $v);
     }
     update_site_setting( 'current_theme', $theme );

     //-------------------------------------------------------------------
     // SPECIES - Activations for special packages
     //------------------------------------------------------------------- 
     if (preg_match('/rex/', $active)) { 
        $active_repo[] = 'objectcode-rex/objectcode.php';
        $active_repo[] = 'advanced-custom-fields-pro/acf.php';
        $active_repo[] = 'bbpress/bbpress.php';
        $active_repo[] = 'buddypress/bp-loader.php';
        $active_repo[] = 'enhanced-media-library/enhanced-media-library.php';
        $active_repo[] = 'ewww-image-optimizer/ewww-image-optimizer.php';
        $active_repo[] = 'formidable-geocoding-master/frm-google-map.php';
     }

     if (preg_match('/stega/', $active)) { 
        $active_repo[] = 'objectcode-stega/objectcode.php';
        $active_repo[] = 'objectcode-armor/objectcode.php';
     }

     //-------------------------------------------------------------------
     // FOSSILS - Deprecated, but may be revived like in Jurassic Park
     //------------------------------------------------------------------- 
     $active_repo = unserialize($repo['active_plugins']);

     if (preg_match('/woocommerce/', $active)) { 
        $active_repo[] = 'woocommerce/woocommerce.php';
     }
     if (preg_match('/revslider/', $active)) {
        $active_repo[] = 'revslider/revslider.php';
     }
     if (preg_match('/reviewer/', $active)) {
        $active_repo[] = 'reviewer/reviewer.php';
     }
     if (preg_match('/livicons/', $active)) {
        $active_repo[] = 'livicons-shortcodes/livicons-shortcodes.php';
     }
     if (preg_match('/comments/', $active)) {
        $active_repo[] = 'disable-comments/disable-comments.php';
     }

     //------------------------------------------------------------------
     // ENVIRONMENTS - Plugins used on dev, staging, prod
     //------------------------------------------------------------------     
     if (preg_match('/w2dc/', $active)) { 
        $active_repo[] = 'directoryp/w2dc.php';
     }
     if (preg_match('/password/', $active)) { 
        $active_repo[] = 'password-protected/password-protected.php';
     }
     // update the active plugins but re-enable any fossils that were left around    
     update_site_setting('active_plugins', serialize($active_repo));

  }


  function change_url(&$var) { 
    global $domain_from_path; if (is_array($var)) { array_Walk( $var, "change_url"); return; }
    if (preg_match('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,15}?/', $var)) {
        if (!preg_match('/patents|uspto|facebook|twitter|google|vimeo|dribble|pinterest|youtube|tumblr|rss|behance|flickr|spotify|instagram|github|stackexchange|soundcloud/', $var))
        $var = preg_replace('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,15}(\/)?/', $domain_from_path, $var);
    } else $var = $var; 
  }

  function kill_webspeak_in_arrays(&$var) {
    $var = str_replace(
       array("webspeak-admin.php","webspeakllc","www.wbspk.com","template.wbspk.com","Web Speak","nik@wbspk.com", "chris@wbspk.com", "http://28s.wbspk.com/"),
       array("large.php","rex","www.large.marketing","28c.large.marketing","Large Marketing Group","chris@large.marketing", "chris@large.marketing", "/"),
      $var
    );
  }

echo " [OK] Independent Options ";

