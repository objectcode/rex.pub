<?php
/* Hash Theme Names
 *
 * This is a shell script that outputs hashes for all the default theme names in '../../themes.upload'
 * All the default themes are the stock directory names that come with the themes
 * This plugin then loads these themes into '../../themes/' and hashes the correct values as directory names
 * 
 * @since 2.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */

$themes= scandir('../../themes.upload');

foreach ($themes as $k=>$themename) {

   if ($themename !== '.' && $themename !== '..' && !preg_match('/child/', $themename))
   {
      $themesha1 = sha1($themename);
      $themehash[$themename] = $themesha1;
      rename('../../themes.upload/'.$themename, '../../themes/'.$themesha1);
   }
}

echo "\n All uploaded themes have been hashed and moved.";

$themes= scandir('../../themes.upload');

foreach ($themes as $childtheme) {
   if ($childtheme !== '.' && $childtheme !== '..') {
     $childhash = sha1(str_replace('-child','', $childtheme)). '-child';
     $themehash[$childtheme] = $childhash;
     rename('../../themes.upload/'.$childtheme, '../../themes/'.$childhash);
   }
} 

echo "\n All child themes have been hashed and moved.";

$theme_shadow = print_r ($themehash, true);
file_put_contents ('../../themes/.shadow', $theme_shadow);
file_put_contents ('../../themes.upload/.shadow', $theme_shadow);

echo "\n All themes/children have been shadowed for future reference.";
file_put_contents('../../themes/index.php','<?php exit; //objectcode genus ?>');

exit;
