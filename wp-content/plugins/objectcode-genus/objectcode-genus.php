<?php
/*
Plugin Name: REX Engine for Objectcode Genus
Plugin URI: //objectcodes.com
Description: REX is a disruptive CRM software package that intends to eat your web app's lunch.  It runs on WordPress, ExtJS, Formidable and Objectcode Genus operating entirely over JSON-API with a compiled Javascript front-end. There are two variations of licensing available, a commercial license and BSD style term proffering Sencha's GPL/LGPL.
Version: G2.9 and last updated for REX Build 39 running Objectcode Genus&trade; for deployment via pTero app server instances running Amazon AWS and FreeBSD/ Nginx stacks (FEMP) in a dinosaur-friendly cloud.
Author: CHO c/o Objectcode LC.
*/





/*                                                                                                                                         
 _|_|_|    _|_|_|_|  _|      _|      _|_|_|    _|_|_|    _|_|_|  _|_|_|    _|    _|  _|_|_|    _|_|_|_|_|  _|_|_|  _|      _|  _|_|_|_|  
 _|    _|  _|          _|  _|        _|    _|    _|    _|        _|    _|  _|    _|  _|    _|      _|        _|    _|      _|  _|        
 _|_|_|    _|_|_|        _|          _|    _|    _|      _|_|    _|_|_|    _|    _|  _|_|_|        _|        _|    _|      _|  _|_|_|    
 _|    _|  _|          _|  _|        _|    _|    _|          _|  _|    _|  _|    _|  _|            _|        _|      _|  _|    _|        
 _|    _|  _|_|_|_|  _|      _|      _|_|_|    _|_|_|  _|_|_|    _|    _|    _|_|    _|            _|      _|_|_|      _|      _|_|_|_|  
                                                                                                                                         
*/

/* Custom Definitions
 *
 * These are custom definitions that we set for the CONTENT URL, CONTENT DIRECTORY, and PLUGIN URL.
 * They are useful sometimes.
 * 
 * @since 2.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
if (!defined('WP_CONTENT_URL')) define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if (!defined('WP_CONTENT_DIR')) define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if (!defined('WP_PLUGIN_URL'))  define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');


/* Adjustment Layer for Formidable AJAX
 *
 * This is an adjustable value for the Offset for Formidable multi-part (post break) and AJAX form submissions.
 * It exists because in certain themes it is necessary in order for the page not to scroll incorrectly after form submission.
 * 
 * @since 2.8.9
 * @version 2.9.4
 * @package objectcode\genus\base
 */
add_filter('frm_scroll_offset', 'frm_scroll_offset');
function frm_scroll_offset(){
  return 390; //adjust this as needed
}


/* Search and Replace
 *
 * This will remove all HTML comments from the page, which is handy since plugins are so vocal these days.
 *
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_search_and_replace() {
  ob_start('oc_handler');
}


/* Handler
 *
 * Since Objectcode 2.6 this was introduced to handle final output buffering.
 * The primary purpose is to offer search and replace functionality.
 *
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_handler($c){

  $content=  str_replace(array('title="Current Orders - WooCommerce Shop">WooCommerce</a>','<span>Right Now in Forums</span>','Yoast SEO','<span id="wp-version-message">     You are using <span class="b">bbPress','<span>WooCommerce Recent Reviews</span>','<span>WooCommerce Status</span>','<h1>Dashboard</h1>'), array('title="Current Orders - REX">Objectcode Store Management</a>','REX&trade; Forums','REX&trade;','<span style="display:none;">Genus&reg;','Recent Reviews','Store Status','<h1 style="font-family:Pathway Gothic One,sans-serif; text-shadow: 1px 1px #64BE76; font-size: 59px!important;color: #fff;">ReX<sup style="font-size:12px; color:#666;">&trade;</sup></h1><span style="padding-left:4px; color: #ccc; top:8px; position:relative; font-size: 10px;"><span style="font-size:14px; " class="b">R</span>apidly deployed<span style="color:#ccc"> &nbsp;&nbsp;//&nbsp;&nbsp;</span>  <span style="font-size:14px;" class="b">E</span>nterprise grade <span style="color:#ccc">  &nbsp;&nbsp;//&nbsp;&nbsp;  </span> e<span class="b" style="font-size:14px;">X</span>tensibile software system</span>'), preg_replace('/<!--(.|\s)*?-->/', '', $c));
  return $content;
}

add_action('wp_loaded', 'oc_search_and_replace', 1000);


/* Get Post Content and Apply a Specific Template
 *
 * This was originally used in 2.6-2.7 Objectcode as an experimental function.
 * We've left the functionality available as a shortcode because it can be utilized to drop in page content
 * Into admin pages while implementing certain script hooks via a template in a child theme.
 * 
 * It's our intention to include child theme support for each Theme we support as of Objectcode 3.X and beyond.
 * That's because we have certain types of things we'd like to do to all themes, particularly over-riding the footer
 * for all client sites in such a way that it doesn't impact the theme when upgrading.
 *
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_post_block( $atts = null, $content = null ) {
  $atts = shortcode_atts( array(
      'name' => '',
      'post_id' => '',
    ), $atts, 'template' );


  // Check template name
  if ( !$atts['name'] ) return sprintf( '<p class="su-error">Template: %s</p>', __( 'please specify template name', 'su' ) );
  if ( !$atts['post_id'] ) return sprintf( '<p class="su-error">Post ID: %s</p>', __( 'please specify a post id', 'su' ) );
  // Get template output
  ob_start();
  set_query_var( 'atts', $atts );
  get_template_part( str_replace( '.php', '', $atts['name'] ) );
  $output = ob_get_contents();
  ob_end_clean();
  // Return result
  return $output;
}
add_shortcode( 'oc_post', 'oc_post_block' );


/* Get a Formidable View Shortcode and Sanitize it for JSON first, before returning it
 *
 * @since 2.9.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_frm_jsonsc( $atts ) {
  $atts = shortcode_atts( array(
      'id' => '',
      'data' => '',
    ), $atts, 'oc_jsonsafe' );
if ($atts['id']) $output = do_shortcode(FrmProDisplaysController::get_shortcode(array('id' => $atts['id'])));
else $output = $atts['data'];

  return addslashes($output);
}
add_shortcode( 'oc_frm_jsonsc', 'oc_jsonsafe' );


/* Get Post Content by SLUG
 *
 * This is a safe function created to sanitize a string and pull the post type by that string.
 * We utilized the esc_attr native wordpress function here to handle all the slug prior to executing the custom query.
 *
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function get_content_by_slug($slug){
    global $wpdb;
    $slug = esc_attr ($slug);
    $sql = "SELECT post_content FROM `wp_posts` WHERE `post_status` LIKE 'publish' AND `post_name` = '$slug' AND `post_type` LIKE 'page' LIMIT 1";
    $post_content = $wpdb->get_var($sql);
    return ($post_content) ? $post_content : false;
}


/* Get a Post by User
 *
 * Utilized for Custom REX deployments.  This creates the oc_user_post shortcode, which can be used to pull any post by the active user logged in.
 * This is a critical method for deploying systems that offer custom dashboards for each user role. 
 *
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_post_by_user() {
$current_user = wp_get_current_user();

    if ( $current_user->user_login )
      $page = get_content_by_slug($current_user->user_login); 

    if (strlen($page)) return do_shortcode($page);
       else return 'No page content available yet for '.$current_user->user_login.' ... you probably need to create that page first!';
}
add_shortcode( 'oc_user_post', 'oc_post_by_user' );


/* Admin Styles
 * 
 * Inclusive of all branding in admin as of 2.9.1 we are now working with ultimate tweaks and ultra admin plugin.   So this method applies additional tweaks on top of those plugins.
 *
 * @since 2.8.0
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_action_admin_styles() {
    remove_action( 'admin_notices', 'update_nag', 3 );
    //$user = new WP_Error('ga_login_error', __( "Notice:  You had old session data in your browser and as an added precaution we have had to clear it before allowing access to Objectcode.  Please click the Google login button again and all will be well." , 'google-apps-login') );

    $buffer = file_get_contents('css/objectcode-admin.css',true);
    echo oc_minify_styles('<style>'.$buffer.'</style>');

}
add_action( 'admin_head', 'oc_action_admin_styles' );


/* Sort the pages listed in the admin by date automatically (by default) 
 * 
 * @since 2.5.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function set_post_order_in_admin( $wp_query ) {
global $pagenow;
  if ( is_admin() && 'edit.php' == $pagenow && !isset($_GET['orderby'])) {
    $wp_query->set( 'orderby', 'date' );
    $wp_query->set( 'order', 'DSC' );
  }
}
add_filter('pre_get_posts', 'set_post_order_in_admin' );


/* Buffer Content Output
 * 
 * Re-usable Output Buffering function.
 * @since 2.5.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_buffer_content_output() {
   $content = ob_start();
   ob_get_clean();
}


/* Minify Styles
 * 
 * Re-usable Function for Compressing CSS Styles in the Admin or Elsewhere
 * @since 2.2.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_minify_styles($buffer) {

    // Remove comments
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

    // Remove space after colons
    $buffer = str_replace(': ', ':', $buffer);

    // Remove whitespace
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

    // Write everything out
    return $buffer;
}


/* Custom Admin Styles
 * 
 * Re-usable Function for Compressing CSS Styles in the Admin or Elsewhere
 * @since 1.1.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_custom_admin_styles(){
              /* Add Custom Search to Admin
               * 
               * This addition will output a keycode detection for "f" and will instantiate a code-highlighter in the admin menu for finding things.
               *
               * @version 2.9.3
               * @package objectcode\genus\base
               */
              $javascripts = <<<EOF
              <script type="text/javascript">
              jQuery.fn.highlight=function(c){function e(b,c){var d=0;if(3==b.nodeType){var a=b.data.toUpperCase().indexOf(c),a=a-(b.data.substr(0,a).toUpperCase().length-b.data.substr(0,a).length);if(0<=a){d=document.createElement("span");d.className
              ="highlight";a=b.splitText(a);a.splitText(c.length);var f=a.cloneNode(!0);d.appendChild(f);a.parentNode.replaceChild(d,a);d=1}}else if(1==b.nodeType&&b.childNodes&&!/(script|style)/i.test(b.tagName))for(a=0;a<b.childNodes.length;++a)a+=e
              (b.childNodes[a],c);return d} return this.length&&c&&c.length?this.each(function(){e(this,c.toUpperCase())}):this};jQuery.fn.removeHighlight=function(){return this.find("span.highlight").each(function(){this.parentNode.firstChild.nodeNam
              e;with(this.parentNode)replaceChild(this.firstChild,this),normalize()}).end()};

              jQuery(document).ready(function($){
                  $("#adminmenuwrap").prepend('<a href="/wp-admin/?dashboardReload=true"><img src="http://genus.29.large.marketing/wp-content/uploads/2015/06/Plesk-logo.png" style="margin-left: 10px;" alt="Objectcode Genus" width="140" /></a><input value=" search the nav" onclick="this.value=\'\'" id="oc-keywords" class="oc-term">');
                  $("#oc-keywords").change(function(){
                    var term = jQuery( this ).val();
                    $("li").highlight(term);
                  })

                  $(document).keypress(function(e) {
                    var tag = e.target.tagName.toLowerCase();

                      if (tag == 'input' || tag == 'textarea' || tag == 'form') return;  

                      if (e.which == '108') {
                            event.preventDefault();
                            // detected the user pressed an s, so we are readirecting the focus to the search field
                            $('#oc-keywords').focus();
                            $('#oc-keywords').val('');
                            $('#oc-keywords').text('');
                      }
                  })
              });
              </script>
EOF;


  echo $javascripts;
}


/* Frontend Modifications
 * 
 * This function basically adds front-end CSS hacks and KeyPress Instant Login functionality to any theme used with Genus.
 * It is very useful for deploying network-wide CSS modifications and hacks when you don't want to modify the theme or child theme.
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_frontend_mods() {
  $login_page_url_ws = '/objectcode/';

    $buffer = <<<EOF
    <style>
.checkbox label .toggle,.checkbox-inline .toggle{margin-left:-20px;margin-right:5px}
.toggle{position:relative;overflow:hidden}
.toggle input[type=checkbox]{display:none}
.toggle-group{position:absolute;width:200%;top:0;bottom:0;left:0;transition:left .35s;-webkit-transition:left .35s;-moz-user-select:none;-webkit-user-select:none}
.toggle.off .toggle-group{left:-100%}
.toggle-on{position:absolute;top:0;bottom:0;left:0;right:50%;margin:0;border:0;border-radius:0}
.toggle-off{position:absolute;top:0;bottom:0;left:50%;right:0;margin:0;border:0;border-radius:0}
.toggle-handle{position:relative;margin:0 auto;padding-top:0;padding-bottom:0;height:100%;width:0;border-width:0 1px}
.toggle.btn{min-width:59px;min-height:34px}
.toggle-on.btn{padding-right:24px}
.toggle-off.btn{padding-left:24px}
.toggle.btn-lg{min-width:79px;min-height:45px}
.toggle-on.btn-lg{padding-right:31px}
.toggle-off.btn-lg{padding-left:31px}
.toggle-handle.btn-lg{width:40px}
.toggle.btn-sm{min-width:50px;min-height:30px}
.toggle-on.btn-sm{padding-right:20px}
.toggle-off.btn-sm{padding-left:20px}
.toggle.btn-xs{min-width:35px;min-height:22px}
.toggle-on.btn-xs{padding-right:12px}
.toggle-off.btn-xs{padding-left:12px}
    </style>
EOF;

// output the above styles on the front-end minified.
echo oc_minify_styles($buffer);


    /* Key Press Instant Login 
     * 
     * This adds a keycode detection script to the front-end on any theme, designed to hunt for "l" and then auto-redirect you to the login page.
     * It's encoded to protect against detection or spidering.
     * 
     * @since 2.6.1
     * @version 2.9.4
     * @package objectcode\genus\base
     */

?>
    <div name="ws-kp-data" style="display:none;" id="ws-keypress-data"></div>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        $(document).keypress(function(e) {
          var tag = e.target.tagName.toLowerCase();

            if (tag == 'input' || tag == 'textarea' || tag == 'form') return;  

            if (e.which == '108') {
          <?php
              $y = '';
              $u = $login_page_url_ws;
              $s = strlen($u);
              for ($i=0; $i < $s; $i++) {
                $y .= '&#' .  ord($u[$i])  . ';';
              }
              $login = $y;
          ?>
              var decoded = $("<div/>").html("<?= $login ?>").text();
              window.location.replace(decoded);

            }
        })
/* bootstrapping */
+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.toggle"),f="object"==typeof b&&b;e||d.data("bs.toggle",e=new c(this,f)),"string"==typeof b&&e[b]&&e[b]()})}var c=function(b,c){this.$element=a(b),this.options=a.extend({},this.defaults(),c),this.render()};c.VERSION="2.2.0",c.DEFAULTS={on:"On",off:"Off",onstyle:"primary",offstyle:"default",size:"normal",style:"",width:null,height:null},c.prototype.defaults=function(){return{on:this.$element.attr("data-on")||c.DEFAULTS.on,off:this.$element.attr("data-off")||c.DEFAULTS.off,onstyle:this.$element.attr("data-onstyle")||c.DEFAULTS.onstyle,offstyle:this.$element.attr("data-offstyle")||c.DEFAULTS.offstyle,size:this.$element.attr("data-size")||c.DEFAULTS.size,style:this.$element.attr("data-style")||c.DEFAULTS.style,width:this.$element.attr("data-width")||c.DEFAULTS.width,height:this.$element.attr("data-height")||c.DEFAULTS.height}},c.prototype.render=function(){this._onstyle="btn-"+this.options.onstyle,this._offstyle="btn-"+this.options.offstyle;var b="large"===this.options.size?"btn-lg":"small"===this.options.size?"btn-sm":"mini"===this.options.size?"btn-xs":"",c=a('<label class="btn">').html(this.options.on).addClass(this._onstyle+" "+b),d=a('<label class="btn">').html(this.options.off).addClass(this._offstyle+" "+b+" active"),e=a('<span class="toggle-handle btn btn-default">').addClass(b),f=a('<div class="toggle-group">').append(c,d,e),g=a('<div class="toggle btn" data-toggle="toggle">').addClass(this.$element.prop("checked")?this._onstyle:this._offstyle+" off").addClass(b).addClass(this.options.style);this.$element.wrap(g),a.extend(this,{$toggle:this.$element.parent(),$toggleOn:c,$toggleOff:d,$toggleGroup:f}),this.$toggle.append(f);var h=this.options.width||Math.max(c.outerWidth(),d.outerWidth())+e.outerWidth()/2,i=this.options.height||Math.max(c.outerHeight(),d.outerHeight());c.addClass("toggle-on"),d.addClass("toggle-off"),this.$toggle.css({width:h,height:i}),this.options.height&&(c.css("line-height",c.height()+"px"),d.css("line-height",d.height()+"px")),this.update(!0),this.trigger(!0)},c.prototype.toggle=function(){this.$element.prop("checked")?this.off():this.on()},c.prototype.on=function(a){return this.$element.prop("disabled")?!1:(this.$toggle.removeClass(this._offstyle+" off").addClass(this._onstyle),this.$element.prop("checked",!0),void(a||this.trigger()))},c.prototype.off=function(a){return this.$element.prop("disabled")?!1:(this.$toggle.removeClass(this._onstyle).addClass(this._offstyle+" off"),this.$element.prop("checked",!1),void(a||this.trigger()))},c.prototype.enable=function(){this.$toggle.removeAttr("disabled"),this.$element.prop("disabled",!1)},c.prototype.disable=function(){this.$toggle.attr("disabled","disabled"),this.$element.prop("disabled",!0)},c.prototype.update=function(a){this.$element.prop("disabled")?this.disable():this.enable(),this.$element.prop("checked")?this.on(a):this.off(a)},c.prototype.trigger=function(b){this.$element.off("change.bs.toggle"),b||this.$element.change(),this.$element.on("change.bs.toggle",a.proxy(function(){this.update()},this))},c.prototype.destroy=function(){this.$element.off("change.bs.toggle"),this.$toggleGroup.remove(),this.$element.removeData("bs.toggle"),this.$element.unwrap()};var d=a.fn.bootstrapToggle;a.fn.bootstrapToggle=b,a.fn.bootstrapToggle.Constructor=c,a.fn.toggle.noConflict=function(){return a.fn.bootstrapToggle=d,this},a(function(){a("input[type=checkbox][data-toggle^=toggle]").bootstrapToggle()}),a(document).on("click.bs.toggle","div[data-toggle^=toggle]",function(b){var c=a(this).find("input[type=checkbox]");c.bootstrapToggle("toggle"),b.preventDefault()})}($);
    });
    </script>
<?php

}


/* Remove Dashboard "Features"
 *
 * This function removes Yoast SEO columns and various other meta boxen and "features" that plugins like Yoast and Modern Tribe litter about in the admin.
 * These "features" are not very useful, and we feel they really just clutter up the interface. 
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_remove_dashboard_features() {
}



/* Customize the Login Page
 * 
 * This applies various customizatons to the login page.
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_custom_login() {


    $buffer = <<<EOF
              <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
                <style>
                      p.galogin a {
                          text-decoration: none!important;
                          font-weight: 400!important;
                          font-family: "Varela Round";
                      }
                      p#nav { 
                          display:none;

                      }
                      .login #backtoblog a, .login #nav a {
                          text-decoration: none;
                          color: #999;
                          display: none; 
                      }
                      .wp-core-ui .button-primary {
                          background: #B1b1b1; border: none;
                      }
                      .wp-core-ui .button-primary:hover {
                          background: #333; font-family: "Varela Round", sans-serif;
                      }
                      form#loginform p.galogin {
                          background: none repeat scroll 0 0 #FFF;
                          border-color: #0074A2;
                          text-decoration: none;
                          text-align: center;
                          vertical-align: middle;
                          border-radius: 3px;
                          padding: 4px;
                          height: 27px;
                          font-size: 14px;
                      }
                      form#loginform p.galogin a {
                          color: #000;

                      }
                      .login label {
                          color: #666;
                          background-color: rgba(255,255,255,0.9);
                          font-size: 19px;
                          font-family: "Varela Round",sans-serif;
                          padding:5px 0px!important;
                      }
                      .login #backtoblog a, .login #nav a {
                          text-decoration: none;
                          color: #000;
                      }
                      .login h1 a {
                        background: url('/wp-content/plugins/objectcode-genus/img/large-admin-white.png') no-repeat top center;
position: relative!important; left: -50px;                       
 width: 400px;
                        height: 234px;
                        margin-left: 8px;
                        padding-bottom: 0px;
                        background-size: contain!important;
                      }
                      #login { padding-top: 18px; }
                      .login form { background: rgba(255,255,255,0.6); border:1px solid #ccc; } 
                      html { background-image: url(/wp-content/uploads/2015/04/loginbg.jpg) !important;background-size:cover;  } body {background: transparent !important;}
                     /*
                     .login label { display: none; } 
                     .galogin-or { display: none; }
                     .login .submit { display: none; } 
                     */
                </style>
EOF;

    echo oc_minify_styles($buffer);
}

add_action('login_form', 'oc_custom_login');


/* Create Login Form Shortcode
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_login_form_shortcode() {

  add_action('login_form', 'oc_custom_login');
  //return wp_login_form( array( 'echo' => false, 'redirect' => '/objectcode/index.php' ) );
}
add_action( 'init', 'oc_add_shortcodes' );


/* Create shortcodes
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_add_shortcodes() {

  add_shortcode( 'oc-login-form', 'oc_login_form_shortcode' );
}

/* Register Actions with the REST API
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function actions_custom_post_type_rest_support() {
  global $wp_post_types;

  //be sure to set this to the name of your post type!
  $post_type_name = 'crmactions';
  if( isset( $wp_post_types[ $post_type_name ] ) ) {
      $wp_post_types[$post_type_name]->show_in_rest = true;
      $wp_post_types[$post_type_name]->rest_base = $post_type_name;
      $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
  }
}
/* Register Tasks with the REST API
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function tasks_custom_post_type_rest_support() {
  global $wp_post_types;

  //be sure to set this to the name of your post type!
  $post_type_name = 'taskstwo';
  if( isset( $wp_post_types[ $post_type_name ] ) ) {
      $wp_post_types[$post_type_name]->show_in_rest = true;
      $wp_post_types[$post_type_name]->rest_base = $post_type_name;
      $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
  }
}

/* Register Contacts as a custom post type with the REST API
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function contacts_custom_post_type_rest_support() {
  global $wp_post_types;

  //be sure to set this to the name of your post type!
  $post_type_name = 'contacts';
  if( isset( $wp_post_types[ $post_type_name ] ) ) {
      $wp_post_types[$post_type_name]->show_in_rest = true;
      $wp_post_types[$post_type_name]->rest_base = $post_type_name;
      $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
  }
}


/* Register Workflow Taxonomy with RESTAPI
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function workflow_custom_taxonomy_rest_support() {
  global $wp_taxonomies;

  //be sure to set this to the name of your taxonomy!
  $taxonomy_name = 'workflow';

  if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
      $wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
      $wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
      $wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
  }
}

/* Register Lead Source Taxonomy with RESTAPI
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function leads_custom_taxonomy_rest_support() {
  global $wp_taxonomies;

  //be sure to set this to the name of your taxonomy!
  $taxonomy_name = 'lead-source';

  if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
      $wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
      $wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
      $wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
  }
}

/* Register Department Taxonomy with RESTAPI
 * 
 * @since CRM 39
 * @version 40 
 * @package objectcode\genus\crm
 */
function dept_custom_taxonomy_rest_support() {
  global $wp_taxonomies;

  //be sure to set this to the name of your taxonomy!
  $taxonomy_name = 'department';

  if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
      $wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
      $wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
      $wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
  }
}

/* Initialize Admin Pages
 * 
 * @package objectcode\genus\base
 */
if (is_admin()) {
  add_action( 'admin_menu', 'oc_buffer_content_output' );
  add_action( 'admin_head', 'oc_custom_admin_styles' );
  add_action( 'admin_init', 'oc_remove_dashboard_features' );
  add_filter( 'wpseo_use_page_analysis', '__return_false' );
}


/* Initialize Front-End Pages
 * 
 * @package objectcode\genus\base
 */
if (!is_admin()) {

  add_action( 'wp_footer' , 'oc_frontend_mods', 30 );
  add_action( 'login_head', 'oc_remove_dashboard_features' );
  add_action( 'init', 'contacts_custom_post_type_rest_support', 25 );
  add_action( 'init', 'tasks_custom_post_type_rest_support', 25 );
  add_action( 'init', 'actions_custom_post_type_rest_support', 25 );
  add_action( 'init', 'workflow_custom_taxonomy_rest_support', 25 );
  add_action( 'init', 'leads_custom_taxonomy_rest_support', 25 );
  add_action( 'init', 'dept_custom_taxonomy_rest_support', 25 );
}

add_action( 'rest_api_init', 'slug_register_contacts', 5 );






/*                                                                                       
       _|    _|_|_|    _|_|    _|      _|        _|_|_|  _|    _|  _|_|_|  _|_|_|_|_|  
       _|  _|        _|    _|  _|_|    _|      _|        _|    _|    _|        _|      
       _|    _|_|    _|    _|  _|  _|  _|        _|_|    _|_|_|_|    _|        _|      
 _|    _|        _|  _|    _|  _|    _|_|            _|  _|    _|    _|        _|      
   _|_|    _|_|_|      _|_|    _|      _|      _|_|_|    _|    _|  _|_|_|      _|      
                                                                                       
*/




//----------------------------------------------------------------------------------------------------------

/* Initialize ACF Local Cache;   It's a very long array.  Duh
 * 
 * @package objectcode\genus\base
 */
if (function_exists('acf_add_local_field_group') && preg_match('/json/',$_SERVER['REQUEST_URI'])):
acf_add_local_field_group(array (
  'key' => 'group_rexeb6e9dade8',
  'title' => 'Contacts',
  'fields' => array (
    array (
      'key' => 'field_rexeb79cffa7c',
      'label' => 'First Name',
      'name' => 'rex_first_name',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => 15,
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexeb7c2ffa7d',
      'label' => 'Middle Initial',
      'name' => 'rex_middle_initial',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => 15,
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => 1,
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexeb826ffa7f',
      'label' => 'City',
      'name' => 'rex_city',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '25%',
        'class' => 'first_column',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexeb802ffa7e',
      'label' => 'Last Name',
      'name' => 'rex_last_name',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '25%',
        'class' => 'one_third',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18e9d47288',
      'label' => 'State Abbrv',
      'name' => 'rex_state_abbrv',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18eda47289',
      'label' => 'Zipcode',
      'name' => 'rex_zipcode',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18f3c15f73',
      'label' => 'Birthdate',
      'name' => 'rex_birthdate',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18ef515f70',
      'label' => 'Country',
      'name' => 'rex_country',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18f0715f71',
      'label' => 'Address Line 1',
      'name' => 'rex_address_line_1',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18fec15f74',
      'label' => 'License',
      'name' => 'rex_license_number',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex1900115f75',
      'label' => 'Files Upload Paths',
      'name' => 'rex_file_uploads_paths',
      'type' => 'text',
      'instructions' => 'This is a list of file paths for photos that have been uploaded',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex18f1915f72',
      'label' => 'Address Line 2',
      'name' => 'rex_address_line_2',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex1ea2747181',
      'label' => 'Lead Source',
      'name' => 'rex_lead_source',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Autotrader.com' => 'Autotrader.com',
        'vAuto.com' => 'vAuto.com',
        'Hagerstownford.com' => 'Hagerstownford.com',
        'Showroom Walk-in' => 'Showroom Walk-in',
        'Referral' => 'Referral',
        'Google Advertising' => 'Google Advertising',
        'Internet Other' => 'Internet Other',
        'Showroom' => 'Showroom',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex19041070ae',
      'label' => 'E-mail Address',
      'name' => 'rex_e-mail_address',
      'type' => 'email',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
    ),
    array (
      'key' => 'field_rex19052070af',
      'label' => 'Phone number',
      'name' => 'rex_phone_number',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex20f2a6d421',
      'label' => 'Workflow Status',
      'name' => 'rex_workflow_status',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Evaluating' => 'Evaluating',
        'Lost' => 'Lost',
        'Scheduled' => 'Scheduled',
        'Signing' => 'Signing',
        'Trashed' => 'Trashed',
        'Unsold' => 'Unsold',
        'Won' => 'Won',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex20bd9a1eb9',
      'label' => 'Assignee ID',
      'name' => 'rex_assignee',
      'type' => 'number',
      'instructions' => 'Name or user id of the sales rep assigned to this contact.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex1ead347182',
      'label' => 'Contact Type',
      'name' => 'rex_contact_type',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Customer' => 'Customer',
        'Lead' => 'Lead',
        'Unknown' => 'Unknown',
      ),
      'default_value' => array (
        'Unknown' => 'Unknown',
      ),
      'allow_null' => 1,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex304bd5d867',
      'label' => 'State Name',
      'name' => 'rex_state_name',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex304705d866',
      'label' => 'Full Name',
      'name' => 'rex_full_name',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => 15,
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex20d2d6d420',
      'label' => 'Department',
      'name' => 'rex_status_department',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'BDC' => 'BDC',
        'Leads' => 'Leads',
        'Accounting' => 'Accounting',
        'Sales' => 'Sales',
        'Services' => 'Services',
        'Support' => 'Support',
        'Technical' => 'Technical',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex30b2ce8201',
      'label' => 'Kelcar Customer ID',
      'name' => 'rex_kelcar_customer_id',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex3063a5d86e',
      'label' => 'HTML Notes',
      'name' => 'rex_notes_html',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'full',
      'media_upload' => 1,
    ),
    array (
      'key' => 'field_rex305f05d86d',
      'label' => 'Basic Notes',
      'name' => 'rex_notes_basic',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex305b95d86c',
      'label' => 'Notes Status',
      'name' => 'rex_notes_status',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Private' => 'Private',
        'Public' => 'Public',
      ),
      'default_value' => array (
        'Private' => 'Private',
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex30b7de8204',
      'label' => 'Kelcar Deal ID',
      'name' => 'rex_kelcar_deal_id',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex30b61e8203',
      'label' => 'Kelcar Dealership ID',
      'name' => 'rex_kelcar_dealership_id',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex30b54e8202',
      'label' => 'Kelcar ADP Customer ID',
      'name' => 'rex_kelcar_adp_customer_id',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex30c82c18cd',
      'label' => 'Total Checklists Completed',
      'name' => 'rex_total_checklists_completed',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex30c57c18cc',
      'label' => 'Total Forms Completed',
      'name' => 'rex_forms_completed_status',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => 0,
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex30b98e8205',
      'label' => 'Kelcar Salesperson ID',
      'name' => 'rex_kelcar_salesperson_id',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa0cfcf44c3',
      'label' => 'WP Modified Datestamp',
      'name' => 'rex_wp_updated_datestamp',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex5ade31f626',
      'label' => 'Imported Stamp Created',
      'name' => 'rex_imported_stamp_created',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex5ae071f627',
      'label' => 'Imported Stamp Updated',
      'name' => 'rex_imported_stamp_updated',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa0db1f44c5',
      'label' => 'Has Groups',
      'name' => 'rex_has_groups',
      'type' => 'true_false',
      'instructions' => 'Is the customer a part of any groups?  ',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_rexa0ceaf44c2',
      'label' => 'Estimated Close Date',
      'name' => 'rex_estimated_close_date',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa0cb7f44c1',
      'label' => 'WP Created Datestamp',
      'name' => 'rex_wp_created_datestamp',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa0df8f44c6',
      'label' => 'Has Autoresponder',
      'name' => 'rex_has_autoresponder',
      'type' => 'true_false',
      'instructions' => 'Notifications will be sent to this customer',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_rexa0e0af44c7',
      'label' => 'Has Opted Mail',
      'name' => 'rex_has_opted_mail',
      'type' => 'true_false',
      'instructions' => 'Contact has double-opted and validated their e-mail address, and wants to receive campaigns from Rex.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_rexa129af44ca',
      'label' => 'Has Campaign',
      'name' => 'rex_has_campaign',
      'type' => 'true_false',
      'instructions' => 'Contact is already part of a mail campaign',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_rexa0d2ef44c4',
      'label' => 'Next Suggested Follow-up Date',
      'name' => 'rex_next_contact_date',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa13eaf44cc',
      'label' => 'Has Referred',
      'name' => 'rex_has_referred',
      'type' => 'text',
      'instructions' => 'Contact has referred leads to us for the purpose of doing business.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa1072f44c9',
      'label' => 'Has Received Forms',
      'name' => 'rex_has_form_notifications',
      'type' => 'text',
      'instructions' => 'Listing of the id(s) of the forms that the customer has received for queue purposes.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa0e89f44c8',
      'label' => 'Has Opted SMS',
      'name' => 'rex_has_opted_sms',
      'type' => 'true_false',
      'instructions' => 'Contact has opted in to receiving text-notifications.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_rexa14a5f44ce',
      'label' => 'Latest Inventory Reference Tag',
      'name' => 'rex_inventory_reference_tag',
      'type' => 'text',
      'instructions' => 'Reference Tag of the RFID chip associated with inventory.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa1439f44cd',
      'label' => 'Last Contacted Action',
      'name' => 'rex_last_contacted_action',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa1354f44cb',
      'label' => 'Last Contacted Date',
      'name' => 'rex_last_contacted_date',
      'type' => 'text',
      'instructions' => 'Most recent action for this contact.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa152ef44d2',
      'label' => 'Latest Inventory Model',
      'name' => 'rex_inventory_model',
      'type' => 'text',
      'instructions' => 'Contact\'s pertinent vehicle interest, for the purpose of filtering.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa14dff44d0',
      'label' => 'Latest Inventory Make',
      'name' => 'rex_inventory_make',
      'type' => 'text',
      'instructions' => 'Contact\'s pertinent vehicle interest, for the purpose of filtering.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa14c7f44cf',
      'label' => 'Latest Inventory Reference Code',
      'name' => 'rex_inventory_reference_code',
      'type' => 'text',
      'instructions' => 'QR Code ID',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb16c32f6d3',
      'label' => 'File Upload IDs',
      'name' => 'rex_file_upload_ids',
      'type' => 'text',
      'instructions' => 'This is a list of the associated file upload ids',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa15c0f44d4',
      'label' => 'Trade-In Searchable',
      'name' => 'rex_trade-in_searchable',
      'type' => 'text',
      'instructions' => 'Make, Model, Year, and any other information about contact\'s trade that can be searched/ referenced in the future.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa1545f44d3',
      'label' => 'Latest Inventory Year',
      'name' => 'rex_inventory_year',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb17082f6d6',
      'label' => 'Assignee Phone',
      'name' => 'rex_assignee_phone',
      'type' => 'text',
      'instructions' => 'This is an aggregator that sets the phone for the assignee.
',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb16f32f6d5',
      'label' => 'Assignee E-mail',
      'name' => 'rex_assignee_email',
      'type' => 'text',
      'instructions' => 'This is an aggregator that sets the email for the assignee.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb16e92f6d4',
      'label' => 'Assignee Name',
      'name' => 'rex_assignee_name',
      'type' => 'text',
      'instructions' => 'This is an aggregator that sets the name from the assignee',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb1aa0d91f4',
      'label' => 'Creator Phone',
      'name' => 'rex_creator_phone',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb1a94d91f3',
      'label' => 'Creator E-mail',
      'name' => 'rex_creator_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb1a8dd91f2',
      'label' => 'Creator Name',
      'name' => 'rex_creator_name',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb1a83d91f1',
      'label' => 'Creator ID',
      'name' => 'rex_creator_id',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'contacts',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
    0 => 'permalink',
    1 => 'the_content',
    2 => 'excerpt',
    3 => 'custom_fields',
    4 => 'discussion',
    5 => 'comments',
    6 => 'revisions',
    7 => 'slug',
    8 => 'author',
    9 => 'format',
    10 => 'page_attributes',
    11 => 'featured_image',
    12 => 'categories',
    13 => 'tags',
    14 => 'send-trackbacks',
  ),
  'active' => 1,
  'description' => 'Aggregation table that collects all customer and lead data.',
));

acf_add_local_field_group(array (
  'key' => 'group_rex2a8fb93ea0',
  'title' => 'Tasks',
  'fields' => array (
    array (
      'key' => 'field_rex2aa90e33e4',
      'label' => 'Assignee ID',
      'name' => 'rex_assignee',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex2ab3ee33e7',
      'label' => 'Modify Contact Type',
      'name' => 'rex_mod_contact_type',
      'type' => 'select',
      'instructions' => 'Associated Type of Contact for this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Unknown' => 'Unknown',
        'Lead' => 'Lead',
        'Customer' => 'Customer',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex2aaebe33e6',
      'label' => 'Due Date',
      'name' => 'rex_due_date',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex2ac4ee33e9',
      'label' => 'Notes',
      'name' => 'rex_notes',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'full',
      'media_upload' => 1,
    ),
    array (
      'key' => 'field_rex2aa5be33e3',
      'label' => 'Created Date',
      'name' => 'rex_created_date',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex312c1a0a00',
      'label' => 'Created Time',
      'name' => 'rex_created_time',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex3119ea09ff',
      'label' => 'Title',
      'name' => 'rex_title',
      'type' => 'text',
      'instructions' => 'Action name is used for quick identification for the user.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex2caaea0334',
      'label' => 'Contact E-mail',
      'name' => 'rex_assoc_contact_email',
      'type' => 'email',
      'instructions' => 'Associated Contact E-mail for this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
    ),
    array (
      'key' => 'field_rex31439a0a07',
      'label' => 'Recipient E-mail',
      'name' => 'rex_recipient_e-mail',
      'type' => 'email',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
    ),
    array (
      'key' => 'field_rex314b1a0a0b',
      'label' => 'Recipient Customer',
      'name' => 'rex_recipient_customer',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex3136da0a03',
      'label' => 'Due Time',
      'name' => 'rex_due_time',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex31307a0a01',
      'label' => 'Notes Private',
      'name' => 'rex_notes_status',
      'type' => 'text',
      'instructions' => 'Private
Public',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex31493a0a0a',
      'label' => 'Recipient SMS Message',
      'name' => 'rex_recipient_sms',
      'type' => 'textarea',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'maxlength' => '',
      'rows' => '',
      'new_lines' => 'wpautop',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex31482a0a09',
      'label' => 'Recipient Message',
      'name' => 'rex_recipient_message',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'full',
      'media_upload' => 1,
    ),
    array (
      'key' => 'field_rex31447a0a08',
      'label' => 'Recipient Phone',
      'name' => 'rex_recipient_phone',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex316020f926',
      'label' => 'Repeat If',
      'name' => 'rex_repeat_if',
      'type' => 'select',
      'instructions' => 'Ignored
Denied
Regardless',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex3155f0f923',
      'label' => 'Request Summary',
      'name' => 'rex_request_summary',
      'type' => 'select',
      'instructions' => 'Acknowledgement
Approval
Memo
Reviewed
Unspecified',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex3159f0f924',
      'label' => 'Response Summary',
      'name' => 'rex_response_summary',
      'type' => 'select',
      'instructions' => 'Approved
Denied
Acknowledged
Reviewed
Unspecified',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex317a1f4e0e',
      'label' => 'Specific forms',
      'name' => 'rex_specific_forms',
      'type' => 'text',
      'instructions' => 'Form/Defects
Form/Disclosure
Form/Insurance
Form/Odometer
Form/Payoff
Form/Promises
Form/References',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex316fff4e0d',
      'label' => 'Send form request',
      'name' => 'rex_send_forms',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Send Finished Copies' => 'Send Finished Copies',
        'Request Information' => 'Request Information',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex315c60f925',
      'label' => 'Additional Notification',
      'name' => 'rex_repeat_notification',
      'type' => 'checkbox',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        '24 hours' => '24 hours',
        '48 hours' => '48 hours',
      ),
      'default_value' => array (
      ),
      'layout' => 'vertical',
      'toggle' => 0,
    ),
    array (
      'key' => 'field_rex82b8507c52',
      'label' => 'Countdown Days',
      'name' => 'rex_countdown_days',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82b5c07c51',
      'label' => 'Countdown Hours',
      'name' => 'rex_countdown_hours',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82a5707c50',
      'label' => 'Action Type',
      'name' => 'rex_action_type',
      'type' => 'select',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Action/Call' => 'Action/Call',
        'Action/E-mail' => 'Action/E-mail',
        'Action/Text' => 'Action/Text',
        'Action/Appointment' => 'Action/Appointment',
        'Action/Campaign' => 'Action/Campaign',
        'Action/Reminder' => 'Action/Reminder',
        'Action/Request' => 'Action/Request',
        'Action/General' => 'Action/General',
        'Action/Message' => 'Action/Message',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex82c2007c55',
      'label' => 'Target CSV',
      'name' => 'rex_csv_upload',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82c0a07c54',
      'label' => 'Target Analytics Tracker',
      'name' => 'rex_google_analytics_ua_tracker',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82b9907c53',
      'label' => 'Schedule on',
      'name' => 'rex_schedule_on',
      'type' => 'select',
      'instructions' => 'This is relevant only for actions that are scheduling a future event to send an e-mail, campaign, sms, or to place a phone call.',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'choices' => array (
        'Countdown' => 'Countdown',
        'Specific date/time' => 'Specific date/time',
      ),
      'default_value' => array (
      ),
      'allow_null' => 0,
      'multiple' => 0,
      'ui' => 0,
      'ajax' => 0,
      'placeholder' => '',
      'disabled' => 0,
      'readonly' => 0,
    ),
    array (
      'key' => 'field_rex8313286625',
      'label' => 'Creator ID',
      'name' => 'rex_assoc_creator_id',
      'type' => 'text',
      'instructions' => 'Associated User ID of Task Creator/Owner',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82c6807c58',
      'label' => 'Target Customer Group',
      'name' => 'rex_customer_group',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex82c4707c57',
      'label' => 'Target Landing Page',
      'name' => 'rex_landing_page',
      'type' => 'url',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
    ),
    array (
      'key' => 'field_rex82c3407c56',
      'label' => 'Target E-mail Template',
      'name' => 'rex_e-mail_template_upload',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex83eab544c8',
      'label' => 'Modify Workflow',
      'name' => 'rex_mod_workflow',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex83c53720ce',
      'label' => 'Contact Full Name',
      'name' => 'rex_assoc_contact_fullname',
      'type' => 'text',
      'instructions' => 'Associated Contact Name for this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rex83c19720cd',
      'label' => 'Contact ID',
      'name' => 'rex_assoc_contact_id',
      'type' => 'number',
      'instructions' => 'Associated Contact ID for this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa080812ef1',
      'label' => 'Modify Priority',
      'name' => 'rex_mod_priority',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa07f912ef0',
      'label' => 'Modify Status',
      'name' => 'rex_mod_status',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa07ea12eef',
      'label' => 'Modify Department',
      'name' => 'rex_mod_department',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa087112ef2',
      'label' => 'Creator E-mail',
      'name' => 'rex_assoc_creator_email',
      'type' => 'email',
      'instructions' => 'Associated E-mail of the Owner/Creator of this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
    ),
    array (
      'key' => 'field_rexa072a12eee',
      'label' => 'Creator Full Name',
      'name' => 'rex_assoc_creator_fullname',
      'type' => 'text',
      'instructions' => 'Associated Full Name of the Owner/Creator of this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa08e912ef4',
      'label' => 'Contact Phone',
      'name' => 'rex_assoc_contact_phone',
      'type' => 'text',
      'instructions' => 'Associated Contact Phone Number for this Task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb3e964adb7',
      'label' => 'Assignee E-mail',
      'name' => 'rex_assignee_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb3e694adb5',
      'label' => 'Assignee Full Name',
      'name' => 'rex_assignee_fullname',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexa088612ef3',
      'label' => 'Creator Phone',
      'name' => 'rex_assoc_creator_phone',
      'type' => 'email',
      'instructions' => 'Phone number of the Owner/Creator of this task',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
    ),
    array (
      'key' => 'field_rexb404624c49',
      'label' => 'Notify Creator via Text',
      'name' => 'rex_notify_creator_text',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb3fc524c46',
      'label' => 'Notify Creator via E-mail',
      'name' => 'rex_notify_creator_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb3e804adb6',
      'label' => 'Assignee Phone',
      'name' => 'rex_assignee_phone',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb40f224c4c',
      'label' => 'Notify Before 1 hour',
      'name' => 'rex_notify_before_1hour',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb406d24c4b',
      'label' => 'Notify Contact via E-mail',
      'name' => 'rex_notify_contact_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb401c24c48',
      'label' => 'Notify Assignee via E-mail',
      'name' => 'rex_notify_assignee_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb3ff824c47',
      'label' => 'Notify Assignee via Text',
      'name' => 'rex_notify_assignee_text',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb413224c4e',
      'label' => 'Notify Before 24 hours',
      'name' => 'rex_notify_before_24hours',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb412324c4d',
      'label' => 'Notify Before 3 hours',
      'name' => 'rex_notify_before_3hours',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb406324c4a',
      'label' => 'Notify Contact via Text',
      'name' => 'rex_notify_contact_text',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb47f790ecd',
      'label' => 'Use Recipient Phone',
      'name' => 'rex_use_recipient_phone',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb414924c4f',
      'label' => 'Notify Before 72 hours',
      'name' => 'rex_notify_before_72hours',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb487baf13b',
      'label' => 'Use Dialer',
      'name' => 'rex_use_dialer',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb55fd9722c',
      'label' => 'Recipients Additional E-mails',
      'name' => 'rex_recipients_additional',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb5ec2adcdb',
      'label' => 'Use Recipient E-mail',
      'name' => 'rex_use_recipient_email',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_rexb5f24c83f9',
      'label' => 'Recipient Subject',
      'name' => 'rex_recipient_subject',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'taskstwo',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'acf_after_title',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'field',
  'hide_on_screen' => array (
    0 => 'permalink',
    1 => 'the_content',
    2 => 'excerpt',
    3 => 'custom_fields',
    4 => 'discussion',
    5 => 'comments',
    6 => 'revisions',
    7 => 'slug',
    8 => 'author',
    9 => 'format',
    10 => 'page_attributes',
    11 => 'featured_image',
    12 => 'categories',
    13 => 'tags',
    14 => 'send-trackbacks',
  ),
  'active' => 1,
  'description' => 'Aggregation table for all actions',
));
endif;

// field groups ----> $groupsArr = acf_get_local_field_groups();
$rex['contacts'] = acf_get_local_fields('group_rexeb6e9dade8');
$rex['tasks'] = acf_get_local_fields('group_rex2a8fb93ea0');
/***** now we modify the JSON-API configuratin to match**********************************************
 
      the main benefit of doing it this way is that we do not have to re-type field names
      
      ** re-generate the above fields by going to the "Custom Fields" > "Export" area in Genus
 
 ****************************************************************************************************/
function slug_register_contacts() {
global $rex;

$contacts = $rex['contacts'];
$tasks = $rex['tasks'];

    // [REX] CONTACTS
    // note: contacts requires a literal shit ton of fields (37) because it's an aggregation table
    foreach ($contacts as $f) {

        register_api_field( 'contacts',
            $f['name'],
            array(
                'get_callback'    => 'slug_get_contacts',
                'update_callback' => 'slug_update_contacts',
                'schema'          => null,
            )
        );
    }

    // [REX] TASKS
    // note: smaller aggregation table, yet it's fed from 6 other independent tables
    foreach ($tasks as $f) {

        register_api_field( 'taskstwo',
            $f['name'],
            array(
                'get_callback'    => 'slug_get_tasks',
                'update_callback' => 'slug_update_tasks',
                'schema'          => null,
            )
        );
    }
}

/*
 * [REX] REST/ Query_Vars
 * 
 * @since rex build 39
 * @version 47.b
 * 
 * This opens up the meta_query, meta_key, and meta_value params
 * 
 */
function rex_rest_query_vars( $valid_vars ) {

  $valid_vars = array_merge( $valid_vars, array( 'meta_key', 'meta_value', 'meta_compare', 'meta_query', 'meta_relation', 'query','compare','relation' ) );  // meta_compare, meta_query are also available
  return $valid_vars;
}
add_filter( 'rest_query_vars', 'rex_rest_query_vars' );


/*
 * [REX] REST/ MetaKeys checks
 * 
 * @since rex build 39
 * @version 47.b
 * 
 * This explicitly defines our list of approved keys, thereby strengtening
 * the overall security of the mechanism.  In a perfect universe I would be using more validatons/ nonce but
 * currently do not have a ton of time to devote to this.   Either way, it's more secure than the alternative.
 * 
 */
function rex_meta_keys( $args ) {
    global $rex;
    // rex is pissed about this whole meta key thing.  dinosURS!
    $contacts = $rex['contacts'];
    $tasks = $rex['tasks'];
 
    // add all the contacts and tasks meta fields from the CPT aggregators first
    foreach ($contacts as $f) {
        $allow[]=$f['name'];
    }
    foreach ($tasks as $f) {
        $allow[]=$f['name'];
    }  

    $allow[]= 'orderby';
    $allow[]= 'order';
    $allow[]= 'notes_status';
    $allow[]= 'notes_html';
    $allow[]= 'notes_basic';
    $allow[]= 'buyer_fullname';
    $allow[]= 'buyer_firstname';
    $allow[]= 'buyer_middleinitial';
    $allow[]= 'buyer_lastname';
    $allow[]= 'buyer_email';
    $allow[]= 'buyer_phone';
    $allow[]= 'buyer_adress';
    $allow[]= 'buyer_address1';
    $allow[]= 'buyer_address2';
    $allow[]= 'buyer_city';
    $allow[]= 'buyer_state';
    $allow[]= 'buyer_statename';
    $allow[]= 'buyer_zipcode';
    $allow[]= 'buyer_birthdate';
    $allow[]= 'buyer_license';
    $allow[]= 'buyer_country';
    $allow[]= 'scans_licensefront';
    $allow[]= 'scans_licenseback';
    $allow[]= 'scans_vehicle';
    $allow[]= 'scans_insurancefront';
    $allow[]= 'scans_insuranceback';
    $allow[]= 'forms_completed';
    $allow[]= 'contact_type';
    $allow[]= 'status_workflow';
    $allow[]= 'status_dept';
    $allow[]= 'status_leadsource';
    $allow[]= 'status_assignee';
    $allow[]= 'status_created';
    $allow[]= 'status_updated';
    $allow[]= 'kelcar_customerid';
    $allow[]= 'kelcar_adpcustid';
    $allow[]= 'kelcar_dealershipid';
    $allow[]= 'kelcar_salespersonid';
    $allow[]= 'kelcar_dealid';
    $allow[]= 'agg_make';
    $allow[]= 'agg_model';
    $allow[]= 'agg_year';
    $allow[]= 'agg_gross';
    $allow[]= 'agg_units';
    $allow[]= 'createdby_userid';
    $allow[]= 'id';
    $allow[]= 'created_at';
    $allow[]= 'updated_at';
    $allow[]= 'meta_query';
    $allow[]= 'meta_compare';
    $allow[]= 'meta_relation';
    $allow[]= 'relation';
    $allow[]= 'query';
    $allow[]= 'compare';
 
  if ( ! in_array( $args['meta_key'], $allow ) ) { 
    unset( $args['meta_key'] );
    unset( $args['meta_value'] );
  }
  
  return $args;
}
add_filter( 'rest_post_query', 'rex_meta_keys' );

/*
 * [REX] REST/  Contacts Update Post Meta
 * 
 * @since rex build 39
 * @version 47.b
 * 
 * WP_Query updater actions for post meta
 * 
 */
function slug_update_contacts( $value, $object, $field_name ) {
    if ( ! $value || ! is_string( $value ) ) {
        return;
    }

    return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );

}

/*
 * [REX] REST/ get contacts custom post type data
 * 
 * @since rex build 39
 * @version 47.b
 * 
 * Retrueves the custom post type meta fields based on ID
 * 
 */
function slug_get_contacts( $object, $field_name, $request ) {
   return get_post_meta( $object[ 'id' ], $field_name );
}

/*
 * [REX] REST/ Tasks Callback for updating post meta on CPT
 * 
 * @since rex build 39
 * @version 47.b
 * 
 * Update cleansing and such
 * 
 */
function slug_update_tasks( $value, $object, $field_name ) {
    if ( ! $value || ! is_string( $value ) ) {
        return;
    }

    return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );

}
/*
 * [REX] REST/ GetPostMeta
 * 
 * @since rex build 39
 * @version 47.b
 *
 * Yet another callback to retrieve CPT; this time for taskstwo 
 * 
 */
function slug_get_tasks( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name );
}
?>
