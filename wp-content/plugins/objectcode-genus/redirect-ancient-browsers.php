<?php
/*
LMG Naughty Browser Redirect Version: 1.2  > Coping Strategy for Browsers.  Example <= IE9.  Another great way to deal with browsers that don't support HTML5 is to stab yourself slowly in the face while scratchin your nails across a chalkboard.
Author: C.Devin Hogan c/o LM Group for Objectcode LTD.
Author URI: //christopherhogan.com
*/

load_plugin_textdomain( 'advanced-browser-check', false, basename( dirname( __FILE__ ) ) . '/languages' );


class objectcode_core {

    static $this;

    public function __construct()
    {

        self::$this;

        add_action('init', array($this, 'default_setting_values')); // Default settings

    }

    /**
    *  Fossils
    **/
    public function redirect_ancient_browsers()
    {

      $user_browser   = $this->get_browser();
      $oc_options     = $this->default_setting_values();
      $check_browsers = $abc_options['check_browser'];
      $show_browsers  = $abc_options['show_browser'];
      $title          = $abc_options['title'];
      $message        = $abc_options['msg'];
      $hide           = $abc_options['hide'];
      $debug          = $abc_options['debug'];
      $check_browsers = $oc_options['check_browser'];

        foreach($check_browsers as $browser => $version) {

          if (
            $user_browser['short_name'] === $browser
            && $user_browser['version'] <= $version
            && $user_browser['version'] > 0
            && $user_browser['platform'] != 'android'
            && $user_browser['platform'] != 'iOS'
            && $user_browser['platform'] != 'Unknown')
          {
              if (!preg_match('/ancient-browsers/', $_SERVER['REQUEST_URI'])) {
                header("Location: /ancient-browsers/");
                exit;
              }
          }
       }

    }
    /**
     * Get activ users browser details
     * @return array [browser details]
     */
    public function get_browser()
    {

        $user_agent        = $_SERVER['HTTP_USER_AGENT'];
        $browser_full_name = 'Unknown';
        $browser_name      = 'Unknown';
        $platform          = 'Unknown';
        $version           = 'Unknown';
        $short_name        = 'Unknown';

        // First get the platform
        if (preg_match('/android/i', $user_agent)) {

            $platform = 'android';

        } elseif (preg_match('/iphone/i', $user_agent) || preg_match('/ipad/i', $user_agent) || preg_match('/ipod/i', $user_agent)) {

            $platform = 'iOS';

        } elseif (preg_match('/linux/i', $user_agent)) {

            $platform = 'linux';

        } elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {

            $platform = 'mac';

        } elseif (preg_match('/windows|win32/i', $user_agent)) {

            $platform = 'windows';

        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/Firefox/i',$user_agent)) {

            $browser_full_name = 'Mozilla Firefox';
            $browser_name      = 'Firefox';
            $short_name        = 'ff';

        } elseif (preg_match('/Chrome/i',$user_agent)) {

            $browser_full_name = 'Google Chrome';
            $browser_name      = 'Chrome';
            $short_name        = 'chrome';

        } elseif (preg_match('/Safari/i',$user_agent)) {

            $browser_full_name = 'Apple Safari';
            $browser_name      = 'Safari';
            $short_name        = 'safari';

        } elseif (preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent)
            || preg_match('/Windows NT/i',$user_agent) && !preg_match('/Opera/i',$user_agent)) {

            $browser_full_name = 'Internet Explorer';
            $browser_name      = 'MSIE';
            $short_name        = 'ie';

        } elseif (preg_match('/Opera/i',$user_agent)) {

            $browser_full_name = 'Opera';

        }

        // finally get the correct version number
        $known = array('Version', $browser_name, 'rv');
        $pattern = '#(?<browser>' . implode('|', $known) . ')[/ |:]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $user_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);

        if ($i != 1) {

            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($user_agent, 'Version') < strripos($user_agent, $browser_name)){

                $version = $matches['version'][0];

            } else {

                $version = $matches['version'][1];

            }

        } else {

            $version = $matches['version'][0];

        }

        // check if we have a number
        if ($version == null || $version == '' || $version == 0) {$version = 'Unknown';}

        return array(
            'user_agent' => $user_agent,
            'name'       => $browser_full_name,
            'short_name' => $short_name,
            'version'    => floor($version),
            'platform'   => $platform,
            'pattern'    => $pattern
        );

    }

    /**
     * The wrapper, added to the site footer, that the popup
     * will be placed in after the ajax load
     * @return html [echos out the html code needed]
     */
    function content_wrapper()
    {


    }

    /**
     * Default settings and settings array used trough out
     * the plugin
     * @return array [default settings array]
     */
    public function default_setting_values()
    {

        // Default settings values
        add_option( 'g_title', __( 'You are using a web browser not supported by this website!', 'advanced-browser-check' ) );
        add_option('g_hide', NULL);
        add_option('g_show', array(
            'ie'        => '',
            'ff'        => 'http://www.mozilla.com/en-US/firefox/all.html',
            'safari'    => '',
            'opera'     => '',
            'chrome'    => 'https://www.google.com/chrome'
        ));
        add_option('g_check', array(
            'ie'        => '9',
            'ff'        => '25',
            'safari'    => '4',
            'opera'     => '17',
            'chrome'    => '30'
        ));
        add_option('g_debug', 'off');

        // Run update function, this is where the plugin version number is update
        $this->update();

        // Return the settings in array format
        return array(
            'title'         => get_option('g_title'),
            'msg'           => get_option('g_message'),
            'hide'          => get_option('g_hide'),
            'show_browser'  => get_option('g_show'),
            'check_browser' => get_option('g_check'),
            'debug'         => get_option('g_debug')
        );

    }

    /**
    * Default browsers settings. This builds the browser dropdowns on the admin page
    **/
    /**
     * Default browsers settings. This builds the browser
     * dropdowns on the admin page
     * @return array [versions of each browser to include]
     */
    public function default_browsers()
    {

        // Included version numbers is current stable (since latest plugin update)
        // and 5 future versions
        // and 8 older versions

        return array(
            'safari'    => array(0,3,4,5,6,7,8,9,10,11),
            'opera'     => array(0,18,19,20,21,22,23,24,25,26,27,28,29,30,31),
            'ff'        => array(0,26,27,28,29,30,31,32,33,34,35,36,37,36,37),
            'chrome'    => array(0,31,32,33,34,35,36,37,38,39,40,41,42,43,44),
            'ie'        => array(0,7,8,9,10,11,12,13,14,15)
        );

    }
    /**
    * Sets plugin version and there update functions will be added when larger updates is made
    **/
    private function update()
    {

        // If no version number exists, we need to update the settings to v2.0.0 and above
        if (!get_option('oc_version')) {

            // Get old settings array
            $old_settings = get_option('advanced-browser-check');

            if ($old_settings) {
                // Add the old settings to the new once
                update_option('oc_title', $old_settings['title']);
                update_option('oc_message', $old_settings['msg']);
                update_option('oc_show', $old_settings['show_browser']);
                update_option('oc_check', $old_settings['check_browser']);
                update_option('oc_hide', $old_settings['hide']);
            }

            // Delete the old settings array from the DB
            delete_option('advanced-browser-check');

        }

        update_option('oc_version', objectcode_VERSION);

    }

}

class objectcode_settings extends objectcode_core {

  public function __construct()
  {

  }

  function settings_panel()
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    $objectcode = $this->default_setting_values();
    <div class="wrap objectcode-settings">
      <h2><?php _e( 'oc_detect', 'oc_debug' ); ?></h2>

      <form method="post" action="options.php">
                <?php wp_nonce_field('update-options'); ?>

        <table class="form-table objectcode-settings">
                    <tbody>
                        <tr valign="top">
                            <th scope="row">
                              <label for="objectcode_title"><?php  _e( 'Title', 'oc_debug' ); ?></label>
                            </th>
                            <td>
                <input type="text" class="large-text" id="objectcode_title" name="objectcode_title" value="<?php echo $objectcode['title']; ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                              <label for="objectcode_message"><?php _e( 'Message', 'oc_debug' ); ?></label>
                            </th>
                            <td>
                <textarea class="large-text" id="objectcode_message" name="objectcode_message" cols="50" rows="8"><?php echo $objectcode['msg']; ?></textarea>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                              <?php _e( 'Let user hide the popup', 'oc_debug'); ?>
                              <p class="description"><?php _e( 'Let the user hide the popup and use your site. The popup will be hidden for 24h only, this is set by a cookie', 'oc_debug' ); ?></p>
                            </th>
                            <td>
                              <label for="hide">
                  <input type="checkbox" id="objectcode_hide" name="objectcode_hide" value="yes" <?php echo !empty($objectcode['hide']) ? 'checked="checked"' : '' ?> /> <?php _e( 'Yes', 'oc_debug' ); ?>
                </label>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                              <?php _e( 'Choose browsers to link', 'oc_debug' ); ?>
                              <p class="description"><?php _e( 'These are the browsers that you can display a link to and tell your visitor to use', 'oc_debug' ); ?></p>
                            </th>
                            <td>
                              <ul>
                                <li>
                    <label for="ie">
                    </label>
                  </li>
                  <li>
                    <label for="ff">
                    </label>
                  </li>
                  <li>
                    <label for="safari">
                    </label>
                  </li>
                  <li>
                    <label for="opera">
                    </label>
                  </li>
                  <li>
                    <label for="chrome">
                    </label>
                  </li>
                </ul>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                              <?php _e( 'Browsers and versions to check', 'oc_debug' ); ?>
                            </th>
                            <td>
                            <ul>
                                <?php $browsers = $this->default_browsers(); ?>
                  <?php $browser_selects = ''; ?>
                  <?php foreach($browsers as $key => $browser) : ?>
                                        <?php
                                            switch($key) {
                                                case $key == 'ff' :
                                                    $browser_name = 'Firefox';
                                                break;
                                                case $key == 'ie' :
                                                    $browser_name = 'Internet Explorer';
                                                break;
                                                case $key == 'safari' :
                                                    $browser_name = 'Safari';
                                                break;
                                                case $key == 'opera' :
                                                    $browser_name = 'Opera';
                                                break;
                                                case $key == 'chrome' :
                                                    $browser_name = 'Chrome';
                                                break;
                                            }
                                        ?>
                    <li>
                      <?php $browser_selects .= $key.','; ?>
                                            <span class="objectcode-browser-select-name">
                                                <?php _e( $browser_name, 'oc_debug' ); ?>:
                                            </span>
                      <select name="objectcode_check[<?php echo $key; ?>]">
                        <?php foreach($browser as $b) : ?>
                          <?php if ($b == '0') : ?>
                            <option value="<?php echo $b; ?>" <?php echo $objectcode['check_browser'][$key] == $b ? 'selected="selected"' : '' ?>>
                              <?php _e( 'Do not block ', 'oc_debug' ); ?>
                            </option>
                          <?php else : ?>
                            <option value="<?php echo $b; ?>" <?php echo $objectcode['check_browser'][$key] == $b ? 'selected="selected"' : '' ?>>
                              <?php //echo $browser_name .' '. $b; ?> <?php //_e( ' or lower', 'oc_debug' ); ?>
                                                            <?php _e( 'version ', 'oc_debug' ); echo $b; ?> <?php _e( ' or lower', 'oc_debug' ); ?>
                            </option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                    </li>
                  <?php endforeach; ?>
                </ul>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <?php _e( 'Debug', 'oc_debug' ); ?>
                                <p class="description"><?php _e( 'Having problems. Activate debug and see what information the plugin detects about your browser. Debug is only visible for administrators!', 'oc_debug' ); ?></p>
                            </th>
                            <td>
                                <?php
                                    $debug_on = $debug_off = null;

                                    if (!empty($objectcode['debug'])) {
                                        $debug_on = $objectcode['debug'] === 'on' ? 'checked="checked"' : '';
                                        $debug_off = $objectcode['debug'] === 'off' ? 'checked="checked"' : '';
                                    }
                                ?>

                                <label for="hide">
                                    <input type="radio" id="objectcode_debug" name="objectcode_debug" value="on" <?php echo $debug_on; ?> /> <?php _e( 'On', 'oc_debug' ); ?>
                                    <br/>
                                    <input type="radio" id="objectcode_debug" name="objectcode_debug" value="off" <?php echo $debug_off; ?> /> <?php _e( 'Off', 'oc_debug' ); ?>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>
        <div class="form-row">
          <input type="hidden" name="action" value="update" />
          <input type="hidden" name="page_options" value="objectcode_title,objectcode_message,objectcode_hide,objectcode_show,objectcode_check,objectcode_debug" />
          <input type="submit" name="submit" value="<?php _e( 'Save', 'oc_debug' ); ?>" class="button-primary save" />
        </div>
      </form>
    </div>
  <?php
  }
}

function objectcode_settings()
{
  $settings_panel = new objectcode_settings;
  return $settings_panel->settings_panel();
}

function objectcode_adminmenu()
{
  add_submenu_page('options-general.php', 'oc_detect', 'oc_detect', 'manage_options', 'oc_debug', 'objectcode_settings');
}

add_action('admin_menu','objectcode_adminmenu');

    new objectcode_core;
    new objectcode_output;

