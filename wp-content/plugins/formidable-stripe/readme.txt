=== Formidable + Stripe ===
Contributors: press_plus, naomicbush
Donate link: https://pressplus.pro/formidable-stripe
Tags: form, forms, formidable, stripe
Requires at least: 4.0
Tested up to: 4.3
Stable tag: 2.0.6

Use Stripe to process credit card payments on your site, easily and securely, with Formidable forms

== Description ==

Use Stripe to process credit card payments on your site, easily and securely, with Formidable forms

== Installation ==

This section describes how to install and setup the Formidable + Stripe extension. Be sure to follow *all* of the instructions in order for the extension to work properly. If you're unsure on any step, there are screenshots.

Requires at least WordPress 3.9, PHP 5.3, and Formidable 2.0.08. Works with WordPress Multisite.

1. Make sure you have your own copy of Formidable Pro. This plugin does not include Formidable Pro.

2. You'll also need a [Stripe](https://stripe.com/) account

3. Upload the plugin to your WordPress site. There are two ways to do this:

    * WordPress dashboard upload

        - Download the plugin zip file by clicking the orange download button on this page
        - In your WordPress dashboard, go to the **Plugins** menu and click the _Add New_ button
        - Click the _Upload_ link
        - Click the _Choose File_ button to upload the zip file you just downloaded

    * FTP upload

        - Download the plugin zip file by clicking the orange download button on this page
        - Unzip the file you just downloaded
        - FTP in to your site
        - Upload the `formidable-stripe` folder to the `/wp-content/plugins/` directory

4. Visit the **Plugins** menu in your WordPress dashboard, find `Formidable + Stripe` in your plugin list, and click the _Activate_ link

5. Visit the **Formidable->Global Settings** menu, select the new _Stripe_ tab, and add your Stripe API keys. Save your settings.

6. If you want to use a different currency from the default currency for your Stripe account, select the currency you'd like to use from the new currency field that appears after you successfully save your Stripe settings.

7. Create a form with your desired fields. If this is a multi-page form, make sure the field used for the charge description is on the last page of the form.

8. In the **Settings menu** for the form, select the new _Stripe_ tab, complete the Stripe settings options, and click the 'Update' button

9. To test your form, use one of the [Stripe test cards](https://stripe.com/docs/testing#cards) with any expiration date and CVC

10. View entry and look for the Stripe payment details box in the sidebar

== Frequently Asked Questions ==

= Do I need to have my own copy of Formidable Pro for this plugin to work? =
Yes, you need to install the [Formidable Pro plugin](http://formidablepro.com "visit the Formidable Pro website") for this plugin to work.

= Does this version work with the latest version of Formidable Pro? =
Yes.

= Does this work for multiple forms on a page? =
Not yet.

= Does this support subscription payments? =
Yes.

= Can I use Stripe and PayPal on the same form? =
Yes.

== Screenshots ==

1. Activate Formidable
2. Activate Formidable + Stripe
3. Stripe global settings page
4. Form Actions->Stripe settings
5. Successful Stripe payment

== Changelog ==

= 2.0.6 =
* Fix utf8mb4 database issue

= 2.0.5 =
* Fix Stripe Checkout not honoring Stripe action conditional logic

= 2.0.4 =
* Update theme override JS function calls

= 2.0.3 =
* Fix incorrect charge value when using radio button field

= 2.0.2 =
* Fix Stripe checkout form not recognizing multiple 'All' conditions in Stripe action

= 2.0.1 =
* Fix issue with form not submitting if Stripe action uses a dropdown field

= 2.0.0 =
* Fix issue with payment errors not showing on form
* Move Stripe form settings to new Formidable 2.0+ form actions
* Add Formidable 2.0 updates

= 1.1.0 =
* Fix DB issue
* Fix issue with trying to use Formidable functions before they're available
* Add latest Stripe API changes

= 1.0.0 =
* Initial release.

== Upgrade Notice ==
