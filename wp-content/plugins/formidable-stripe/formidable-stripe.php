<?php
/**
 * @wordpress-plugin
 * Plugin Name: Formidable + Stripe
 * Plugin URI: https://pressplus.pro/formidable-stripe
 * Description: Use Stripe to process credit card payments on your site, easily and securely, with Formidable forms
 * Version: 2.0.6
 * Author: press+
 * Author URI: https://pressplus.pro
 * Text Domain: formidable-stripe
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package   PPFRM_Stripe
 * @version   2.0.6
 * @author    press+ <formidable@pressplus.pro>
 * @license   GPL-2.0+
 * @link      https://pressplus.pro
 * @copyright 2014-2015 press+
 *
 * last updated: August 31, 2015
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//Setup Constants

/**
 * Plugin version, used for cache-busting of style and script file references.
 *
 * @since   1.0.0
 */
define( 'PPFRM_STRIPE_CURRENT_VERSION', '2.0.6' );

/**
 * Minimum version of Formidable for which this plugin will work
 *
 * @since 1.0.0
 */
define( 'PPFRM_STRIPE_MIN_FORMIDABLE_VERSION', '2.0.08' );

/**
 * Unique identifier
 *
 * @since 1.0.0
 */
define( 'PPFRM_STRIPE_SLUG', plugin_basename( dirname( __FILE__ ) ) );

define( 'PPFRM_STRIPE_FILE', __FILE__ );

define( 'PPFRM_STRIPE_PATH', plugin_dir_path( __FILE__ ) );

define( 'PPFRM_STRIPE_URL', plugin_dir_url( __FILE__ ) );

//Let's get it started! Load all of the necessary class files for the plugin
require_once( 'includes/utilities/class-loader.php' );
PPFRM_Stripe_Loader::load();

$ppfrm_stripe = new PPFRM_Stripe_App_Controller();
$ppfrm_stripe->run();