<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PPFRM_Stripe_DB_Transactions extends PPP_DB {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array $args
	 */
	public function __construct( $args ) {

		parent::__construct( $args );

		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Add hook for updating table
	 *
	 * @since 1.0.0
	 */
	public function init() {

		add_action( 'ppfrm_stripe_before_update_version', array( $this, 'ppfrm_stripe_before_update_version' ) );

	}

	/**
	 * Create table when version is updated
	 *
	 * @since 1.0.0
	 */
	public function ppfrm_stripe_before_update_version() {

		$this->create_table();

	}

	/**
	 * Get columns and formats
	 *
	 * @since   1.0.0
	 */
	public function get_columns() {

		return array(
			'id'               => '%d',
			'method'           => '%s',
			'entry_id'         => '%d',
			'user_id'          => '%d',
			'transaction_type' => '%s',
			'transaction_id'   => '%s',
			'status'           => '%s',
			'amount'           => '%d',
			'currency'         => '%s',
			'date_created'     => '%s',
			'mode'             => '%s',
			'meta'             => '%s'
		);

	}

	/**
	 * Get default column values
	 *
	 * @since   1.0.0
	 */
	public function get_column_defaults() {

		return array(
			'id'               => 0,
			'method'           => '',
			'entry_id'         => '',
			'user_id'          => 0,
			'transaction_type' => '',
			'transaction_id'   => '',
			'status'           => '',
			'amount'           => 0.00,
			'currency'         => '',
			'date_created'     => date( 'Y-m-d H:i:s' ),
			'mode'             => '',
			'meta'             => ''
		);

	}

	/**
	 * Add a transaction
	 *
	 * @since   1.0.0
	 *
	 * @param array $data
	 *
	 * @return bool|int
	 */
	public function add( $data = array() ) {

		$args = $data;

		if ( empty( $args[ 'method' ] ) || empty( $args[ 'transaction_type' ] ) || empty( $args[ 'transaction_id' ] ) || empty( $args[ 'status' ] ) || empty( $args[ 'mode' ] ) ) {
			PPFRM_Stripe_App_Controller::$logger->log->error( 'Missing required data. Unable to add transaction to DB' );

			return false;
		}

		return $this->insert( $args, 'transaction' );

	}

	/**
	 * Checks if a transaction exists by transaction ID
	 *
	 * @since   1.0.0
	 */
	public function exists( $transaction_id = '' ) {

		return (bool) $this->get_column_by( 'id', 'transaction_id', $transaction_id );

	}

	/**
	 * Retrieve transactions from the database
	 *
	 * @since   1.0.0
	 *
	 * @param array $args
	 *
	 * @return bool|mixed
	 */
	public function get_transactions( $args = array() ) {

		global $wpdb;

		$defaults = array(
			'number'  => 20,
			'offset'  => 0,
			'orderby' => 'date_created',
			'order'   => 'DESC'
		);

		$args = wp_parse_args( $args, $defaults );

		if ( $args[ 'number' ] < 1 ) {
			$args[ 'number' ] = 999999999999;
		}

		$where = '';

		// specific transactions
		if ( ! empty( $args[ 'transaction_id' ] ) ) {

			if ( is_array( $args[ 'transaction_id' ] ) ) {
				$ids = implode( ',', $args[ 'transaction_id' ] );
			} else {
				$ids = intval( $args[ 'transaction_id' ] );
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `transaction_id` IN( {$ids} ) ";
			} else {
				$where .= "WHERE `transaction_id` IN( {$ids} ) ";
			}

		}

		// transactions for specific entries
		if ( ! empty( $args[ 'entry_id' ] ) ) {

			if ( is_array( $args[ 'entry_id' ] ) ) {
				$ids = implode( ',', $args[ 'entry_id' ] );
			} else {
				$ids = intval( $args[ 'entry_id' ] );
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `entry_id` IN( {$ids} ) ";
			} else {
				$where .= "WHERE `entry_id` IN( {$ids} ) ";
			}

		}

		// transactions for specific users
		if ( ! empty( $args[ 'user_id' ] ) ) {

			if ( is_array( $args[ 'user_id' ] ) ) {
				$user_ids = implode( ',', $args[ 'user_id' ] );
			} else {
				$user_ids = intval( $args[ 'user_id' ] );
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `user_id` IN( {$user_ids} ) ";
			} else {
				$where .= "WHERE `user_id` IN( {$user_ids} ) ";
			}

		}

		//specific transactions by method
		if ( ! empty( $args[ 'method' ] ) ) {

			if ( is_array( $args[ 'method' ] ) ) {
				$methods = "'" . implode( "', '", $args[ 'method' ] ) . "'";
			} else {
				$methods = "'" . $args[ 'method' ] . "'";
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `method` IN( {$methods} ) ";
			} else {
				$where .= "WHERE `method` IN( {$methods} ) ";
			}

		}

		//specific transactions by transaction type
		if ( ! empty( $args[ 'transaction_type' ] ) ) {

			if ( is_array( $args[ 'transaction_type' ] ) ) {
				$types = "'" . implode( "', '", $args[ 'transaction_type' ] ) . "'";
			} else {
				$types = "'" . $args[ 'transaction_type' ] . "'";
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `transaction_type` IN( {$types} ) ";
			} else {
				$where .= "WHERE `transaction_type` IN( {$types} ) ";
			}

		}

		//specific transactions by status
		if ( ! empty( $args[ 'status' ] ) ) {

			if ( is_array( $args[ 'status' ] ) ) {
				$statuses = "'" . implode( "', '", $args[ 'status' ] ) . "'";
			} else {
				$statuses = "'" . $args[ 'status' ] . "'";
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `status` IN( {$statuses} ) ";
			} else {
				$where .= "WHERE `status` IN( {$statuses} ) ";
			}

		}

		//specific transactions by mode
		if ( ! empty( $args[ 'mode' ] ) ) {

			if ( is_array( $args[ 'mode' ] ) ) {
				$modes = "'" . implode( "', '", $args[ 'mode' ] ) . "'";
			} else {
				$modes = "'" . $args[ 'mode' ] . "'";
			}

			if ( ! empty( $where ) ) {
				$where .= " AND `mode` IN( {$modes} ) ";
			} else {
				$where .= "WHERE `mode` IN( {$modes} ) ";
			}

		}

		// Transactions created for a specific date or in a date range
		if ( ! empty( $args[ 'date' ] ) ) {

			if ( is_array( $args[ 'date' ] ) ) {

				if ( ! empty( $args[ 'date' ][ 'start' ] ) ) {

					$start = date( 'Y-m-d H:i:s', strtotime( $args[ 'date' ][ 'start' ] ) );

					if ( ! empty( $where ) ) {

						$where .= " AND `date_created` >= '{$start}'";

					} else {

						$where .= " WHERE `date_created` >= '{$start}'";

					}

				}

				if ( ! empty( $args[ 'date' ][ 'end' ] ) ) {

					$end = date( 'Y-m-d H:i:s', strtotime( $args[ 'date' ][ 'end' ] ) );

					if ( ! empty( $where ) ) {

						$where .= " AND `date_created` <= '{$end}'";

					} else {

						$where .= " WHERE `date_created` <= '{$end}'";

					}

				}

			} else {

				$year  = date( 'Y', strtotime( $args[ 'date' ] ) );
				$month = date( 'm', strtotime( $args[ 'date' ] ) );
				$day   = date( 'd', strtotime( $args[ 'date' ] ) );

				if ( empty( $where ) ) {
					$where .= " WHERE";
				} else {
					$where .= " AND";
				}

				$where .= " $year = YEAR ( date_created ) AND $month = MONTH ( date_created ) AND $day = DAY ( date_created )";
			}

		}

		$args['orderby'] = ! array_key_exists( $args['orderby'], $this->get_columns() ) ? 'date_created' : $args['orderby'];

		$cache_key = md5( $this->table_name . serialize( $args ) );

		$transactions = wp_cache_get( $cache_key, 'ppfrm_stripe' );

		if ( $transactions === false ) {

			$transactions = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM  $this->table_name $where ORDER BY {$args['orderby']} {$args['order']} LIMIT %d,%d;", absint( $args[ 'offset' ] ), absint( $args[ 'number' ] ) ) );

			if ( ! empty( $transactions ) ) {

				foreach ( $transactions as $key => $transaction ) {

					$transactions[$key]->meta = json_decode( $transaction->meta, true );

				}

			}

			wp_cache_set( $cache_key, $transactions, 'ppfrm_stripe', 3600 );

		}

		return $transactions;

	}

	/**
	 * Count the total number of transactions in the database
	 *
	 * @since   1.0.0
	 */
	public function count( $args = array() ) {

		global $wpdb;

		$where = '';

		if ( ! empty( $args[ 'date' ] ) ) {

			if ( is_array( $args[ 'date' ] ) ) {

				$start = date( 'Y-m-d H:i:s', strtotime( $args[ 'date' ][ 'start' ] ) );
				$end   = date( 'Y-m-d H:i:s', strtotime( $args[ 'date' ][ 'end' ] ) );

				if ( empty( $where ) ) {

					$where .= " WHERE `date_created` >= '{$start}' AND `date_created` <= '{$end}'";

				} else {

					$where .= " AND `date_created` >= '{$start}' AND `date_created` <= '{$end}'";

				}

			} else {

				$year  = date( 'Y', strtotime( $args[ 'date' ] ) );
				$month = date( 'm', strtotime( $args[ 'date' ] ) );
				$day   = date( 'd', strtotime( $args[ 'date' ] ) );

				if ( empty( $where ) ) {
					$where .= " WHERE";
				} else {
					$where .= " AND";
				}

				$where .= " $year = YEAR ( date_created ) AND $month = MONTH ( date_created ) AND $day = DAY ( date_created )";
			}

		}


		$cache_key = md5( $this->plugin_slug . '_transactions_count' . serialize( $args ) );

		$count = wp_cache_get( $cache_key, 'ppfrm_stripe' );

		if ( $count === false ) {
			$count = $wpdb->get_var( "SELECT COUNT($this->primary_key) FROM " . $this->table_name . "{$where};" );
			wp_cache_set( $cache_key, $count, 'ppfrm_stripe', 3600 );
		}

		return absint( $count );

	}

	/**
	 * Create the table
	 *
	 * @since   1.0.0
	 *
	 */
	public function create_table() {

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		}
		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE $wpdb->collate";
		}

		$sql = "CREATE TABLE {$this->table_name} (
				              id int(11) unsigned not null auto_increment,
				              method varchar(10) not null,
				              entry_id int(11) unsigned,
				              user_id int(10) unsigned,
				              transaction_type varchar(30) not null,
				              transaction_id varchar(50) not null,
				              status varchar(20) not null,
				              amount decimal(19,2),
				              currency varchar(5),
				              date_created datetime,
				              mode char(4) not null,
				              meta longtext,
				              PRIMARY KEY  (id),
				              KEY entry_id (entry_id),
				              KEY user_id ( user_id ),
				              KEY transaction_type (transaction_type)
				            )$charset_collate;";

		dbDelta( $sql );

		update_option( str_replace( $wpdb->prefix, '', $this->table_name ) . '_db_version', $this->version );
	}
}