<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_App_Helper Class
 *
 * @since 1.0.0
 * */
class PPFRM_Stripe_App_Helper {

	/**
	 * Version update check
	 *
	 * @since 1.0.0
	 */
	public static function check_if_new_version() {

		if ( ( $current_version = get_option( 'ppfrm_stripe_version' ) ) != PPFRM_STRIPE_CURRENT_VERSION ) {

			if ( self::get_wp_option( 'ppfrm_stripe_version' ) != PPFRM_STRIPE_CURRENT_VERSION ) {

				do_action( 'ppfrm_stripe_before_update_version', $current_version );

				update_option( 'ppfrm_stripe_version', PPFRM_STRIPE_CURRENT_VERSION );

				do_action( 'ppfrm_stripe_after_update_version' );

			}

		}
	}

	/**
	 * Get option directly from DB
	 *
	 * @param $option_name
	 *
	 * @return null|string
	 */
	public static function get_wp_option( $option_name ) {
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare( "SELECT option_value FROM {$wpdb->prefix}options WHERE option_name=%s", $option_name ) );
	}

	/**
	 *  Make sure necessary extensions are available on server
	 *
	 * The plugin will not work without certain server extensions
	 *
	 * @since 1.0.0
	 *
	 * @uses  deactivate_plugins()
	 * @uses  __()
	 *
	 * @return bool
	 */
	public static function check_server_requirements() {
		$server_requirements  = array( 'curl', 'mbstring' );
		$missing_requirements = array();
		foreach ( $server_requirements as $extension ) {
			if ( ! extension_loaded( $extension ) ) {
				$missing_requirements[ ] = $extension;
			}
		}

		if ( ! empty( $missing_requirements ) ) {
			deactivate_plugins( plugin_basename( trim( PPFRM_STRIPE_FILE ) ) );
			$missing_requirements = implode( ', ', $missing_requirements );
			$message              = __( "Formidable + Stripe needs {$missing_requirements} to be installed on your server. Please contact your host to enable this.", 'formidable-stripe' );
			die( $message );
		}
	}

	/**
	 *  Add permissions
	 *
	 * @since 1.0.0
	 *
	 * @uses  add_cap()
	 *
	 * @return void
	 */
	public static function add_permissions() {
		global $wp_roles;
		$wp_roles->add_cap( 'administrator', 'ppfrm_stripe' );
		$wp_roles->add_cap( 'administrator', 'ppfrm_stripe_global_settings' );
		$wp_roles->add_cap( 'administrator', 'ppfrm_stripe_form_settings' );
		$wp_roles->add_cap( 'administrator', 'ppfrm_stripe_uninstall' );
	}

	/**
	 * Set option to redirect to settings page
	 *
	 * @since 1.0.0
	 *
	 * @uses  set_transient()
	 *
	 * @return void
	 */
	public static function set_global_settings_page_redirect() {
		set_transient( 'ppfrm_stripe_global_settings_page_redirect', true, HOUR_IN_SECONDS );
	}

	/**
	 *  Redirect to settings page if not activating multiple plugins at once
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_transient()
	 * @uses  delete_transient()
	 * @uses  admin_url()
	 * @uses  wp_redirect()
	 *
	 * @return void
	 */
	public static function redirect_to_settings_page() {
		if ( true == get_transient( 'ppfrm_stripe_global_settings_page_redirect' ) ) {
			delete_transient( 'ppfrm_stripe_global_settings_page_redirect' );
			if ( ! isset( $_GET[ 'activate-multi' ] ) ) {
				wp_redirect( self_admin_url( 'admin.php?page=formidable-settings&t=stripe_settings' ) );
			}
		}
	}

	/**
	 * Add a link to this plugin's settings page
	 *
	 * @since 1.0.0
	 *
	 * @uses  self_admin_url()
	 * @uses  __()
	 *
	 * @param $links
	 *
	 * @return array
	 */
	public static function plugin_action_links( $links ) {
		return array_merge(
			array(
				'settings' => '<a href="' . self_admin_url( 'admin.php?page=formidable-settings&t=stripe_settings' ) . '">' . __( 'Settings', 'formidable-stripe' ) . '</a>'
			),
			$links
		);
	}

	/**
	 * See if Stripe is enabled on this form
	 *
	 * @since 1.0.0
	 *
	 * @param $form
	 *
	 * @return bool
	 */
	public static function is_stripe_form( $form ) {

		$is_stripe_form = false;

		if ( ! empty( $form ) ) {

			if ( ! is_object( $form ) ) {
				$form = FrmForm::getOne( $form );
			}

			if ( is_object( $form ) ) {

				$form_actions = FrmFormActionsHelper::get_action_for_form( $form->id, 'stripe' );

				if ( ! empty( $form_actions ) ) {
					$is_stripe_form = true;
				}

			}
		}

		return $is_stripe_form;
	}

	/**
	 * Detect if we're navigating to the last/only page of the form
	 *
	 * @since 1.0.0
	 *
	 * @param $form
	 *
	 * @return bool
	 */
	public static function is_last_page( $form ) {

		$is_form_last_page = false;

		if ( self::is_multi_page_form( $form ) ) {

			if ( ( ! empty( $_POST ) && isset( $_POST[ 'frm_next_page'] ) && empty( $_POST[ 'frm_next_page'] ) ) && ( ! ( FrmProFormsHelper::going_to_prev( $form->id ) || FrmProFormsHelper::saving_draft( $form->id ) ) ) ) {
				$is_form_last_page = true;
			}

		} else {
			$is_form_last_page = true;
		}

		return $is_form_last_page;

	}

	/**
	 * See if there are any page breaks on this form
	 *
	 * @since 1.0.0
	 *
	 * @param $form
	 *
	 * @return bool
	 */
	public static function is_multi_page_form( $form ) {

		$is_multi_page_form = false;

		$frm_field   = new FrmField();
		$form_fields = $frm_field->getAll( "fi.form_id='{$form->id}' and fi.type='break'" );

		if ( ! empty( $form_fields ) ) {
			$is_multi_page_form = true;
		}

		return $is_multi_page_form;

	}

	public static function get_array_value( $array, $key, $stripslashes = true ) {
		return isset( $array[ $key ] ) ? $array[ $key ] : '';
	}

	public static function get_array_values( $array, $keys ) {
		$keys  = explode( '/', $keys );
		$value = $array;
		foreach ( $keys as $current_key ) {
			$value = self::get_array_value( $value, $current_key );
		}

		return $value;
	}

	public static function get_post_value( $name, $stripslashes = true ) {
		$value = '';

		if ( isset( $_POST[ $name ] ) ) {
			$value = $stripslashes ? stripslashes_deep( $_POST[ $name ] ) : $_POST[ $name ];
		}

		return $value;
	}

	/**
	 * Return the desired API key from the database
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_option()
	 * @uses  PPFRM_Stripe_App_Helper::get_array_value()
	 * @uses  esc_attr()
	 *
	 * @param      $type
	 * @param bool $mode
	 *
	 * @return string
	 */
	public static function get_api_key( $type, $mode = false ) {
		$settings = get_option( 'ppfrm_stripe_global_settings' );
		if ( ! $mode ) {
			$mode = self::get_array_value( $settings, 'mode' );
		}
		$key = $mode . '_' . $type . '_key';

		return trim( esc_attr( self::get_array_value( $settings, $key ) ) );

	}

	/**
	 * Get the current mode of a Stripe form
	 *
	 * Remember: this is the *current* mode of the form
	 *
	 * @since 1.0.0
	 *
	 * @param object $action
	 *
	 * @return string
	 */
	public static function get_form_stripe_mode( $action ) {

		return self::get_array_values( $action->post_content, 'mode' );

	}

	/**
	 * Get global Stripe mode
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public static function get_global_stripe_mode() {

		$settings = get_option( 'ppfrm_stripe_global_settings' );

		return self::get_array_value( $settings, 'mode' );

	}

	/**
	 * Currencies supported by Stripe account
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_transient()
	 * @uses  PPFRM_Stripe_App_Helper::get_api_key()
	 * @uses  PPFRM_Stripe_API::retrieve()
	 * @uses  set_transient()
	 *
	 * @param $currencies
	 *
	 * @return array
	 */
	public static function ppp_currencies( $currencies ) {

		$current_currency = get_transient( 'ppfrm_stripe_currency' );
		if ( false === $current_currency ) {
			$api_key = self::get_api_key( 'secret' );
			if ( ! empty( $api_key ) ) {
				$account = PPP_Stripe_API::retrieve( 'account', $api_key );
				if ( is_object( $account ) ) {
					$default_currency     = strtoupper( $account[ 'default_currency' ] );
					$currencies_supported = array_map( 'strtoupper', $account[ 'currencies_supported' ] );
					set_transient( 'ppfrm_stripe_currency',
					               array(
						               'default'   => $default_currency,
						               'supported' => $currencies_supported
					               ),
					               24 * HOUR_IN_SECONDS );
				}
			}
		}

		if ( ( ! empty( $current_currency ) ) || ( ! empty( $default_currency ) ) && ( ! empty( $currencies_supported ) ) ) {
			$currencies = array_intersect_key( $currencies, ( $current_currency ) ? array_flip( $current_currency[ 'supported' ] ) : array_flip( $currencies_supported ) );
		}

		return $currencies;
	}

	public static function get_default_currency() {
		$stripe_currency = get_transient( 'ppfrm_stripe_currency' );

		return $stripe_currency[ 'default' ];
	}

	/**
	 * Get Stripe customer object for a WP user
	 *
	 * @since 1.0.0
	 *
	 * @param $api_key
	 * @param $user_id
	 *
	 * @return mixed|void
	 */
	public static function get_user_customer_object( $api_key, $user_id ) {

		$customer_id = PPFRM_Stripe_Customer_API::get_stripe_customer_id( $user_id );

		if ( empty( $customer_id ) ) {
			PPFRM_Stripe_App_Controller::$logger->log->error( "User {$user_id} does not contain a saved Stripe customer -- perhaps this user was created manually through the Stripe dashboard instead of through this website" );

			return $customer_id;
		}

		PPFRM_Stripe_App_Controller::$logger->log->debug( "Retrieving Stripe customer object for user: {$user_id}" );

		$customer = PPP_Stripe_API::retrieve( 'customer', $api_key, $customer_id );

		return $customer;

	}

	/**
	 * Add metadata to an object
	 *
	 * @since 1.0.0
	 *
	 * @param $object_type
	 * @param $object
	 * @param $metadata
	 * @param $additional_args
	 *
	 * @return bool
	 */
	public static function add_metadata_to_object( $object_type, $object, $metadata, $additional_args ) {

		$metadata_added = false;

		$updated_metadata = PPP_Stripe_API::create_updated_metadata_array( $object, $metadata );
		$api_key          = apply_filters( 'ppfrm_stripe_api_key', PPFRM_Stripe_App_Helper::get_api_key( 'secret', PPP_Stripe_API::get_object_mode( $object ) ), 'add_metadata_to_object' );


		if ( 'PPP\Stripe_Charge' == $object_type ) {
			$updated_object = PPP_Stripe_API::update_charge( $api_key, $object, array( 'metadata' => $updated_metadata ) );
		}

		if ( 'PPP\Stripe_Subscription' == $object_type ) {
					$updated_object = PPP_Stripe_API::update_subscription( $api_key, $additional_args['customer'], $object['id'], array( 'metadata' => $updated_metadata ) );
				}

		if ( ! empty( $updated_object ) && is_object( $updated_object ) && is_a( $updated_object, $object_type ) ) {
			$metadata_added = true;
		}

		return $metadata_added;

	}

}