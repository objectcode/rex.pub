<?php
/*
 * @package   PPP_Admin_Notices
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPP_Admin_Notices Class
 *
 * Handles admin notices
 *
 * @since 1.0.0
 */
class PPP_Admin_Notices {

	private $plugin_slug = '';

	/**
	 * @var PPP_Parent
	 */
	private $parent = '';

	function __construct ( $plugin_slug, $parent ) {

		$this->plugin_slug = $plugin_slug;
		$this->parent = $parent;
		$this->trigger_admin_notices();

	}

	function trigger_admin_notices () {
		add_action( 'admin_notices', array( $this, 'admin_notices' ) );
	}

	/**
	 *  Output admin notices
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_site_transient()
	 * @uses  get_transient()
	 * @uses  delete_site_transient()
	 * @uses  delete_transient()
	 *
	 * @return void
	 */
	public function admin_notices () {

		$admin_notices = function_exists( 'get_site_transient' ) ? get_site_transient( "{$this->plugin_slug}-admin_notices" ) : get_transient( "{$this->plugin_slug}-admin_notices" );
		if ( $admin_notices ) {
			$message = '';

			foreach ( $admin_notices as $type => $notices ) {

				if ( ( 'errors' == $type ) && ( ! $this->parent->is_parent_supported() ) ) {
					foreach ( $notices as $notice ) {
						$message .= '<div class="error"><p>' . $notice . '</p></div>';
					}
				}

				if ( 'updates' == $type ) {
					foreach ( $notices as $notice ) {
						$message .= '<div class="updated"><p>' . $notice . '</p></div>';
					}
				}

			}

			echo $message;

			if ( function_exists( 'delete_site_transient' ) ) {
				delete_site_transient( "{$this->plugin_slug}-admin_notices" );
			}
			else {
				delete_transient( "{$this->plugin_slug}-admin_notices" );
			}
		}
	}

	/**
	 * Create an admin notice
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_site_transient()
	 * @uses  get_transient()
	 * @uses  set_site_transient()
	 * @uses  set_transient()
	 *
	 * @param $notice
	 * @param $type
	 *
	 * @return void
	 */
	public function set_admin_notice ( $notice, $type ) {
		if ( function_exists( 'get_site_transient' ) ) {
			$notices = get_site_transient( "{$this->plugin_slug}-admin_notices" );
		}
		else {
			$notices = get_transient( "{$this->plugin_slug}-admin_notices" );
		}

		if ( ! is_array( $notices ) || ! array_key_exists( $type, $notices ) || ! in_array( $notice, $notices[ $type ] ) ) {
			$notices[ $type ][ ] = $notice;
		}

		if ( function_exists( 'set_site_transient' ) ) {
			set_site_transient( "{$this->plugin_slug}-admin_notices", $notices );
		}
		else {
			set_transient( "{$this->plugin_slug}-admin_notices", $notices );
		}
	}

}