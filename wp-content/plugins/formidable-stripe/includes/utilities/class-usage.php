<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PPP_Usage {

	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
	}

	public function init() {

		add_action( 'ppfrm_stripe_after_update_version', array( $this, 'ppfrm_stripe_after_update_version' ) );
		add_action( 'ppfrm_stripe_usage_event', array( $this, 'ppfrm_stripe_usage_event' ), 1, 1 );

	}

	public function ppfrm_stripe_after_update_version() {
		delete_transient( 'ppfrm_stripe_usage_stats_cache_data' );
	}

	/**
	 *
	 * @since 1.0.0
	 *
	 * @uses  get_transient()
	 * @uses  wp_count_posts()
	 * @uses  wp_count_comments()
	 * @uses  wp_get_theme()
	 * @uses  get_stylesheet_directory()
	 * @uses  get_plugins()
	 * @uses  get_plugin_data
	 * @uses  site_url()
	 * @uses  get_bloginfo()
	 * @uses  get_option()
	 * @uses  wp_remote_get()
	 * @uses  set_transient
	 *
	 * @return void
	 */
	public static function do_usage_stats() {
		// Start of Metrics
		global $wpdb;
		$data = get_transient( 'ppfrm_stripe_usage_stats_cache_data' );
		if ( ! $data || $data == '' ) {
			$api_url        = 'https://pressplus.pro/?gfpsw_action=update_usage';
			$count_posts    = wp_count_posts();
			$count_pages    = wp_count_posts( 'page' );
			$comments_count = wp_count_comments();
			if ( function_exists( 'wp_get_theme' ) ) {
				$theme_data = wp_get_theme();
				$theme      = array(
					'Name'      => $theme_data[ 'Name' ],
					'ThemeURI'  => $theme_data[ 'ThemeURI' ],
					'Author'    => $theme_data[ 'Author' ],
					'AuthorURI' => $theme_data[ 'AuthorURI' ],
					'Version'   => $theme_data[ 'Version' ]
				);
			} else {
				$theme_data = get_theme_data( get_stylesheet_directory() . '/style.css' );
				$theme      = $theme_data[ 'Name' ] . ' ' . $theme_data[ 'Version' ];
			}

			if ( ! function_exists( 'get_plugins' ) ) {
				include ABSPATH . 'wp-admin/includes/plugin.php';
			}

			$plugins        = array_keys( get_plugins() );
			$active_plugins = get_option( 'active_plugins', array() );

			foreach ( $plugins as $key => $plugin ) {
				if ( in_array( $plugin, $active_plugins ) ) {
					unset( $plugins[ $key ] );
				}
			}

			$plugin_data = get_plugin_data( PPFRM_STRIPE_FILE );
			/*$currency    = GFCommon::get_currency();
			$form_meta   = GFP_Stripe_Data::get_all_feeds();
			$feed_count  = 0;
			$feed_types  = $feed_options = array();
			foreach ( $form_meta as $meta ) {
				foreach ( $meta['rules'] as $feed ) {
					$feed_count ++;
					if ( empty( $feed_types[$feed['type']] ) ) {
						$feed_types[$feed['type']] = 1;
					}
					else {
						$feed_types[$feed['type']] ++;
					}
					foreach ( $feed as $key => $value ) {
						if ( empty( $feed_options[$key] ) ) {
							$feed_options[$key] = ( ! empty( $value ) );
						}
					}
				}
			}*/
			$events     = get_option( 'ppfrm_stripe_usage_events' );
			$data       = array(
				'url'              => home_url(),
				'posts'            => $count_posts->publish,
				'pages'            => $count_pages->publish,
				'comments'         => $comments_count->total_comments,
				'approved'         => $comments_count->approved,
				'spam'             => $comments_count->spam,
				'pingbacks'        => $wpdb->get_var( "SELECT COUNT(comment_ID) FROM $wpdb->comments WHERE comment_type = 'pingback'" ),
				'plugin'           => $plugin_data,
				'theme'            => $theme,
				'active_plugins'   => $active_plugins,
				'inactive_plugins' => $plugins,
				'wpversion'        => get_bloginfo( 'version' ),
				'multisite'        => is_multisite(),
				'users'            => $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users" ),
				/*'currency'         => $currency,
				'feeds'            => $feed_count,
				'feed_types'       => $feed_types,
				'feed_options'     => $feed_options,*/
				'events'           => $events
			);
			$data       = apply_filters( 'ppfrm_stripe_usage_stats', $data );
			$user_agent = 'PPFRM_Stripe/' . PPFRM_STRIPE_CURRENT_VERSION . '; ' . get_bloginfo( 'url' );
			$args       = array( 'user-agent' => $user_agent, 'blocking' => false, 'body' => $data );

			wp_remote_post( $api_url, $args );
			set_transient( 'ppfrm_stripe_usage_stats_cache_data', $data, 60 * 60 * 24 );
			update_option( 'ppfrm_stripe_usage_events', array() );
		}
	}

	/**
	 * @param $event_name
	 */
	public function ppfrm_stripe_usage_event( $event_name ) {
		$usage_events = get_option( 'ppfrm_stripe_usage_events' );
		$usage_events[ $event_name ] ++;

		update_option( 'ppfrm_stripe_usage_events', $usage_events );
	}
}