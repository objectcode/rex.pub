<?php
/**
 * @package   Press_Plus
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

/**
 * Class PPP_Logger
 *
 * Provides a PSR-3 compliant logger — currently KLogger. Logs will be created in the WP uploads directory and a
 * maximum of 10 days of files are kept.
 *
 * Log levels in order of highest priority to lowest:
 * LogLevel::EMERGENCY;
 * LogLevel::ALERT;
 * LogLevel::CRITICAL;
 * LogLevel::ERROR;
 * LogLevel::WARNING;
 * LogLevel::NOTICE;
 * LogLevel::INFO;
 * LogLevel::DEBUG;
 *
 * To use:
 * $logger = new PPP_Logger( 'plugin-slug', Psr\Log\LogLevel::WARNING );
 * $logger->log->error( 'An error occurred' ); //will be logged
 * $logger->log->info( 'Something happened here' ); //will not be logged
 *
 * @since 1.0.0
 */
class PPP_Logger {

	/**
	 * Instance of this class
	 *
	 * @since 1.0.0
	 *
	 * @var null
	 */
	private static $_this = null;

	/**
	 * Unique ID for plugin, used for log directory
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $plugin_slug = '';

	/**
	 * Minimum level at which to log messages — any messages below will not be logged
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $log_level_threshold = '';

	/**
	 * Maximum size (bytes) allowed for log files
	 *
	 * @since 1.0.0
	 *
	 * @var int
	 */
	private $max_file_size = 409600;

	/**
	 * Maximum number of log files allowed for the plugin
	 *
	 * @since 1.0.0
	 *
	 * @var int
	 */
	private $max_file_count = 10;

	/**
	 * Logger
	 *
	 * @since 1.0.0
	 *
	 * @var Katzgrau\KLogger\Logger
	 */
	public $log = null;

	public function __construct ( $plugin_slug, $library_path, $log_level_threshold = false ) {

		$this->load_files( $library_path );

		$this->plugin_slug = $plugin_slug;

		$this->log_level_threshold = ( $log_level_threshold ) ? constant( $log_level_threshold ) : Psr\Log\LogLevel::DEBUG;

		$this->create_logger();

		self::$_this = $this;
	}

	public function load_files ( $library_path ) {
		require_once( trailingslashit( $library_path ) . 'ppp-logger/Psr/Log/LogLevel.php' );
		require_once( trailingslashit( $library_path ) . 'ppp-logger/Psr/Log/LoggerInterface.php' );
		require_once( trailingslashit( $library_path ) . 'ppp-logger/Psr/Log/AbstractLogger.php' );
		require_once( trailingslashit( $library_path ) . 'ppp-logger/Logger.php' );
	}

	private function create_logger () {
		$log_dir = $this->create_log_dir();
		if ( $log_dir ) {
			$this->log = new Katzgrau\KLogger\Logger( $log_dir, $this->log_level_threshold );
			$this->log->setDateFormat( 'Y-m-d G:i:s' );
			$this->clean_up();
		}
	}

	private function create_log_dir () {
		$log_dir = $this->get_log_dir();

		if ( ! wp_mkdir_p( $log_dir ) ) {
			$log_dir = false;
		}

		return $log_dir;
	}

	private function get_log_dir () {
		$upload_dir = wp_upload_dir();

		if ( $upload_dir[ 'error' ] ) {
			$log_dir = null;
		}
		else {
			$log_dir = "{$upload_dir['basedir']}/{$this->plugin_slug}/logs/";
		}

		return $log_dir;
	}

	private function get_log_file_path () {
		return $this->get_log_dir() . 'log_' . date( 'Y-m-d' ) . '.txt';
	}

	private function clean_up () {
		$file_path  = $this->get_log_file_path();
		$path       = pathinfo( $file_path );
		$folder     = $path[ 'dirname' ] . '/';
		$file_base  = $path[ 'filename' ];
		$file_ext   = $path[ 'extension' ];

		//check size of current file, if greater than certain size, rename using year, month, day, hour, minute, second
		if ( file_exists( $file_path ) && filesize( $file_path ) > $this->max_file_size ) {
			$adjusted_date = date( 'Gis' );
			$new_name      = "{$file_base}_{$adjusted_date}.{$file_ext}";
			rename( $file_path, $folder . $new_name );
		}
		//check quantity of files and delete older ones if too many
		//get files which match the base filename
		$aryFiles  = glob( $folder . $file_base . "*.*" );
		$fileCount = count( $aryFiles );

		if ( $aryFiles != false && $fileCount > $this->max_file_count ) {
			//sort by date so oldest are first
			usort( $aryFiles, create_function( '$a,$b', 'return filemtime($a) - filemtime($b);' ) );
			$countDelete = $fileCount - $this->max_file_count;
			for ( $i = 0; $i < $countDelete; $i ++ ) {
				if ( file_exists( $aryFiles[ $i ] ) ) {
					unlink( $aryFiles[ $i ] );
				}
			}
		}
	}

	private function delete_log_files () {
		$log_dir = $this->get_log_dir();
		if ( is_dir( $log_dir ) ) {
			$files = glob( "{$log_dir}{,.}*", GLOB_BRACE ); // get all file names
			foreach ( $files as $file ) { // iterate files
				if ( is_file( $file ) ) {
					unlink( $file );
				} // delete file
			}
			rmdir( $log_dir );
		}
	}
}