<?php
/**
 * @package   Press_Plus
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PPP_Parent {

	/**
	 * Instance of this class
	 *
	 * @since 1.0.0
	 *
	 * @var null
	 */
	private static $_this = null;

	/**
	 * Path to parent main plugin file with plugin data
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $path = '';

	/**
	 * Main parent class, for checking if it's loaded
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $main_class = '';

	/**
	 * Current version of parent plugin
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $current_version = '';

	/**
	 * Path to child main plugin file with plugin data
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $child = '';

	/**
	 * Minimum version of parent allowed by child
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private $min_version = '';

	/**
	 * @param array  $parent {
	 *
	 * @type string  $path
	 * @type string  $main_class
	 * @type string  $current_version
	 * @type string  $min_version
	 * }
	 *
	 * @param string $child
	 */
	public function __construct( $parent, $child ) {

		$this->path        = plugin_basename( $parent[ 'path' ] );

		$this->main_class  = $parent[ 'main_class' ];
		$this->min_version = $parent[ 'min_version' ];

		if ( empty( $parent[ 'current_version' ] ) ) {

			$plugin_data = get_file_data( $parent[ 'path' ], array( 'Version' => 'Version' ), 'plugin' );

			$this->current_version = $plugin_data[ 'Version' ];

		} else {

			$this->current_version = $parent[ 'current_version' ];

		}

		$this->child = trim( $child );

		self::$_this = $this;

		add_action( 'init', array( $this, 'init' ) );
	}

	public function init() {
		register_deactivation_hook( $this->path, array( $this, 'deactivate_parent' ) );
	}

	/**
	 * Make sure parent is installed before activating this plugin
	 *
	 * @since 1.0.0
	 *
	 * @uses  deactivate_plugins()
	 * @uses  __()
	 *
	 * @return void
	 */
	public static function check_for_parent( $path = null, $main_class = null, $child = null ) {
		$path       = empty( $path ) ? self::$_this->path : $path;
		$main_class = empty( $main_class ) ? self::$_this->main_class : $main_class;
		$child      = empty( $child ) ? self::$_this->child : $child;

		if ( ( array_key_exists( 'action', $_POST ) ) && ( 'activate-selected' == $_POST[ 'action' ] ) && ( in_array( $path, $_POST[ 'checked' ] ) ) ) {
			return;
		} else if ( ! class_exists( $main_class ) ) {
			deactivate_plugins( $child );
			$message = __( 'You must install and activate the parent plugin first.', '' );
			die( $message );
		}
	}

	/**
	 * Check for the correct version of parent
	 *
	 * @since 1.0.0
	 *
	 * @return bool|mixed
	 */
	public function is_parent_supported() {
		if ( class_exists( $this->main_class ) ) {
			$is_correct_version = version_compare( $this->current_version, $this->min_version, '>=' );

			return $is_correct_version;
		} else {
			return false;
		}
	}


	/**
	 *  Deactivate active child plugin
	 *
	 * Prevents a fatal error if child plugin is still active when user attempts to deactivate parent
	 *
	 * @since 1.0.0
	 *
	 * @uses  plugin_basename()
	 * @uses  is_plugin_active()
	 * @uses  __()
	 * @uses  get_site_transient()
	 * @uses  get_transient()
	 * @uses  set_site_transient()
	 * @uses  set_transient()
	 * @uses  self_admin_url()
	 * @uses  wp_redirect()
	 *
	 * @param $network_deactivating
	 *
	 * @return void
	 */
	public function deactivate_parent( $network_deactivating ) {
		$plugin = plugin_basename( trim( $this->child ) );
		if ( ( array_key_exists( 'action', $_POST ) ) && ( 'deactivate-selected' == $_POST[ 'action' ] ) && ( in_array( $plugin, $_POST[ 'checked' ] ) ) ) {
			return;
		} else if ( is_plugin_active( $plugin ) ) {
			if ( $network_deactivating ) {
				add_action( 'update_site_option_active_sitewide_plugins', array(
					$this,
					'update_site_option_active_sitewide_plugins'
				) );
			} else {
				add_action( 'update_option_active_plugins', array( $this, 'update_option_active_plugins' ) );
			}
		}
	}

	public function update_option_active_plugins() {
		remove_action( 'update_option_active_plugins', array( $this, 'update_options_active_plugins' ) );
		$plugin = plugin_basename( trim( $this->child ) );
		deactivate_plugins( $plugin );
		update_option( 'recently_activated', array( $plugin => time() ) + (array) get_option( 'recently_activated' ) );
	}

	public function update_site_option_active_sitewide_plugins() {
		remove_action( 'update_site_option_active_sitewide_plugins', array(
			$this,
			'update_site_option_active_sitewide_plugins'
		) );
		$plugin = plugin_basename( trim( $this->child ) );
		deactivate_plugins( $plugin );
	}

}