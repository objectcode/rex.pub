<?php

/**
 * Adapted from WP Metadata API UI
 */
class PPFRM_Stripe_Loader {

	private static $_autoload_classes = array(
		'PPFRM_Stripe_App_Controller'      => 'class-app-controller.php',
		'PPFRM_Stripe_App_Helper'          => 'class-app-helper.php',
		'PPP_Stripe_API'                   => 'api/class-stripe-api.php',
		'PPFRM_Stripe_Customer_API'        => 'api/class-customer-api.php',
		'PPFRM_Stripe_DB_Transactions'     => 'database/class-db-transactions.php',
		'PPFRM_Stripe_Global_Settings'     => 'global-settings/class-global-settings.php',
		'PPFRM_Stripe_Form_Settings'       => 'form-settings/class-form-settings.php',
		'PPFRM_Stripe_Form_Action'       => 'form-settings/class-form-action.php',
		'PPFRM_Stripe_Form_Display'        => 'form-display/class-form-display.php',
		'PPFRM_Stripe_Entry_Processor'     => 'entries/class-processor.php',
		'PPFRM_Stripe_Entry_Display'       => 'entries/class-entry-display.php',
		'PPFRM_Stripe_DB_Events'           => 'events/class-db-events.php',
		'PPP_Stripe_Event'                 => 'events/class-stripe-event.php',
		'PPFRM_Stripe_Event_Handler'       => 'events/class-event-handler.php',
		'PPFRM_Stripe_Integration_Members' => 'integrations/class-members.php',
		'PPP_Currency'                     => 'currency/class-currency.php',
		'PPP_Parent'                       => 'utilities/class-parent.php',
		'PPP_Logger'                       => 'utilities/class-logger.php',
		'PPP_Admin_Notices'                => 'utilities/class-notices.php',
		'PPP_DB'                           => 'utilities/class-db.php',
	);

	static function load() {
		spl_autoload_register( array( __CLASS__, '_autoloader' ) );
	}

	/**
	 * @param string $class_name
	 * @param string $class_filepath
	 *
	 * @return bool Return true if it was registered, false if not.
	 */
	static function register_autoload_class( $class_name, $class_filepath ) {

		if ( ! isset( self::$_autoload_classes[ $class_name ] ) ) {

			self::$_autoload_classes[ $class_name ] = $class_filepath;

			return true;

		}

		return false;

	}

	/**
	 * @param string $class_name
	 */
	static function _autoloader( $class_name ) {

		if ( isset( self::$_autoload_classes[ $class_name ] ) ) {

			$filepath = self::$_autoload_classes[ $class_name ];

			/**
			 * @todo This needs to be made to work for Windows...
			 */
			if ( '/' == $filepath[ 0 ] ) {

				require_once( $filepath );

			} else {

				require_once( dirname( dirname( __FILE__ ) ) . "/{$filepath}" );

			}

		}

	}
}