<?php
/**
 * @package   Press_Plus
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PPP_Currency {

	public static function get_all_currencies() {
		$currencies = array(
			'USD' => array( 'name'               => __( 'United States Dollar', 'press-plus' ),
			                'symbol_left'        => '$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'AED' => array( 'name'               => __( 'United Arab Emirates Dirham', 'press-plus' ),
			                'symbol_left'        => '&#1583;.&#1573;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'AFN' => array( 'name'               => __( 'Afghan Afghani', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => '&#1547;',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'ALL' => array( 'name'               => __( 'Albanian Lek', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'L',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'AMD' => array( 'name'               => __( 'Armenian Dram', 'press-plus' ),
			                'symbol_left'        => 'AMD',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'ANG' => array( 'name'               => __( 'Netherlands Antillean Gulden', 'press-plus' ),
			                'symbol_left'        => '&#402;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'AOA' => array( 'name'               => __( 'Angolan Kwanza', 'press-plus' ),
			                'symbol_left'        => 'Kz',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'ARS' => array( 'name'               => __( 'Argentine Peso', 'press-plus' ),
			                'symbol_left'        => 'ARS$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'AUD' => array( 'name'               => __( 'Australian Dollar', 'press-plus' ),
			                'symbol_left'        => 'A$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'AWG' => array( 'name'               => __( 'Aruban Florin', 'press-plus' ),
			                'symbol_left'        => '&#402;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'AZN' => array( 'name'               => __( 'Azerbaijani Manat', 'press-plus' ),
			                'symbol_left'        => '&#1084;&#1072;&#1085;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BAM' => array( 'name'               => __( 'Bosnia & Herzegovina Convertible Mark', 'press-plus' ),
			                'symbol_left'        => 'KM',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BBD' => array( 'name'               => __( 'Barbadian Dollar', 'press-plus' ),
			                'symbol_left'        => 'Bbd$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BDT' => array( 'name'               => __( 'Bangladeshi Taka', 'press-plus' ),
			                'symbol_left'        => '&#2547;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BGN' => array( 'name'               => __( 'Bulgarian Lev', 'press-plus' ),
			                'symbol_left'        => '&#1083;&#1074;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BIF' => array( 'name'               => __( 'Burundian Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BIF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'BMD' => array( 'name'               => __( 'Bermudian Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BMD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BND' => array( 'name'               => __( 'Brunei Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BND',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BOB' => array( 'name'               => __( 'Bolivian Boliviano', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BOB',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'BRL' => array( 'name'               => __( 'Brazilian Real', 'press-plus' ),
			                'symbol_left'        => 'R$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'BSD' => array( 'name'               => __( 'Bahamian Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BSD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BWP' => array( 'name'               => __( 'Botswana Pula', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BWP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'BZD' => array( 'name'               => __( 'Belize Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'BZP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'CAD' => array( 'name'               => __( 'Canadian Dollar', 'press-plus' ),
			                'symbol_left'        => 'CAD$',
			                'symbol_right'       => 'CAD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'CDF' => array( 'name'               => __( 'Congolese Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'CDF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'CHF' => array( 'name'               => __( 'Swiss Franc', 'press-plus' ),
			                'symbol_left'        => 'Fr',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => "'",
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'CLP' => array( 'name'               => __( 'Chilean Peso', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'CLP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'CNY' => array( 'name'               => __( 'Chinese Renminbi Yuan', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'CNY',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'COP' => array( 'name'               => __( 'Colombian Peso', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'COP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'CRC' => array( 'name'               => __( 'Costa Rican Colón', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'CRC',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'CVE' => array( 'name'               => __( 'Cape Verdean Escudo', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'CVE',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'CZK' => array( 'name'               => __( 'Czech Koruna', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => '&#75;&#269;',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ' ',
			                'decimal_separator'  => ',',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'DJF' => array( 'name'               => __( 'Djiboutian Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'DJF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'DKK' => array( 'name'               => __( 'Danish Krone', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'kr.',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => '.',
			                'decimal_separator'  => ',',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'DOP' => array( 'name'               => __( 'Dominican Peso', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'DOP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'DZD' => array( 'name'               => __( 'Algerian Dinar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'DZD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'EEK' => array( 'name'               => __( 'Estonian Kroon', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'EEK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'EGP' => array( 'name'               => __( 'Egyptian Pound', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'EGP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'ETB' => array( 'name'               => __( 'Ethiopian Birr', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'ETB',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'EUR' => array( 'name'               => __( 'Euro', 'press-plus' ),
			                'symbol_left'        => '&#8364;',
			                'symbol_right'       => '',
			                'symbol_padding'     => '',
			                'thousand_separator' => ' ',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'FJD' => array( 'name'               => __( 'Fijian Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'FJD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'FKP' => array( 'name'               => __( 'Falkland Islands Pound', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'FKP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'GBP' => array( 'name'               => __( 'British Pound', 'press-plus' ),
			                'symbol_left'        => '&#163;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'GEL' => array( 'name'               => __( 'Georgian Lari', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GEL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'GIP' => array( 'name'               => __( 'Gibraltar Pound', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GIP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'GMD' => array( 'name'               => __( 'Gambian Dalasi', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GMD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'GNF' => array( 'name'               => __( 'Guinean Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GNF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'GTQ' => array( 'name'               => __( 'Guatemalan Quetzal', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GTQ',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'GYD' => array( 'name'               => __( 'Guyanese Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'GYD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'HKD' => array( 'name'               => __( 'Hong Kong Dollar', 'press-plus' ),
			                'symbol_left'        => 'HK$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'HNL' => array( 'name'               => __( 'Honduran Lempira', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'HNL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'HRK' => array( 'name'               => __( 'Croatian Kuna', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'HRK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'HTG' => array( 'name'               => __( 'Haitian Gourde', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'HTG',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'HUF' => array( 'name'               => __( 'Hungarian Forint', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'Ft',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => '.',
			                'decimal_separator'  => ',',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'IDR' => array( 'name'               => __( 'Indonesian Rupiah', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'IDR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'ILS' => array( 'name'               => __( 'Israeli New Sheqel', 'press-plus' ),
			                'symbol_left'        => '&#8362;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'INR' => array( 'name'               => __( 'Indian Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'INR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'ISK' => array( 'name'               => __( 'Icelandic Króna', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'ISK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'JMD' => array( 'name'               => __( 'Jamaican Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'JMD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'JPY' => array( 'name'               => __( 'Japanese Yen', 'press-plus' ),
			                'symbol_left'        => '&#165;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'KES' => array( 'name'               => __( 'Kenyan Shilling', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KES',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'KGS' => array( 'name'               => __( 'Kyrgyzstani Som', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KGS',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'KHR' => array( 'name'               => __( 'Cambodian Riel', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KHR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'KMF' => array( 'name'               => __( 'Comorian Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KMF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'KRW' => array( 'name'               => __( 'South Korean Won', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KRW',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'KYD' => array( 'name'               => __( 'Cayman Islands Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KYD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'KZT' => array( 'name'               => __( 'Kazakhstani Tenge', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'KZT',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LAK' => array( 'name'               => __( 'Lao Kip', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LAK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'LBP' => array( 'name'               => __( 'Lebanese Pound', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LBP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LKR' => array( 'name'               => __( 'Sri Lankan Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LKR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LRD' => array( 'name'               => __( 'Liberian Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LRD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LSL' => array( 'name'               => __( 'Lesotho Loti', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LSL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LTL' => array( 'name'               => __( 'Lithuanian Litas', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LTL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'LVL' => array( 'name'               => __( 'Latvian Lats', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'LVL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MAD' => array( 'name'               => __( 'Moroccan Dirham', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MAD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MDL' => array( 'name'               => __( 'Moldovan Leu', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MDL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MGA' => array( 'name'               => __( 'Malagasy Ariary', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MGA',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'MKD' => array( 'name'               => __( 'Macedonian Denar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MKD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MNT' => array( 'name'               => __( 'Mongolian Tögrög', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MNT',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MOP' => array( 'name'               => __( 'Macanese Pataca', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MOP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MRO' => array( 'name'               => __( 'Mauritanian Ouguiya', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MRO',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MUR' => array( 'name'               => __( 'Mauritian Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MUR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'MVR' => array( 'name'               => __( 'Maldivian Rufiyaa', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MVR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MWK' => array( 'name'               => __( 'Malawian Kwacha', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MWK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MXN' => array( 'name'               => __( 'Mexican Peso', 'press-plus' ),
			                'symbol_left'        => 'MXN$',
			                'symbol_right'       => 'MXN',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'MYR' => array( 'name'               => __( 'Malaysian Ringgit', 'press-plus' ),
			                'symbol_left'        => '&#82;&#77;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'MZN' => array( 'name'               => __( 'Mozambican Metical', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'MZN',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'NAD' => array( 'name'               => __( 'Namibian Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'NAD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'NGN' => array( 'name'               => __( 'Nigerian Naira', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'NGN',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'NIO' => array( 'name'               => __( 'Nicaraguan Córdoba', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'NIO',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'NOK' => array( 'name'               => __( 'Norwegian Krone', 'press-plus' ),
			                'symbol_left'        => 'Kr',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'NPR' => array( 'name'               => __( 'Nepalese Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'NPR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'NZD' => array( 'name'               => __( 'New Zealand Dollar', 'press-plus' ),
			                'symbol_left'        => 'NZ$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'PAB' => array( 'name'               => __( 'Panamanian Balboa', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'PAB',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'PEN' => array( 'name'               => __( 'Peruvian Nuevo Sol', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'PEN',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'PGK' => array( 'name'               => __( 'Papua New Guinean Kina', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'PGK',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'PHP' => array( 'name'               => __( 'Philippine Peso', 'press-plus' ),
			                'symbol_left'        => '&#8369;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'PKR' => array( 'name'               => __( 'Pakistani Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'PKR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'PLN' => array( 'name'               => __( 'Polish Złoty', 'press-plus' ),
			                'symbol_left'        => '&#122;&#322;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => '.',
			                'decimal_separator'  => ',',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'PYG' => array( 'name'               => __( 'Paraguayan Guaraní', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'PYG',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'QAR' => array( 'name'               => __( 'Qatari Riyal', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'QAR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'RON' => array( 'name'               => __( 'Romanian Leu', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'RON',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'RSD' => array( 'name'               => __( 'Serbian Dinar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'RSD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'RUB' => array( 'name'               => __( 'Russian Ruble', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'RUB',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'RWF' => array( 'name'               => __( 'Rwandan Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'RWF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'SAR' => array( 'name'               => __( 'Saudi Riyal', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SAR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SBD' => array( 'name'               => __( 'Solomon Islands Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SBD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SCR' => array( 'name'               => __( 'Seychellois Rupee', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SCR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SEK' => array( 'name'               => __( 'Swedish Krona', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'kr',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ' ',
			                'decimal_separator'  => ',',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SGD' => array( 'name'               => __( 'Singapore Dollar', 'press-plus' ),
			                'symbol_left'        => 'S$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SHP' => array( 'name'               => __( 'Saint Helenian Pound', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SHP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'SLL' => array( 'name'               => __( 'Sierra Leonean Leone', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SLL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SOS' => array( 'name'               => __( 'Somali Shilling', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SOS',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SRD' => array( 'name'               => __( 'Surinamese Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SRD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'STD' => array( 'name'               => __( 'São Tomé and Príncipe Dobra', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'STD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'SVC' => array( 'name'               => __( 'Salvadoran Colón', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SVC',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'SZL' => array( 'name'               => __( 'Swazi Lilangeni', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'SZL',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'THB' => array( 'name'               => __( 'Thai Baht', 'press-plus' ),
			                'symbol_left'        => '&#3647;',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TJS' => array( 'name'               => __( 'Tajikistani Somoni', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'TJS',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TOP' => array( 'name'               => __( 'Tongan Paʻanga', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'TOP',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TRY' => array( 'name'               => __( 'Turkish Lira', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'TRY',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TTD' => array( 'name'               => __( 'Trinidad and Tobago Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'TTD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TWD' => array( 'name'               => __( 'New Taiwan Dollar', 'press-plus' ),
			                'symbol_left'        => 'NT$',
			                'symbol_right'       => '',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'TZS' => array( 'name'               => __( 'Tanzanian Shilling', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'TZS',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'UAH' => array( 'name'               => __( 'Ukrainian Hryvnia', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'UAH',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'UGX' => array( 'name'               => __( 'Ugandan Shilling', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'UGX',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'UYU' => array( 'name'               => __( 'Uruguayan Peso', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'UYU',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'UZS' => array( 'name'               => __( 'Uzbekistani Som', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'UZS',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'VEF' => array( 'name'               => __( 'Venezuelan Bolívar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'VEF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => false
			),
			'VND' => array( 'name'               => __( 'Vietnamese Đồng', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'VND',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'VUV' => array( 'name'               => __( 'Vanuatu Vatu', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'VUV',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'WST' => array( 'name'               => __( 'Samoan Tala', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'WST',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'XAF' => array( 'name'               => __( 'Central African Cfa Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'XAF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => true
			),
			'XCD' => array( 'name'               => __( 'East Caribbean Dollar', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'XCD',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'XOF' => array( 'name'               => __( 'West African Cfa Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'XOF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'XPF' => array( 'name'               => __( 'Cfp Franc', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'XPF',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 0,
			                'american_express'   => false
			),
			'YER' => array( 'name'               => __( 'Yemeni Rial', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'YER',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'ZAR' => array( 'name'               => __( 'South African Rand', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'ZAR',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			),
			'ZMW' => array( 'name'               => __( 'Zambian Kwacha', 'press-plus' ),
			                'symbol_left'        => '',
			                'symbol_right'       => 'ZMW',
			                'symbol_padding'     => ' ',
			                'thousand_separator' => ',',
			                'decimal_separator'  => '.',
			                'decimals'           => 2,
			                'american_express'   => true
			)
		);

		$currencies = apply_filters( 'ppp_currencies', $currencies );

		return $currencies;
	}

	public static function get_currency( $currency_code ) {
		$currencies = self::get_all_currencies();

		return $currencies[ $currency_code ];
	}

	public static function to_number( $money, $currency_code ) {
		$money = strval( $money );

		if ( is_numeric( $money ) ) {
			return floatval( $money );
		}

		//Making sure symbol is in unicode format (i.e. &#4444;)
		$money = preg_replace( "/&.*?;/", '', $money );

		$currency = self::get_currency( $currency_code );

		//Removing symbol from text
		$money = str_replace( $currency[ 'symbol_right' ], '', $money );
		$money = str_replace( $currency[ 'symbol_left' ], '', $money );


		//Removing all non-numeric characters
		$array        = str_split( $money );
		$is_negative  = false;
		$clean_number = '';
		foreach ( $array as $char ) {

			if ( ( $char >= '0' && $char <= '9' ) || $char == $currency[ 'decimal_separator' ] ) {
				$clean_number .= $char;
			} else if ( $char == '-' ) {
				$is_negative = true;
			}
		}

		$decimal_separator = $currency && $currency[ 'decimal_separator' ] ? $currency[ 'decimal_separator' ] : '.';

		//Removing thousand separators but keeping decimal point
		$array        = str_split( $clean_number );
		$float_number = '';
		for ( $i = 0, $count = sizeof( $array ); $i < $count; $i ++ ) {
			$char = $array[ $i ];

			if ( $char >= '0' && $char <= '9' ) {
				$float_number .= $char;
			} else if ( $char == $decimal_separator ) {
				$float_number .= '.';
			}
		}

		if ( $is_negative ) {
			$float_number = '-' . $float_number;
		}

		return is_numeric( $float_number ) ? floatval( $float_number ) : false;
	}

	public static function to_money( $number, $currency_code, $do_encode = false ) {

		if ( ! is_numeric( $number ) ) {
			$number = self::to_number( $number, $currency_code );
		}

		if ( $number === false ) {
			return '';
		}

		$negative = '';
		if ( strpos( strval( $number ), '-' ) !== false ) {
			$negative = '-';
			$number   = floatval( substr( $number, 1 ) );
		}

		$currency = self::get_currency( $currency_code );

		$money        = number_format( $number, $currency[ 'decimals' ], $currency[ 'decimal_separator' ], $currency[ 'thousand_separator' ] );
		$symbol_left  = ! empty( $currency[ 'symbol_left' ] ) ? $currency[ 'symbol_left' ] . $currency[ 'symbol_padding' ] : '';
		$symbol_right = ! empty( $currency[ 'symbol_right' ] ) ? $currency[ 'symbol_padding' ] . $currency[ 'symbol_right' ] : '';

		if ( $do_encode ) {
			$symbol_left  = html_entity_decode( $symbol_left );
			$symbol_right = html_entity_decode( $symbol_right );
		}

		return $negative . $symbol_left . $money . $symbol_right;
	}

	/**
	 * @param $symbol
	 *
	 * @return string
	 */
	public static function get_currency_from_symbol( $symbol ) {
		$currency = '';

		$currencies = self::get_all_currencies();
		foreach ( $currencies as $currency_code => $currency_info ) {
			if ( ( $currency_info[ 'symbol_left' ] == $symbol )
			     || ( $currency_info[ 'symbol_right' ] == $symbol )
			     || ( html_entity_decode( $currency_info[ 'symbol_left' ], ENT_QUOTES, 'UTF-8' ) == $symbol )
			     || ( html_entity_decode( $currency_info[ 'symbol_right' ], ENT_QUOTES, 'UTF-8' ) == $symbol )
			) {
				$currency = $currency_code;
				break;
			}
		}

		return $currency;
	}

	/**
	 * @param $text
	 *
	 * @return string
	 */
	public static function get_currency_symbol_from_text( $text ) {
		$text = strval( $text );

		//Making sure symbol is in unicode format (i.e. &#4444;)
		$text = preg_replace( "/&.*?;/", "", $text );

		$array           = str_split( $text );
		$currency_symbol = '';
		foreach ( $array as $key => $char ) {
			if ( ( ' ' !== $char ) && ( ! ctype_digit( $char ) ) && ( ',' !== $char ) ) {
				if ( ( '.' !== $char ) || ( ( ! ctype_digit( $array[ $key - 1 ] ) ) && ( ! ctype_digit( $array[ $key + 1 ] ) ) ) ) {
					$currency_symbol .= $char;
				}
			}
		}

		return $currency_symbol;
	}

	/**
	 * Builds a dropdown list of currencies
	 *
	 * Adapted from wp_dropdown_categories()
	 *
	 * @since 1.0.0
	 *
	 * @param string $args
	 *
	 * @return string
	 */
	public static function dropdown_currencies( $args = '' ) {
		$defaults = array(
			'show_option_all'  => '',
			'show_option_none' => '',
			'exclude'          => '',
			'echo'             => 1,
			'selected'         => 0,
			'name'             => '',
			'id'               => '',
			'class'            => '',
			'tab_index'        => 0
		);

		$build_options = wp_parse_args( $args, $defaults );

		extract( $build_options );

		$tab_index_attribute = '';
		if ( (int) $tab_index > 0 ) {
			$tab_index_attribute = " tabindex=\"$tab_index\"";
		}

		$currencies = array_keys( self::get_all_currencies() );
		$name       = esc_attr( $name );
		$class      = esc_attr( $class );
		$id         = $id ? esc_attr( $id ) : $name;

		if ( ! empty( $currencies ) ) {
			$output = "<select name='$name' id='$id' class='$class' $tab_index_attribute>\n";
		} else {
			$output = '';
		}

		if ( empty( $currencies ) && ! empty( $show_option_none ) ) {
			$output .= "\t<option value='-1' selected='selected'>$show_option_none</option>\n";
		}

		if ( ! empty( $currencies ) ) {

			if ( ! empty( $show_option_all ) ) {
				$selected = ( '0' === strval( $build_options[ 'selected' ] ) ) ? " selected='selected'" : '';
				$output .= "\t<option value='0'$selected>$show_option_all</option>\n";
			}

			if ( ! empty( $show_option_none ) ) {
				$selected = ( '-1' === strval( $build_options[ 'selected' ] ) ) ? " selected='selected'" : '';
				$output .= "\t<option value='-1'$selected>$show_option_none</option>\n";
			}

			foreach ( $currencies as $currency ) {
				$output .= "\t<option value=\"" . $currency . "\"";
				if ( $currency == $args[ 'selected' ] ) {
					$output .= ' selected="selected"';
				}
				$output .= '>';
				$output .= $currency;
				$output .= "</option>\n";
			}
		}

		if ( ! empty( $currencies ) ) {
			$output .= "</select>\n";
		}

		if ( $echo ) {
			echo $output;
		}

		return $output;
	}
}