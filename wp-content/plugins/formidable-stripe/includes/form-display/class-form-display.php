<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Form_Display Class
 *
 * Controls everything related to the form display
 *
 * @since 1.0.0
 *
 */
class PPFRM_Stripe_Form_Display {

	/**
	 * Form we are displaying
	 *
	 * @since 1.0.0
	 *
	 * @var
	 */
	private $form;

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Init actions
	 *
	 * @since 1.0.0
	 */
	public function init() {

		add_action( 'frm_enqueue_form_scripts', array( $this, 'frm_enqueue_form_scripts' ) );
		add_action( 'frm_entry_form', array( $this, 'frm_entry_form' ), 10, 3 );

	}

	/**
	 * Add Stripe checkout JS
	 *
	 * @since 1.0.0
	 *
	 * @param array $params
	 */
	public function frm_enqueue_form_scripts( $params ) {

		global $frm_vars;

		if ( isset( $frm_vars[ 'created_entries' ][ $params[ 'form_id' ] ][ 'errors' ] ) && isset( $frm_vars[ 'created_entries' ][ $params[ 'form_id' ] ][ 'entry_id' ] ) ) {

			return;

		}

		$frm_form   = new FrmForm();
		$this->form = $frm_form->getOne( $params[ 'form_id' ] );

		if ( ( PPFRM_Stripe_App_Helper::is_stripe_form( $this->form ) ) && ( ! empty( $this->form->options[ 'ajax_submit' ] ) || PPFRM_Stripe_App_Helper::is_last_page( $this->form ) ) ) {

			if ( ! empty( $frm_vars[ 'ajax' ] ) ) {
				add_action( 'wp_print_footer_scripts', array( $this, 'footer_js' ), 100 );
			} else {
				add_action( 'wp_footer', array( $this, 'footer_js' ), 100 );
			}

			wp_enqueue_script( 'ppfrm_stripe_checkout', 'https://checkout.stripe.com/checkout.js', array( 'jquery' ) );

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			wp_register_script( 'ppfrm_stripe_form_display_js', trailingslashit( PPFRM_STRIPE_URL ) . "includes/form-display/js/form-display{$suffix}.js", array(
				'jquery',
				'ppfrm_stripe_checkout'
			), PPFRM_STRIPE_CURRENT_VERSION );

			$form_actions = FrmFormActionsHelper::get_action_for_form( $this->form->id, 'stripe' );

			foreach ( $form_actions as $action ) {

				$actions[ ] = $action->post_content;

				$publishable_key = PPFRM_Stripe_App_Helper::get_api_key( 'publishable', PPFRM_Stripe_App_Helper::get_form_stripe_mode( $action ) );

			}


			foreach ( $actions as $key => $action ) {

				$actions[ $key ][ 'currency' ] = array(
					'code' => $action[ 'currency' ],
					'info' => PPP_Currency::get_currency( $action[ 'currency' ] )
				);

				if ( ! empty( $action['charge_amount_field'] ) ) {

					$field = FrmField::getOne( $action['charge_amount_field'] );

					$actions[ $key ][ 'charge_amount_field_type' ] = $field->type;

					unset( $field );
				}

				if ( ! empty( $action['charge_description_field'] ) ) {

									$field = FrmField::getOne( $action['charge_description_field'] );

									$actions[ $key ][ 'charge_description_field_type' ] = $field->type;

									unset( $field );
								}

				if ( ! empty( $action[ 'conditions' ] ) ) {

					foreach ( $action[ 'conditions' ] as $condition_key => $condition ) {

						if ( ! is_numeric( $condition_key ) ) {
							continue;
						}

						$field = FrmField::getOne( $condition[ 'hide_field' ] );

						$actions[ $key ][ 'conditions' ][ $condition_key ][ 'hide_field_type' ] = $field->type;

						unset( $field );
						unset( $condition_key );
						unset( $condition );
					}

				}

				unset( $key );
				unset( $action );
			}

			$ppfrm_stripe_vars = array(
				'form_id' => $this->form->id,
				'key'     => $publishable_key,
				'actions' => $actions
			);

			wp_localize_script( 'ppfrm_stripe_form_display_js', 'ppfrm_stripe_vars', $ppfrm_stripe_vars );
		}

	}

	/**
	 * Add script
	 *
	 * @since 1.0.0
	 *
	 */
	public function footer_js() {

		global $wp_scripts;

		if ( ! is_a( $wp_scripts, 'WP_Scripts' ) ) {

			$wp_scripts = new WP_Scripts();

		}

		$wp_scripts->print_extra_script( 'ppfrm_stripe_form_display_js' );

		$suffix  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		$version = PPFRM_STRIPE_CURRENT_VERSION;

		echo "<script type='text/javascript' src='" . trailingslashit( PPFRM_STRIPE_URL ) . "includes/form-display/js/form-display{$suffix}.js?ver={$version}'></script>";

	}

	/**
	 * Add Stripe token field to form
	 *
	 * @since 1.0.0
	 *
	 * @param $form
	 * @param $form_action
	 * @param $errors
	 */
	public function frm_entry_form( $form, $form_action, $errors ) {

		if ( ( 'create' == $form_action ) && ( PPFRM_Stripe_App_Helper::is_stripe_form( $form ) ) ) {

			echo '<input type="hidden" name="ppfrm_stripe[error]" value=""/>' . "\n";
			echo '<input type="hidden" name="ppfrm_stripe[token]" value=""/>' . "\n";

		}

	}
}