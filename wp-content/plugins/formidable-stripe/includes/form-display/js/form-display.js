/**
 *
 */

var stripe_checkout_handler = StripeCheckout.configure( {
															key: ppfrm_stripe_vars.key,
															image: '',
															token: ppfrm_stripe_token_callback
														} );

jQuery( document ).ready( function () {

	jQuery( document ).off( 'submit.formidable', '.frm-show-form' );
	jQuery( document ).on( 'submit.formidable', '.frm-show-form', ppfrm_stripe_submit_form );


	ppfrm_stripe_vars.sent_to_validation = false;

	jQuery( window ).on( 'popstate', function () {

		ppfrm_stripe_vars.sent_to_validation = false;

		stripe_checkout_handler.close();

	} );

} );


function ppfrm_stripe_submit_form( e ) {

	e.preventDefault();

	if ( jQuery( this ).find( '.wp-editor-wrap' ).length && typeof(tinyMCE) != 'undefined' ) {
		tinyMCE.triggerSave();
	}

	var object = this;

	ppfrm_stripe_vars.form_object = object;

	var action = jQuery( object ).find( 'input[name="frm_action"]' ).val();

	ppfrm_stripe_vars.form_action = action;

	var jsErrors = [];

	if ( typeof frmThemeOverride_jsErrors == 'function' ) {

		jsErrors = frmThemeOverride_jsErrors( action, object );

	}

	if ( jsErrors.length === 0 ) {

		if ( ( 'create' == action ) && ( ppfrm_is_final_submit() ) && ( !ppfrm_is_draft_save() ) ) {

			var validation_error = jQuery( '.frm_error' ).length;

			if ( ppfrm_stripe_vars.sent_to_validation && 0 == validation_error ) {

				jQuery( '#frm_form_' + ppfrm_stripe_vars.form_id + '_container .frm-show-form' ).get( 0 ).submit();

			}
			else {

				ppfrm_stripe_vars.sent_to_validation = true;

				var submit_form = ppfrm_stripe_open_checkout();

				if ( submit_form ) {

					frmFrontForm.checkFormErrors( object, action );

				}
				else {

					return false;

				}

			}

		}
		else {

			frmFrontForm.checkFormErrors( object, action );

		}

	} else {

		for ( var key in jsErrors ) {

			$fieldCont = jQuery( object ).find( jQuery( document.getElementById( 'frm_field_' + key + '_container' ) ) );

			if ( $fieldCont.length && $fieldCont.is( ':visible' ) ) {

				$fieldCont.addClass( 'frm_blank_field' );

				if ( typeof frmThemeOverride_frmPlaceError == 'function' ) {

					frmThemeOverride_frmPlaceError( key, jsErrors );

				} else {

					$fieldCont.append( '<div class="frm_error">' + jsErrors[key] + '</div>' );

				}

			}

		}

	}

}

function ppfrm_stripe_token_callback( token ) {

	var form = jQuery( '#frm_form_' + ppfrm_stripe_vars.form_id + '_container .frm-show-form' );

	if ( token.error ) {
		form.find( "[name='ppfrm_stripe[error]']" ).val( token.error.message );

	}
	else {
		form.find( "[name='ppfrm_stripe[token]']" ).val( token.id );

	}

	frmFrontForm.checkFormErrors( ppfrm_stripe_vars.form_object, ppfrm_stripe_vars.form_action );

}

function ppfrm_stripe_open_checkout() {

	var form = jQuery( '#frm_form_' + ppfrm_stripe_vars.form_id + '_container .frm-show-form' );

	for ( var i = 0; i < ppfrm_stripe_vars.actions.length; i++ ) {

		var condition_met = ppfrm_action_conditions_met( form, ppfrm_stripe_vars.actions[i] );

		if ( condition_met ) {

			var action = ppfrm_stripe_vars.actions[i];

			break;
		}

	}

	if ( condition_met ) {

		var email = form.find( "[name='item_meta[" + action['customer_email_field'] + "]']" ).val();

		if ( '' === action['charge_description_field'] ) {
			var description = false;
		}
		else {

			if ( 'checkbox' === action['charge_description_field_type'] || 'radio' === action['charge_description_field_type'] ) {

				var description = form.find( "[name='item_meta[" + action['charge_description_field'] + "]']:checked" ).val();

			}
			else {

				var description = form.find( "[name='item_meta[" + action['charge_description_field'] + "]']" ).val();

			}

		}
		var currency = action['currency']['code'];

		if ( '' === action['charge_amount_field'] ) {

			var amount = false;

		}
		else {

			if ( 'checkbox' === action['charge_amount_field_type'] || 'radio' === action['charge_amount_field_type'] ) {

				var amount = form.find( "[name='item_meta[" + action['charge_amount_field'] + "]']:checked" ).val();

			}
			else {

				var amount = form.find( "[name='item_meta[" + action['charge_amount_field'] + "]']" ).val();

			}

			if ( 2 == action['currency']['info']['decimals'] ) {

				var amount = amount * 100;

			}

		}

		var handler_options = {
			currency: currency,
			email: email,
			address: true
		};

		if ( amount ) {
			handler_options.amount = amount;
		}

		if ( description ) {
			handler_options.description = description;
		}

		stripe_checkout_handler.open( handler_options );

		return false;
	}
	else {
		return true;
	}

}

function ppfrm_action_conditions_met( form, action ) {

	var action_conditions_met = false;

	var met = [];


	if ( ( 'undefined' === typeof( action['conditions'][0] ) ) || ( 0 === action['conditions'][0].length ) ) {

		action_conditions_met = true;

	}
	else {

		for ( var condition in action['conditions'] ) {

			if ( action['conditions'].hasOwnProperty( condition ) ) {

				if ( !ppfrm_is_numeric( condition ) ) {

					continue;
				}

				if ( !action_conditions_met && 'any' == action['conditions']['any_all'] && 'stop' == action['conditions']['send_stop'] ) {

					continue;
				}

				if ( Array.isArray( action['conditions'][condition]['hide_opt'] ) ) {

				}

				if ( 'checkbox' === action['conditions'][condition]['hide_field_type'] || 'radio' === action['conditions'][condition]['hide_field_type'] ) {
					var field_value = form.find( "[name='item_meta[" + action['conditions'][condition]['hide_field'] + "]']:checked" ).val();
				}
				else if ( 'select' == action['conditions'][condition]['hide_field_type'] ) {
					var field_value = form.find( "[name='item_meta[" + action['conditions'][condition]['hide_field'] + "]']" ).val();
				}
				else {
					var field_value = form.find( "[name='item_meta[" + action['conditions'][condition]['hide_field'] + "]']" ).val();
				}

				var operator = action['conditions'][condition]['hide_field_cond'];
				var condition_value = action['conditions'][condition]['hide_opt'];

				action_conditions_met = ppfrm_value_meets_condition( field_value, operator, condition_value );

				met[action_conditions_met | 0] = action_conditions_met;

			}

		}

		/**
		 * if met[0] and met[1], one condition is true and the other is false
		 */
		if ( action['conditions']['any_all'] == 'all' && 'undefined' !== typeof( met ) && 0 < met.length && 'undefined' !== typeof( met[0]) && 'undefined' !== typeof( met[1]) ) {

			action_conditions_met = (action['conditions']['send_stop'] == 'send') ? false : true;

		} else if ( action['conditions']['any_all'] == 'any' && action['conditions']['send_stop'] == 'send' && 'undefined' !== typeof( met[1]) ) {

			action_conditions_met = true;

		}

	}

	return action_conditions_met;
}


function ppfrm_value_meets_condition( observed_value, cond, hide_opt ) {

	var meets_condition = false;

	if ( '==' == cond ) {
		meets_condition = ( observed_value == hide_opt );
	} else if ( '!=' == cond ) {
		meets_condition = (observed_value != hide_opt);
	} else if ( '>' == cond ) {
		meets_condition = (observed_value > hide_opt);
	} else if ( '<' == cond ) {
		meets_condition = (observed_value < hide_opt);
	} else if ( 'LIKE' == cond ) {
		meets_condition = ( -1 !== observed_value.indexOf( hide_opt ) );
	}
	else if ( 'not LIKE' == cond ) {
		meets_condition = ( -1 === observed_value.indexOf( hide_opt ) );
	}

	return meets_condition;

}

function ppfrm_is_last_page() {

	var is_last_page = false;
	var next_page = jQuery( 'input[name="frm_page_order_' + ppfrm_stripe_vars.form_id + '"]' ).val();

	if ( 'undefined' == typeof( next_page ) ) {
		is_last_page = true;
	}

	return is_last_page;

}

function ppfrm_going_to_prev_page() {

	var going_to_prev_page = false;

	var page_to_navigate_to = jQuery( 'input[name="frm_next_page"]' ).val();
	var next_page = jQuery( 'input[name="frm_page_order_' + ppfrm_stripe_vars.form_id + '"]' ).val();

	if ( ( 'undefined' !== typeof( page_to_navigate_to ) ) && ( '' !== page_to_navigate_to ) && ( ppfrm_is_last_page() || page_to_navigate_to < next_page ) ) {
		going_to_prev_page = true;
	}

	return going_to_prev_page;
}

function ppfrm_is_final_submit() {

	var is_final_submit = false;

	if ( ppfrm_is_last_page() && !ppfrm_going_to_prev_page() ) {
		is_final_submit = true;
	}

	return is_final_submit;
}

function ppfrm_is_draft_save() {

	var is_draft_save = false;

	var saving_draft = jQuery( 'input[name="frm_saving_draft"]' ).val();

	if ( ( 'undefined' !== typeof( saving_draft ) ) && ( '1' == saving_draft ) ) {
		is_draft_save = true;
	}

	return is_draft_save;
}

function ppfrm_is_numeric( input ) {
	return (input - 0) == input && input.length > 0;
}