<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Entry_Processor Class
 *
 * Processes the entries
 *
 * @since 1.0.0
 *
 */
class PPFRM_Stripe_Entry_Processor {

	/**
	 * Stripe transaction details
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	private $transactions = array();

	/**
	 * Whether or not we should process the form
	 *
	 * @since 1.0.0
	 *
	 * @var bool
	 */
	private $process = null;

	/**
	 * Information needed to process the transaction
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	private $processor_vars = array();

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Init actions
	 *
	 * @since 1.0.0
	 */
	public function init() {

		add_filter( 'frm_validate_field_entry', array( $this, 'frm_validate_field_entry' ), 10, 3 );

		add_filter( 'frm_validate_entry', array( $this, 'frm_validate_entry' ), 100, 2 );

		add_action( 'frm_trigger_stripe_create_action', array( $this, 'frm_trigger_stripe_create_action' ), 10, 3 );

	}

	/**
	 * Check for Stripe token and add error to charge amount field if there isn't one
	 *
	 * @since    1.0.0
	 *
	 * @param array  $errors
	 * @param object $field
	 * @param mixed  $value
	 *
	 * @return array
	 *
	 */
	public function frm_validate_field_entry( $errors, $field, $value ) {

		$form_id = $field->form_id;
		$form    = FrmForm::getOne( $form_id );

		$this->process = $this->allowed_to_process( $form );

		if ( $this->process ) {

			$this->set_error_field_id();

			if ( $field->id == $this->processor_vars['error_field_id'] ) {

			if ( empty( $_POST[ 'ppfrm_stripe' ][ 'token' ] ) ) {

				$this->process = false;

				if ( 'test' == PPFRM_Stripe_App_Helper::get_form_stripe_mode( $this->processor_vars[ 'action' ] ) ) {

					$error_message = PPFRM_Stripe_App_Helper::get_array_values( $_POST, 'ppfrm_stripe/error' );

				} else {

					$error_message = __( 'Unable to process your card. Please check your card details and try again, or contact the site owner.', 'formidable-stripe' );

				}

				$errors[ 'field' . $field->id ] = $error_message;

				PPFRM_Stripe_App_Controller::$logger->log->error( sprintf( __( 'No token. Error: %s', 'formidable-stripe' ), PPFRM_Stripe_App_Helper::get_array_values( $_POST, 'ppfrm_stripe/error' ) ) );

			}
	}

		}

		return $errors;
	}

	/**
	 * Do Stripe transaction
	 *
	 * Make sure this only happens on the second validation — it needs to be the LAST thing that occurs
	 *
	 * @since 1.0.0
	 *
	 * @param array $errors
	 * @param array $values $_POST values
	 *
	 * @return array
	 */
	public function frm_validate_entry( $errors, $values ) {

		if ( empty( $errors ) ) {

			global $frm_vars;

			if ( ( $this->process ) && ( ( ! empty( $frm_vars[ 'form_params' ][ $_POST[ 'form_id' ] ] ) ) || ( $this->is_ajax_submit() ) ) ) {

				$form = FrmForm::getOne( $_POST[ 'form_id' ] );

				PPFRM_Stripe_App_Controller::$logger->log->debug( "Processing Stripe transaction for form {$_POST['form_id']}" );


				$this->set_processor_vars( $form );

				$this->set_error_field_id();


				$status = $this->get_customer_and_card();

				if ( true !== $status ) {

					$errors[ 'field' . $this->processor_vars['error_field_id'] ] = $status;

					return $errors;

				}

				if ( 'one-time' == $this->processor_vars[ 'transaction_type' ] ) {

					$status = $this->capture();

				} else if ( 'subscription' == $this->processor_vars[ 'transaction_type' ] ) {

					$status = $this->subscribe();

				}

				if ( true !== $status ) {

					$errors[ 'field' . $this->processor_vars['error_field_id'] ] = $status;

					return $errors;

				}

			}

		}

		return $errors;
	}

	/**
	 * Set the field ID where we will return any Stripe errors to the form
	 *
	 * @since 2.0.0
	 *
	 * @return string
	 */
	private function set_error_field_id() {

		if ( ! isset( $this->processor_vars['error_field_id'] ) ) {

			if ( 'one-time' == $this->processor_vars[ 'action' ]->post_content[ 'transaction_type' ] ) {

				$error_field_id = $this->processor_vars[ 'action' ]->post_content[ 'charge_amount_field' ];

			} else if ( 'subscription' == $this->processor_vars[ 'action' ]->post_content[ 'transaction_type' ] ) {

				if ( 'predefined' == $this->processor_vars[ 'action' ]->post_content[ 'subscription_type' ] ) {

					$error_field_id = $this->processor_vars[ 'action' ]->post_content[ 'predefined_plan_id_field' ];

				} else if ( 'custom' == $this->processor_vars[ 'action' ]->post_content[ 'subscription_type' ] ) {

					$error_field_id = $this->processor_vars[ 'action' ]->post_content[ 'custom_plan_amount_field' ];

				}

			}

			$this->processor_vars[ 'error_field_id' ] = $error_field_id;
		}

	}

	/**
	 * Save transactions
	 *
	 * @since    2.0
	 *
	 * @param object $action
	 * @param object $entry
	 * @param object $form
	 */
	public function frm_trigger_stripe_create_action( $action, $entry, $form ) {

		global $ppfrm_stripe_transaction;

		if ( ! empty( $ppfrm_stripe_transaction ) ) {

			$this->save_stripe_info_to_wp_user();

			$this->add_customer_metadata( $entry->id );

			$this->insert_transactions( $entry->id );

			$this->add_object_metadata( $entry->id );

			do_action( 'frm_after_payment_completed', $entry->id, $form->id );
		}

	}

	/**
	 * Check if posted form meets condition to be sent to Stripe
	 *
	 * @since 1.0.0
	 *
	 * @param object $form
	 *
	 * @return bool
	 */
	private function meets_stripe_condition( $form ) {

		$meets_condition = false;

		$form_actions = FrmFormActionsHelper::get_action_for_form( $form->id, 'stripe' );

		foreach ( $form_actions as $action ) {

			$stop = $this->action_conditions_met( $action, $_POST[ 'item_meta' ] );

			if ( $stop ) {

				continue;

			} else {

				$meets_condition = true;

				$this->processor_vars[ 'action' ] = $action;

				break;

			}

		}

		return $meets_condition;

	}

	/**
	 * Check to see if this form should be processed for Stripe
	 *
	 * Only process when a new entry is being created that is not a draft and meets the Stripe condition.
	 * Don't process forms if there's no POST, if this is an entry update
	 *
	 * @since 1.0.0
	 *
	 * @param object $form
	 */
	private function allowed_to_process( $form ) {

		if ( is_null( $this->process ) ) {

			//not a form submission
			if ( empty( $_POST ) ) {
				return false;
			}

			//not the last page of the form
			if ( isset( $_POST[ 'frm_page_order_' . $form->id ] ) ) {
				return false;
			}

			//going to previous page
			if ( FrmProFormsHelper::going_to_prev( $form->id ) ) {
				return false;
			}

			//saving a draft
			if ( FrmProFormsHelper::saving_draft( $form->id ) ) {
				return false;
			}

			//this is an admin submission
			if ( ( ! defined( 'DOING_AJAX' ) ) && is_admin() ) {
				return false;
			}

			//Stripe is not enabled on this form
			if ( ! PPFRM_Stripe_App_Helper::is_stripe_form( $form ) ) {
				return false;
			}

			//Stripe condition was not met
			if ( ! $this->meets_stripe_condition( $form ) ) {
				return false;
			}

			//Stripe token was not added to the form
			if ( empty( $_POST[ 'ppfrm_stripe' ] ) ) {
				return false;
			}

			return true;

		} else {

			return $this->process;

		}

	}

	/**
	 * Detect ajax submit
	 *
	 * Taken from FrmProEntriesController::ajax_create
	 *
	 * @since 1.0.0
	 */
	private function is_ajax_submit() {

		$ajax_submit = false;

		global $frm_form, $wpdb;

		$form = $frm_form->getOne( $_POST[ 'form_id' ] );

		$ajax = ( isset( $form->options[ 'ajax_submit' ] ) ) ? $form->options[ 'ajax_submit' ] : 0;

		//ajax submit if no file, rte, captcha
		if ( $ajax ) {

			$where = $wpdb->prepare( "form_id=%d", $form->id );

			if ( isset( $_POST[ 'frm_page_order_' . $form->id ] ) ) {
				$where .= $wpdb->prepare( " AND field_order < %d", $_POST[ 'frm_page_order_' . $form->id ] );
			}

			$no_ajax_fields = array( 'file' );
			$no_ajax        = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}frm_fields WHERE type in ('" . implode( "','", $no_ajax_fields ) . "') AND {$where} LIMIT 1" );

			if ( $no_ajax ) {
				$ajax = false;
			}

		}

		if ( $ajax ) {

			if ( ( ! isset( $_POST[ 'frm_page_order_' . $form->id ] ) && ! FrmProFormsHelper::going_to_prev( $form->id ) ) || FrmProFormsHelper::saving_draft( $form->id ) ) {
				$ajax_submit = true;
			}
		}

		return $ajax_submit;

	}

	/**
	 * See if action conditions are met
	 *
	 * @see FrmFormActionsHelper::action_conditions_met
	 *
	 * @param $action
	 * @param $entry
	 *
	 * @return bool|int
	 */
	public function action_conditions_met( $action, $submitted_values ) {

		$notification = $action->post_content;

		$stop = false;
		$met  = array();

		if ( ! isset( $notification[ 'conditions' ] ) || empty( $notification[ 'conditions' ] ) ) {
			return $stop;
		}

		foreach ( $notification[ 'conditions' ] as $k => $condition ) {

			if ( ! is_numeric( $k ) ) {
				continue;
			}

			if ( $stop && 'any' == $notification[ 'conditions' ][ 'any_all' ] && 'stop' == $notification[ 'conditions' ][ 'send_stop' ] ) {
				continue;
			}

			if ( is_array( $condition[ 'hide_opt' ] ) ) {
				$condition[ 'hide_opt' ] = reset( $condition[ 'hide_opt' ] );
			}

			$observed_value = isset( $submitted_values[ $condition[ 'hide_field' ] ] ) ? $submitted_values[ $condition[ 'hide_field' ] ] : '';

			if ( $condition[ 'hide_opt' ] == 'current_user' ) {
				$condition[ 'hide_opt' ] = get_current_user_id();
			}

			$stop = FrmFieldsHelper::value_meets_condition( $observed_value, $condition[ 'hide_field_cond' ], $condition[ 'hide_opt' ] );

			if ( $notification[ 'conditions' ][ 'send_stop' ] == 'send' ) {
				$stop = $stop ? false : true;
			}

			$met[ $stop ] = $stop;
		}

		if ( $notification[ 'conditions' ][ 'any_all' ] == 'all' && ! empty( $met ) && isset( $met[ 0 ] ) && isset( $met[ 1 ] ) ) {
			$stop = ( $notification[ 'conditions' ][ 'send_stop' ] == 'send' ) ? true : false;
		} else if ( $notification[ 'conditions' ][ 'any_all' ] == 'any' && $notification[ 'conditions' ][ 'send_stop' ] == 'send' && isset( $met[ 0 ] ) ) {
			$stop = false;
		}

		return $stop;
	}

	/**
	 * Parse field value to obtain subscription variables
	 *
	 * @since 1.0.0
	 *
	 * @param $field_value
	 *
	 * @return array
	 */
	private function get_subscription_vars( $field_value ) {

		//replace all commas
		$plan_field_value = trim( $field_value );
		$plan_field_value = str_replace( ',', '', $plan_field_value );

		//get separator
		$separator_location = strrpos( $plan_field_value, '-' );

		if ( false === $separator_location ) {

			//This is either a plan ID *or* a plan amount

			//Remove all characters before number
			$plan_amount = preg_match( "/-?[0-9.]+$/", $plan_field_value, $matched_plan_amount );

			//Return plan ID
			if ( ! is_numeric( $plan_field_value ) ) {
				$plan_id = $plan_field_value;
			}

		} else {

			//Remove all characters before number
			$plan_amount = preg_match( "/-?[0-9.]+$/", $plan_field_value, $matched_plan_amount );

			//get value before separator
			$plan_id = trim( substr( $plan_field_value, 0, $separator_location ) );

		}

		$subscription_vars = array(
			'plan_id'     => empty( $plan_id ) ? '' : $plan_id,
			'plan_amount' => $matched_plan_amount[ 0 ]
		);

		return $subscription_vars;

	}

	/**
	 * Set all of the variables we will need to process this transaction
	 *
	 * @since 1.0.0
	 *
	 * @param object $form
	 */
	private function set_processor_vars( $form ) {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Setting variables needed to process this transaction' );

		$action = $this->processor_vars[ 'action' ]->post_content;

		$form_mode = PPFRM_Stripe_App_Helper::get_form_stripe_mode( $this->processor_vars[ 'action' ] );


		$this->processor_vars = array(
			'form_id'          => $form->id,
			'form_name'        => $form->name,
			'mode'             => $form_mode,
			'customer_name'    => trim( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['customer_name_field']}" ) ),
			'customer_email'   => trim( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['customer_email_field']}" ) ),
			'transaction_type' => $action[ 'transaction_type' ],
			'currency'         => $action[ 'currency' ],
			'api_key'          => PPFRM_Stripe_App_Helper::get_api_key( 'secret', $form_mode ),
			'token'            => trim( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "ppfrm_stripe/token" ) ),
			'action'           => $this->processor_vars[ 'action' ]
		);

		$currency_info                                   = PPP_Currency::get_currency( $this->processor_vars[ 'currency' ] );
		$this->processor_vars[ 'zero_decimal_currency' ] = ( 0 == $currency_info[ 'decimals' ] ) ? true : false;

		if ( 'one-time' == $this->processor_vars[ 'transaction_type' ] ) {

			$this->processor_vars[ 'charge' ] = array(
				'description' => trim( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['charge_description_field']}" ) ),
				'amount'      => trim( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['charge_amount_field']}" ) ),
			);

			if ( $this->processor_vars[ 'zero_decimal_currency' ] ) {
				$this->processor_vars[ 'charge' ][ 'stripe_amount' ] = round( floatval( $this->processor_vars[ 'charge' ][ 'amount' ] ), 0 );
			} else {
				$this->processor_vars[ 'charge' ][ 'stripe_amount' ] = round( $this->processor_vars[ 'charge' ][ 'amount' ] * 100, 0 );
			}

		} else if ( 'subscription' == $this->processor_vars[ 'transaction_type' ] ) {

			if ( 'predefined' == $action[ 'subscription_type' ] ) {

				$subscription_vars = $this->get_subscription_vars( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['predefined_plan_id_field']}" ) );

				$this->processor_vars[ 'subscriptions' ][ ] = array(
					'type'    => 'predefined',
					'plan_id' => $subscription_vars[ 'plan_id' ]
				);

			} else if ( 'custom' == $action[ 'subscription_type' ] ) {

				$plan_id = $this->get_subscription_vars( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['custom_plan_id_field']}" ) );

				$plan_amount = $this->get_subscription_vars( PPFRM_Stripe_App_Helper::get_array_values( $_POST, "item_meta/{$action['custom_plan_amount_field']}" ) );

				$this->processor_vars[ 'subscriptions' ][ ] = array(
					'type'           => 'custom',
					'plan_id'        => "{$plan_id['plan_id']}-{$this->processor_vars['customer_name']}-{$this->processor_vars['customer_email']}",
					'plan_amount'    => ( $this->processor_vars[ 'zero_decimal_currency' ] ) ? round( floatval( $plan_amount[ 'plan_amount' ] ), 0 ) : round( $plan_amount[ 'plan_amount' ] * 100, 0 ),
					'interval_count' => trim( $action[ 'custom_plan_interval_count' ] ),
					'interval'       => trim( $action[ 'custom_plan_interval' ] )
				);

			}

		}

		if ( is_user_logged_in() ) {
			$this->processor_vars[ 'user_id' ] = get_current_user_id();
		}

		PPP_Stripe_API::set_mode( $form_mode );

		PPFRM_Stripe_App_Controller::$logger->log->debug( print_r( $this->processor_vars, true ) );

	}

	/**
	 * Get customer object and card to use for transaction
	 *
	 * @since 1.0.0
	 *
	 * @return string|bool
	 */
	private function get_customer_and_card() {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Getting customer object and card to use for transaction' );

		$create_new_customer = true;
		$create_new_card     = false;

		$new_card   = $this->processor_vars[ 'token' ];
		$saved_card = '';

		if ( is_user_logged_in() ) {

			$this->processor_vars[ 'customer' ] = PPFRM_Stripe_App_Helper::get_user_customer_object( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'get_user_customer_object' ), $this->processor_vars[ 'user_id' ] );

			if ( is_object( $this->processor_vars[ 'customer' ] ) ) {

				$this->processor_vars[ 'new_stripe_customer' ] = $create_new_customer = apply_filters( 'ppfrm_stripe_create_new_customer', false, $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'customer' ] );

			}

			if ( ( ! $create_new_customer ) && ( ! empty( $new_card ) ) ) {

				$create_new_card = true;

				$this->processor_vars[ 'card' ] = $new_card;

			} else if ( $create_new_customer ) {

				$this->processor_vars[ 'card' ] = $new_card;

			} else {

				$this->processor_vars[ 'card' ] = $saved_card;

				if ( 'subscription' == $this->processor_vars[ 'transaction_type' ] ) {

					if ( ! PPFRM_Stripe_Customer_API::is_default_card( $this->processor_vars[ 'user_id' ], $saved_card ) ) {

						$customer = PPP_Stripe_API::update_customer( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'update_customer' ), $this->processor_vars[ 'customer' ], array( 'default_source' => $saved_card ) );

						if ( ! is_object( $customer ) ) {

							PPFRM_Stripe_App_Controller::$logger->log->error( 'Unable to update customer with new default card for subscription' );

							return $customer;

						} else {

							$this->processor_vars[ 'customer' ] = $customer;

						}
					}

				}
			}

		} else {

			$this->processor_vars[ 'card' ] = $new_card;

		}

		if ( $create_new_customer ) {

			$this->processor_vars[ 'new_stripe_customer' ] = true;

			$this->create_customer();

			if ( ! is_object( $this->processor_vars[ 'customer' ] ) ) {

				return $this->processor_vars[ 'customer' ];

			}

			$this->processor_vars[ 'card' ] = $this->processor_vars[ 'customer' ]->default_source;

			if ( is_user_logged_in() ) {

				$this->save_customer_id( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'customer' ][ 'id' ] );

				PPFRM_Stripe_Customer_API::save_new_card( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'card' ] );

			}
		}

		$this->processor_vars[ 'new_card' ] = $create_new_card;

		if ( $create_new_card ) {

			$this->processor_vars[ 'card' ] = PPP_Stripe_API::create_card(
				apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'create_new_card' ),
				$this->processor_vars[ 'customer' ],
				$this->processor_vars[ 'card' ]
			);

			if ( ! is_object( $this->processor_vars[ 'card' ] ) ) {

				return $this->processor_vars[ 'card' ];

			}

			if ( 'subscription' == $this->processor_vars[ 'transaction_type' ] ) {

				$customer = PPP_Stripe_API::update_customer( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'update_customer' ), $this->processor_vars[ 'customer' ], array( 'default_source' => $this->processor_vars[ 'card' ][ 'id' ] ) );

				if ( ! is_object( $customer ) ) {

					PPFRM_Stripe_App_Controller::$logger->log->error( 'Unable to update customer with new default card for subscription' );

					PPP_Stripe_API::delete_card( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'delete_card' ), $this->processor_vars[ 'card' ][ 'id' ], $this->processor_vars[ 'customer' ] );

					return $customer;

				} else {

					$this->processor_vars[ 'customer' ] = $customer;
					$this->processor_vars[ 'card' ]     = $this->processor_vars[ 'customer' ]->default_source;

				}

			}

			if ( is_user_logged_in() ) {
				PPFRM_Stripe_Customer_API::save_new_card( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'card' ] );
			}

		}

		return true;

	}

	/**
	 * Save Stripe information to new WP user created by Formidable Registration
	 *
	 * @since 1.0.0
	 */
	private function save_stripe_info_to_wp_user() {

		global $ppfrm_stripe_transaction;

		if ( '' !== PPFRM_Stripe_App_Helper::get_post_value( 'frm_user_id' ) ) {

			PPFRM_Stripe_App_Controller::$logger->log->debug( 'Saving Stripe info to WP user' );

			$this->processor_vars[ 'user_id' ]            = PPFRM_Stripe_App_Helper::get_post_value( 'frm_user_id' );
			$ppfrm_stripe_transaction[ 'processor_vars' ] = $this->processor_vars;

			if ( $this->processor_vars[ 'new_stripe_customer' ] ) {

				$this->save_customer_id( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'customer' ][ 'id' ] );

				PPFRM_Stripe_Customer_API::save_new_card( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'card' ] );

			} else if ( $this->processor_vars[ 'new_card' ] ) {

				PPFRM_Stripe_Customer_API::save_new_card( $this->processor_vars[ 'user_id' ], $this->processor_vars[ 'card' ] );

			}

			foreach ( $this->transactions as $transaction ) {

				if ( 'subscription' == $transaction[ 'type' ] ) {

					PPFRM_Stripe_Customer_API::add_active_subscription( $this->processor_vars[ 'user_id' ], $transaction[ 'object' ][ 'id' ] );
					PPFRM_Stripe_Customer_API::save_subscription( $this->processor_vars[ 'user_id' ], $transaction[ 'object' ] );

				}

			}

		}

	}

	/**
	 * Add entry ID and WP user ID (if applicable) to Stripe customer metadata
	 *
	 * @since 1.0.0
	 *
	 * @param $entry_id
	 */
	private function add_customer_metadata( $entry_id ) {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Adding metadata to Stripe customer' );

		if ( empty( $this->processor_vars[ 'customer' ]->metadata[ 'wp_user_id' ] ) && ! empty( $this->processor_vars[ 'user_id' ] ) ) {

			$customer_metadata[ 'wp_user_id' ] = $this->processor_vars[ 'user_id' ];
		}

		if ( empty( $this->processor_vars[ 'customer' ]->metadata[ 'pressplus_frm_entry' ] ) ) {

			$customer_metadata[ 'pressplus_frm_entry' ] = $entry_id;

		}

		if ( ! empty( $customer_metadata ) ) {

			PPFRM_Stripe_Customer_API::add_metadata_to_customer( '', $this->processor_vars[ 'customer' ], apply_filters( 'ppfrm_stripe_customer_metadata', $customer_metadata ) );

		}

	}

	/**
	 * Insert successful transactions to DB
	 *
	 * Transaction types:
	 * - customer
	 * - charge
	 * - subscription
	 * - card
	 * - customer_update
	 * - subscription_update
	 * - subscription_payment
	 * - invoice_item
	 * - transfer
	 * - card_update
	 *
	 * Methods:
	 * - entry
	 * - event
	 * - admin
	 * - user_admin
	 *
	 * Statuses
	 * - paid
	 * - refunded
	 * - captured
	 * - authorized
	 * - see also Stripe object statuses
	 *
	 * @since 1.0.0
	 *
	 * @param $entry_id
	 */
	private function insert_transactions( $entry_id ) {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Inserting transactions' );

		foreach ( $this->transactions as $transaction ) {

			PPFRM_Stripe_App_Controller::$logger->log->debug( "Transaction {$transaction['transaction_id']}" );

			$transaction = array(
				'method'           => $transaction[ 'method' ],
				'entry_id'         => $entry_id,
				'transaction_type' => $transaction[ 'transaction_type' ],
				'transaction_id'   => $transaction[ 'transaction_id' ],
				'status'           => $transaction[ 'status' ],
				'amount'           => $transaction[ 'amount' ],
				'currency'         => $this->processor_vars[ 'currency' ],
				'mode'             => $this->processor_vars[ 'mode' ],
				'meta'             => $transaction[ 'meta' ]
			);

			if ( ! empty( $this->processor_vars[ 'user_id' ] ) ) {
				$transaction[ 'user_id' ] = $this->processor_vars[ 'user_id' ];
			}

			PPFRM_Stripe_App_Controller::$transactions_db->add( $transaction );

		}

	}

	/**
	 * Add metadata to Stripe object
	 *
	 * @since 1.0.0
	 *
	 * @param $entry_id
	 */
	private function add_object_metadata( $entry_id ) {

		foreach ( $this->transactions as $transaction ) {

			$metadata_objects = array(
				'charge'       => 'PPP\Stripe_Charge',
				'subscription' => 'PPP\Stripe_Subscription'
			);

			if ( in_array( $transaction[ 'transaction_type' ], array_keys( $metadata_objects ) ) ) {

				PPFRM_Stripe_App_Controller::$logger->log->debug( "Adding metadata to {$transaction['transaction_type']} {$transaction['transaction_id']}" );

				$metadata[ 'pressplus_frm_entry' ] = (string) $entry_id;

				$additional_args = array();

				if ( 'subscription' == $transaction[ 'transaction_type' ] ) {

					$additional_args[ 'customer' ] = $this->processor_vars[ 'customer' ];

				}

				PPFRM_Stripe_App_Helper::add_metadata_to_object( $metadata_objects[ $transaction[ 'transaction_type' ] ], $transaction[ 'object' ], $metadata, $additional_args );

			}

		}

	}

	/**
	 * Create a new customer
	 *
	 * @since 1.0.0
	 */
	private function create_customer() {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Creating a new customer' );

		$customer_args = apply_filters( 'ppfrm_stripe_create_new_customer_args',
		                                array(
			                                'source'      => $this->processor_vars[ 'card' ],
			                                'description' => apply_filters( 'ppfrm_stripe_customer_description', $this->processor_vars[ 'customer_name' ] ),
			                                'email'       => $this->processor_vars[ 'customer_email' ],
			                                'metadata'    => array( 'created_by' => 'pressplus_frm_stripe' )
		                                ) );

		if ( ! empty( $this->processor_vars[ 'user_id' ] ) ) {
			$customer_args[ 'metadata' ][ 'wp_user_id' ] = $this->processor_vars[ 'user_id' ];
		}

		$customer = PPP_Stripe_API::create_customer(
			apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'create_new_customer' ),
			$customer_args
		);

		$this->processor_vars[ 'customer' ] = apply_filters( 'ppfrm_stripe_customer_object', $customer, true );

	}

	private function authorize() {
	}

	/**
	 * Capture charge
	 *
	 * @since 1.0.0
	 *
	 * @return array|bool|mixed|Stripe_Charge|void
	 */
	private function capture() {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Capturing charge' );

		$charge_args = array(
			'amount'      => $this->processor_vars[ 'charge' ][ 'stripe_amount' ],
			'currency'    => $this->processor_vars[ 'currency' ],
			'customer'    => $this->processor_vars[ 'customer' ][ 'id' ],
			'description' => apply_filters( 'ppfrm_stripe_customer_charge_description', $this->processor_vars[ 'charge' ][ 'description' ] ),
			'metadata'    => array(
				'created_by'         => 'pressplus_frm_stripe',
				'pressplus_frm_form' => $this->processor_vars[ 'form_id' ]
			)
		);

		$charge_args[ 'source' ]  = ( is_object( $this->processor_vars[ 'card' ] ) ) ? $this->processor_vars[ 'card' ][ 'id' ] : $this->processor_vars[ 'card' ];
		$charge_args[ 'capture' ] = true;
		$charge_args              = apply_filters( 'ppfrm_stripe_create_charge_args', $charge_args );

		if ( ! is_array( $charge_args ) ) {

			if ( $this->processor_vars[ 'new_stripe_customer' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new customer' );

				$this->processor_vars[ 'customer' ]->delete();

			} else if ( $this->processor_vars[ 'new_card' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new card' );

				PPP_Stripe_API::delete_card( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'delete_card' ), $this->processor_vars[ 'card' ][ 'id' ], $this->processor_vars[ 'customer' ] );
			}

			return $charge_args;
		}

		$charge = PPP_Stripe_API::create_charge( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'create_charge' ), $charge_args );

		if ( ! is_object( $charge ) ) {

			if ( $this->processor_vars[ 'new_stripe_customer' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new customer' );
				$this->processor_vars[ 'customer' ]->delete();

			} else if ( $this->processor_vars[ 'new_card' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new card' );

				PPP_Stripe_API::delete_card( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'delete_card' ), $this->processor_vars[ 'card' ][ 'id' ], $this->processor_vars[ 'customer' ] );
			}

			return $charge;
		}

		$this->processor_vars[ 'customer' ] = $charge->customer;

		$this->transactions[ ] = array(
			'status'           => 'Paid',
			'method'           => 'entry',
			'transaction_type' => 'charge',
			'transaction_id'   => $charge[ 'id' ],
			'amount'           => $this->processor_vars[ 'charge' ][ 'amount' ],
			'meta'             => json_encode( array( 'object' => $charge->__toArray( true ) ) ),
			'object'           => $charge
		);

		global $ppfrm_stripe_transaction;

		$ppfrm_stripe_transaction = array(
			'processor_vars' => $this->processor_vars,
			'transactions'   => $this->transactions
		);

		return true;

	}

	/**
	 * Create subscription
	 *
	 * @since 1.0.0
	 *
	 * @return array|bool|mixed|\PPP\Stripe_Charge|void
	 */
	private function subscribe() {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Subscribing customer' );

		if ( empty( $this->processor_vars[ 'subscriptions' ] ) || empty( $this->processor_vars[ 'subscriptions' ][ 0 ][ 'type' ] ) || empty( $this->processor_vars[ 'subscriptions' ][ 0 ][ 'plan_id' ] ) ) {

			PPFRM_Stripe_App_Controller::$logger->log->debug( 'Unable to subscribe customer — no subscription information found' );

			return __( 'Unable to create subscription', 'formidable-stripe' );

		}

		$subscription = $this->processor_vars[ 'subscriptions' ][ 0 ];

		if ( ( 'custom' == $subscription[ 'type' ] ) && ( empty( $subscription[ 'plan_amount' ] ) || empty( $subscription[ 'interval_count' ] ) || empty( $subscription[ 'interval' ] ) ) ) {

			PPFRM_Stripe_App_Controller::$logger->log->debug( 'Unable to create custom subscription for customer — custom subscription information not found' );

			return __( 'Unable to create subscription', 'formidable-stripe' );

		}

		if ( 'predefined' == $subscription[ 'type' ] ) {

			$plan = PPP_Stripe_API::retrieve_plan( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'retrieve_plan' ), $subscription[ 'plan_id' ] );

		} else if ( 'custom' == $subscription[ 'type' ] ) {

			$plan = $this->get_plan_for_subscription( $subscription );

		}

		if ( ! is_object( $plan ) ) {

			PPFRM_Stripe_App_Controller::$logger->log->error( 'Unable to create or retrieve plan' );

			if ( $this->processor_vars[ 'new_stripe_customer' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new customer' );
				$this->processor_vars[ 'customer' ]->delete();

			}

			return $plan;

		}

		$subscription_args = array(
			'plan'     => $plan[ 'id' ],
			'metadata' => array(
				'created_by'         => 'pressplus_frm_stripe',
				'pressplus_frm_form' => $this->processor_vars[ 'form_id' ]
			)
		);

		$subscription_args = apply_filters( 'ppfrm_stripe_create_subscription_args', $subscription_args );

		$subscription = PPP_Stripe_API::create_subscription( $this->processor_vars[ 'api_key' ], $this->processor_vars[ 'customer' ], $subscription_args );

		if ( ! is_object( $subscription ) ) {

			PPFRM_Stripe_App_Controller::$logger->log->error( 'Subscription failed' );

			if ( $this->processor_vars[ 'new_stripe_customer' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new customer' );

				$this->processor_vars[ 'customer' ]->delete();

			} else if ( $this->processor_vars[ 'new_card' ] ) {

				PPFRM_Stripe_App_Controller::$logger->log->error( 'Deleting new card' );

				PPP_Stripe_API::delete_card( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'delete_card' ), $this->processor_vars[ 'card' ][ 'id' ], $this->processor_vars[ 'customer' ] );
			}

			return $subscription;

		}

		$this->processor_vars[ 'customer' ] = $subscription->customer;

		PPFRM_Stripe_App_Controller::$logger->log->debug( "Subscription {$subscription['id']} for {$subscription['plan']['amount']} successfully created for {$this->processor_vars['customer']['id']}" );

		$last_invoice = PPP_Stripe_API::list_invoices( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'list_invoices' ), array(
			'customer' => $subscription[ 'customer' ][ 'id' ],
			'limit'    => 1
		) );

		if ( ! is_object( $last_invoice ) ) {
			$last_invoice = '';
		}

		$this->transactions[ ] = array(
			'status'           => 'Subscribed',
			'method'           => 'entry',
			'transaction_type' => 'subscription',
			'transaction_id'   => $subscription[ 'id' ],
			'amount'           => ( $this->processor_vars[ 'zero_decimal_currency' ] ) ? $subscription[ 'plan' ][ 'amount' ] : round( $subscription[ 'plan' ][ 'amount' ] / 100, 2 ),
			'meta'             => json_encode( array(
				                                   'object'  => $subscription->__toArray( true ),
				                                   'invoice' => $last_invoice[ 'data' ][ 0 ]->__toArray( true )
			                                   ) ),
			'object'           => $subscription
		);

		global $ppfrm_stripe_transaction;

		$ppfrm_stripe_transaction = array(
			'processor_vars' => $this->processor_vars,
			'transactions'   => $this->transactions
		);

		return true;

	}

	private function refund() {
	}

	/**
	 * Get plan to subscribe customer to
	 *
	 * @since 1.0.0
	 *
	 * @param $args
	 *
	 * @return mixed|\PPP\Stripe_Plan|string|void
	 */
	private function get_plan_for_subscription( $args ) {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Getting plan to subscribe customer to' );

		$plan = PPP_Stripe_API::retrieve_plan( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'retrieve_plan' ), $args[ 'plan_id' ] );

		/* Plan does not exist */
		if ( ! is_object( $plan ) ) {

			$plan_args = array(
				'id'                => $args[ 'plan_id' ],
				'amount'            => $args[ 'plan_amount' ],
				'currency'          => $this->processor_vars[ 'currency' ],
				'interval'          => $args[ 'interval' ],
				'interval_count'    => $args[ 'interval_count' ],
				'name'              => "{$this->processor_vars['form_name']}-{$this->processor_vars['customer_name']}",
				'trial_period_days' => 0
			);

			$plan_args = apply_filters( 'ppfrm_stripe_create_plan_args', $plan_args );

			$plan = PPP_Stripe_API::create_plan( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'create_plan' ),
			                                     $plan_args );
		} /* Plan exists */
		else {

			$plan = PPP_Stripe_API::delete_plan( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'delete_plan' ), $plan );

			if ( is_object( $plan ) ) {

				$plan_args = array(
					'id'                => $plan[ 'id' ],
					'amount'            => $args[ 'plan_amount' ],
					'currency'          => $this->processor_vars[ 'currency' ],
					'interval'          => $args[ 'interval' ],
					'interval_count'    => $args[ 'interval_count' ],
					'name'              => "{$this->processor_vars['form_name']}-{$this->processor_vars['customer_name']}",
					'trial_period_days' => 0
				);

				$plan_args = apply_filters( 'gfp_more_stripe_create_plan_args', $plan_args );

				$plan = PPP_Stripe_API::create_plan( apply_filters( 'ppfrm_stripe_api_key', $this->processor_vars[ 'api_key' ], 'create_plan' ), $plan_args );
			}
		}

		return $plan;

	}

	/**
	 * Save Stripe customer ID to user meta
	 *
	 * @since 1.0.0
	 *
	 * @param      $user_id
	 * @param      $card
	 * @param bool $make_default
	 */
	private static function save_customer_id( $user_id, $customer_id ) {

		PPFRM_Stripe_App_Controller::$logger->log->debug( 'Saving customer ID to WP user' );

		update_user_meta( $user_id, '_ppfrm_stripe_customer_id', apply_filters( 'ppfrm_stripe_user_meta_customer_id_value', $customer_id ) );

	}

}