<div id="submitdiv" class="stuffbox">
	<h3 class="hndle">
		<span><?php _e( 'Stripe', 'formidable-stripe' ); ?></span>
	</h3>

	<div class="inside">
		<div id="submitcomment" class="submitbox">
			<div id="minor-publishing" style="padding:10px;">
				<br/>
				<?php
					_e( 'Status', 'formidable-stripe' ); ?>: <span
						id="ppfrm_stripe_payment_status" style="font-weight:bold;color:#008000;"><?php echo apply_filters( 'ppfrm_stripe_entry_sidebar_payment_status', $transaction->status, $entry ) ?></span>
					<br/><br/>
					<?php
					_e( 'Transaction ID', 'formidable-stripe' ); ?>: <span style="font-weight:bold;"><?php echo apply_filters( 'ppfrm_stripe_entry_sidebar_transaction_id', $transaction->transaction_id, $entry ) ?></span>
					<br/><br/>
					<?php

					if ( ! empty($transaction->amount ) ) {
						_e( 'Amount', 'formidable-stripe' ); ?>: <span style="font-weight:bold;"><?php echo PPP_Currency::to_money( $transaction->amount, $transaction->currency ) ?></span>
						<br/><br/>
					<?php
					}
				do_action( 'ppfrm_stripe_payment_details', $entry );

				?>
			</div>
		</div>
	</div>
</div>