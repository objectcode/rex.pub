<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Entry_Display Class
 *
 * Displays payment information on the entry detail screen
 *
 * @since 1.0.0
 *
 */
class PPFRM_Stripe_Entry_Display {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Init actions
	 *
	 * @since 1.0.0
	 */
	public function init() {

		add_action( 'frm_show_entry_sidebar', array( $this, 'frm_show_entry_sidebar' ) );

	}

	public function frm_show_entry_sidebar( $entry ) {

		$transaction = PPFRM_Stripe_App_Controller::$transactions_db->get_by( 'entry_id', $entry->id );

		if ( ! empty( $transaction ) ) {

			$transaction->meta = json_decode( $transaction->meta, true );

			require_once( trailingslashit( PPFRM_STRIPE_PATH ) . 'includes/entries/views/entry-payment-info.php' );

		}

	}

}