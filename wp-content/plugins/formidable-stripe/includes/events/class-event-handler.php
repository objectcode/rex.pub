<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Event_Handler Class
 *
 * Receives & processes Stripe events
 *
 * @since 1.0.0
 * */
class PPFRM_Stripe_Event_Handler extends PPP_Stripe_Event {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array $args
	 */
	public function __construct( $args ) {
		add_action( 'parse_request', array( $this, 'parse_request' ) );
		add_filter( 'ppp_stripe_event_api_key', array( $this, 'ppp_stripe_event_api_key' ), 10, 4 );
		add_filter( 'ppp_stripe_pre_process_event', array( $this, 'ppp_stripe_pre_process_event' ), 10, 3 );
		add_action( 'ppp_stripe_post_process_event', array( $this, 'ppp_stripe_post_process_event' ), 10, 2 );

		parent::__construct( $args );
	}

	/**
	 * Receive Stripe event notification
	 *
	 * @since 1.0.0
	 */
	public function parse_request() {

		$endpoint = PPFRM_Stripe_App_Helper::get_array_value( $_GET, 'page' );
		if ( 'ppfrm_stripe_event' == $endpoint || 'ppfrm_stripe_user_event' == $endpoint ) {
			$this->process_event( $endpoint );
		}

		return;
	}

	/**
	 * Cancel processing this event if it's already been processed
	 *
	 * @since 1.0.0
	 *
	 * @param bool             $cancel_processing
	 * @param string           $plugin
	 * @param PPP\Stripe_Event $event
	 *
	 * @return bool
	 */
	public function ppp_stripe_pre_process_event( $cancel_processing, $plugin, $event ) {

		if ( PPFRM_STRIPE_SLUG == $plugin ) {
			$already_processed = PPFRM_Stripe_App_Controller::$events_db->exists( $event[ 'id' ] );
			if ( $already_processed ) {
				$cancel_processing = true;
			}
		}

		return $cancel_processing;
	}

	/**
	 * Retrieve correct API key
	 *
	 * @since 1.0.0
	 *
	 * @param string           $api_key
	 * @param string           $plugin
	 * @param PPP\Stripe_Event $event
	 * @param array|null       $args
	 *
	 * @return string
	 */
	public function ppp_stripe_event_api_key( $api_key, $plugin, $event, $args = null ) {

		if ( PPFRM_STRIPE_SLUG == $plugin && 'process_event' == $event && ! empty( $args[ 'event' ] ) ) {
			$mode    = $this->get_mode( $args[ 'event' ] );
			$api_key = PPFRM_Stripe_App_Helper::get_api_key( 'secret', $mode );
		}

		return $api_key;
	}

	/**
	 * Add event to events DB
	 *
	 * @since 1.0.0
	 *
	 * @param string           $plugin
	 * @param PPP\Stripe_Event $event
	 */
	public function ppp_stripe_post_process_event( $plugin, $event ) {

		if ( PPFRM_STRIPE_SLUG == $plugin ) {
			PPFRM_Stripe_App_Controller::$events_db->add( array(
				                                              'event_id'     => $event[ 'id' ],
				                                              'date_created' => date( 'Y-m-d H:i:s', $event[ 'created' ] )
			                                              ) );
		}
	}
}