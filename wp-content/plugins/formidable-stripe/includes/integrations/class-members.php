<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PPFRM_Stripe_Integration_Members {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'admin_init' ) );
	}

	/**
	 * Filter capabilities
	 *
	 * @since 1.0.0
	 */
	public function admin_init() {

		if ( function_exists( 'members_get_capabilities' ) ) {
			add_filter( 'members_get_capabilities', array( $this, 'members_get_capabilities' ) );
		}

	}

	/**
	 * Provide the Members plugin with this plugin's list of capabilities
	 *
	 * @since 1.0.0
	 *
	 * @param $caps
	 *
	 * @return array
	 */
	public function members_get_capabilities( $caps ) {
		return array_merge( $caps, array(
			'ppfrm_stripe',
			'ppfrm_stripe_global_settings',
			'ppfrm_stripe_form_settings',
			'ppfrm_stripe_uninstall'
		) );
	}

	/**
	 * Check if user has the required permission
	 *
	 * @since 1.0.0
	 *
	 * @uses  current_user_can()
	 *
	 * @param $required_permission
	 *
	 * @return bool|string
	 */
	public static function has_access( $required_permission ) {
		$has_members_plugin = function_exists( 'members_get_capabilities' );
		$has_access         = $has_members_plugin ? current_user_can( $required_permission ) : current_user_can( 'level_7' );
		if ( $has_access ) {
			return $has_members_plugin ? $required_permission : 'level_7';
		} else {
			return false;
		}
	}
}