<?php
/**
 * Form Settings
 */
?>

<table class="form-table">

	<tr>
		<th scope="row"><?php _e( 'Mode', 'formidable-stripe' ) ?></th>
		<td>
			<input type="radio" name="<?php echo $this->get_field_name('mode') ?>"
			       value="live" <?php checked( 'live', $form_mode, true ); ?>/> <label class="inline"
			                                                                           for="stripe_mode_live"><?php _e( 'Live', 'formidable-stripe' ); ?></label>
			&nbsp;&nbsp;&nbsp; <input type="radio" name="<?php echo $this->get_field_name('mode') ?>"
			                          value="test" <?php checked( 'test', $form_mode, true ); ?>/> <label class="inline"
			                                                                                              for="stripe_mode_test"><?php _e( 'Test', 'formidable-stripe' ); ?></label>
		</td>
	</tr>
	<tr>
		<th scope="row"><?php _e( 'Currency', 'formidable-stripe' ) ?></th>
		<td><?php PPP_Currency::dropdown_currencies( array(
			                                             'selected' => $form_currency,
			                                             'id'       => $this->get_field_id( 'stripe_currency' ),
			                                             'name'     => $this->get_field_name('currency')
		                                             ) ); ?></td>
	</tr>
	<tr>
		<th colspan="3"><h3
				class="stripe-form-settings-section"><?php _e( 'Customer Information', 'formidable-stripe' ) ?></h3>
			<hr/>
		</th>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap">
			<label for="stripe_customer_name_field"><?php _e( 'Name', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('customer_name_field') ?>">

				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>

				<?php

				if ( isset( $form_fields ) and is_array( $form_fields ) ) {

						foreach ( $form_fields as $field ) {

							if ( 'checkbox' == $field->type ) {
							continue;
						}

							?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['customer_name_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap">
			<label for="stripe_customer_email_field"><?php _e( 'Email', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('customer_email_field') ?>">
				
				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>
				
				<?php foreach ( $email_fields as $field ) { ?>
				
						<option
						value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['customer_email_field' ] ) ?>>
						<?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 ); ?>
					</option>
				
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="3"><h3
				class="stripe-form-settings-section"><?php _e( 'Transaction Information', 'formidable-stripe' ) ?></h3>
			<hr/>
		</th>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap">
			<label for="stripe_transaction_type"><?php _e( 'Transaction Type', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('transaction_type') ?>" onChange="ppfrm_stripe_toggle_transaction_options(this, <?php echo $args['action_key'] ?>)">
				<option value="">&mdash; <?php _e( 'Select transaction type', 'formidable-stripe' ) ?> &mdash;</option>
				<option
					value="one-time" <?php selected( 'one-time', $form_action->post_content['transaction_type' ] ) ?>><?php _e( 'One-Time', 'formidable-stripe' ) ?></option>
				<option
					value="subscription" <?php selected( 'subscription', $form_action->post_content['transaction_type' ] ) ?>><?php _e( 'Subscription', 'formidable-stripe' ) ?></option>
			</select>
		</td>
	</tr>
	<tr class="stripe_one-time" <?php echo $hide_one_time ?>>
		<th scope="row" nowrap="nowrap">
			<label
				for="stripe_charge_description_field"><?php _e( 'Charge description', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('charge_description_field')?>">
				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>
				<?php
				if ( isset( $form_fields ) and is_array( $form_fields ) ) {
					foreach ( $form_fields as $field ) {
						if ( 'checkbox' == $field->type ) {
							continue;
						}
						?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['charge_description_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr class="stripe_one-time" <?php echo $hide_one_time ?>>
		<th scope="row" nowrap="nowrap">
			<label for="stripe_charge_amount_field"><?php _e( 'Charge Amount', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('charge_amount_field')?>">
				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>

				<?php

				if ( isset( $form_fields ) and is_array( $form_fields ) ) {

						foreach ( $form_fields as $field ) {

							if ( $field->type == 'checkbox' ) {
							continue;
						}

						?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['charge_amount_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr class="stripe_subscription stripe_subscription_type" <?php echo $hide_subscription ?>>

		<td colspan="2">
			<input type="radio" name="<?php echo $this->get_field_name('subscription_type')?>"
			       value="predefined" onchange="ppfrm_stripe_toggle_subscription_options( this, <?php echo $args['action_key'] ?>)" <?php checked( 'predefined', $form_action->post_content['subscription_type' ], true ); ?>/>
			<label class="inline"
			       for="stripe_subscription_predefined"><?php _e( 'Use Pre-defined Plan', 'formidable-stripe' ); ?></label>
			&nbsp;&nbsp;&nbsp;
			<input type="radio" name="<?php echo $this->get_field_name('subscription_type')?>"
			                          value="custom" onchange="ppfrm_stripe_toggle_subscription_options( this, <?php echo $args['action_key'] ?>)" <?php checked( 'custom', $form_action->post_content['subscription_type' ], true ); ?>/>
			<label class="inline"
			       for="stripe_subscription_custom"><?php _e( 'Create Custom Plan', 'formidable-stripe' ); ?></label>
		</td>
	</tr>
	<tr class="stripe_subscription stripe_subscription_predefined" <?php echo $hide_subscription_predefined ?>>

		<th scope="row" nowrap="nowrap">
			<label
				for="stripe_subscription_predefined_plan_id_field"><?php _e( 'Plan ID', 'formidable-stripe' ); ?></label>
		</th>

		<td>

			<select name="<?php echo $this->get_field_name('predefined_plan_id_field')?>">

				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>
				<?php

				if ( isset( $form_fields ) and is_array( $form_fields ) ) {

					foreach ( $form_fields as $field ) {

						if ( 'checkbox' == $field->type ) {
							continue;
						}


						?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['predefined_plan_id_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr class="stripe_subscription stripe_subscription_custom" <?php echo $hide_subscription_custom ?>>
		<th scope="row" nowrap="nowrap">
			<label for="stripe_subscription_custom_plan_id_field"><?php _e( 'Plan ID', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('custom_plan_id_field')?>'">

				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>

				<?php
				if ( isset( $form_fields ) and is_array( $form_fields ) ) {

					foreach ( $form_fields as $field ) {

						if ( $field->type == 'checkbox' ) {
							continue;
						}

						?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['custom_plan_id_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr class="stripe_subscription stripe_subscription_custom" <?php echo $hide_subscription_custom ?>>
		<th scope="row" nowrap="nowrap">
			<label
				for="stripe_subscription_custom_plan_amount_field"><?php _e( 'Plan Amount', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<select name="<?php echo $this->get_field_name('custom_plan_amount_field')?>'">
				<option value="">&mdash; <?php _e( 'Select Field', 'formidable-stripe' ) ?> &mdash;</option>
				<?php
				if ( isset( $form_fields ) and is_array( $form_fields ) ) {

					foreach ( $form_fields as $field ) {

						if ( $field->type == 'checkbox' ) {
							continue;
						}

						?>
						<option
							value="<?php echo $field->id ?>" <?php selected( $field->id, $form_action->post_content['custom_plan_amount_field' ] ) ?>><?php echo substr( esc_attr( stripslashes( $field->name ) ), 0, 50 );
							unset( $field );
							?></option>
					<?php
					}
				}
				?>
			</select>
		</td>
	</tr>
	<tr class="stripe_subscription stripe_subscription_custom" <?php echo $hide_subscription_custom ?>>
		<th scope="row" nowrap="nowrap">
			<label
				for="stripe_subscription_custom_plan_interval_field"><?php _e( 'Plan Frequency', 'formidable-stripe' ); ?></label>
		</th>
		<td>
			<span style="vertical-align:middle;"><?php _e( 'every', 'formidable-stripe' ); ?></span>

			<input
				class="inline" step="1" min="1" max="365" type="number"
				name="<?php echo $this->get_field_name('custom_plan_interval_count')?>"
				value="<?php echo $form_action->post_content['custom_plan_interval_count' ] ?>"/>

			<select class="inline" name="<?php echo $this->get_field_name('custom_plan_interval')?>'">

				<option value="">&mdash; <?php _e( 'Select Interval', 'formidable-stripe' ) ?> &mdash;</option>
				<option
					value="day" <?php selected( 'day', $form_action->post_content['custom_plan_interval' ] ) ?>><?php _e( 'day(s)', 'formidable-stripe' ) ?></option>
				<option
					value="week" <?php selected( 'week', $form_action->post_content['custom_plan_interval' ] ) ?>><?php _e( 'week(s)', 'formidable-stripe' ) ?></option>
				<option
					value="month" <?php selected( 'month', $form_action->post_content['custom_plan_interval' ] ) ?>><?php _e( 'month(s)', 'formidable-stripe' ) ?></option>
				<option
					value="year" <?php selected( 'year', $form_action->post_content['custom_plan_interval' ] ) ?>><?php _e( 'year', 'formidable-stripe' ) ?></option>
			</select>
		</td>
	</tr>

</table>