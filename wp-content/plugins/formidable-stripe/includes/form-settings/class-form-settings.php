<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Form_Settings Class
 *
 * Controls form settings
 *
 * @since 1.0.0
 *
 */
class PPFRM_Stripe_Form_Settings {

	private static $_this = null;

	public function __construct() {

		add_action( 'admin_init', array( $this, 'admin_init' ) );

		add_action( 'frm_before_list_actions', array( $this, 'frm_before_list_actions' ) );

		add_filter( 'frm_registered_form_actions', array( $this, 'frm_registered_form_actions' ) );

		//add_action( 'frm_after_duplicate_form', array( $this, 'frm_after_duplicate_form'), 10, 3);

		self::$_this = $this;

	}

	public function admin_init() {

		if ( 'formidable' == PPFRM_Stripe_App_Helper::get_array_value( $_GET, 'page' ) && 'settings' == PPFRM_Stripe_App_Helper::get_array_value( $_GET, 'frm_action' ) ) {

			wp_enqueue_style( 'ppfrm_stripe_admin', trailingslashit( PPFRM_STRIPE_URL ) . 'css/admin.css', array(), PPFRM_STRIPE_CURRENT_VERSION );

		}

	}

	/**
	 * Register Stripe form action
	 *
	 * @since 2.0.0
	 *
	 * @param array $action_classes
	 *
	 * @return mixed
	 */
	public function frm_registered_form_actions( $action_classes ) {

		$action_classes[ 'stripe' ] = 'PPFRM_Stripe_Form_Action';

		return $action_classes;
	}

	/**
	 * Migrate old form settings into a new form action
	 *
	 * @since 2.0.0
	 *
	 * @param object $form
	 */
	public function frm_before_list_actions( $form ) {

		$this->migrate_form_settings_to_actions( $form );

		$this->enqueue_js( $form );

	}

	private function migrate_form_settings_to_actions( $form ) {

		if ( '1' == PPFRM_Stripe_App_Helper::get_array_values( $form->options, 'stripe/enable' ) ) {

					$form->options[ 'enable' ]                     = $form->options[ 'stripe' ][ 'enable' ];
					$form->options[ 'mode' ]                       = $form->options[ 'stripe' ][ 'mode' ];
					$form->options[ 'currency' ]                   = $form->options[ 'stripe' ][ 'currency' ];
					$form->options[ 'customer_name_field' ]        = $form->options[ 'stripe' ][ 'customer_name_field' ];
					$form->options[ 'customer_email_field' ]       = $form->options[ 'stripe' ][ 'customer_email_field' ];
					$form->options[ 'transaction_type' ]           = $form->options[ 'stripe' ][ 'transaction_type' ];
					$form->options[ 'charge_description_field' ]   = $form->options[ 'stripe' ][ 'charge_description_field' ];
					$form->options[ 'charge_amount_field' ]        = $form->options[ 'stripe' ][ 'charge_amount_field' ];
					$form->options[ 'subscription_type' ]          = $form->options[ 'stripe' ][ 'subscription_type' ];
					$form->options[ 'predefined_plan_id_field' ]   = $form->options[ 'stripe' ][ 'predefined_plan_id_field' ];
					$form->options[ 'custom_plan_id_field' ]       = $form->options[ 'stripe' ][ 'custom_plan_id_field' ];
					$form->options[ 'custom_plan_amount_field' ]   = $form->options[ 'stripe' ][ 'custom_plan_amount_field' ];
					$form->options[ 'custom_plan_interval_count' ] = $form->options[ 'stripe' ][ 'custom_plan_interval_count' ];
					$form->options[ 'custom_plan_interval' ]       = $form->options[ 'stripe' ][ 'custom_plan_interval' ];

					unset ( $form->options[ 'stripe' ] );

					$form->options[ 'conditions' ] = $form->options[ 'stripe_list' ];

					unset( $form->options[ 'stripe_list' ] );

					$action_control = FrmFormActionsController::get_form_actions( 'stripe' );
					$post_id = $action_control->migrate_to_2( $form );

				}

	}

	private function enqueue_js( $form ) {

			wp_enqueue_script( 'ppfrm_stripe_form_settings_js', trailingslashit( PPFRM_STRIPE_URL ) . 'includes/form-settings/js/form-settings.js', array( 'jquery' ), PPFRM_STRIPE_CURRENT_VERSION );

	}

	public function frm_after_duplicate_form( $form_id, $new_values, $addl_vars ) {
	}

}