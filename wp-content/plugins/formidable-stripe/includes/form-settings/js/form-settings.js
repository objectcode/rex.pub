/**
 *
 */

function ppfrm_stripe_toggle_transaction_options( payment_type_select, action_key ) {

	var transaction_type = jQuery( payment_type_select ).val();

	var form_action = jQuery( '#frm_form_action_' + action_key );


	if ( 'one-time' === transaction_type ) {

		form_action.find( '.stripe_subscription' ).fadeOut( 'slow' );

		ppfrm_stripe_clear_subscription_settings( action_key );

		form_action.find( '.stripe_one-time' ).fadeIn( 'slow' );
	}
	else if ( 'subscription' === transaction_type ) {

		form_action.find( '.stripe_one-time' ).fadeOut( 'slow' );

		ppfrm_stripe_clear_one_time_settings( action_key );

		form_action.find( '.stripe_subscription_type' ).fadeIn( 'slow' );
	}

}

function ppfrm_stripe_toggle_subscription_options( subscription_type_radio, action_key ) {

	var subscription_type = jQuery( subscription_type_radio ).val();

	var form_action = jQuery( '#frm_form_action_' + action_key );


	if ( 'predefined' === subscription_type ) {

		form_action.find( '.stripe_subscription_custom' ).fadeOut( 'slow' );

		ppfrm_stripe_clear_custom_subscription_settings( action_key );

		form_action.find( '.stripe_subscription_predefined' ).fadeIn( 'slow' );

	}
	else if ( 'custom' === subscription_type ) {

		form_action.find( '.stripe_subscription_predefined' ).fadeOut( 'slow' );

		ppfrm_stripe_clear_predefined_subscription_settings( action_key );

		form_action.find( '.stripe_subscription_custom' ).fadeIn( 'slow' );
	}

}

function ppfrm_stripe_clear_subscription_settings( action_key ){

	jQuery( 'input[name="frm_stripe_action[' + action_key + '][post_content][subscription_type]"]:checked' ).prop( 'checked', false );
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][predefined_plan_id_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_id_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_amount_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_interval]"]' ).val('');
	jQuery( 'input[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_interval_count]"]' ).val('');

}

function ppfrm_stripe_clear_predefined_subscription_settings( action_key ){

	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][predefined_plan_id_field]"]' ).val('');

}

function ppfrm_stripe_clear_custom_subscription_settings( action_key ){

	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_id_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_amount_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_interval]"]' ).val('');
	jQuery( 'input[name="frm_stripe_action[' + action_key + '][post_content][custom_plan_interval_count]"]' ).val('');

}

function ppfrm_stripe_clear_one_time_settings( action_key ){

	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][charge_amount_field]"]' ).val('');
	jQuery( 'select[name="frm_stripe_action[' + action_key + '][post_content][charge_description_field]"]' ).val('');

}