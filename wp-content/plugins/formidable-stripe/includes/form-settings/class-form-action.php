<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2015 press+
 * @license   GPL-2.0+
 * @since     2.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class PPFRM_Stripe_Form_Action
 *
 * Adds Stripe Form action
 *
 * @since 2.0.0
 */
class PPFRM_Stripe_Form_Action extends FrmFormAction {

	private $defaults = array();

	public function __construct() {

		$this->defaults = array(
			'enable'                     => 1,
			'mode'                       => 'test',
			'currency'                   => '',
			'customer_name_field'        => '',
			'customer_email_field'       => '',
			'transaction_type'           => 'one-time',
			'charge_description_field'   => '',
			'charge_amount_field'        => '',
			'subscription_type'          => '',
			'predefined_plan_id_field'   => '',
			'custom_plan_id_field'       => '',
			'custom_plan_amount_field'   => '',
			'custom_plan_interval_count' => '',
			'custom_plan_interval'       => ''
		);

		$action_ops = array(
			'classes'  => 'icon-stripe',
			'active'   => true,
			'event'    => array( 'create' ),
			'limit'    => 99,
			'priority' => 31
		);

		$this->FrmFormAction( 'stripe', __( 'Stripe', 'formidable-stripe' ), $action_ops );
	}

	public function form( $form_action, $args = array() ) {

		$form_fields = FrmField::getAll( "fi.form_id='" . $args[ 'form' ]->id . "' and fi.type not in ('divider', 'html', 'break', 'captcha', 'rte', 'form')", ' ORDER BY field_order' );

		$email_fields = array();

		if ( ! empty( $form_fields ) && is_array( $form_fields ) ) {

			foreach ( $form_fields as $field ) {

				if ( 'email' == $field->type ) {

					$email_fields[ ] = $field;

				}

			}

		}

		$show_amount = empty( $form_action->post_content[ 'amount' ] ) ? false : true;

		$form_mode = empty( $form_action->post_content[ 'mode' ] ) ? $this->defaults[ 'mode' ] : $form_action->post_content[ 'mode' ];

		$form_currency = empty( $form_action->post_content[ 'currency' ] ) ? PPFRM_Stripe_App_Helper::get_default_currency() : $form_action->post_content[ 'currency' ];

		$hide_transaction_type = empty( $form_action->post_content[ 'transaction_type' ] ) ? 'style="display:none;"' : '';

		$hide_one_time = ( empty( $form_action->post_content[ 'transaction_type' ] ) || 'subscription' == $form_action->post_content[ 'transaction_type' ] ) ? 'style="display:none;"' : '';

		$hide_subscription = ( empty( $form_action->post_content[ 'transaction_type' ] ) || 'one-time' == $form_action->post_content[ 'transaction_type' ] ) ? 'style="display:none;"' : '';

		$hide_subscription_predefined = ( empty( $form_action->post_content[ 'transaction_type' ] ) || 'one-time' == $form_action->post_content[ 'transaction_type' ] || 'custom' == $form_action->post_content[ 'subscription_type' ] ) ? 'style="display:none;"' : '';

		$hide_subscription_custom = ( empty( $form_action->post_content[ 'transaction_type' ] ) || 'one-time' == $form_action->post_content[ 'transaction_type' ] || 'predefined' == $form_action->post_content[ 'subscription_type' ] ) ? 'style="display:none;"' : '';


		include( trailingslashit( PPFRM_STRIPE_PATH ) . 'includes/form-settings/views/form-settings.php' );

	}

	public function get_defaults() {

		return $this->defaults;

	}

	public function migrate_values( $action, $form ) {

		if ( isset( $action->post_content[ 'conditions' ][ 'hide_field' ] ) && ! empty( $action->post_content[ 'conditions' ][ 'hide_field' ] ) ) {

			$new_conditions = array();

			$action->post_content[ 'conditions' ][ 'send_stop' ] = 'send';

			foreach ( $action->post_content[ 'conditions' ][ 'hide_field' ] as $key => $condition_field_id ) {

				$new_conditions[ ] = array(
					'hide_field'      => $condition_field_id,
					'hide_field_cond' => isset( $action->post_content[ 'conditions' ][ 'hide_field_cond' ][ $key ] ) ? $action->post_content[ 'conditions' ][ 'hide_field_cond' ][ $key ] : '==',
					'hide_opt'        => isset( $action->post_content[ 'conditions' ][ 'hide_opt' ][ $key ] ) ? $action->post_content[ 'conditions' ][ 'hide_opt' ][ $key ] : '',
				);
			}

			$action->post_content[ 'conditions' ] = $new_conditions;
		}

		$action->post_content[ 'event' ] = array( 'create' );

		return $action;
	}

}
