<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_App_Controller Class
 *
 * Controls setup
 *
 * @since 1.0.0
 *
 */
class PPFRM_Stripe_App_Controller {

	/**
	 * Parent plugin manager instance variable
	 *
	 * @since 1.0.0
	 *
	 * @var PPP_Parent
	 */
	public static $parent = null;

	/**
	 * Admin notices instance variable
	 *
	 * @since 1.0.0
	 *
	 * @var PPP_Admin_Notices
	 */
	public static $notices = null;

	/**
	 * Logger instance variable
	 *
	 * @since 1.0.0
	 *
	 * @var PPP_Logger
	 */
	public static $logger = null;

	/**
	 * Submitted transactions
	 *
	 * @var PPFRM_Stripe_DB_Transactions
	 */
	public static $transactions_db = null;

	/**
	 * Processed events
	 *
	 * @var PPFRM_Stripe_DB_Events
	 */
	public static $events_db = null;

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
	}

	/**
	 * Register WordPress hooks
	 *
	 * @since 1.0.0
	 */
	public function run() {

		register_activation_hook( PPFRM_STRIPE_FILE, array( 'PPFRM_Stripe_App_Controller', 'activate' ) );

		add_action( 'plugins_loaded', array( $this, 'setup' ) );

		add_action( 'init', array( $this, 'init' ) );

		add_action( 'admin_init', array( $this, 'admin_init' ) );
	}

	/**
	 * Activation actions
	 *
	 * @since     1.0.0
	 */
	public static function activate() {

		PPP_Parent::check_for_parent( 'formidable/formidable.php', 'FrmAppHelper', plugin_basename( PPFRM_STRIPE_FILE ) );

		PPFRM_Stripe_App_Helper::check_server_requirements();
		PPFRM_Stripe_App_Helper::add_permissions();
		PPFRM_Stripe_App_Helper::set_global_settings_page_redirect();

		delete_transient( 'ppfrm_stripe_currency' );
		delete_transient( 'ppfrm_stripe_usage_stats_cache_data' );
	}

	/**
	 * Load language files
	 *
	 * @since  1.0.0
	 *
	 */
	public function load_textdomain() {

		$ppfrm_stripe_lang_dir = dirname( plugin_basename( PPFRM_STRIPE_FILE ) ) . '/languages/';
		$ppfrm_stripe_lang_dir = apply_filters( 'ppfrm_stripe_language_dir', $ppfrm_stripe_lang_dir );

		$locale = apply_filters( 'plugin_locale', get_locale(), 'formidable-stripe' );
		$mofile = sprintf( '%1$s-%2$s.mo', 'formidable-stripe', $locale );

		$mofile_local  = $ppfrm_stripe_lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/formidable-stripe/' . $mofile;

		if ( file_exists( $mofile_global ) ) {
			load_textdomain( 'formidable-stripe', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			load_textdomain( 'formidable-stripe', $mofile_local );
		} else {
			load_plugin_textdomain( 'formidable-stripe', false, $ppfrm_stripe_lang_dir );
		}
	}


	/**
	 * Setup
	 *
	 * @since     1.0.0
	 */
	public function setup() {

		$this->load_textdomain();

		self::$parent = new PPP_Parent( array(
			                                'path'            => WP_PLUGIN_DIR . '/formidable/formidable.php',
			                                'main_class'      => 'FrmAppHelper',
			                                'min_version'     => PPFRM_STRIPE_MIN_FORMIDABLE_VERSION,
			                                'current_version' => ''
		                                ), plugin_basename( PPFRM_STRIPE_FILE ) );

		self::$notices = new PPP_Admin_Notices( PPFRM_STRIPE_SLUG, self::$parent );

		if ( $this->is_parent_active() ) {

			self::$logger = new PPP_Logger( PPFRM_STRIPE_SLUG, trailingslashit( PPFRM_STRIPE_PATH ) . 'includes/utilities/libraries/' );

			new PPP_Stripe_API(
				array(
					'slug'                 => PPFRM_STRIPE_SLUG,
					'path'                 => PPFRM_STRIPE_PATH,
					'settings_option_name' => 'ppfrm_stripe_global_settings',
					'logger'               => self::$logger
				)
			);

			new PPFRM_Stripe_App_Helper();

			self::$transactions_db = new PPFRM_Stripe_DB_Transactions( array(
				                                                           'plugin_slug' => PPFRM_STRIPE_SLUG,
				                                                           'table_name'  => 'frm_stripe_transactions',
				                                                           'primary_key' => 'id',
				                                                           'version'     => PPFRM_STRIPE_CURRENT_VERSION
			                                                           ) );

			new PPFRM_Stripe_Global_Settings();

			new PPFRM_Stripe_Form_Settings();

			new PPFRM_Stripe_Form_Display();

			new PPFRM_Stripe_Entry_Processor();

			new PPFRM_Stripe_Entry_Display();

			self::$events_db = new PPFRM_Stripe_DB_Events( array(
				                                               'plugin_slug' => PPFRM_STRIPE_SLUG,
				                                               'table_name'  => 'frm_stripe_events',
				                                               'primary_key' => 'id',
				                                               'version'     => PPFRM_STRIPE_CURRENT_VERSION
			                                               ) );

			$event_handler = new PPFRM_Stripe_Event_Handler(
				array(
					'plugin_slug' => PPFRM_STRIPE_SLUG,
					'logger'      => self::$logger
				)
			);

			new PPFRM_Stripe_Customer_API();

			new PPFRM_Stripe_Integration_Members();

		}
	}

	/**
	 * Check to see if parent plugin is active
	 *
	 * @since     1.0.0
	 */
	private function is_parent_active() {

		$parent_active = true;

		if ( ( ! self::$parent->is_parent_supported() ) && ( ! ( isset( $_GET[ 'action' ] ) && ( ( 'upgrade-plugin' == $_GET[ 'action' ] ) || ( 'update-selected' == $_GET[ 'action' ] ) ) ) ) ) {

			$message = __( 'Formidable + Stripe requires at least Formidable ' . PPFRM_STRIPE_MIN_FORMIDABLE_VERSION . '.', 'formidable-stripe' );
			self::$notices->set_admin_notice( $message, 'errors' );
			self::$notices->trigger_admin_notices();

			$parent_active = false;

		}

		return $parent_active;

	}

	/**
	 * Init actions
	 *
	 * @since     1.0.0
	 */
	public function init() {

		if ( ! $this->is_parent_active() ) {
			return;
		}

		add_filter( 'ppp_currencies', array( 'PPFRM_Stripe_App_Helper', 'ppp_currencies' ) );

	}

	/**
	 * Admin init actions
	 *
	 * @since 1.0.0
	 */
	public function admin_init() {

		PPFRM_Stripe_App_Helper::check_if_new_version();

		add_filter( 'plugin_action_links_' . plugin_basename( PPFRM_STRIPE_FILE ), array(
			'PPFRM_Stripe_App_Helper',
			'plugin_action_links'
		) );

		PPFRM_Stripe_App_Helper::redirect_to_settings_page();

	}

}