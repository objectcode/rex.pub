<?php
/**
 * @package   PPFRM_Stripe
 * @copyright 2014-2015 press+
 * @license   GPL-2.0+
 * @since     1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * PPFRM_Stripe_Global_Settings Class
 *
 * Controls global settings
 *
 * @since 1.0.0
 * */
class PPFRM_Stripe_Global_Settings {

	private static $_this = null;

	private $defaults = array();

	public function __construct() {
		add_action( 'admin_init', array( $this, 'admin_init' ) );

		self::$_this    = $this;
		$this->defaults = array(
			'mode'                 => 'test',
			'currency'             => 'USD',
			'test_publishable_key' => '',
			'test_secret_key'      => '',
			'live_publishable_key' => '',
			'live_secret_key'      => ''
		);
	}

	public function admin_init() {
		if ( 'formidable-settings' == PPFRM_Stripe_App_Helper::get_array_value( $_GET, 'page' ) ) {
			wp_enqueue_style( 'ppfrm_stripe_admin', trailingslashit( PPFRM_STRIPE_URL ) . 'css/admin.css', array(), /*PPFRM_STRIPE_CURRENT_VERSION*/
			                  time() );
			add_filter( 'frm_add_settings_section', array( $this, 'frm_add_settings_section' ) );
		}
	}

	public function frm_add_settings_section( $sections ) {
		$sections[ 'stripe' ] = array( 'class' => 'PPFRM_Stripe_Global_Settings', 'function' => 'do_settings_section' );

		return $sections;
	}

	public static function do_settings_section() {
		$action = isset( $_REQUEST[ 'frm_action' ] ) ? 'frm_action' : 'action';
		$action = FrmAppHelper::get_param( $action );
		if ( $action == 'process-form' && 'stripe_settings' == PPFRM_Stripe_App_Helper::get_array_value( $_GET, 't' ) ) {
			$message = '';
			$errors  = self::$_this->validate();
			if ( empty( $errors ) ) {
				self::$_this->update();
				$message = __( 'Stripe Settings Saved.', 'formidable-stripe' );
			}
			self::$_this->display( $errors, $message, true );
		} else {
			self::$_this->display();
		}
	}

	public function validate() {
		$errors = array();

		$mode = PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_mode' );
		if ( empty( $mode ) ) {
			$errors[ ] = __( 'Unable to update Stripe settings: You must select a mode.', 'formidable-stripe' );
		} else {
			$key = PPFRM_Stripe_App_Helper::get_post_value( "ppfrm_stripe_{$mode}_secret_key" );
			if ( empty( $key ) ) {
				$errors[ ] = sprintf( __( 'Unable to update Stripe settings: You must have a %s secret key.', 'formidable-stripe' ), $mode );
			}
			$key = PPFRM_Stripe_App_Helper::get_post_value( "ppfrm_stripe_{$mode}_publishable_key" );
			if ( empty( $key ) ) {
				$errors[ ] = sprintf( __( 'Unable to update Stripe settings: You must have a %s publishable key.', 'formidable-stripe' ), $mode );
			}
		}

		$errors = apply_filters( 'ppfrm_stripe_global_settings_validate', $errors );

		return $errors;
	}

	public function update() {
		$settings = array(
			'test_secret_key'      => trim( PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_test_secret_key' ) ),
			'test_publishable_key' => trim( PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_test_publishable_key' ) ),
			'live_secret_key'      => trim( PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_live_secret_key' ) ),
			'live_publishable_key' => trim( PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_live_publishable_key' ) ),
			'mode'                 => PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_mode' ),
			'currency'             => PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_currency' )
		);
		if ( empty( $settings[ 'currency' ] ) ) {
			$account                = PPP_Stripe_API::retrieve( 'account', $settings[ "{$settings['mode']}_secret_key" ] );
			$settings[ 'currency' ] = is_object( $account ) ? strtoupper( $account[ 'default_currency' ] ) : $this->defaults[ 'currency' ];
		}
		$settings = apply_filters( 'ppfrm_stripe_update_global_settings', $settings );


		update_option( 'ppfrm_stripe_global_settings', $settings );
		$usage_event_option = get_option( 'ppfrm_stripe_usage_events' );
		if ( empty( $usage_event_option ) ) {
			add_option( 'ppfrm_stripe_usage_events' );
		}

		$ppfrm_stripe_support_key = get_option( 'ppfrm_stripe_support_key' );
		$key                      = PPFRM_Stripe_App_Helper::get_post_value( 'ppfrm_stripe_support_key' );
		if ( empty( $key ) ) {
			delete_option( 'ppfrm_stripe_support_key' );
		} else if ( $ppfrm_stripe_support_key != $key ) {
			$key = md5( trim( $key ) );
			update_option( 'ppfrm_stripe_support_key', $key );
		}

		delete_transient( 'ppfrm_stripe_currency' );
	}

	public function display( $errors = array(), $message = '', $action = false ) {
		$ppfrm_stripe_support_key = get_option( 'ppfrm_stripe_support_key' );
		if ( ! empty( $ppfrm_stripe_support_key ) ) {
			$version_info = PPP_Updater::check_key( $ppfrm_stripe_support_key, $action );
		}
		$global_settings = get_option( 'ppfrm_stripe_global_settings' );
		if ( empty( $global_settings ) ) {
			$global_settings = $this->defaults;
			$valid_keys      = array();
		} else {
			$valid_keys = $this->validate_keys();
		}

		$valid_icon   = "&nbsp;<span class='dashicons dashicons-yes ppp_keystatus_valid valid_credentials' alt='valid key' title='valid key'></span>";
		$invalid_icon = "&nbsp;<span class='dashicons dashicons-no ppp_keystatus_invalid invalid_credentials' alt='invalid key' title='invalid key'></span>";

		require( trailingslashit( FrmAppHelper::plugin_path() ) . 'classes/views/shared/errors.php' );
		require( trailingslashit( PPFRM_STRIPE_PATH ) . 'includes/global-settings/views/global-settings.php' );
	}

	public function validate_keys() {
		$settings   = get_option( 'ppfrm_stripe_global_settings' );
		$valid_keys = array();

		if ( ! empty( $settings ) ) {
			$valid_keys = array(
				'test_secret_key'      => false,
				'test_publishable_key' => false,
				'live_secret_key'      => false,
				'live_publishable_key' => false
			);
			foreach ( $valid_keys as $key => $value ) {
				if ( ! empty( $settings[ $key ] ) ) {
					$valid_keys[ $key ] = PPP_Stripe_API::validate_key( $settings[ $key ] );
				}
			}
		}

		return $valid_keys;
	}
}