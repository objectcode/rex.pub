<?php
/**
 * Global Settings
 */
?>
<h2><span class="icon-stripe"></span><?php _e( ' Stripe Settings', 'formidable-stripe' ); ?></h2>
<div id="support-license-key" class="postbox ">
	<h3 class="hndle"><span><?php _e( "press+ Support License", 'formidable-stripe' ); ?></span></h3>

	<div class="inside">
		<div class="submitbox" id="submitpost">

			<div id="minor-publishing">
				<input type="password" name="ppfrm_stripe_support_key" id="ppfrm_stripe_support_key"
					   value="<?php echo empty( $ppfrm_stripe_support_key ) ? '' : $ppfrm_stripe_support_key; ?>"/>
				<?php echo ! empty( $version_info ) ? ( $version_info[ 'is_valid_key' ] ? $valid_icon : $invalid_icon ) : ''; ?>
				<br/>
				<?php _e( sprintf( "The license key is used for access to %spress+%s automatic upgrades and support.", "<a href='https://pressplus.pro/formidable-forms-stripe' target='_blank'>", "</a>" ), 'formidable-stripe' ); ?>


			</div>
		</div>

	</div>
</div>
<table class="form-table">
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_mode"><?php _e( 'API Mode', 'formidable-stripe' ); ?> <?php //gform_tooltip( 'stripe_api' ) ?></label>
		</th>
		<td width="88%">
			<input type="radio" name="ppfrm_stripe_mode" id="ppfrm_stripe_mode_live"
				   value="live" <?php checked( 'live', PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'mode' ), true ); ?>/>
			<label class="inline" for="ppfrm_stripe_mode_live"><?php _e( 'Live', 'formidable-stripe' ); ?></label>
			&nbsp;&nbsp;&nbsp; <input type="radio" name="ppfrm_stripe_mode" id="ppfrm_stripe_mode_test"
									  value="test" <?php checked( 'test', PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'mode' ), true ); ?>/>
			<label class="inline" for="ppfrm_stripe_mode_test"><?php _e( 'Test', 'formidable-stripe' ); ?></label>
		</td>
	</tr>
	<?php if ( ! empty( $global_settings['mode'] ) && ! empty( $global_settings["{$global_settings['mode']}_secret_key"] ) ) { ?>
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_currency"><?php _e( 'Currency', 'formidable-stripe' ); ?></label>
		</th>
		<td width="88%">
			<?php PPP_Currency::dropdown_currencies( array( 'id'               => 'ppfrm_stripe_currency',
															'name'             => 'ppfrm_stripe_currency',
															'selected'         => empty( $global_settings[ 'currency' ] ) ? '' : $global_settings[ 'currency' ]
													 ) );?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan='2'>
			<p><?php _e( sprintf( "You can find your <strong>Stripe API keys</strong> needed below in your Stripe dashboard 'Account Settings' %shere%s", "<a href='https://dashboard.stripe.com/account/apikeys' target='_blank'>", "</a>" ), 'formidable-stripe' ); ?></p>
		</td>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_test_secret_key"><?php _e( 'Test Secret Key', 'formidable-stripe' ); ?> <?php //gform_tooltip( 'stripe_test_secret_key' ) ?></label>
		</th>
		<td width="88%">
			<input class="size-1" id="ppfrm_stripe_test_secret_key" name="ppfrm_stripe_test_secret_key"
				   value="<?php echo trim( esc_attr( PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'test_secret_key' ) ) ) ?>"/>
			<?php echo empty( $valid_keys ) ? '' : ( $valid_keys[ 'test_secret_key' ] ? $valid_icon : $invalid_icon ) ?>
			<br/>
		</td>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_test_publishable_key"><?php _e( 'Test Publishable Key', 'formidable-stripe' ); ?> <?php //gform_tooltip( 'stripe_test_publishable_key' ) ?></label>
		</th>
		<td width="88%">
			<input class="size-1" id="ppfrm_stripe_test_publishable_key" name="ppfrm_stripe_test_publishable_key"
				   value="<?php echo trim( esc_attr( PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'test_publishable_key' ) ) ) ?>"/>
			<?php echo empty( $valid_keys ) ? '' : ( $valid_keys[ 'test_publishable_key' ] ? $valid_icon : $invalid_icon ) ?>
			<br/>
		</td>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_live_secret_key"><?php _e( 'Live Secret Key', 'formidable-stripe' ); ?> <?php //gform_tooltip( 'stripe_live_secret_key' ) ?></label>
		</th>
		<td width="88%">
			<input class="size-1" id="ppfrm_stripe_live_secret_key" name="ppfrm_stripe_live_secret_key"
				   value="<?php echo trim( esc_attr( PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'live_secret_key' ) ) ) ?>"/>
			<?php echo empty( $valid_keys ) ? '' : ( $valid_keys[ 'live_secret_key' ] ? $valid_icon : $invalid_icon ) ?>
			<br/>
		</td>
	</tr>
	<tr>
		<th scope="row" nowrap="nowrap"><label
				for="ppfrm_stripe_live_publishable_key"><?php _e( 'Live Publishable Key', 'formidable-stripe' ); ?> <?php //gform_tooltip( 'stripe_live_publishable_key' ) ?></label>
		</th>
		<td width="88%">
			<input class="size-1" id="ppfrm_stripe_live_publishable_key" name="ppfrm_stripe_live_publishable_key"
				   value="<?php echo trim( esc_attr( PPFRM_Stripe_App_Helper::get_array_value( $global_settings, 'live_publishable_key' ) ) ) ?>"/>
			<?php echo empty( $valid_keys ) ? '' : ( $valid_keys[ 'live_publishable_key' ] ? $valid_icon : $invalid_icon ) ?>
			<br/>
		</td>
	</tr>
</table>