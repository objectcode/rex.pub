<?php

/**

 * Plugin Name: VC Animated Text

 * Description: This plugin makes Animated Text and Words for visual composer add-on Plugin Simple, easy to use interface - perfect for individual users, developers & clients!.

 * Version: 1.0

 * Author: Hitesh Khunt

 * Author URI: http://www.saragna.com/Hitesh-Khunt

 * Plugin URI: http://plugin.saragna.com/animated-slider/vc-animated-text/

 * License: GPLv2 or later

 * 

 */
error_reporting(0);
add_action('init', 'do_output_buffer_vc_text');
if(!function_exists('do_output_buffer_vc_text')){
	function do_output_buffer() {
		ob_start();
	}
}
$vcanimatetext_Version = "1.0";

$currentFile = __FILE__;

$currentFolder = dirname($currentFile);
if(is_admin()){
	wp_enqueue_style( 'vc-animated-admin-css', plugins_url( ltrim( 'assets/css/admin.css', '/' ), __FILE__ ), array(), '' );
}
if(!is_admin()){
	wp_enqueue_style( 'vc-animated-css', plugins_url( ltrim( 'assets/css/animate.css', '/' ), __FILE__ ), array(), '' );
	wp_enqueue_script( 'vc-fittext-js', plugins_url( ltrim( 'assets/js/jquery.fittext.js', '/' ), __FILE__ ), array( 'jquery' ), false, true );
	wp_enqueue_script( 'vc-lettering-js', plugins_url( ltrim( 'assets/js/jquery.lettering.js', '/' ), __FILE__ ), array( 'jquery' ), false, true );
	wp_enqueue_script( 'vc-textillate-js', plugins_url( ltrim( 'assets/js/jquery.textillate.js', '/' ), __FILE__ ), array( 'jquery' ), false, true );
	wp_enqueue_script( 'vc-viewportchecker-js', plugins_url( ltrim( 'assets/js/jquery.viewportchecker.js', '/' ), __FILE__ ), array( 'jquery' ), false, true );
}
require_once $currentFolder . '/inc/all_function.php';


function vc_animated_text_shortcode($attr){
	extract(shortcode_atts( array(
		'stype' => '1',
		'uid' => '',
		'text' => '',
		'in_effect' => 'flash',
		'in_sequence' => 'sequence',
		'in_delay_scale' =>'1.5',
		'in_delay' => '50',
		'in_callback' => '',
		'out_effect' => 'flash',
		'out_sequence' => 'sequence',
		'out_delay_scale' =>'1.5',
		'out_delay' => '50',
		'out_callback' => '',
		'loop' => '0',
		'min_display_time' => '2000',
		'init_delay' => '0',
		'auto_start' => '1',
		'type' => 'char',
		'viewport' => '1',
		'css' => '',
		'compressor' => '1.0',
		'min_font_size' => '14',
		'max_font_size' => '22'
	), $attr));
	ob_start();
	if($stype == '2'){
	$sent_line = explode('|',$text);
		if(count($sent_line) > 1){?>
		<div id="vc-animate-text-<?php echo $uid;?>">
			<ul class="texts" <?php echo ($viewport == '1') ? 'style="display: none"' : '';?>>
			<?php for($i=0; $i<count($sent_line);$i++){?>
				<li><?php echo $sent_line[$i];?></li>
			<?php }?>
			</ul>
		</div>
		<?php }else{?>
		<div id="vc-animate-text-<?php echo $uid;?>">
			<ul class="texts" <?php echo ($viewport == '1') ? 'style="display: none"' : '';?>>
				<li><?php echo $text;?></li>
			</ul>
		</div>
	<?php }
	}else{?>
	<div id="vc-animate-text-<?php echo $uid;?>">
		<ul class="texts" <?php echo ($viewport == '1') ? 'style="display: none"' : '';?>>
			<li><?php echo $text;?></li>
		</ul>
	</div>
	<?php }
	if($css != ''){?>
		<style type='text/css'><?php echo strip_tags($css);?></style>
	<?php }?>
	<script type="text/javascript">;
	(function($) {
		$(document).ready(function() {
			var viweport = true;
			var $vcat = $("#vc-animate-text-<?php echo $uid;?>");
			$vcat.fitText(<?php echo $compressor;?>, { minFontSize:<?php echo $min_font_size;?>, maxFontSize:<?php echo $max_font_size;?>});
			$vcat.textillate({
				loop:<?php echo ($loop == '0') ? 'false' : 'true';?>,
				minDisplayTime:<?php echo $min_display_time;?>,
				initialDelay:<?php echo $init_delay;?>,
				autoStart:<?php echo ($auto_start == '1') ? 'true' : 'false';?>,
				in: { 
					effect:"<?php echo $in_effect;?>", 
					delayScale:<?php echo $in_delay_scale;?>, 
					delay:<?php echo $in_delay;?>,
					sync:<?php echo ($in_sequence == 'sync') ? 'true' : 'false';?>, 
					shuffle:<?php echo ($in_sequence == 'shuffle') ? 'true' : 'false';?>, 
					reverse:<?php echo ($in_sequence == 'reverse') ? 'true' : 'false';?>,
					callback: function () { <?php echo $in_callback;?> }
				},
				out: { 
					effect:"<?php echo $out_effect;?>", 
					delayScale:<?php echo $out_delay_scale;?>, 
					delay:<?php echo $out_delay;?>, 
					sync:<?php echo ($out_sequence == 'sync') ? 'true' : 'false';?>, 
					shuffle:<?php echo ($out_sequence == 'shuffle') ? 'true' : 'false';?>, 
					reverse:<?php echo ($out_sequence == 'reverse') ? 'true' : 'false';?>,
					callback: function () { <?php echo $out_callback;?> }
				},
				type:"<?php echo $type;?>"
			});
			
			<?php if($viewport == '1'){?>
			$("#vc-animate-text-<?php echo $uid;?>").viewportChecker({
				offset: 100,
				repeat: false,
				callbackFunction: function(elem, action){
					if (action == "add" && viweport) {
						$(elem).textillate("start");
					}
				}
			});
			<?php }?>
		});
	})(jQuery);
	</script>
	<?php
	$message = ob_get_clean();
	return $message;
}
add_shortcode('vc_animate_text','vc_animated_text_shortcode');
