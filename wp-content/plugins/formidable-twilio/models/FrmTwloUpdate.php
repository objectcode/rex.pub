<?php

class FrmTwloUpdate extends FrmAddon {
	public $plugin_file;
	public $plugin_name = 'Twilio';
	public $version = '1.05';

	public function __construct() {
		$this->plugin_file = dirname( dirname( __FILE__ ) ) . '/formidable-twilio.php';
		parent::__construct();
	}

	public static function load_hooks() {
		add_filter( 'frm_include_addon_page', '__return_true' );
		new FrmTwloUpdate();
	}

}
