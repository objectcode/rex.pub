msgid ""
msgstr ""
"Project-Id-Version: Easy Print - Responsive FlipBook WP Extension\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Tue Jun 23 2015 10:05:54 GMT+0200 (Środkowoeuropejski "
"czas letni)\n"
"POT-Revision-Date: Tue Jun 23 2015 10:05:57 GMT+0200 (Środkowoeuropejski "
"czas letni)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"

#: ../index.php:41
msgid "Print?"
msgstr ""

#: ../index.php:50
msgid "Print"
msgstr ""

#: ../index.php:51
msgid "Print:"
msgstr ""

#: ../index.php:57
msgid "Enable/Disable Print button."
msgstr ""

#: ../index.php:62
msgid "Menu Order"
msgstr ""

#: ../index.php:63
msgid "Menu order:"
msgstr ""

#: ../index.php:71
msgid "Icon"
msgstr ""

#: ../index.php:72
msgid "Icon:"
msgstr ""

#: ../index.php:90
msgid "Combine pages"
msgstr ""

#: ../index.php:91
msgid "Combine First & Last page: "
msgstr ""

#: ../index.php:97
msgid "Enable this option to print First & Last page near to each other."
msgstr ""

#: ../index.php:100
msgid "Print Settings"
msgstr ""

#: ../index.php:103
msgid "Hard Covers"
msgstr ""

#: ../index.php:104
msgid "Print Hard Covers?: "
msgstr ""

#: ../index.php:110
msgid "Enable this option to add Hard Covers to print view."
msgstr ""

#: ../index.php:114
msgid "User Choice"
msgstr ""

#: ../index.php:115
msgid "Enable User Choice: "
msgstr ""

#: ../index.php:121
msgid "Enable this option to allow users select which page should be printed."
msgstr ""

#: ../index.php:160
msgid "print"
msgstr ""
