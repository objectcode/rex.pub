<?php
/*
Plugin Name: OBJECTCODE SOA
Plugin URI: //disruptiveware.com
Description: This package marks the introduction of Service Oriented Architecture into the Rex engine, and Genus.  It specifically runs all services through APIs that manage their own dependencies with Bower and other dependency management tools.  The clear benefit of this is that we can update services easily with OpWorks deployment across any EC2 instance that uses our GIT repositories for pulling build files.
Version 0.1
Author: CHO c/o Objectcode LC.
*/


/*                                                                                                                                         
 _|_|_|    _|_|_|_|  _|      _|      _|_|_|    _|_|_|    _|_|_|  _|_|_|    _|    _|  _|_|_|    _|_|_|_|_|  _|_|_|  _|      _|  _|_|_|_|  
 _|    _|  _|          _|  _|        _|    _|    _|    _|        _|    _|  _|    _|  _|    _|      _|        _|    _|      _|  _|        
 _|_|_|    _|_|_|        _|          _|    _|    _|      _|_|    _|_|_|    _|    _|  _|_|_|        _|        _|    _|      _|  _|_|_|    
 _|    _|  _|          _|  _|        _|    _|    _|          _|  _|    _|  _|    _|  _|            _|        _|      _|  _|    _|        
 _|    _|  _|_|_|_|  _|      _|      _|_|_|    _|_|_|  _|_|_|    _|    _|    _|_|    _|            _|      _|_|_|      _|      _|_|_|_|  
                                                                                                                                         
*/



/* Custom Definitions
 *
 * These are custom definitions that we set for the CONTENT URL, CONTENT DIRECTORY, and PLUGIN URL.
 * They are useful sometimes.
 * 
 * @since 2.9.9
 * @version 2.9.10
 * @package objectcode\genus\dsapp
 */
if (!defined('WP_CONTENT_URL')) define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if (!defined('WP_CONTENT_DIR')) define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if (!defined('WP_PLUGIN_URL'))  define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');


/* OC Translate
 *
 * Takes JSON Code and translates it into a file based on the type of data.
 * There are currently two types of data supported:  inventory, customer
 * 
 * @since 2.9.9
 * @version 2.9.10
 * @package objectcode\genus\dsapp
 */
function oc_translate($type, $data) {

 if (strlen($data)<10) return false;
   $data = json_decode($data);


}


/* OC Dependency Management for Bower, Composer, Docker, etc.
 *
 * lists out available bower componentry and presents availability of async endppoints by querying them 
 * using a structured query tool based off skeleton.ml (an objectcode language)
 * 
 * @since 2.9.9
 * @version 2.9.10
 * @package objectcode\genus\dep.manager
 */
function oc_deps() {



}
