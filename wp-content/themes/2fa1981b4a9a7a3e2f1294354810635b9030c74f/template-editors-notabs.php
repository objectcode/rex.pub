<?php
/*
Template Name: AJAX Editors No Tabs
*/
wp_enqueue_scripts();
?>
                            <?php if (have_posts()) :
                                while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
<style>
.tab_heading { 
  background-color: #ddd;
  color: #aaa;
  font-size: 13px;
  display: inline-block;
  font-weight: bold;
  margin-right:5px;
  padding:5px;
  border: 1px solid #ddd;
}
.tab_heading_active, .tab_heading:hover { 
  background: #aaa;
  color: #fff;
  font-size: 13px;
  display: inline-block;
  margin-right:5px;
  padding:5px;
  border:1px solid #aaa;
}
.frm_section_heading { 
  width: 100%;
  border: 1px solid #aaa;
  margin-top: 0px !important;
  overflow: hidden;
  padding-left:5px;
  padding-right:5px;
}
.frm_pos_top {
}
</style>
<?php

          wp_footer();
?>
