<?php
/*
Template Name: AJAX Editors
*/
?>

<style>
label.btn-default { background-color: #515965!important; }
.frm_style_formidable-style.with_frm_style .frm_radio label, .frm_style_formidable-style.with_frm_style .frm_checkbox label, div.rex-frmchk label, div.rex-frmchk div, div.rex-frmchk span, div.rex-frmchk input
{
  border-radius: 0px!important; 
    font-family: "Open Sans Condensed"!important;
    font-size: 15px !important;
    color: #fff!important;
}
#loader-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
}
#loader {
    display: block;
    position: relative;
    left: 50%;
    top: 50%;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #3498db;

    -webkit-animation: spin .5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin .5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    z-index: 1001;
}
@-webkit-keyframes spin {
0%   { 
    -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
    -ms-transform: rotate(0deg);  /* IE 9 */
    transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
}
100% {
    -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
    -ms-transform: rotate(360deg);  /* IE 9 */
    transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
}
}
@keyframes spin {
0%   { 
    -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
    -ms-transform: rotate(0deg);  /* IE 9 */
    transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
}
100% {
    -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
    -ms-transform: rotate(360deg);  /* IE 9 */
    transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
}
}

#loader-wrapper .loader-section {
position: fixed;
top: 0;
width: 51%;
height: 100%;
background: rgba(255,255,255,0.5);
z-index: 1000;
}

#loader-wrapper .loader-section.section-left {
left: 0;
}
#loader-wrapper .loader-section.section-right {
right: 0;
}

.loaded #loader {
opacity: 0;

-webkit-transition: all 0.3s ease-out;  /* Android 2.1+, Chrome 1-25, iOS 3.2-6.1, Safari 3.2-6  */
        transition: all 0.3s ease-out;  /* Chrome 26, Firefox 16+, iOS 7+, IE 10+, Opera, Safari 6.1+  */

}
.loaded #loader-wrapper {
visibility: hidden;

-webkit-transform: translateY(-100%);  /* Chrome, Opera 15+, Safari 3.1+ */
    -ms-transform: translateY(-100%);  /* IE 9 */
        transform: translateY(-100%);  /* Firefox 16+, IE 10+, Opera */

-webkit-transition: all 0.3s 0.2s ease-out;  /* Android 2.1+, Chrome 1-25, iOS 3.2-6.1, Safari 3.2-6  */
        transition: all 0.3s 0.2s ease-out;  /* Chrome 26, Firefox 16+, iOS 7+, IE 10+, Opera, Safari 6.1+  */
}
</style>
<div id="loader-wrapper">
    <div id="loader"></div>
 
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
 
</div>
<?php wp_enqueue_scripts(); ?>
                            <?php if (have_posts()) :
                                while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; ?>
                            <?php endif; ?>

<?php wp_footer();  ?>                                                                                                                                  
<script>
jQuery('.frm-show-form').prepend('<div id="tab_headings"></div>'); 
jQuery('.frm_section_heading').each(function(i, obj) { 
  heading = jQuery(this).find('.frm_pos_top').text(); 
  jQuery('#tab_headings').append( '<div class="tab_heading" target="'+jQuery(this).attr('id')+'">' + heading + '</div>'); 
 if(i == 0) jQuery('#tab_headings').find('[target="'+jQuery(this).attr('id')+'"]').addClass('tab_heading_active'); 
 if(i != 0) jQuery(this).hide(); 
});

jQuery('.tab_heading').click(function() {
  jQuery('.frm_section_heading').hide(); 
  jQuery('.tab_heading').removeClass('tab_heading_active');
  jQuery(this).addClass('tab_heading_active');
  jQuery('#'+jQuery(this).attr('target')).show(); 
});
jQuery(document).ready(function($){ 
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 500);
 
});
</script>
<style>
.tab_heading { 
  background-color: #515965!important;
  color: #fff!important;
  font-size: 13px;
  display: inline-block;
  font-weight: normal;
  font-family: "Open Sans Condensed";
  margin-right:5px;
  padding:5px;
}
.tab_heading_active, .tab_heading:hover { 
  background: #c9302c!important;
  color: #fff;
  font-size: 13px;
  font-family: "Open Sans Condensed";
  display: inline-block;
  margin-right:5px;
  padding:5px;
}
.frm_section_heading { 
  width: 100%;
  margin-top: 0px !important;
}
.frm_pos_top {
}
.ui-widget-header {
    color: #32373c;
    font-weight: bold;
    border: 1px solid #ccc;
    border-radius: 0px!important;
    background-image: none;
    font-weight: 300;
    font-size: 12px;
    background-color: #DFF0D8;
}
.ui-widget-content {
    border: 0px solid #FFF;
}
</style>
