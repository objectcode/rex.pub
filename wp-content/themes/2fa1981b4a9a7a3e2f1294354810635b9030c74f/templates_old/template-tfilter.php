<?php
/*
Template Name: AJAX Ext Task Grid Filters
*/
?>
<html>
<head><title>Task Grid Filters</title>
<script type="text/javascript" src="/ext-6.0.1/build/objectcode/classic/shared/include-ext.js?theme=triton"></script>
<script src="/wp-includes/js/jquery/jquery.js?ver=1.11.3"></script>
<link rel="stylesheet" type="text/css" href="/ext-6.0.1/build/objectcode/base.css"/>
<style>
.x-window-default > * {
    height: -10px;
    border-color: #DD5F5F;
    -webkit-border-radius: 0 0 0 0;
    -moz-border-radius: 0 0 0 0;
    -ms-border-radius: 0 0 0 0;
    -o-border-radius: 0 0 0 0;
    border-radius: 0 0 0 0;
}
.x-message-box .x-window-body {
    background-color: #fff;
    overflow: hidden!important;
}
.x-panel-header-default-framed-top {
   background-color: #C3C1C1;
}
.x-panel-header-default-framed {
    font-size: 16px;
    border: 0px;
}
.x-panel-default-framed {
    border-color: #E1E1E1;
    padding: 0;
}
.x-panel-default {
    border-color: #949494;
    padding: 0;
}
.x-panel-header-default {
    font-size: 16px;
    border: 0px;
}
.x-panel-header-default {
    background-image: none;
    background-color: #fff; 
}
.x-panel-body-default {
    color: #EBEBEB;
}
.x-panel-header-default-framed .x-tool-img, .x-btn-over {
    background-color: #C3C1C1;
}
.x-btn-over.x-btn-default-small {
    border-style: solid;
    background-color: #222!important;
}
.x-btn-default-small {
    border-style: solid;
    background-color: #DF5204!important;
}
.x-btn-default-small {
    border-color: rgba(64, 64, 64, 0.31)!important;
}
</style>
<script type="text/javascript">
Logger = (function(){
    var panel;

    return {
        init: function(log){
            panel = log;
        },

        log: function(msg, isStart){
            panel.update({
                now: new Date(),
                cls: isStart ? 'beforeload' : 'afterload',
                msg: msg
            });
            panel.body.scroll('b', 100000, true);
        },
        pms: function() {
            var lsp = Ext.urlDecode(location.search.substring(1));  
            return lsp.class;  
        }
    };
})();
/*********************
  FILTERING MODELS 
**********************/
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.toolbar.Paging',
    'Ext.ux.ProgressBarPager',
    'Ext.panel.Panel',
    'Ext.layout.container.Card',
    'Ext.tip.QuickTipManager'
]);

Ext.define('ObjectCode.model.Base', {
    extend: 'Ext.data.Model',
    schema: {
        namespace: 'ObjectCode.model'
    }
});

Ext.define('ObjectCode.model.grid.Product', {
    extend: 'ObjectCode.model.Base',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: '713dvd5',
        type: 'string'
    }, {
        name: '4egcx35',
        type: 'string'
    }, {
        name: '74sxrp45',
        type: 'string'
    }, {
        name: '1p2bb44',
        type: 'string'
    }, {
        name: 'wrxj8d5',
        type: 'string'
    }, {
        name: 'vtt5jn',
        type: 'string'
    }, {
        name: 'h2pda7',
        type: 'string'
    }, {
        name: 'k67ek844',
        type: 'string'
    }]
});
Ext.define('ObjectCode.store.Products', {
    extend: 'Ext.data.Store',

    alias: 'store.products',
    model: 'ObjectCode.model.grid.Product',
    proxy: { 
        type: 'ajax',
        url: '/wp-json/frm/forms/58/entries/?sessk=385IR-BV734-78BJA-0W3TB',
        reader: {
            type: 'json',
            rootProperty: 'entries',
            idProperty: 'id',
        }
    },
});


/*********************
  FILTERING VIEW 
**********************/

Ext.define('ObjectCode.view.grid.GridFiltering', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid-filtering',
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.util.*',
        'Ext.toolbar.Paging',
        'Ext.ux.SlidingPager',
        'ObjectCode.store.Products'
    ],
    title: 'Actions/Appointments',
    collapsible: true,
    frame: true,
    width: 700,
    height: 900,
    resizable: true,

    plugins: 'gridfilters',

    emptyText: 'No Matching Records',
    loadMask: true,
    stateful: true,

    /* Set a stateId so that this grid's state is persisted. */
    stateId: 'stateful-filter-grid',

    store: {
        type: 'products',
        autoLoad: true,
        autoDestroy: true,
        url: '/wp-json/frm/forms/58/entries/?sessk=385IR-BV734-78BJA-0W3TB',
    },

    /* Dispatch named listener and handler methods to this instance */
    defaultListenerScope: true,

    tbar: [
    {
        text: 'Clear Filters',
        tooltip: 'Clear all filters',
        handler: 'onClearFilters'
    }
    , {
        text: 'Reload',
        tooltip: 'Reload this view',
        handler: 'onOtherFilters'
    }],

    columns: [{
        dataIndex: 'id',
        text: 'Id',
        width: 50,
        filter: 'number',
        hidden: true,
    }, {
        dataIndex: '713dvd5',
        text: 'Customer E-mail',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: '4egcx35',
        text: 'Status',
        flex: 1,
        filter: {
            type: 'list',
        }
    }, {
        dataIndex: '74sxrp45',
        text: 'Assigned Salesman',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: '1p2bb44',
        text: 'Priority',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: 'wrxj8d5',
        text: 'Created Date',
        flex: 1,
        filter: {
            type: 'date',
        }
    }, {
        dataIndex: 'vtt5jn',
        text: 'Scheduled Date',
        flex: 1,
        filter: {
            type: 'date',
        }
    }, {
        dataIndex: 'h2pda7',
        text: 'Scheduled Time',
        flex: 1,
        renderer: Ext.util.Format.stripTags,
        filter: {
            type: 'string',
        }
    },
    {
       dataIndex: '', 
       text: 'Edit',
       width: 70,
       xtype: 'widgetcolumn',
       widget: {
          width: 40,
          text: '',
          textAlign: 'center',
          value: 'id',
          xtype: 'button',
          iconCls: 'fa fa-edit',
          renderer: function(val, meta, record, rowIndex) {
                    console.log(record.get('id'));
          },
          handler: function(btn) {
            var rec = btn.getWidgetRecord();
            Ext.create('Ext.window.Window', {
                title: 'Edit',
                height: 600,
                width: 1500,
                layout: 'fit',
                html: '<iframe width="100%" height="100%" src="/add-action-appointment/?frm_action=edit&entry='+rec.get('id')+'"></iframe>',
            }).show();
          },
       }
    }],
    onOtherFilters: function () {
            this.refresh();
    },
    onShowFilters: function () {
            Ext.create('Ext.window.Window', {
                title: 'Create a Appointment',
                height: 600,
                width: 1500,
                layout: 'fit',
                html: '<iframe width="100%" height="100%" src="/add-action-appointment/"></iframe>',
            }).show();
    },
    onClearFilters: function () {
        /* The "filters" property is added to the grid (this) by gridfilters */
        this.filters.clearFilters();
    },

});

/*********************
  LOADER 
**********************/


Ext.onReady(function(){

    var main = Ext.create('Ext.panel.Panel', {
        renderTo: document.body,
        width: "100%",
        height: 900,
        border: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            height: 900,
            xtype: 'container',
            layout: 'card',
            margin: '0 0 5 0'
        }, {
            flex: 1,
            title: 'Loader log',
            tplWriteMode: 'append',
            tpl: '<div class="{cls}">[{now:date("H:i:s")}] - {msg}</div>',
            bodyPadding: 5,
            scrollable: true,
            listeners: {
                render: Logger.init
            }
        }]
    });
    Logger.log('Begin loading information for filters', true);
    main.items.first().add({
        xtype: 'grid-filtering'
    });

});
</script>
</head>
<body>
</body>
</html>
