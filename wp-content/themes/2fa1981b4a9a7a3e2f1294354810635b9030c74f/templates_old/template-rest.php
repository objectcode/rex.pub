<?php
/*
Template Name: AJAX REST
*/ 
if (!$_GET['f']) { echo 'No Form Specified'; exit; }
if (!is_numeric($_GET['f'])) { echo 'Numeric IDs only!'; exit; }

$api_key = "&k=385IR-BV734-78BJA-0W3TB";
$url = 'http://a.badasscrm.com/wp-json/frm/forms/'.$_GET['f'].'/entries/?'.$_SERVER['QUERY_STRING'].$api_key;
$response = wp_remote_get( $url, array('timeout' => 45 ) );

if ( is_wp_error( $response ) ) {
   $error_message = $response->get_error_message();
   echo "Something went wrong: $error_message";
} else {
echo $response['body'];
}
