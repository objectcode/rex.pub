<?php
/*
Template Name: AJAX Ext Grid Filters
*/
?>
<html>
<head><title>Grid Filters</title>
<script type="text/javascript" src="/ext-6.0.1/build/objectcode/classic/shared/include-ext.js?theme=triton"></script>
<link rel="stylesheet" type="text/css" href="/ext-6.0.1/build/objectcode/base.css"/>
<style>
.x-window-default > * {
    height: -10px;
    border-color: #DD5F5F;
    -webkit-border-radius: 0 0 0 0;
    -moz-border-radius: 0 0 0 0;
    -ms-border-radius: 0 0 0 0;
    -o-border-radius: 0 0 0 0;
    border-radius: 0 0 0 0;
}
.x-message-box .x-window-body {
    background-color: #fff;
    overflow: hidden!important;
}
.x-panel-header-default-framed-top {
   background-color: #C3C1C1;
}
.x-panel-header-default-framed {
    font-size: 16px;
    border: 0px;
}
.x-panel-default-framed {
    border-color: #E1E1E1;
    padding: 0;
}
.x-panel-default {
    border-color: #949494;
    padding: 0;
}
.x-panel-header-default {
    font-size: 16px;
    border: 0px;
}
.x-panel-header-default {
    background-image: none;
    background-color: #fff; 
}
.x-panel-body-default {
    color: #EBEBEB;
}
.x-panel-header-default-framed .x-tool-img, .x-btn-over {
    background-color: #C3C1C1;
}
.x-btn-over.x-btn-default-small {
    border-style: solid;
    background-color: #222!important;
}
.x-btn-default-small {
    border-style: solid;
    background-color: #DF5204!important;
}
.x-btn-default-small {
    border-color: rgba(64, 64, 64, 0.31)!important;
}
</style>
<script type="text/javascript">
/*********************
  FILTERING MODELS 
**********************/
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.toolbar.Paging',
    'Ext.ux.ProgressBarPager',
    'Ext.panel.Panel',
    'Ext.layout.container.Card',
    'Ext.tip.QuickTipManager'
]);

Ext.define('ObjectCode.model.Base', {
    extend: 'Ext.data.Model',
    schema: {
        namespace: 'ObjectCode.model'
    }
});

Ext.define('ObjectCode.model.grid.Product', {
    extend: 'ObjectCode.model.Base',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'company'
    }, {
        name: 'price',
        type: 'float'
    }, {
        name: 'date',
        type: 'date',
        dateFormat: 'Y-m-d'
    }, {
        name: 'visible',
        type: 'boolean'
    }, {
        name: 'size'
    }]
});

Ext.define('ObjectCode.store.Products', {
    extend: 'Ext.data.Store',

    alias: 'store.products',
    model: 'ObjectCode.model.grid.Product',
    proxy: {
        type: 'ajax',
        url: '/ext-6.0.1/build/objectcode/grid-filter.json',
        reader: {
            type: 'json',
            rootProperty: 'data',
            idProperty: 'id',
            totalProperty: 'total'
        }
    },
    remoteSort: false,
    sorters: [{
        property: 'company',
        direction: 'ASC'
    }],
    pageSize: 10
});

/*********************
  FILTERING VIEW 
**********************/

Ext.define('ObjectCode.view.grid.GridFiltering', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid-filtering',
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.util.*',
        'Ext.toolbar.Paging',
        'Ext.ux.SlidingPager',
        'ObjectCode.store.Products'
    ],
<?php $mode= ($_GET['m'] == 'c') ? 'Customers':'Tasks'; ?>
    title: '<?=$mode?>',
    collapsible: true,
    frame: true,
    width: 700,
    height: 900,
    resizable: true,

    plugins: 'gridfilters',

    emptyText: 'No Matching Records',
    //loadMask: true,
    //stateful: true,

    /* Set a stateId so that this grid's state is persisted. */
    stateId: 'stateful-filter-grid',

    store: {
        type: 'products',
        url: '/ext-6.0.1/build/objectcode/grid-filter.json',
        autoLoad: true,
        autoDestroy: true
    },

    /* Dispatch named listener and handler methods to this instance */
    defaultListenerScope: true,
   initComponent: function(){
        this.width = 650;
        var store = new Ext.data.Store({
            model: ObjectCode.model.grid.Product,
            remoteSort: true,
            proxy: {
                type: 'ajax',
                url: '/ext-6.0.1/build/objectcode/grid-filter.json',
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    idProperty: 'id',
                    totalProperty: 'total'
                }
            },
            enablePaging: true,
            limit: 10,
            pageSize: 10,
        });
        
        Ext.apply(this, {
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: store,
                plugins: new Ext.ux.SlidingPager()
            }
        });
        this.callParent();
    },
    
    afterRender: function(){
        this.callParent(arguments);
        this.getStore().load();
    },

    tbar: [
    {
        text: 'Clear Filters',
        tooltip: 'Clear all filters',
        handler: 'onClearFilters'
    }],

    columns: [{
        dataIndex: 'id',
        text: 'Id',
        width: 50,

        /* Specify that this column has an associated Filter. This is
           processed by the gridfilters plugin. If this is a string,
           this is the type of filter to apply. */
        filter: 'number'
    }, {
        dataIndex: 'company',
        text: 'Company',
        flex: 1,

        /* As an object, the type property indicates the type of filter to
         apply. All other properties configure that filter instance.  */
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: 'price',
        text: 'Price',
        width: 90,
        formatter: 'usMoney',

        filter: 'number'
    }, {
        dataIndex: 'size',
        text: 'Size',
        width: 120,

        filter: 'list' /* Use the unique field values for the pick list */
    }, {
        xtype: 'datecolumn',
        dataIndex: 'date',
        text: 'Date',
        width: 120,

        filter: true  /* use dataIndex first then fallback to column type */
    }, {
        dataIndex: 'visible',
        text: 'Visible',
        width: 80,

        filter: 'boolean'
    },
    {       dataIndex: 'Action', 
            text: 'Action',
            width: 105,
            xtype: 'widgetcolumn',
            widget: {
                width: 50,
                text: 'Act',
                textAlign: 'left',
                xtype: 'splitbutton',
                iconCls: 'fa fa-edit',
                handler: function(btn) {
                    var rec = btn.getWidgetRecord();
                    Ext.Msg.alert("Editor", "We are now going to transfer you to a new tab that will allow for editing of this information. ");
                },
                menu: new Ext.menu.Menu({
                    items: [
                        // these will render as dropdown menu items when the arrow is clicked:
                        {text: 'Clone', handler: function(){ Ext.Msg.alert("Clone", "Cloning has been temporarily disable by the systems administrator."); }},
                        {text: 'Delete', handler: function(){ Ext.Msg.alert("Delete", "You are not currently authorized to delete stuff.  Sorry. "); }}
                    ]
                })
            }
        }
],

    onClearFilters: function () {
        /* The "filters" property is added to the grid (this) by gridfilters */
        this.filters.clearFilters();
    },

    onShowFilters: function () {
        var data = [];

        /* The actual record filters are placed on the Store. */
        this.store.getFilters().each(function (filter) {
            data.push(filter.serialize());
        });

        /* Pretty it up for presentation */
        data = Ext.JSON.encodeValue(data, '\n').replace(/^[ ]+/gm, function (s) {
            for (var r = '', i = s.length; i--; ) {
                r += '&#160;';
            }
            return r;
        });
        data = data.replace(/\n/g, '<br>');

        Ext.Msg.alert('Filter Data', data);
    }
});

/*********************
  LOADER 
**********************/

Logger = (function(){
    var panel;

    return {
        init: function(log){
            panel = log;
        },

        log: function(msg, isStart){
            panel.update({
                now: new Date(),
                cls: isStart ? 'beforeload' : 'afterload',
                msg: msg
            });
            panel.body.scroll('b', 100000, true);
        }
    };
})();

Ext.onReady(function(){

    var main = Ext.create('Ext.panel.Panel', {
        renderTo: document.body,
        width: "100%",
        height: 900,
        border: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            height: 900,
            xtype: 'container',
            layout: 'card',
            margin: '0 0 5 0'
        }, {
            flex: 1,
            title: 'Loader log',
            tplWriteMode: 'append',
            tpl: '<div class="{cls}">[{now:date("H:i:s")}] - {msg}</div>',
            bodyPadding: 5,
            scrollable: true,
            listeners: {
                render: Logger.init
            }
        }]
    });
    Logger.log('Begin loading information for filters', true);
    main.items.first().add({
        xtype: 'grid-filtering'
    });

});
</script>
</head>
<body>
</body>
</html>
