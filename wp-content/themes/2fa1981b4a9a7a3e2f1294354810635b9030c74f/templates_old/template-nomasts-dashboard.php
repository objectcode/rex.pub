<?php
/*
Template Name: AJAX Dashboard Template
*/
?>

                            <?php if (have_posts()) :
                                while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; ?>
                            <?php endif; ?>

<script>
jQuery(document).ready(function($){
  $('body:not(.su-other-shortcodes-loaded)').on('click', '.su-spoiler-title', function (e) {
    var $title = $(this),
      $spoiler = $title.parent(),
      bar = ($('#wpadminbar').length > 0) ? 28 : 0;
    // Open/close spoiler
    $spoiler.toggleClass('su-spoiler-closed');
    // Close other spoilers in accordion
    $spoiler.parent('.su-accordion').children('.su-spoiler').not($spoiler).addClass('su-spoiler-closed');
    // Scroll in spoiler in accordion
    if ($(window).scrollTop() > $title.offset().top) $(window).scrollTop($title.offset().top - $title.height() - bar);
    e.preventDefault();
  });
  $('.su-spoiler-content').removeAttr('style');
});
</script>
<style>
.shiftnav-loading { display:none; } 
</style>
<script src="/wp-content/plugins/svg-support/js/min/svg-inline-min.js"></script>
<script src="/wp-content/themes/2fa1981b4a9a7a3e2f1294354810635b9030c74f/js/default_dynamic.js"></script> 
<script src="/wp-content/themes/2fa1981b4a9a7a3e2f1294354810635b9030c74f/js/default.min.js"></script> 
<script src="/wp-content/themes/2fa1981b4a9a7a3e2f1294354810635b9030c74f/js/custom_js.js"></script> 
<script src="/wp-content/plugins/js_composer/assets/js/js_composer_front.js"></script>

<script> jQuery(document).ready(function($){ var request = jQuery.ajax({ url: "/manager-dashboard-ajax", data: {"filter":"day"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#orig-charts" ).html( msg ); }); var 
request = jQuery.ajax({ url: "/manager-dashboard-ajax-more-charts", data: {"filter":"day"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#more-charts" ).html( msg ); }); $('.su-tabs-nav > span:nth-child(1)').cli
ck(function() { var request = jQuery.ajax({ url: "/manager-dashboard-ajax", data: {"filter":"day"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#orig-charts" ).html( msg ); }); var request = jQuery.ajax({ url: 
"/manager-dashboard-ajax-more-charts", data: {"filter":"day"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#more-charts" ).html( msg ); }); }); $('.su-tabs-nav > span:nth-child(2)').click(function() { var reque
st = jQuery.ajax({ url: "/manager-dashboard-ajax", data: {"filter":"week"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#orig-charts" ).html( msg ); }); var request = jQuery.ajax({ url: "/manager-dashboard-ajax
-more-charts", data: {"filter":"week"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#more-charts" ).html( msg ); }); }); $('.su-tabs-nav > span:nth-child(3)').click(function() { var request = jQuery.ajax({ url:
 "/manager-dashboard-ajax", data: {"filter":"month"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#orig-charts" ).html( msg ); }); var request = jQuery.ajax({ url: "/manager-dashboard-ajax-more-charts", data: {
"filter":"month"}, method: "GET", dataType: "html" }); request.done(function( msg ) { $( "#more-charts" ).html( msg ); }); }); }); </script>
