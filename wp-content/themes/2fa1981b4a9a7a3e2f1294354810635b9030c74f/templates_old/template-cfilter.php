<?php
/*
Template Name: AJAX Ext Cust Grid Filters
*/
?>
<html>
<head><title>Customer Grid Filters</title>
<script type="text/javascript" src="/ext-6.0.1/build/objectcode/classic/shared/include-ext.js?theme=triton"></script>
<script src="/wp-includes/js/jquery/jquery.js?ver=1.11.3"></script>
<link rel="stylesheet" type="text/css" href="/ext-6.0.1/build/objectcode/base.css"/>
<style>
.x-window-default > * {
    height: -10px;
    border-color: #DD5F5F;
    -webkit-border-radius: 0 0 0 0;
    -moz-border-radius: 0 0 0 0;
    -ms-border-radius: 0 0 0 0;
    -o-border-radius: 0 0 0 0;
    border-radius: 0 0 0 0;
}
.x-message-box .x-window-body {
    background-color: #fff;
    overflow: hidden!important;
}
.x-panel-header-default-framed-top {
   background-color: #C3C1C1;
}
.x-panel-header-default-framed {
    font-size: 16px;
    border: 0px;
}
.x-panel-default-framed {
    border-color: #E1E1E1;
    padding: 0;
}
.x-panel-default {
    border-color: #949494;
    padding: 0;
}
.x-panel-header-default {
    font-size: 16px;
    border: 0px;
}
.x-panel-header-default {
    background-image: none;
    background-color: #fff; 
}
.x-panel-body-default {
    color: #EBEBEB;
}
.x-panel-header-default-framed .x-tool-img, .x-btn-over {
    background-color: #C3C1C1;
}
.x-btn-over.x-btn-default-small {
    border-style: solid;
    background-color: #222!important;
}
.x-btn-default-small {
    border-style: solid;
    background-color: #DF5204!important;
}
.x-btn-default-small {
    border-color: rgba(64, 64, 64, 0.31)!important;
}
</style>
<script type="text/javascript">
Logger = (function(){
    var panel;

    return {
        init: function(log){
            panel = log;
        },

        log: function(msg, isStart){
            panel.update({
                now: new Date(),
                cls: isStart ? 'beforeload' : 'afterload',
                msg: msg
            });
            panel.body.scroll('b', 100000, true);
        },
        pms: function() {
            var lsp = Ext.urlDecode(location.search.substring(1));  
            return lsp.class;  
        }
    };
})();
/*********************
  FILTERING MODELS 
**********************/
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.toolbar.Paging',
    'Ext.ux.ProgressBarPager',
    'Ext.panel.Panel',
    'Ext.layout.container.Card',
    'Ext.tip.QuickTipManager'
]);

Ext.define('ObjectCode.model.Base', {
    extend: 'Ext.data.Model',
    schema: {
        namespace: 'ObjectCode.model'
    }
});

Ext.define('ObjectCode.model.grid.Product', {
    extend: 'ObjectCode.model.Base',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'if909n',
        type: 'string'
    }, {
        name: '7fpbwv',
        type: 'string'
    }, {
        name: '6a1exn',
        type: 'string'
    }, {
        name: 'dx0vid',
        type: 'string'
    }, {
        name: 'e1vppe',
        type: 'string'
    }, {
        name: 'i20rrj',
        type: 'string'
    }, {
        name: '8c5r8r',
        type: 'string'
    }, {
        name: 'g5oyzv',
        type: 'string'
    }, {
        name: '2iffd3',
        type: 'string'
    }, {
        name: 'lxkjse',
        type: 'string'
    }, {
        name: 'nqqjtb',
        type: 'string'
    }, {
        name: '20ipze',
        type: 'string'
    }, {
        name: '5h2d8a',
        type: 'date'
    }, {
        name: '8ppakj',
        type: 'float'
    }]
});
Ext.define('ObjectCode.store.Products', {
    extend: 'Ext.data.Store',

    alias: 'store.products',
    model: 'ObjectCode.model.grid.Product',
    proxy: { 
        type: 'ajax',
        extraParams: { 'class': "Lead" },
        /* url: '/ext-6.0.1/build/objectcode/json.php', */
        url: '/wp-json/frm/forms/34/entries/?sessk=385IR-BV734-78BJA-0W3TB&class='+Logger.pms(),
        reader: {
            type: 'json',
            rootProperty: 'entries',
            idProperty: 'id',
        }
    },
});


/*********************
  FILTERING VIEW 
**********************/

Ext.define('ObjectCode.view.grid.GridFiltering', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid-filtering',
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.util.*',
        'Ext.toolbar.Paging',
        'Ext.ux.SlidingPager',
        'ObjectCode.store.Products'
    ],
    title: 'Customers',
    collapsible: true,
    frame: true,
    width: 700,
    height: 600,
    resizable: true,

    plugins: 'gridfilters',

    emptyText: 'No Matching Records',
    loadMask: true,
    stateful: true,

    /* Set a stateId so that this grid's state is persisted. */
    stateId: 'stateful-filter-grid',

    store: {
        type: 'products',
        autoLoad: true,
        autoDestroy: true,
        url: '/wp-json/frm/forms/34/entries/?sessk=385IR-BV734-78BJA-0W3TB&class='+Logger.pms(),
    },

    /* Dispatch named listener and handler methods to this instance */
    defaultListenerScope: true,

    tbar: [
    {
        text: 'Clear Filters',
        tooltip: 'Clear all filters',
        handler: 'onClearFilters'
    }, {
        text: 'New Customer',
        tooltip: 'Create a new customer',
        handler: 'onShowFilters'
    }, {
        text: 'Reload',
        tooltip: 'Reload this view',
        handler: 'onOtherFilters'
    }],

    columns: [{
        dataIndex: 'id',
        text: 'Id',
        width: 50,
        filter: 'number',
        hidden: true,
    }, {
        dataIndex: 'if909n',
        text: 'First Name',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: '7fpbwv',
        text: 'Last Name',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: '6a1exn',
        text: 'E-mail',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: 'dx0vid',
        text: 'Phone Number',
        flex: 1,
        filter: {
            type: 'string',
            itemDefaults: {
                emptyText: 'Search for...'
            }
        }
    }, {
        dataIndex: 'e1vppe',
        text: 'Class',
        flex: 1,
        filter: {
            type: 'list',
        }
    }, {
        dataIndex: 'i20rrj',
        text: 'Workflow',
        flex: 1,
        renderer: Ext.util.Format.stripTags,
        filter: {
                    renderer: Ext.util.Format.stripTags,
            type: 'list',
        }
    }, {
        dataIndex: '8c5r8r',
        text: 'Department',
        flex: 1,
        renderer: Ext.util.Format.stripTags,
        filter: {
            type: 'list',
        }
    }, {
        dataIndex: 'i810vu',
        text: 'Lead Source',
        width: 90,
        renderer: Ext.util.Format.stripTags,
        filter: { type: 'list' }
    }, {
        dataIndex: 'g5oyzv',
        text: 'Salesman',
        width: 90,
        filter: { type: 'list' }
    }, {
        dataIndex: '2iffd3',
        text: 'Make',
        width: 80,
        filter: { type: 'string' }
    }, {
        dataIndex: 'lxkjse',
        text: 'Model',
        width: 80,
        filter: { type: 'string' }
    }, {
        dataIndex: 'nqqjtb',
        text: 'Year',
        width: 80,
        filter: { type: 'list' }
    }, {
        dataIndex: '20ipze',
        text: 'Units',
        width: 80,
        filter: 'number'
    }, {
        dataIndex: '8ppakj',
        text: 'Gross',
        width: 90,
        formatter: 'usMoney',
        filter: 'number'
    }, {
        dataIndex: '5h2d8a',
        text: 'Date Modified',
        width: 90,
        formatter: 'date',
        filter: 'date'
    }, 
    {
       dataIndex: '', 
       text: 'Edit',
       width: 70,
       xtype: 'widgetcolumn',
       widget: {
          width: 40,
          text: '',
          textAlign: 'center',
          value: 'id',
          xtype: 'button',
          iconCls: 'fa fa-edit',
          renderer: function(val, meta, record, rowIndex) {
                    console.log(record.get('id'));
          },
          handler: function(btn) {
            var rec = btn.getWidgetRecord();
            Ext.create('Ext.window.Window', {
                title: 'Edit Customer',
                height: 600,
                width: 1500,
                layout: 'fit',
                html: '<iframe width="100%" height="100%" src="/customers-editor/?frm_action=edit&entry='+rec.get('id')+'"></iframe>',
            }).show();
          },
       }
    }],
    onOtherFilters: function () {
            this.refresh();
    },
    onShowFilters: function () {
            Ext.create('Ext.window.Window', {
                title: 'Create a New Customer',
                height: 600,
                width: 1500,
                layout: 'fit',
                html: '<iframe width="100%" height="100%" src="/customers-editor/"></iframe>',
            }).show();
    },
    onClearFilters: function () {
        /* The "filters" property is added to the grid (this) by gridfilters */
        this.filters.clearFilters();
    },

});

/*********************
  LOADER 
**********************/


Ext.onReady(function(){

    var main = Ext.create('Ext.panel.Panel', {
        renderTo: document.body,
        width: "100%",
        height: 900,
        border: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            height: 900,
            xtype: 'container',
            layout: 'card',
            margin: '0 0 5 0'
        }, {
            flex: 1,
            title: 'Loader log',
            tplWriteMode: 'append',
            tpl: '<div class="{cls}">[{now:date("H:i:s")}] - {msg}</div>',
            bodyPadding: 5,
            scrollable: true,
            listeners: {
                render: Logger.init
            }
        }]
    });
    Logger.log('Begin loading information for filters', true);
    main.items.first().add({
        xtype: 'grid-filtering'
    });

});
</script>
</head>
<body>
</body>
</html>
