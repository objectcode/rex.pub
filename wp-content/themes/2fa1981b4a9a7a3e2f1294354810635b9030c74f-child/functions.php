<?php
/* Bridge Child
 * For Genus.
 * 
 * This applies various customizatons to the login page.
 * 
 * @since 2.6.1
 * @version 2.9.4
 * @package objectcode\genus\base
 */
function oc_custom_login() {


    $buffer = <<<EOF
              <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
                <style>
                      p.galogin a {
                          text-decoration: none!important;
                          font-weight: 400!important;
                          font-family: "Varela Round";
                      }
                      p#nav {
                          display:none;

                      }
                      .login #backtoblog a, .login #nav a {
                          text-decoration: none;
                          color: #999;
                          display: none;
                      }
                      .wp-core-ui .button-primary {
                          background: #B1b1b1; border: none;
                      }
                      .wp-core-ui .button-primary:hover {
                          background: #333; font-family: "Varela Round", sans-serif;
                      }
                      form#loginform p.galogin {
                          background: none repeat scroll 0 0 #FFF;
                          border-color: #0074A2;
                          text-decoration: none;
                          text-align: center;
                          vertical-align: middle;
                          border-radius: 3px;
                          padding: 4px;
                          height: 27px;
                          font-size: 14px;
                      }
                      form#loginform p.galogin a {
                          color: #000;

                      }
                      .login label {
                          color: #666;
                          background-color: rgba(255,255,255,0.9);
                          font-size: 19px;
                          font-family: "Varela Round",sans-serif;
                          padding:5px 0px!important;
                      }
                      .login #backtoblog a, .login #nav a {
                          text-decoration: none;
                          color: #000;
                      }
                      .login h1 a {
                        background: url('/wp-content/plugins/objectcode-genus/img/large-admin-white.png') no-repeat top center;
position: relative!important; left: -50px;
 width: 400px;
                        height: 234px;
                        margin-left: 8px;
                        margin-top: 15px;
                        padding-bottom: 0px;
                        background-size: contain!important;
                      }
                      #login { padding-top: 18px; }
                      .login form { background: rgba(255,255,255,0.6); border:1px solid #ccc; }
                      html { background-image: url(/wp-content/uploads/2015/04/loginbg.jpg) !important;background-size:cover;  } body {background: transparent !important;}
                     /*
                     .login label { display: none; }
                     .galogin-or { display: none; }
                     .login .submit { display: none; }
                     */
                </style>
EOF;

    echo $buffer;
}



add_action( 'wp_head', 'oc_custom_login' );
