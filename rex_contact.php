<?php
/*
 * [REX] REST/JSON GATEWAY FOR SINGLE POST MANIPULATIONS 
 * 
 * @since rex build 39
 * @version 47.b
 *
 * a simplified gateway to implement meta-key and meta-value filters across the board 
 * 
 */
//define('DOING_CRON', true);

//require_once "wp-load.php";
if (isset($_REQUEST['s']))
{
$h = ($_SERVER['HTTPS']) ? 'https://': 'http://';
  $urlsingle = $h.$_SERVER['HTTP_HOST'].'/wp-json/wp/v2/contacts/'.$_REQUEST['s'];
	$single = file_get_contents($urlsingle);
	$single_json = json_decode($single);
	$redir= strip_tags($single_json->content->rendered);
if ($_REQUEST['d']=='destroy')
	header("Location: /customers-editor/?frm_action=destroy&entry=$redir");
if (!isset($_REQUEST['d'])) 
	header("Location: /customers-editor/?frm_action=edit&entry=$redir");
}
?> 
