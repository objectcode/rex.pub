```
#!javascript

  _____ ______ _________________________________ _____ ______ _______ (R)                                 
 |     ||_____]  |  |______|         |   |      |     ||     \|______                                                                                                             
 |_____||_____]__|  |______|_____    |   |_____ |_____||_____/|______                    
                                                                                                                                                                
       `*+**: Disruptiveware, LLP.                                                                                                           
                                                                                                                          
      Creator:  Christopher D. Hogan               
       Author:  <dr.c.hogan@disruptiveware.com>                                            
    Workspace:  Rex 4.0.2+


+-- async@0.2.10
+-- aws-sdk@2.2.34
¦ +-- sax@1.1.5
¦ +-- xml2js@0.4.16
¦ +-- xmlbuilder@4.2.1
¦   +-- lodash@4.0.1
+-- base-widget@1.0.10
¦ +-- blessed@0.1.81
¦ +-- bluebird@3.1.1
¦ +-- lodash@3.10.1
¦ +-- rc@1.1.6
¦ ¦ +-- deep-extend@0.4.1
¦ ¦ +-- ini@1.3.4
¦ ¦ +-- minimist@1.2.0
¦ ¦ +-- strip-json-comments@1.0.4
¦ +-- slap-util@1.0.6
¦ ¦ +-- bluebird@3.1.1
¦ ¦ +-- lodash@3.10.1
¦ ¦ +-- longjohn@0.2.11
¦ ¦ ¦ +-- source-map-support@0.3.2
¦ ¦ ¦   +-- source-map@0.1.32
¦ ¦ ¦     +-- amdefine@1.0.0
¦ ¦ +-- traverse@0.6.6
¦ ¦ +-- winston@1.1.2
¦ ¦   +-- async@1.0.0
¦ ¦   +-- colors@1.0.3
¦ ¦   +-- cycle@1.0.3
¦ ¦   +-- eyes@0.1.8
¦ ¦   +-- isstream@0.1.2
¦ ¦   +-- pkginfo@0.3.1
¦ ¦   +-- stack-trace@0.0.9
¦ +-- text-buffer@8.0.6
¦ ¦ +-- atom-diff@2.0.0
¦ ¦ +-- delegato@1.0.0
¦ ¦ ¦ +-- mixto@1.0.0
¦ ¦ +-- emissary@1.3.3
¦ ¦ ¦ +-- es6-weak-map@0.1.4
¦ ¦ ¦ ¦ +-- es6-iterator@0.1.3
¦ ¦ ¦ ¦ +-- es6-symbol@2.0.1
¦ ¦ ¦ +-- property-accessors@1.1.3
¦ ¦ +-- event-kit@1.5.0
¦ ¦ ¦ +-- grim@1.5.0
¦ ¦ +-- fs-plus@2.8.1
¦ ¦ ¦ +-- async@0.2.10
¦ ¦ ¦ +-- mkdirp@0.3.5
¦ ¦ ¦ +-- rimraf@2.2.8
¦ ¦ +-- marker-index@2.1.1
¦ ¦ ¦ +-- random-seed@0.2.0
¦ ¦ +-- serializable@1.0.3
¦ ¦ ¦ +-- get-parameter-names@0.2.0
¦ ¦ +-- span-skip-list@0.2.0
¦ +-- through2@2.0.0
¦   +-- readable-stream@2.0.5
¦   ¦ +-- core-util-is@1.0.2
¦   ¦ +-- isarray@0.0.1
¦   ¦ +-- process-nextick-args@1.0.6
¦   ¦ +-- string_decoder@0.10.31
¦   ¦ +-- util-deprecate@1.0.2
¦   +-- xtend@4.0.1
+-- bluebird@3.1.5
+-- browserify@13.0.0
¦ +-- assert@1.3.0
¦ +-- browser-pack@6.0.1
¦ ¦ +-- combine-source-map@0.7.1
¦ ¦ ¦ +-- convert-source-map@1.1.3
¦ ¦ ¦ +-- inline-source-map@0.6.1
¦ ¦ ¦ +-- lodash.memoize@3.0.4
¦ ¦ ¦ +-- source-map@0.4.2
¦ ¦ ¦   +-- amdefine@1.0.0
¦ ¦ +-- umd@3.0.1
¦ +-- browser-resolve@1.11.1
¦ +-- browserify-zlib@0.1.4
¦ ¦ +-- pako@0.2.8
¦ +-- buffer@4.4.0
¦ ¦ +-- base64-js@1.0.2
¦ ¦ +-- ieee754@1.1.6
¦ ¦ +-- isarray@1.0.0
¦ +-- concat-stream@1.5.1
¦ ¦ +-- typedarray@0.0.6
¦ +-- console-browserify@1.1.0
¦ ¦ +-- date-now@0.1.4
¦ +-- constants-browserify@1.0.0
¦ +-- crypto-browserify@3.11.0
¦ ¦ +-- browserify-cipher@1.0.0
¦ ¦ ¦ +-- browserify-aes@1.0.6
¦ ¦ ¦ ¦ +-- buffer-xor@1.0.3
¦ ¦ ¦ ¦ +-- cipher-base@1.0.2
¦ ¦ ¦ +-- browserify-des@1.0.0
¦ ¦ ¦ ¦ +-- cipher-base@1.0.2
¦ ¦ ¦ ¦ +-- des.js@1.0.0
¦ ¦ ¦ ¦   +-- minimalistic-assert@1.0.0
¦ ¦ ¦ +-- evp_bytestokey@1.0.0
¦ ¦ +-- browserify-sign@4.0.0
¦ ¦ ¦ +-- bn.js@4.10.1
¦ ¦ ¦ +-- browserify-rsa@4.0.0
¦ ¦ ¦ +-- elliptic@6.2.3
¦ ¦ ¦ ¦ +-- brorand@1.0.5
¦ ¦ ¦ ¦ +-- hash.js@1.0.3
¦ ¦ ¦ +-- parse-asn1@5.0.0
¦ ¦ ¦   +-- asn1.js@4.4.0
¦ ¦ ¦   ¦ +-- minimalistic-assert@1.0.0
¦ ¦ ¦   +-- browserify-aes@1.0.6
¦ ¦ ¦   ¦ +-- buffer-xor@1.0.3
¦ ¦ ¦   ¦ +-- cipher-base@1.0.2
¦ ¦ ¦   +-- evp_bytestokey@1.0.0
¦ ¦ +-- create-ecdh@4.0.0
¦ ¦ ¦ +-- bn.js@4.10.1
¦ ¦ ¦ +-- elliptic@6.2.3
¦ ¦ ¦   +-- brorand@1.0.5
¦ ¦ ¦   +-- hash.js@1.0.3
¦ ¦ +-- create-hash@1.1.2
¦ ¦ ¦ +-- cipher-base@1.0.2
¦ ¦ ¦ +-- ripemd160@1.0.1
¦ ¦ ¦ +-- sha.js@2.4.4
¦ ¦ +-- create-hmac@1.1.4
¦ ¦ +-- diffie-hellman@5.0.2
¦ ¦ ¦ +-- bn.js@4.10.1
¦ ¦ ¦ +-- miller-rabin@4.0.0
¦ ¦ ¦   +-- brorand@1.0.5
¦ ¦ +-- pbkdf2@3.0.4
¦ ¦ +-- public-encrypt@4.0.0
¦ ¦ ¦ +-- bn.js@4.10.1
¦ ¦ ¦ +-- browserify-rsa@4.0.0
¦ ¦ ¦ +-- parse-asn1@5.0.0
¦ ¦ ¦   +-- asn1.js@4.4.0
¦ ¦ ¦   ¦ +-- minimalistic-assert@1.0.0
¦ ¦ ¦   +-- browserify-aes@1.0.6
¦ ¦ ¦   ¦ +-- buffer-xor@1.0.3
¦ ¦ ¦   ¦ +-- cipher-base@1.0.2
¦ ¦ ¦   +-- evp_bytestokey@1.0.0
¦ ¦ +-- randombytes@2.0.2
¦ +-- defined@1.0.0
¦ +-- deps-sort@2.0.0
¦ +-- domain-browser@1.1.7
¦ +-- duplexer2@0.1.4
¦ +-- events@1.1.0
¦ +-- glob@5.0.15
¦ ¦ +-- inflight@1.0.4
¦ ¦ ¦ +-- wrappy@1.0.1
¦ ¦ +-- minimatch@3.0.0
¦ ¦ ¦ +-- brace-expansion@1.1.2
¦ ¦ ¦   +-- balanced-match@0.3.0
¦ ¦ ¦   +-- concat-map@0.0.1
¦ ¦ +-- once@1.3.3
¦ ¦ ¦ +-- wrappy@1.0.1
¦ ¦ +-- path-is-absolute@1.0.0
¦ +-- has@1.0.1
¦ ¦ +-- function-bind@1.0.2
¦ +-- htmlescape@1.1.0
¦ +-- https-browserify@0.0.1
¦ +-- inherits@2.0.1
¦ +-- insert-module-globals@7.0.1
¦ ¦ +-- combine-source-map@0.7.1
¦ ¦ ¦ +-- convert-source-map@1.1.3
¦ ¦ ¦ +-- inline-source-map@0.6.1
¦ ¦ ¦ +-- lodash.memoize@3.0.4
¦ ¦ ¦ +-- source-map@0.4.2
¦ ¦ ¦   +-- amdefine@1.0.0
¦ ¦ +-- is-buffer@1.1.2
¦ ¦ +-- lexical-scope@1.2.0
¦ ¦   +-- astw@2.0.0
¦ ¦     +-- acorn@1.2.2
¦ +-- isarray@0.0.1
¦ +-- JSONStream@1.0.7
¦ ¦ +-- jsonparse@1.2.0
¦ ¦ +-- through@2.3.8
¦ +-- labeled-stream-splicer@2.0.0
¦ ¦ +-- stream-splicer@2.0.0
¦ +-- module-deps@4.0.5
¦ ¦ +-- detective@4.3.1
¦ ¦ ¦ +-- acorn@1.2.2
¦ ¦ +-- stream-combiner2@1.1.1
¦ +-- os-browserify@0.1.2
¦ +-- parents@1.0.1
¦ ¦ +-- path-platform@0.11.15
¦ +-- path-browserify@0.0.0
¦ +-- process@0.11.2
¦ +-- punycode@1.4.0
¦ +-- querystring-es3@0.2.1
¦ +-- read-only-stream@2.0.0
¦ +-- readable-stream@2.0.5
¦ ¦ +-- core-util-is@1.0.2
¦ ¦ +-- process-nextick-args@1.0.6
¦ ¦ +-- util-deprecate@1.0.2
¦ +-- resolve@1.1.7
¦ +-- shasum@1.0.2
¦ ¦ +-- json-stable-stringify@0.0.1
¦ ¦ ¦ +-- jsonify@0.0.0
¦ ¦ +-- sha.js@2.4.4
¦ +-- shell-quote@1.4.3
¦ ¦ +-- array-filter@0.0.1
¦ ¦ +-- array-map@0.0.0
¦ ¦ +-- jsonify@0.0.0
¦ +-- stream-browserify@2.0.1
¦ +-- stream-http@2.1.0
¦ ¦ +-- builtin-status-codes@1.0.0
¦ ¦ +-- to-arraybuffer@1.0.1
¦ +-- string_decoder@0.10.31
¦ +-- subarg@1.0.0
¦ ¦ +-- minimist@1.2.0
¦ +-- syntax-error@1.1.5
¦ ¦ +-- acorn@2.7.0
¦ +-- through2@2.0.0
¦ +-- timers-browserify@1.4.2
¦ +-- tty-browserify@0.0.0
¦ +-- url@0.11.0
¦ ¦ +-- punycode@1.3.2
¦ ¦ +-- querystring@0.2.0
¦ +-- util@0.10.3
¦ +-- vm-browserify@0.0.4
¦ ¦ +-- indexof@0.0.1
¦ +-- xtend@4.0.1

+-- cheerio@0.19.0
¦ +-- css-select@1.0.0
¦ ¦ +-- boolbase@1.0.0
¦ ¦ +-- css-what@1.0.0
¦ ¦ +-- domutils@1.4.3
¦ ¦ +-- nth-check@1.0.1
¦ +-- dom-serializer@0.1.0
¦ ¦ +-- domelementtype@1.1.3
¦ +-- entities@1.1.1
¦ +-- htmlparser2@3.8.3
¦ ¦ +-- domelementtype@1.3.0
¦ ¦ +-- domhandler@2.3.0
¦ ¦ +-- domutils@1.5.1
¦ ¦ +-- entities@1.0.0
¦ ¦ +-- readable-stream@1.1.13
¦ +-- lodash@3.10.1
+-- copy-paste@1.1.4
¦ +-- iconv-lite@0.4.13
¦ +-- sync-exec@0.6.2
+-- drawille-canvas@1.1.2
¦ +-- bresenham@0.0.3
¦ +-- drawille@1.1.0
¦ +-- gl-matrix@2.3.1
+-- es6-set@0.1.3
¦ +-- d@0.1.1
¦ +-- es5-ext@0.10.11
¦ +-- es6-iterator@2.0.0
¦ +-- es6-symbol@3.0.2
¦ +-- event-emitter@0.3.4
+-- go@0.0.1
+-- highlight.js@9.0.0
+-- interval-skip-list@2.0.1
¦ +-- underscore-plus@1.6.6
¦   +-- underscore@1.6.0
+-- nan@2.2.0
+-- node-clap@0.0.5
¦ +-- bluebird@2.10.2
¦ +-- npm@2.14.18
¦   +-- abbrev@1.0.7
¦   +-- ansi@0.3.1
¦   +-- ansi-regex@2.0.0
¦   +-- ansicolors@0.3.2
¦   +-- ansistyles@0.1.3
¦   +-- archy@1.0.0
¦   +-- async-some@1.0.2
¦   +-- block-stream@0.0.8
¦   +-- char-spinner@1.0.1
¦   +-- chmodr@1.0.2
¦   +-- chownr@1.0.1
¦   +-- cmd-shim@2.0.1
¦   ¦ +-- graceful-fs@3.0.8
¦   +-- columnify@1.5.4
¦   ¦ +-- wcwidth@1.0.0
¦   ¦   +-- defaults@1.0.3
¦   ¦     +-- clone@1.0.2
¦   +-- config-chain@1.1.10
¦   ¦ +-- proto-list@1.2.4
¦   +-- dezalgo@1.0.3
¦   ¦ +-- asap@2.0.3
¦   +-- editor@1.0.0
¦   +-- fs-vacuum@1.2.7
¦   +-- fs-write-stream-atomic@1.0.8
¦   ¦ +-- iferr@0.1.5
¦   +-- fstream@1.0.8
¦   +-- fstream-npm@1.0.7
¦   ¦ +-- fstream-ignore@1.0.3
¦   +-- github-url-from-git@1.4.0
¦   +-- github-url-from-username-repo@1.0.2
¦   +-- glob@5.0.15
¦   ¦ +-- path-is-absolute@1.0.0
¦   +-- graceful-fs@4.1.3
¦   +-- hosted-git-info@2.1.4
¦   +-- imurmurhash@0.1.4
¦   +-- inflight@1.0.4
¦   +-- inherits@2.0.1
¦   +-- ini@1.3.4
¦   +-- init-package-json@1.9.3
¦   ¦ +-- glob@6.0.4
¦   ¦ ¦ +-- path-is-absolute@1.0.0
¦   ¦ +-- promzard@0.3.0
¦   +-- lockfile@1.0.1
¦   +-- lru-cache@3.2.0
¦   ¦ +-- pseudomap@1.0.1
¦   +-- minimatch@3.0.0
¦   ¦ +-- brace-expansion@1.1.1
¦   ¦   +-- balanced-match@0.2.1
¦   ¦   +-- concat-map@0.0.1
¦   +-- mkdirp@0.5.1
¦   ¦ +-- minimist@0.0.8
¦   +-- node-gyp@3.2.1
¦   ¦ +-- fstream@1.0.8
¦   ¦ +-- glob@4.5.3
¦   ¦ ¦ +-- minimatch@2.0.10
¦   ¦ +-- minimatch@1.0.0
¦   ¦ ¦ +-- lru-cache@2.7.3
¦   ¦ ¦ +-- sigmund@1.0.1
¦   ¦ +-- nopt@3.0.6
¦   ¦ ¦ +-- abbrev@1.0.7
¦   ¦ +-- npmlog@1.2.1
¦   ¦ ¦ +-- ansi@0.3.1
¦   ¦ ¦ +-- are-we-there-yet@1.0.6
¦   ¦ ¦ ¦ +-- delegates@1.0.0
¦   ¦ ¦ +-- gauge@1.2.5
¦   ¦ ¦   +-- has-unicode@2.0.0
¦   ¦ ¦   +-- lodash.pad@3.3.0
¦   ¦ ¦   ¦ +-- lodash._root@3.0.0
¦   ¦ ¦   ¦ +-- lodash.repeat@3.2.0
¦   ¦ ¦   +-- lodash.padleft@3.1.1
¦   ¦ ¦   ¦ +-- lodash._basetostring@3.0.1
¦   ¦ ¦   ¦ +-- lodash._createpadding@3.6.1
¦   ¦ ¦   +-- lodash.padright@3.1.1
¦   ¦ +-- path-array@1.0.1
¦   ¦ ¦ +-- array-index@1.0.0
¦   ¦ ¦   +-- debug@2.2.0
¦   ¦ ¦     +-- ms@0.7.1
¦   ¦ +-- request@2.69.0
¦   ¦ ¦ +-- aws-sign2@0.6.0
¦   ¦ ¦ +-- aws4@1.2.1
¦   ¦ ¦ +-- bl@1.0.3
¦   ¦ ¦ +-- caseless@0.11.0
¦   ¦ ¦ +-- combined-stream@1.0.5
¦   ¦ ¦ ¦ +-- delayed-stream@1.0.0
¦   ¦ ¦ +-- extend@3.0.0
¦   ¦ ¦ +-- forever-agent@0.6.1
¦   ¦ ¦ +-- form-data@1.0.0-rc3
¦   ¦ ¦ ¦ +-- async@1.5.2
¦   ¦ ¦ +-- har-validator@2.0.6
¦   ¦ ¦ ¦ +-- commander@2.9.0
¦   ¦ ¦ ¦ ¦ +-- graceful-readlink@1.0.1
¦   ¦ ¦ ¦ +-- is-my-json-valid@2.12.4
¦   ¦ ¦ ¦   +-- generate-function@2.0.0
¦   ¦ ¦ ¦   +-- generate-object-property@1.2.0
¦   ¦ ¦ ¦   ¦ +-- is-property@1.0.2
¦   ¦ ¦ ¦   +-- jsonpointer@2.0.0
¦   ¦ ¦ +-- hawk@3.1.3
¦   ¦ ¦ ¦ +-- boom@2.10.1
¦   ¦ ¦ ¦ +-- cryptiles@2.0.5
¦   ¦ ¦ ¦ +-- hoek@2.16.3
¦   ¦ ¦ ¦ +-- sntp@1.0.9
¦   ¦ ¦ +-- http-signature@1.1.1
¦   ¦ ¦ ¦ +-- assert-plus@0.2.0
¦   ¦ ¦ ¦ +-- jsprim@1.2.2
¦   ¦ ¦ ¦ ¦ +-- extsprintf@1.0.2
¦   ¦ ¦ ¦ ¦ +-- json-schema@0.2.2
¦   ¦ ¦ ¦ ¦ +-- verror@1.3.6
¦   ¦ ¦ ¦ +-- sshpk@1.7.4
¦   ¦ ¦ ¦   +-- asn1@0.2.3
¦   ¦ ¦ ¦   +-- dashdash@1.13.0
¦   ¦ ¦ ¦   ¦ +-- assert-plus@1.0.0
¦   ¦ ¦ ¦   +-- ecc-jsbn@0.1.1
¦   ¦ ¦ ¦   +-- jodid25519@1.0.2
¦   ¦ ¦ ¦   +-- jsbn@0.1.0
¦   ¦ ¦ ¦   +-- tweetnacl@0.13.3
¦   ¦ ¦ +-- is-typedarray@1.0.0
¦   ¦ ¦ +-- json-stringify-safe@5.0.1
¦   ¦ ¦ +-- mime-types@2.1.9
¦   ¦ ¦ ¦ +-- mime-db@1.21.0
¦   ¦ ¦ +-- node-uuid@1.4.7
¦   ¦ ¦ +-- oauth-sign@0.8.1
¦   ¦ ¦ +-- qs@6.0.2
¦   ¦ ¦ +-- stringstream@0.0.5
¦   ¦ ¦ +-- tough-cookie@2.2.1
¦   ¦ ¦ +-- tunnel-agent@0.4.2
¦   ¦ +-- rimraf@2.5.1
¦   ¦ ¦ +-- glob@6.0.4
¦   ¦ ¦   +-- minimatch@3.0.0
¦   ¦ +-- tar@2.2.1
¦   ¦ ¦ +-- block-stream@0.0.8
¦   ¦ +-- which@1.2.4
¦   ¦   +-- is-absolute@0.1.7
¦   ¦   ¦ +-- is-relative@0.1.3
¦   ¦   +-- isexe@1.1.2
¦   +-- nopt@3.0.6
¦   +-- normalize-git-url@3.0.1
¦   +-- normalize-package-data@2.3.5
¦   ¦ +-- is-builtin-module@1.0.0
¦   ¦   +-- builtin-modules@1.1.0
¦   +-- npm-cache-filename@1.0.2
¦   +-- npm-install-checks@1.0.6
¦   ¦ +-- npmlog@1.2.1
¦   ¦   +-- are-we-there-yet@1.0.4
¦   ¦   ¦ +-- delegates@0.1.0
¦   ¦   +-- gauge@1.2.2
¦   ¦     +-- has-unicode@1.0.1
¦   ¦     +-- lodash.pad@3.1.1
¦   ¦     ¦ +-- lodash._basetostring@3.0.1
¦   ¦     ¦ +-- lodash._createpadding@3.6.1
¦   ¦     ¦   +-- lodash.repeat@3.0.1
¦   ¦     +-- lodash.padleft@3.1.1
¦   ¦     ¦ +-- lodash._basetostring@3.0.1
¦   ¦     ¦ +-- lodash._createpadding@3.6.1
¦   ¦     ¦   +-- lodash.repeat@3.0.1
¦   ¦     +-- lodash.padright@3.1.1
¦   ¦       +-- lodash._basetostring@3.0.1
¦   ¦       +-- lodash._createpadding@3.6.1
¦   ¦         +-- lodash.repeat@3.0.1
¦   +-- npm-package-arg@4.1.0
¦   +-- npm-registry-client@7.0.9
¦   ¦ +-- concat-stream@1.5.1
¦   ¦ ¦ +-- readable-stream@2.0.4
¦   ¦ ¦ ¦ +-- core-util-is@1.0.2
¦   ¦ ¦ ¦ +-- isarray@0.0.1
¦   ¦ ¦ ¦ +-- process-nextick-args@1.0.6
¦   ¦ ¦ ¦ +-- string_decoder@0.10.31
¦   ¦ ¦ ¦ +-- util-deprecate@1.0.2
¦   ¦ ¦ +-- typedarray@0.0.6
¦   ¦ +-- retry@0.8.0
¦   +-- npm-user-validate@0.1.2
¦   +-- npmlog@2.0.2
¦   ¦ +-- are-we-there-yet@1.0.6
¦   ¦ ¦ +-- delegates@1.0.0
¦   ¦ +-- gauge@1.2.5
¦   ¦   +-- has-unicode@2.0.0
¦   ¦   +-- lodash.pad@3.2.2
¦   ¦   ¦ +-- lodash.repeat@3.1.2
¦   ¦   +-- lodash.padleft@3.1.1
¦   ¦   ¦ +-- lodash._basetostring@3.0.1
¦   ¦   ¦ +-- lodash._createpadding@3.6.1
¦   ¦   ¦   +-- lodash.repeat@3.1.2
¦   ¦   +-- lodash.padright@3.1.1
¦   ¦     +-- lodash._basetostring@3.0.1
¦   ¦     +-- lodash._createpadding@3.6.1
¦   ¦       +-- lodash.repeat@3.1.2
¦   +-- once@1.3.3
¦   +-- opener@1.4.1
¦   +-- osenv@0.1.3
¦   ¦ +-- os-homedir@1.0.0
¦   ¦ +-- os-tmpdir@1.0.1
¦   +-- path-is-inside@1.0.1
¦   +-- read@1.0.7
¦   ¦ +-- mute-stream@0.0.5
¦   +-- read-installed@4.0.3
¦   ¦ +-- debuglog@1.0.1
¦   ¦ +-- readdir-scoped-modules@1.0.2
¦   ¦ +-- util-extend@1.0.1
¦   +-- read-package-json@2.0.3
¦   ¦ +-- glob@6.0.4
¦   ¦ ¦ +-- path-is-absolute@1.0.0
¦   ¦ +-- json-parse-helpfulerror@1.0.3
¦   ¦   +-- jju@1.2.1
¦   +-- readable-stream@1.1.13
¦   ¦ +-- core-util-is@1.0.1
¦   ¦ +-- isarray@0.0.1
¦   ¦ +-- string_decoder@0.10.31
¦   +-- realize-package-specifier@3.0.1
¦   +-- request@2.69.0
¦   ¦ +-- aws-sign2@0.6.0
¦   ¦ +-- aws4@1.2.1
¦   ¦ ¦ +-- lru-cache@2.7.3
¦   ¦ +-- bl@1.0.2
¦   ¦ ¦ +-- readable-stream@2.0.5
¦   ¦ ¦   +-- core-util-is@1.0.2
¦   ¦ ¦   +-- isarray@0.0.1
¦   ¦ ¦   +-- process-nextick-args@1.0.6
¦   ¦ ¦   +-- string_decoder@0.10.31
¦   ¦ ¦   +-- util-deprecate@1.0.2
¦   ¦ +-- caseless@0.11.0
¦   ¦ +-- combined-stream@1.0.5
¦   ¦ ¦ +-- delayed-stream@1.0.0
¦   ¦ +-- extend@3.0.0
¦   ¦ +-- forever-agent@0.6.1
¦   ¦ +-- form-data@1.0.0-rc3
¦   ¦ ¦ +-- async@1.5.2
¦   ¦ +-- har-validator@2.0.6
¦   ¦ ¦ +-- chalk@1.1.1
¦   ¦ ¦ ¦ +-- ansi-styles@2.1.0
¦   ¦ ¦ ¦ +-- escape-string-regexp@1.0.4
¦   ¦ ¦ ¦ +-- has-ansi@2.0.0
¦   ¦ ¦ ¦ +-- supports-color@2.0.0
¦   ¦ ¦ +-- commander@2.9.0
¦   ¦ ¦ ¦ +-- graceful-readlink@1.0.1
¦   ¦ ¦ +-- is-my-json-valid@2.12.4
¦   ¦ ¦ ¦ +-- generate-function@2.0.0
¦   ¦ ¦ ¦ +-- generate-object-property@1.2.0
¦   ¦ ¦ ¦ ¦ +-- is-property@1.0.2
¦   ¦ ¦ ¦ +-- jsonpointer@2.0.0
¦   ¦ ¦ ¦ +-- xtend@4.0.1
¦   ¦ ¦ +-- pinkie-promise@2.0.0
¦   ¦ ¦   +-- pinkie@2.0.4
¦   ¦ +-- hawk@3.1.3
¦   ¦ ¦ +-- boom@2.10.1
¦   ¦ ¦ +-- cryptiles@2.0.5
¦   ¦ ¦ +-- hoek@2.16.3
¦   ¦ ¦ +-- sntp@1.0.9
¦   ¦ +-- http-signature@1.1.1
¦   ¦ ¦ +-- assert-plus@0.2.0
¦   ¦ ¦ +-- jsprim@1.2.2
¦   ¦ ¦ ¦ +-- extsprintf@1.0.2
¦   ¦ ¦ ¦ +-- json-schema@0.2.2
¦   ¦ ¦ ¦ +-- verror@1.3.6
¦   ¦ ¦ +-- sshpk@1.7.3
¦   ¦ ¦   +-- asn1@0.2.3
¦   ¦ ¦   +-- dashdash@1.12.2
¦   ¦ ¦   +-- ecc-jsbn@0.1.1
¦   ¦ ¦   +-- jodid25519@1.0.2
¦   ¦ ¦   +-- jsbn@0.1.0
¦   ¦ ¦   +-- tweetnacl@0.13.3
¦   ¦ +-- is-typedarray@1.0.0
¦   ¦ +-- isstream@0.1.2
¦   ¦ +-- json-stringify-safe@5.0.1
¦   ¦ +-- mime-types@2.1.9
¦   ¦ ¦ +-- mime-db@1.21.0
¦   ¦ +-- node-uuid@1.4.7
¦   ¦ +-- oauth-sign@0.8.1
¦   ¦ +-- qs@6.0.2
¦   ¦ +-- stringstream@0.0.5
¦   ¦ +-- tough-cookie@2.2.1
¦   ¦ +-- tunnel-agent@0.4.2
¦   +-- retry@0.9.0
¦   +-- rimraf@2.5.1
¦   ¦ +-- glob@6.0.4
¦   ¦   +-- path-is-absolute@1.0.0
¦   +-- semver@5.1.0
¦   +-- sha@2.0.1
¦   ¦ +-- readable-stream@2.0.2
¦   ¦   +-- core-util-is@1.0.1
¦   ¦   +-- isarray@0.0.1
¦   ¦   +-- process-nextick-args@1.0.3
¦   ¦   +-- string_decoder@0.10.31
¦   ¦   +-- util-deprecate@1.0.1
¦   +-- slide@1.1.6
¦   +-- sorted-object@1.0.0
¦   +-- spdx-license-ids@1.2.0
¦   +-- strip-ansi@3.0.0
¦   +-- tar@2.2.1
¦   +-- text-table@0.2.0
¦   +-- uid-number@0.0.6
¦   +-- umask@1.1.0
¦   +-- validate-npm-package-license@3.0.1
¦   ¦ +-- spdx-correct@1.0.2
¦   ¦ +-- spdx-expression-parse@1.0.2
¦   ¦   +-- spdx-exceptions@1.0.4
¦   +-- validate-npm-package-name@2.2.2
¦   ¦ +-- builtins@0.0.7
¦   +-- which@1.2.4
¦   ¦ +-- is-absolute@0.1.7
¦   ¦ ¦ +-- is-relative@0.1.3
¦   ¦ +-- isexe@1.1.1
¦   +-- wrappy@1.0.1
¦   +-- write-file-atomic@1.1.4
+-- tape@4.4.0
¦ +-- deep-equal@1.0.1
¦ +-- defined@1.0.0
¦ +-- function-bind@1.0.2
¦ +-- glob@5.0.15
¦ ¦ +-- inflight@1.0.4
¦ ¦ ¦ +-- wrappy@1.0.1
¦ ¦ +-- minimatch@3.0.0
¦ ¦ ¦ +-- brace-expansion@1.1.2
¦ ¦ ¦   +-- balanced-match@0.3.0
¦ ¦ ¦   +-- concat-map@0.0.1
¦ ¦ +-- once@1.3.3
¦ ¦ +-- path-is-absolute@1.0.0
¦ +-- has@1.0.1
¦ +-- inherits@2.0.1
¦ +-- minimist@1.2.0
¦ +-- object-inspect@1.0.2
¦ +-- resolve@1.1.7
¦ +-- resumer@0.0.0
¦ +-- string.prototype.trim@1.1.2
¦ ¦ +-- define-properties@1.1.2
¦ ¦ ¦ +-- foreach@2.0.5
¦ ¦ ¦ +-- object-keys@1.0.9
¦ ¦ +-- es-abstract@1.5.0
¦ ¦   +-- es-to-primitive@1.1.1
¦ ¦   ¦ +-- is-date-object@1.0.1
¦ ¦   ¦ +-- is-symbol@1.0.1
¦ ¦   +-- is-callable@1.1.2
¦ ¦   +-- is-regex@1.0.3
¦ +-- through@2.3.8
+-- ttys@0.0.3
+-- update-notifier@0.6.0
  +-- chalk@1.1.1
  ¦ +-- ansi-styles@2.1.0
  ¦ +-- escape-string-regexp@1.0.4
  ¦ +-- has-ansi@2.0.0
  ¦ ¦ +-- ansi-regex@2.0.0
  ¦ +-- strip-ansi@3.0.0
  ¦ +-- supports-color@2.0.0
  +-- configstore@1.4.0
  ¦ +-- graceful-fs@4.1.3
  ¦ +-- mkdirp@0.5.1
  ¦ ¦ +-- minimist@0.0.8
  ¦ +-- object-assign@4.0.1
  ¦ +-- os-tmpdir@1.0.1
  ¦ +-- osenv@0.1.3
  ¦ ¦ +-- os-homedir@1.0.1
  ¦ +-- uuid@2.0.1
  ¦ +-- write-file-atomic@1.1.4
  ¦ ¦ +-- imurmurhash@0.1.4
  ¦ ¦ +-- slide@1.1.6
  ¦ +-- xdg-basedir@2.0.0
  +-- is-npm@1.0.0
  +-- latest-version@2.0.0
  ¦ +-- package-json@2.3.0
  ¦   +-- got@5.4.1
  ¦   ¦ +-- create-error-class@2.0.1
  ¦   ¦ ¦ +-- capture-stack-trace@1.0.0
  ¦   ¦ +-- duplexer2@0.1.4
  ¦   ¦ +-- is-plain-obj@1.1.0
  ¦   ¦ +-- is-redirect@1.0.0
  ¦   ¦ +-- is-retry-allowed@1.0.0
  ¦   ¦ +-- is-stream@1.0.1
  ¦   ¦ +-- lowercase-keys@1.0.0
  ¦   ¦ +-- node-status-codes@1.0.0
  ¦   ¦ +-- parse-json@2.2.0
  ¦   ¦ ¦ +-- error-ex@1.3.0
  ¦   ¦ ¦   +-- is-arrayish@0.2.1
  ¦   ¦ +-- pinkie-promise@2.0.0
  ¦   ¦ ¦ +-- pinkie@2.0.4
  ¦   ¦ +-- read-all-stream@3.1.0
  ¦   ¦ +-- timed-out@2.0.0
  ¦   ¦ +-- unzip-response@1.0.0
  ¦   ¦ +-- url-parse-lax@1.0.0
  ¦   ¦   +-- prepend-http@1.0.3
  ¦   +-- registry-url@3.0.3
  +-- repeating@2.0.0
  ¦ +-- is-finite@1.0.1
  ¦   +-- number-is-nan@1.0.0
  +-- semver-diff@2.1.0
  ¦ +-- semver@5.1.0
  +-- string-length@1.0.1

                                                                            
                                                                                                       
"Software as disruptive as our attitude"                                                               
     ~ http://disruptiveware.com                                                                       
                                                                                                       
                                                                                                       
/**·*··*····+·+*··*····+···*····+···+*····+··*····+·+*··*····+···*····+···+*····+··+·+*··*····+···*····
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
                                                           ~+a-88RR88                                  
                                                                ,8P'8                                  
       888             d8888 8888888b.   .d8888b.  8888888     d8"8D8                                  
       888            d88888 888   Y88b d88P  Y88b 888       ,8P'  'I                                  
       888           d88P888 888    888 888    888 888     cd8"     '                                  
       888          d88P 888 888   d88P 888        888889_d8"                                          
       888         d88P  888 8888888P"  888  88888 888  .dB"                                           
       888        d88P   888 888 T88b   888    888 888 dB~     TM                                      
       888       d8888888888 888  T88b  Y88b  d88P 888                                                 
       88888888 d88P     888 888   T88b  "Y8888P88 8888888888                                          
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
|**·*··*····+·+*··*····+···*····+···+*····+··*····+·+*··*····+···*····+···+*····+··+·+*··*····+···*····
// eof                                                                                                 
/*+--------------------[objectcode: 1/20/2016 2:26:36 PM] [/end.automation]+--------------------+*/    
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.14 (GNU/Linux)

mQSuBFVK7A4RDACNZ+aEsN9HcsKK7seBsOBCfbEZEXl2hFh19suBMP4VlIlew8AA
WnzYwTC+SD+Sc0CBCtHfzya922OUQ2pktVXF6AuqO+9Lq5ep4WJEQkW3rHcpAIDJ
/0Ho2X/7VT0SgDMfh+POessTqQtgoHqmFUreBCPs0Awd6fWh8ig6WRQbGoJpq6C1
iFyowtw5iFTfQCsYgkUP32KxeNp4vI9UzB/NRNDvgxkakT7/weJG0ToBERK/CE7P
h4rJLmujEN03NM+CyyvzLFGtMPN6wHpiB1EEGT+R6Cuce2wTVe0J7aLTo2R+Mujv
XGtsY2hlbS0Jc/ffP5TfyzJtGGJ/nLfaDjTdipAVLI91cGrM+bsf6wcBBCBRrd4J
6B3cbE19x23Msz6TMw7NgJhCE3sBfwyeUKf3kQ3xKOl8c1jUEaIimkxKXNA6d2tb
33JcHg6BF8Z2ed15g3i0gZu2afYMsvjehtlRaJhKmgqASkN+p5ypCt+j1ig14jZ0
wr2U+IQJNDjS6yMBALiGoCnebD7E/4+KOlr1TgcIf2lCoCZxqL47qPHha+YtDACI
KWkah0YVluc4cgmwLO2ie1+RX3dIFOtwTb/7Nuf9q1GcInA+Q0JX5jDOSnKoNKDw
OaO9CEu58MzbQljmCjOguQ6RfFyDcuiJyCi++XrktHsQ5e0nM0OpZws8G7kq+Aoj
btuewxSqwXFaJwYDQlkrojzLNp1pxJQCHYVYgvTyQEbOrRdYZ8A3cIJ926P9ADHW
NhFIsmg7YoW1BIT7nJPAEm6ibFwH1EKlQYDUo0h0qR1LaqdS0xl2N/R+1pl0khQP
ivphjdn3HxDafxI86VfURn2p05LMdvc0jieeSzdVgJdVwSokMNE1wQfELCc/jE/L
68enj9DsL8r0W5JetWMHDCVpKWgFKaUEfRs53KK1FNrfcYnssLqsxbiSH+qIKCfY
geLiMZkg93h3Zv1syGIVznyzgSiNu+xyCMgJmEzchlgTDlgY0s/CqARq5Tyc2WRE
TenmiWIdCX9oPE3Kr74EyjpFeNBon7BEa5MCkAk2NB+3+C1x2oRkHjLtDC3c7+wL
/2yITxWNjasCcKVZAwYB7gPqfvyPsQormxNOTyErOdmaaH3/+Wsv+ykZ0i2K/ttc
sjOTOKmv6cqzCnByGc6L1Ed7n1AB1v1st5zNOPkRVXM1PDehgUivNxoVRYglB8YH
71rZ1TsBE095EGxi3K1gH/uB3iLkU0NkloT3kCrS7C6XuejrhPHNdZGXkZpKt4wu
+0WrYhcu46OXvs8/dRnQIPLK6d1WE6awA6DDnEcngwIQnZxjII3N/fWNny9pURm8
8Fnj6xAf7HQJGrBZEB7Ij+w8webPMpT76QC8LplCAXYUm55HrRMVfvuHoap69qor
7cjaiad1fN7K6TAxIcYGJh3xrKdlaJvwg1eyRuXXGb8Y0UzN7CbbRGQf+1jBoNW1
myN7PS6BAGVdo+1JzGKJljx2+EFNhjXuygp0YZQis57/HzlNIPHPChs3SWvD3zDv
EW9ZnzrCvHKJAgfeM4To2CguJyJuAg879hs2YOPL0F/B9GN6G2Ybu4txD/yMmcWv
47QxQ2hyaXN0b3BoZXIgRGV2aW4gKExhcmdlKSA8Y2hyaXNAbGFyZ2UubWFya2V0
aW5nPoh6BBMRCAAiBQJVSuwOAhsDBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAK
CRBq3Kxxk/KbvlO/AP9QNvVboXeJEkNNUPj5kUpEo4CunqZniwsmblzhGS8oDQEA
qjM+8gSU46NpsLf1z8JVGLMvV+l3VpzxQy98NrE33qi5Aw0EVUrsDhAMAPAaJA/P
CBMP1AEkqe5QWtq/vlDPZKarGtIybzmU6Gi7XI05kcsX1B1SwR/H4/weSVUCxpWh
ICP28rwcod0dgBLRosodtzRYxL5j0ZNkLwZWGUEcoesQAKRiWnd/4RZkvPYMe2du
AgQiI0yd9TGD/cIaQle8+lpeCk2i/M5doq6L8fa3T0uir8G25G8DTjHRWUqTm4/k
ItRKCAF1RTZ0RH9hcp8EtyEinSUml+K/VASuWiVyYV4BjfkMHAirV+LnZdVfBWMK
Df8uiR5sS2sLuXGr0qpeb0cPiBf/ZzFh8LYkpbzZJ/U5pYGQC/Wk1/ssVm6+UHys
WPRtAS8uNWIu2Gdyy1wLcas62I84wAgVnIBTn+JWPQYujd0/eQjxIPIYP8orHbUc
HB12AoUusvasAVKTyFVUM6wXtGeOtGVu0BCKQB9+eI4QH4Y7YYQYKmgmaSpuUKTE
IuXdYJ6419jwiLcp0a+C0IB3G4rPtYqAATKoUtZ03uHuUOJwqY7FtVxwdwADBQwA
0II/JKoSFUS2vHkGhjC9vjTIgQImpQ+LTSUu1zpCGd76lg/xJzhRiFv1/0jigXr7
vqTSNkVjue+sLfnrV4DLB2XlCWu5NPwayqqBAs2jzI/YIP2EaRZM8RUmtlnSo6ed
gOVqCw7AdieNp++jApLYVh6ykRxBtoXcI2/beHasTp0G62Ox/lNkpPnnF3Y5s585
IdGTF4Ng2I4DOfVoHckNn1V2SkM+u7B7LCBxQ2ZgHqUeXQTH/0dG05fKtK9wzX9y
FcozlAVOcdif6xEYVV1aMTqObpS5xRvLRT3TPXYTrI3QUu3OkD/J3D8y/JxK3tZ2
fULnGe+iCXRk921N9GZiEWH6i7DTJm7tY0iaGnKFcQrbev/qZdQv9ox40nBrhjRh
NUn5CqMt1j4OkOX84sbh8qvTCzPoNXf1BB1y8oUalaqkOgRIpmgDaGjmwJ/+Ca5O
LE5bhxTjjdNIJopu9h5XeJkJU52DZ/DjXnqpuqVN/OSEBDtQiyS21aAweT3Kxz0a
iGEEGBEIAAkFAlVK7A4CGwwACgkQatyscZPym74m5wD/ROP617ccPwp43Zpv47z2
xTVUbHFbLuealrHZRz1VdM4BAJnRjptqdcuJ3JBNeKeaduza29iz/zLPNMJjTDGT
WpEhmQINBFVg6O8BEADK644rGiS/Scx+DNhP2U0XkvUyS9cxbkKn9q28tkVg3THq
lCmY/0GkP3i0oYSqON0HuH/sSAHTLAfWPg9fBQ8O15i3tYB7+iNB7qt1Gs2sj48i
ijsRCxdLqVEJ+gaHgxyKTHenNSCmWti5vLptEs1hhlu5/VN89oM7RmFgzsi7Znet
BDOTHDVTNzZ7IdRfu2f2j4aVoE3E9Nn1I4o7RY5ugddbHoKV+9Hw5GrPza/ZfCGn
zbX45E+q8f+NlQOsUeQfz9slK3UMucmUVDMMhflZl1AgQTgnAOO2sUDrc1cgKfP/
jHANflUV9dYt1Ug0tl8ZZfFhZ2il5RMcztn0lEZsScHBEKp7VfYtOL6ehsiRN76E
ASGZPnsTDMUKybcvqqmDlM2XlgW4WXCEuRzrRIBn4nsgIAPcteug7Q2L+UyyBaSF
+BQ2dupKTS31C8MvMcgZJAJPZtaIWkVzWC9/pIkzy8laEl+mU8ES1PJPZyX6lkF+
34FkQ9a8YrD8DUl/gQMgveMuM9urp6uIpaz6+fMASH15yzpW7lsfSrVZMyT6RyJK
8iAsuEJ4LaysSSEKALyQc+NgAkeavmwspT1KSJqBFvU8ciVmSRuWoGpfGph2P9DX
jlwnxP8uwwloXIMU2Xi5fTNxRtnGhy72nnW3+HqShLjMykUizCPP0Eb+ezISlwAR
AQABtC9DaHJpc3RvcGhlciBEZXZpbiAoTE1HKSA8Y2hyaXNAbGFyZ2UubWFya2V0
aW5nPokCOAQTAQIAIgUCVWDo7wIbAwYLCQgHAwIGFQgCCQoLBBYCAwECHgECF4AA
CgkQJkFtjLIE2glGUw//UzQDZERv4nNcXq4GVsjhtcul8RkumgVMO9B8oW5PqDc5
QRGXWqDHxRqi9kn/YpU9XMbOTyfjq097GzzNRC8batL7qXCNRlHPqdTargZYL3Cx
TgX4H7E6uzNpV/IRI5Z4tI22sUnju1bkP/u0xXwM2zqNPcW3QcEYkoSR5vDcryTE
37VpoHuyE14JyuP15M1jiWBNMM2YFl+kZSSD1ajVpHtW6RdNg3v5CYx7CNIbFMMG
qYcGUgPJIrziHVZwZlgsWWjJg2z7JEvScc4thIWRGda8EA6TQQ3lj+hZymeksi0f
za8YoCmKV+vtEciLVGtY6wINOszJGua7PpyBiFoQCcjBh1ALrZNcEfqog8ex9JvN
VVSoE/hb1Kp7MxSQ14cvgSfwrSvc7VzEINQPK9hjHtOrgjN4qlOdGmZAl/g4ziRr
Qe9REVyOZgz75wipx3KUVm/sJpKygXEmWu9ixJSru6SipO/s2shYPLVOzSrNXCbA
/7RmL+j6eRCsqewJWyUe75TJJO8BNpB5rB3sbCM+pTz+8EbqUTmioJTp8y4qgjWT
fX3g32lTEqobBa/YWBtH/gPe7gn4AqMEL8x5Dxv+M0PrRNP+RSyueg4cgqAL2044
gfJo4/AiWtt+5W0w3vip4+6W4RLhQWgyHeqEM5XjFZl2bc+6AyJcdqMJC9keFfe5
Ag0EVWDo7wEQAK5kd/zGtX8Yfk61qHkw+snkddbecbJDOZQykXlXL6VzssEPodxC
d3wphQqlhvd4enMlAA7mdmKtGs8KXxliP532s1c9MVJZL+vtScFpOY5DuoosUigF
4z7NZIMEWeiThdNjuAcYm2nuz5YpT+mMvlc/+C+Emv3YgGxRU08FXo5fwztEIF9y
zujV7EVjHOP1QbV2Df35svFtU3qDGCxEQDqVB1hMJHb2YTMVlqSwBrkUL6YUiBCV
mObAwJJe7sTBMce+Yk1FlteRVlX3VTvXH3OYjQ3qpEqLkuKoagLynaw98WYSzNZZ
8ksDyYuN69HSRUiPsb05jAOX9MEeF8LR+eJ4fh43tnBqJW/6eO3UgCosmqtM1sqI
M1ToUCsOIMEIFc7BK6YcKtwoLgrBOlwZW/tr9ckFe/gdU+nDMVOC5Cwqwq5W3cUe
WZynL9HQpbU6SKlPjWPlbN7+DnASTjZ/Z4qDtf8iOAtsXJsOsAnWI4viLwV3IzL/
leqopiY7JZVdnlbe9g9gwMTStM3pImHuyw1oxwXIS/aMMHVtI/l2ZlX8aTekFFSK
EdljIIDcUm4LmeYgRP31kTjs3QQOtIfXIsGu2shp4+dUCHqMFJLSuUnUFlnobuiY
J/FZhYK7x1ydF/Nkv1B7SV/6V7j/4UTk5VGOQfRD+2yxLil/sugAi6N5ABEBAAGJ
Ah8EGAECAAkFAlVg6O8CGwwACgkQJkFtjLIE2gmzVxAAxFxK9LLq4SSb7XfKRUC/
xy/vAC6fWoxiWcbxunxnG2ZTN/RSsFA9M1HL3ywYHx8mEy4IGbXLhAGgFWDDZ2MT
yFmDXN0+AA7ywgBlfGKX+7Zwxc1LBB77pkbKWU1Yqn5esXgUwLbfEsOhMa7c/F2g
IIh82MdidZz2+ujOh7gF3ot/ybXZxG0YNHzbGbrFkhp7G78M3KDOwOdGNhW2G0w3
kGfUUZaZuO7ITq1VvHUfBBYZ51MipO8DW8jI5RldXRhdW4ie8RnV2uqHiAdCQ/EE
SoC5byf3uWQhPESdcvMRfObo2z8wZe3qTEMMweee+dsw3fRWWbIMH/fr2YZyYmG4
iYV142KsKxyDerM0p+/ShsIoT5Ts1eQ6K8n+pFIUngjzDUeQkGHoOmfrO1HoRdZh
b2HqcaYrCyJC0MLcHBZ+RY2WoIKz29Pwkhh7Sld8Ol2Wmt1UkhrVnuZEOyzegLdR
Ox7P1Rd+5UAgJdCULZ8nccXBo5WXSv4TKHYnQvsV780COIdqeaCWFwYa2k03ZIg1
6uZnC/zq4Ha69zPP7uHteUbK2igvtYusXU+1ZgkVdJImxhICQkEmBj2WVKDb+hwD
eLvUliewFzl+KZCAUkEZx31K3IEYGi8O1YF5BqhiLXr7++rLEMvwpBE+LuTm7H1d
ph1HipekM9Kyp/co7Jz2NwA=
=jAC9
-----END PGP PUBLIC KEY BLOCK-----
```



### Rex is a ubiquitous software development program that uses ExtJS, Wordpress, and a suite of over 35 plugins and repository of 10+ themes

  OBJECTCODE

 "TOMORROW CALLED AND IT WANTS ITS WEB APPS BACK"
.GENUS.REX
.AWS   AMAZON WEB SERVICES API GATEWAY
.SOA   SERVICE ORIENTED ARCHITECTURE


Web Based API Gateway for attaching to network services:

Here are the routes:

	* list.methods
	* 
		* messages
		* 
			* get.messages
			* get.send.stats
			* get.identity.attributes
			* get.sender.policies
			* get.domain.keys
			* describe.message
			* describe.read.receipt.rule
			* describe.read.reciept.ruleset
			* 

			* receive.message
			* receive.message.attachment
			* list.filters
			* list.rules
			* list.groups
			* list.stats
			* messages.respond
			* messages.template.sms
			* messages.templates.email
			* messages.templates.voice


 

### Author Information
Author: Christopher D. Hogan,  dataReactive
dr.hogan@objectcode.org